import PropTypes from "prop-types";
import CustomSelect from "./CustomSelect";
import React, {useEffect} from "react";
import {APIFetchURI} from "../assets/useful-data/APICalls";

const CustomSelectContainer = (props) => {

    const optionsObject = props.optionsObject;
    const setOptionsObject = props.setOptionsObject;
    // const initOnMount = props.initOnMount;
    const fetchURI = props.fetchURI;
    const fetchURIArgs = props.fetchURIArgs;
    const disabled = props.disabled;
    const injectOnChange = props.onChange;

    // If fetchURI exists, is true, else false.
    const isOptionsOutsourced = !!fetchURI;

    const getOptionsFromAPI = async () => {
        // console.log(fetchURI);
        let requestOptions = {
            credentials: 'include',
        }
        if (fetchURIArgs) {
            requestOptions = {
                ...requestOptions,
                ...fetchURIArgs,
            }
        }

        const submitRequest = async (requestOptions) => {
            // console.log(requestOptions);
            return await fetch(fetchURI, requestOptions);
        };

        let response = await submitRequest(requestOptions);

        if (response.ok) {
            // console.log("HERE");
            // console.log(response);
            let result = await response.json();
            // console.log(result);
            let jsonObj = JSON.parse(JSON.stringify(result));
            // console.log(jsonObj);
            let optionsArray = jsonObj.key;
            // console.log(optionsArray);
            let optionsDict = [].concat(optionsArray.map((value) => ({value: value, label: value})));
            // console.log(optionsDict);
            setOptionsObject({
                ...optionsObject,
                isAvailable: true,
                options: optionsDict,
            });
        }
        // else if (response.key["message"]) {
        //     alert(response.key["message"]);
        // }

    };

    useEffect(() => {
        // console.log(fetchURI);
        if (!disabled && isOptionsOutsourced) {
            getOptionsFromAPI();
        }
    }, [fetchURIArgs,disabled]);

    const onChange = (changeValue) => {
        setOptionsObject({
            ...optionsObject,
            chosenId: changeValue,
        });
        if (injectOnChange && changeValue.length !== 0){
            injectOnChange(changeValue);
        }
    };

    return (
        <React.Fragment>
            <CustomSelect
                onChange={onChange}
                disabled={disabled}
                optionsDict={optionsObject.options}
            >
            </CustomSelect>
            {/*<br/>*/}
            {/*Comp:{disabled.toString()}*/}
            {/*<br/>*/}
        </React.Fragment>
    );

};

export default CustomSelectContainer;

CustomSelectContainer.propTypes = {
    optionsObject: PropTypes.object.isRequired,
    setOptionsObject: PropTypes.func.isRequired,
    fetchURI: PropTypes.string,
    fetchURIArgs: PropTypes.object,
    disabled: PropTypes.bool.isRequired,
    onChange: PropTypes.func,
}