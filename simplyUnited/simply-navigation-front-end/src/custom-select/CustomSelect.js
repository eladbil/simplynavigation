import PropTypes from "prop-types";
import Select from 'react-select';

const CustomSelect = (props) => {

    const optionsDict = props.optionsDict;
    const disabled = props.disabled;
    // console.log(optionsDict)

    const handleSelect = (target) => {
        // console.log(event)
        // console.log(target.value)
        props.onChange(target.value);
    };

    return (
        <>
            <Select
                onChange={handleSelect}
                isDisabled={disabled}
                closeMenuOnSelect={true}
                options={optionsDict}
            />
            {/*{optionsArray.map((x, y) => {*/}
            {/*    return <option key={y} value={x}>{x}</option>;*/}
            {/*})}*/}
        </>
    );
}

CustomSelect.propTypes = {
    optionsDict: PropTypes.array.isRequired,
    onChange: PropTypes.func.isRequired,
};

export default CustomSelect;