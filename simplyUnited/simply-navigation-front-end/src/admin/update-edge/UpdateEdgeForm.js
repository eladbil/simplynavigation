import React, {useState} from "react";
import CustomSelectContainer from "../../custom-select/CustomSelectContainer";
import TextInput from "../../text-input/TextInput";
import DirectionsAdder from "../add-edge-form/DirectionsAdder";
import {APIFetchURI} from "../../assets/useful-data/APICalls";


/**
 * This component is a form for *updating* a Path (aka. Edge in the DB).
 * The logic to this form is found in the user instructions PDF.
 * @param {*} props No props needed or used in this form.
 * @returns React.Fragment
 */
const UpdateEdgeForm = (props) => {

    const [newPathType, setNewPathType] = useState({
        /** The chosen ID of the item selected. */
        chosenId: "",
        /** Indicates wether the select component is available or not. */
        isAvailable: true,
        /** A list to keep all the options in the select. */
        options : [],
    });
    // const [isTypePathAvailable, setIsTypePathAvailable] = useState(false);

    const [optionalBuilding, setOptionalBuilding] = useState({
        /** The chosen ID of the item selected. */
        chosenId: "",
        /** Indicates wether the select component is available or not. */
        isAvailable: true,
        /** A list to keep all the options in the select. */
        options : [],
    });

    const [srcPoint, setSrcPoint] = useState({
        /** The chosen ID of the item selected. */
        chosenId: "",
        /** Indicates wether the select component is available or not. */
        isAvailable: true,
        /** A list to keep all the options in the select. */
        options : [],
    });
    const [isSrcReady, setIsSrcReady] = useState(false);

    const [destPoint, setDestPoint] = useState({
        /** The chosen ID of the item selected. */
        chosenId: "",
        /** Indicates wether the select component is available or not. */
        isAvailable: true,
        /** A list to keep all the options in the select. */
        options : [],
    });

    const [totalLength, setTotalLength] = useState("");
    const [directionsObject, setDirectionsObject] = useState({});

    /**
     * A function to handle the selection of a Building from the select.
     * This function sets up the server request configurations for the path points select components.
     * This function is passed to the CustomSelectContainer component in order to run once an option is selected.
     * @param {string} newBuilding The ID of the building selected.
     */
    const handleBuildingChange = (newBuilding) => {
        setIsSrcReady(true);
        const requestOptions = {
            method: 'POST',
            withCredentials: 'true',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                typePath : newPathType.chosenId,
                building : newBuilding,
            })
        };
        setSrcPoint({
            ...srcPoint,
            fetchURIArgs: requestOptions,
        });
        setDestPoint({
            ...destPoint,
            fetchURIArgs: requestOptions,
        });
    };

    const handleDestSelect = async (newDestID) => {
        const requestOptions = {
            method: 'POST',
            withCredentials: 'true',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                type: newPathType.chosenId,
                building : optionalBuilding.chosenId,
                src: srcPoint.chosenId,
                dest: newDestID,
            })
        };

        const submitRequest = async (requestOptions) => {
            const response  = await fetch(APIFetchURI.GetPathDetails, requestOptions);
            return await response.json();
        };

        const result = await submitRequest(requestOptions);
        
        if (result.key["message"]) {
            alert(result.key["message"]);
            return
        }

        const areaInfo = JSON.parse(JSON.stringify(result)).key;
        console.log(areaInfo);
        setTotalLength(areaInfo["weight"] + "");
        // setDirectionsObject();
        const zip = (rows) => rows[0].map((_,c) => rows.map(row => row[c]))
        const dirArray = zip([
            areaInfo["directions"],
            areaInfo["classifications"],
            areaInfo["distances"],
            areaInfo["freeText"]
        ]);
        setDirectionsObject({...dirArray});

    };

    const handleSubmit = async (event) => {
        event.preventDefault();

        const directions = Object.values(directionsObject);
        const directionArray = directions.map((value, index) =>  value[0]);
        const directionClassArray = directions.map((value, index) =>  value[1]);
        const directionLengthArray = directions.map((value, index) =>  value[2]);
        const directionCommentArray = directions.map((value, index) =>  value[3]);

        const requestBody = {
            type: newPathType.chosenId,
            building: optionalBuilding.chosenId,
            src: srcPoint.chosenId,
            dest: destPoint.chosenId,
            weight: totalLength,
            directions: directionArray,
            classification: directionClassArray,
            distances: directionLengthArray,
            freeText: directionCommentArray,
        };

        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(requestBody),
        };

        const submitRequest = async (requestOptions) => {
            const response  = await fetch(APIFetchURI.UpdatePathDetails, requestOptions);
            return await response.json();
        };

        const result = await submitRequest(requestOptions);
        if (result.key["message"]) {
            alert(result.key["message"]);
        }
    };

    return (
        <>
            <form onSubmit={handleSubmit}>
                <h3>טופס עדכון מסלול</h3>
                בחר את סוג המסלול אותו תרצה לעדכן
                <br/>
                <CustomSelectContainer
                    optionsObject={newPathType}
                    setOptionsObject={setNewPathType}
                    fetchURI={APIFetchURI.GetPathTypes}
                    disabled={!newPathType.isAvailable}
                    // onChange={handlePathSelect}
                />
                <br/>
                בחר את הבניין בו המסלול נמצא: (אופציונלי)
                <br/>
                <CustomSelectContainer
                    optionsObject={optionalBuilding}
                    setOptionsObject={setOptionalBuilding}
                    fetchURI={APIFetchURI.GetListOfBuilding}
                    disabled={false}
                    onChange={handleBuildingChange}
                />
                <br/>
                בחר את נקודת ההתחלה של המסלול
                <br/>
                <CustomSelectContainer
                    optionsObject={srcPoint}
                    setOptionsObject={setSrcPoint}
                    fetchURI={APIFetchURI.SetSrcPath}
                    fetchURIArgs={srcPoint.fetchURIArgs}
                    disabled={!isSrcReady}
                    // onChange={handleBuildingChange}
                />
                <br/>
                בחר את נקודת הסיום של המסלול
                <br/>
                <CustomSelectContainer
                    optionsObject={destPoint}
                    setOptionsObject={setDestPoint}
                    fetchURI={APIFetchURI.SetDestPath}
                    fetchURIArgs={destPoint.fetchURIArgs}
                    disabled={!isSrcReady}
                    onChange={handleDestSelect}
                />
                <br/>
                <h5>שנה את פרטי המסלול</h5>
                <TextInput
                    textInput={totalLength}
                    setTextInput={setTotalLength}
                    label={"Inset total length of route"}
                />
                <br/>
                מקצים
                <br/>
                <DirectionsAdder
                    directionsObject={directionsObject}
                    setDirectionsObject={setDirectionsObject}
                />
                <br/>
                <button type="submit">החל שינויים!</button>
            </form>
        </>
    );
};

export default UpdateEdgeForm;