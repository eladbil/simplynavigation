import React, {useState} from "react";
import CustomSelectContainer from "../../custom-select/CustomSelectContainer";
import {APIFetchURI} from "../../assets/useful-data/APICalls";



/**
 * This component is a form for *removing* a Checkpoint (aka. Outer Vertex in the DB).
 * The logic to this form is found in the user instructions PDF.
 * @param {*} props No props needed or used in this form.
 * @returns React.Fragment
 */
const DeleteOuterForm = (props) => {

    /**
     * Helper object of Checkpoint select component. 
     */
    const [checkpoint, setCheckpoint] = useState({
        /** The chosen ID of the item selected. */
        chosenId: "",
        /** Indicates wether the select component is available or not. */
        isAvailable: true,
        /** A list to keep all the options in the select. */
        options : [],
        /** The server request configuration. */
        fetchURIArgs: {
            method: 'POST',
            withCredentials: 'true',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json' },
        },
    });

    /**
     * Handle the submition of the DeleteOuterForm.
     * Send a POST request to the server including all the Checkpoint details. This request will tell the server 
     * *remove* the Checkpoint (specified in the form) from the DB.
     * The server may send a one time message to the user about the status of the request (success or failed).
     */
    const handleSubmit = async (event) => {

        // Prevent the reloading of the page.
        event.preventDefault();

        // The Checkpoint details from the form.
        const args = {
            outer: checkpoint.chosenId,
        };

        // The server request configuration.
        const requestOptions = {
            method: 'POST',
            withCredentials: 'true',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(args)
        };

        // Submitting the request in an async way so I will be able to wait for a response.
        const submitRequest = async (requestOptions) => {
            const response  = await fetch(APIFetchURI.DeleteCheckpoint, requestOptions);
            return await response.json();
        };

        // Waiting for a response.
        const result = await submitRequest(requestOptions);
        if (result.key["message"]) {
            alert(result.key["message"]);
        }
    }

    return (
        <React.Fragment>
            <form onSubmit={handleSubmit}>
                <h3>טופס מחיקת נקודת ציון</h3>
                בחר את נקודת הציון אותה תרצה למחוק
                <CustomSelectContainer
                    optionsObject={checkpoint}
                    setOptionsObject={setCheckpoint}
                    fetchURI={APIFetchURI.GetListOfCheckpoints}
                    fetchURIArgs={checkpoint.fetchURIArgs}
                    disabled={false}
                />
                <br/>
                <button type="submit">מחק את נקודת הציון!</button>
            </form>
        </React.Fragment>
    );
};

export default DeleteOuterForm;