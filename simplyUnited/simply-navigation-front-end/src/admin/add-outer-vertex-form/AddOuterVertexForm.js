import React, {useState} from "react";
import TextInput from "../../text-input/TextInput";
import {APIFetchURI} from "../../assets/useful-data/APICalls";


/**
 * This component is a form for *adding* a Checkpoint (aka. Outer Vertex in the DB).
 * The logic to this form is found in the user instructions PDF.
 * @param {*} props No props needed or used in this form.
 * @returns React.Fragment
 */
const AddOuterVertexForm = (props) => {

    const [newOuterVertexName, setNewOuterVertexForm] = useState("");
    const newOuterVertexNameLabel = "Name of New Markup";

    /**
     * Handle the submition of the AddOuterVertexForm.
     * Send a POST request to the server including all the Checkpoint details. This request will tell the server 
     * add the Checkpoint to the DB.
     * The server may send a one time message to the user about the status of the request (success or failed).
     */
    const handleSubmit = async (event) => {
        // Prevent the reloading of the page.
        event.preventDefault();

        // The Checkpoint details from the form.
        // NOTE: allowEntry should always be 1, as the path finding algorithm used in the server doesn't support
        // unreachable nodes.
        const args = {
          name: newOuterVertexName,
          allowEntry: 1,
        };

        // The server request configuration.
        const requestOptions = {
            method: 'POST',
            withCredentials: 'true',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(args)
        };

        // Submitting the request in an async way so I will be able to wait for a response.
        const submitRequest = async (requestOptions) => {
            const response  = await fetch(APIFetchURI.CreateNewCheckpoint, requestOptions);
            return await response.json();
        };

        // Waiting for a response.
        const result = await submitRequest(requestOptions);
        if (result.key["message"]) {
            alert(result.key["message"]);
        }
    };

    return (
        <React.Fragment>
            <h3>טופס הוספת נקודת ציון</h3>
            <form onSubmit={handleSubmit}>
                השם של נקודת הציון החדשה
                <br/>
                <TextInput
                    textInput={newOuterVertexName}
                    setTextInput={setNewOuterVertexForm}
                    label={newOuterVertexNameLabel}
                />
                {/*<BinaryInput {...binaryInputProps}/>*/}
                <br/>
                <br/>
                <button type="submit">הוסף נקודת ציון!</button>
            </form>
            </React.Fragment>
    );
};

export default AddOuterVertexForm;