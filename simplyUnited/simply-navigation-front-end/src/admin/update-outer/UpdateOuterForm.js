import React, {useState} from "react";
import CustomSelectContainer from "../../custom-select/CustomSelectContainer";
import TextInput from "../../text-input/TextInput";
import {APIFetchURI} from "../../assets/useful-data/APICalls";


/**
 * This component is a form for *updating* a Checkpoint (aka. Outer Vertex in the DB).
 * The logic to this form is found in the user instructions PDF.
 * @param {*} props No props needed nor used in this form.
 * @returns React.Fragment
 */
const UpdateOuterForm = (props) => {

    const [existingOuters, setExistingOuters] = useState({
        /** The chosen ID of the item selected. */
        chosenId: "",
        /** Indicates wether the select component is available or not. */
        isAvailable: true,
        /** A list to keep all the options in the select. */
        areaFetchURIArgs: {
            method: 'POST',
            withCredentials: 'true',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json' },
        },
    });

    const [newOuterVertexName, setNewOuterVertexName] = useState("");
    const newOuterVertexNameLabel = "Name of New Markup";

    const handleOuterSelect = (outerID) => {
        setNewOuterVertexName(outerID);
    };

    /**
     * Handle the submition of the UpdateOuterForm.
     * Send a POST request to the server including all the Checkpoint details. This request will tell the server 
     * to *update* this Checkpoint in the DB with the new details entered from the user.
     * The server may send a one time message to the user about the status of the request (success or failed).
     */
    const handleSubmit = async (event) => {
        event.preventDefault();
        const args = {
            ogName: existingOuters.chosenId,
            newName: newOuterVertexName,
            allowEntry: 1,
        };

        const requestOptions = {
            method: 'POST',
            withCredentials: 'true',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(args)
        };

        const submitRequest = async (requestOptions) => {
            const response  = await fetch(APIFetchURI.UpdateCheckpointDetails, requestOptions);
            return await response.json();
        };

        const result = await submitRequest(requestOptions);
        if (result.key["message"]) {
            alert(result.key["message"]);
        }

    };

    return (
        <>
            <form onSubmit={handleSubmit}>
                <h2>עדכן נקודת ציון:</h2>
                בחר את נקודת הציון אותה תרצה לשנות:
                <br/>
                <CustomSelectContainer
                    optionsObject={existingOuters}
                    setOptionsObject={setExistingOuters}
                    fetchURI={APIFetchURI.GetListOfCheckpoints}
                    fetchURIArgs={existingOuters.areaFetchURIArgs}
                    disabled={false}
                    onChange={handleOuterSelect}
                />
                <br/>
                הכנס את השם המעודכן של נקודת הציון:
                <br/>
                <TextInput
                    textInput={newOuterVertexName}
                    setTextInput={setNewOuterVertexName}
                    label={newOuterVertexNameLabel}
                />
                <br/>
                <br/>
                <button type="submit">החל שינוי!</button>
            </form>
        </>
    );
};

export default UpdateOuterForm;