import React, {useState} from "react";
import CustomSelectContainer from "../../custom-select/CustomSelectContainer";
import DirectionsAdder from "./DirectionsAdder";
import TextInput from "../../text-input/TextInput";
import {APIFetchURI} from "../../assets/useful-data/APICalls";




/**
 * This component is a form for *adding* a Path (aka. Edge in the DB).
 * The logic to this form is found in the user instructions PDF.
 * @param {*} props No props needed or used in this form.
 * @returns React.Fragment
 */
const AddEdgeForm = (props) => {

    /**
     * Helper object of PathType select component. 
     */
    const [newPathType, setNewPathType] = useState({
        /** The chosen ID of the item selected. */
        chosenId: "",
        /** Indicates wether the select component is available or not. */
        isAvailable: true,
        /** A list to keep all the options in the select. */
        options : [],
    });

    /**
     * Helper object of OptionalBuilding select component.
     * This select is optional and should be only present for path types that require him (hence optional). 
     */
    const [optionalBuilding, setOptionalBuilding] = useState({
        /** The chosen ID of the item selected. */
        chosenId: "",
        /** Indicates wether the select component is available or not. */
        isAvailable: true,
        /** A list to keep all the options in the select. */
        options : [],
    });

    /**
     * Helper object of SrcPoint select component.
     * Source Point is the beginning location of the path. 
     */
    const [srcPoint, setSrcPoint] = useState({
        /** The chosen ID of the item selected. */
        chosenId: "",
        /** Indicates wether the select component is available or not. */
        isAvailable: true,
        /** A list to keep all the options in the select. */
        options : [],
    });
    const [isSrcReady, setIsSrcReady] = useState(false);

    /**
     * Helper object of DestPoint select component.
     * Destination Point is the end location of the *current* path. 
     */
    const [destPoint, setDestPoint] = useState({
        /** The chosen ID of the item selected. */
        chosenId: "",
        /** Indicates wether the select component is available or not. */
        isAvailable: true,
        /** A list to keep all the options in the select. */
        options : [],
    });

    /**
     * Object to store the directions given from the user.
     * This component is passed to the DirectionsAdder component as a prop.
     */
    const [directionsObject, setDirectionsObject] = useState({});

    const [totalLength, setTotalLength] = useState("");


    /**
     * A function to handle the selection of a Building from the select.
     * This function sets up the server request configurations for the path points select components.
     * This function is passed to the CustomSelectContainer component in order to run once an option is selected.
     * @param {string} newBuilding The ID of the building selected.
     */
    const handleBuildingChange = (newBuilding) => {
        setIsSrcReady(true);
        const requestOptions = {
            method: 'POST',
            withCredentials: 'true',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                typePath : newPathType.chosenId,
                building : newBuilding,
            })
        };
        setSrcPoint({
            ...srcPoint,
            fetchURIArgs: requestOptions,
        });
        setDestPoint({
            ...destPoint,
            fetchURIArgs: requestOptions,
        });
    };

    /**
     * Handle the submition of the AddEdgeForm.
     * Send a POST request to the server including all the path details. This request will tell the server 
     * add the path to the DB.
     * The server may send a one time message to the user about the status of the request (success or failed).
     */
    const handleSubmit = async (event) => {
        // Prevent the reloading of the page.
        event.preventDefault();

        const directions = Object.values(directionsObject);
        const directionArray = directions.map((value, index) =>  value[0]);
        const directionClassArray = directions.map((value, index) =>  value[1]);
        const directionLengthArray = directions.map((value, index) =>  value[2]);
        const directionCommentArray = directions.map((value, index) =>  value[3]);

        // The path details from the form.
        const requestBody = {
            type: newPathType.chosenId,
            building: optionalBuilding.chosenId,
            src: srcPoint.chosenId,
            dest: destPoint.chosenId,
            weight: totalLength,
            directions: directionArray,
            classification: directionClassArray,
            distances: directionLengthArray,
            freeText: directionCommentArray,
        };

        // The server request configuration.
        const requestOptions = {
            method: 'POST',
            withCredentials: 'true',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(requestBody),
        };

        // Submitting the request in an async way so I will be able to wait for a response.
        const submitRequest = async (requestOptions) => {
            const response  = await fetch(APIFetchURI.CreateNewPath, requestOptions);
            return await response.json();
        };

        // Waiting for a response.
        const result = await submitRequest(requestOptions);
        if (result.key["message"]) {
            alert(result.key["message"]);
        }
    };

    return (
        <React.Fragment>
            <form onSubmit={handleSubmit} >
                <h3>טופס הוספת מסלול</h3>
                בחר סוג מסלול
                <CustomSelectContainer
                    optionsObject={newPathType}
                    setOptionsObject={setNewPathType}
                    fetchURI={APIFetchURI.GetPathTypes}
                    disabled={!newPathType.isAvailable}
                    // onChange={handlePathSelect}
                />
                <br/>
                בחר בניין (אופציונלי)
                <CustomSelectContainer
                    optionsObject={optionalBuilding}
                    setOptionsObject={setOptionalBuilding}
                    fetchURI={APIFetchURI.GetListOfBuilding}
                    disabled={false}
                    onChange={handleBuildingChange}
                />
                <br/>
                נקודת ההתחלה של המסלול
                <br/>
                <CustomSelectContainer
                    optionsObject={srcPoint}
                    setOptionsObject={setSrcPoint}
                    fetchURI={APIFetchURI.SetSrcPath}
                    fetchURIArgs={srcPoint.fetchURIArgs}
                    disabled={!isSrcReady}
                    // onChange={handleBuildingChange}
                />
                <br/>
                נקודת הסוף של המסלול
                <br/>
                <CustomSelectContainer
                    optionsObject={destPoint}
                    setOptionsObject={setDestPoint}
                    fetchURI={APIFetchURI.SetDestPath}
                    fetchURIArgs={destPoint.fetchURIArgs}
                    disabled={!isSrcReady}
                    // onChange={handleBuildingChange}
                />
                <br/>
                <TextInput
                    textInput={totalLength}
                    setTextInput={setTotalLength}
                    label={"Inset total length of route"}
                />
                <br/>
                <h5>בניית המסלול החדש</h5>
                הוסף חלק במסלול
                <br/>
                <DirectionsAdder
                    directionsObject={directionsObject}
                    setDirectionsObject={setDirectionsObject}
                />
                <br/>
                <button type="submit">הוסף מסלול!</button>
            </form>
        </React.Fragment>
    );
};

export default AddEdgeForm;