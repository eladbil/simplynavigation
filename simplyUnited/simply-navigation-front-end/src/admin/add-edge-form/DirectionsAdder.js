import React, {useState} from "react";
import CustomSelectContainer from "../../custom-select/CustomSelectContainer";
import TextInput from "../../text-input/TextInput";
import PropTypes from "prop-types";
import {APIFetchURI} from "../../assets/useful-data/APICalls";

/**
 * This component is able to manage a given object and form to add wanted Directions to Path.
 * 
 * NOTE: This component doesn't send the directions to the server nor having connection to path type.
 * It's simply to manage the logic of adding directions in QOL way.
 * 
 * Mandatory Props:
 * 1. directionsObject - a react state object to save and manage the directions.
 * 2. setDirectionsObject - a react set state function to change the object.
 * 
 * @param {*} props 
 * @returns React Component aka. JSX
 */
const DirectionsAdder = (props) => {
    const directionsObject = props.directionsObject;
    const setDirectionsObject= props.setDirectionsObject;


    const [direction, setDirection] = useState({
         /** The chosen ID of the item selected. */
        chosenId: "",
        /** Indicates wether the select component is available or not. */
        isAvailable: true,
        /** A list to keep all the options in the select. */
        options : [],
        fetchURIArgs: {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
        },
    });

    const [type, setType] = useState({
        /** The chosen ID of the item selected. */
        chosenId: "",
        /** Indicates wether the select component is available or not. */
        isAvailable: true,
        /** A list to keep all the options in the select. */
        options : [],
        fetchURIArgs: {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
        },
    });

    const [counter, setCounter] = useState(1);

    const [length, setLength] = useState("");
    const lengthInputLabel = "Length in Meters";

    const [comments, setComments] = useState("");
    const commentInputLabel = "Free Room For Comments";

    /**
     * Logic function to remove a direction from the component.
     * The function is called once a direction button is clicked on hence the passing in the creation of the buttons.
     * @param {*} param0 Event of mouse click on one of the Direction buttons.
     */
    const handleRemove = ({target}) => {
        const {name} = target;
        const myKey = name;
        // Remove the clicked Button and item from the directionsObject.
        const newRoomObject = Object.keys(directionsObject).reduce((object, key) => {
            if (key !== myKey) {
                object[key] = directionsObject[key];
            }
            return object;
        }, {});
        // Updating the directionObject.
        setDirectionsObject(newRoomObject);
    };

    // Creating the Directions buttons.
    let directionsButtons = Object.keys(directionsObject).map((key, index) => {
        return (
            <React.Fragment key={key}>
                <button key={key} name={key} value={index} onClick={handleRemove}>
                    {key + ": " + directionsObject[key]}
                </button>
                <br/>
            </React.Fragment>
        );
    });

    // Add a Direction to the directionObject.
    const addDirections = (event) => {
        event.preventDefault();
        // Number of rooms added.
        setCounter(() => (counter + 1));
        setDirectionsObject({
            ...directionsObject,
            ["חלק מסלול"]: [direction.chosenId, type.chosenId, length, comments],
        });
        // That's how I initialize the input of text after submit
        setComments("");
        setLength("");
    };

    return (
        <React.Fragment>
            כיוון המסלול:
            <CustomSelectContainer
                optionsObject={direction}
                setOptionsObject={setDirection}
                fetchURI={APIFetchURI.GetDirectionTypes}
                fetchURIArgs={direction.fetchURIArgs}
                disabled={false}
            />
            סוג המסלול:
            <CustomSelectContainer
                optionsObject={type}
                setOptionsObject={setType}
                fetchURI={APIFetchURI.GetDirectionClassification}
                fetchURIArgs={type.fetchURIArgs}
                disabled={false}
            />
            <br/>
            אורך המקצה:
            <br/>
            <TextInput
                textInput={length}
                setTextInput={setLength}
                label={lengthInputLabel}
            />
            <br/>
            הערות למקצה:
            <br/>
            <TextInput
                textInput={comments}
                setTextInput={setComments}
                label={commentInputLabel}
            />
            <br/>
            <br/>
            <button onClick={addDirections}>הוסף מקצה!</button>
            <br/>
            <br/>

            {directionsButtons}

            <br/>

        </React.Fragment>
    );
};

DirectionsAdder.propTypes = {
    directionsObject: PropTypes.object.isRequired,
    setDirectionsObject: PropTypes.func.isRequired,
};

export default DirectionsAdder;