
import React, {useState} from "react";
import TextInput from "../../text-input/TextInput";
import CustomSelectContainer from "../../custom-select/CustomSelectContainer";
import RoomAdder from "./RoomAdder";
import {APIFetchURI} from "../../assets/useful-data/APICalls";
import RegexTextInput from "../../text-input/RegexTextInput";
import {onlyNumbersRegex} from "../../assets/useful-data/ParametersOrConstants";



/**
 * This component is a form for adding an Area (aka. inner vector in the DB).
 * The logic to this form is found in the user instructions PDF.
 * @param {*} props No props needed or used in this form.
 * @returns React.Fragment
 */
const AddAreaForm = (props) => {

    const [destinationBuilding, setDestinationBuilding] = useState({
        /** The chosen ID of the item selected. */
        chosenId: "",
        /** Indicates wether the select component is available or not. */
        isAvailable: true,
        /** A list to keep all the options in the select. */
        options : [],
    });

    const [newAreaName, setNewAreaName]= useState("");
    const [newAreaID, setNewAreaID] = useState("");
    const [newFloor, setNewFloor] = useState("");
    const [roomObject, setRoomObject] = useState({});


    /**
     * Handle the submition of the AddAreaForm.
     * Send a POST request to the server including all the Area details. This request will tell the server 
     * add the area to the DB.
     * The server may send a one time message to the user about the status of the request (success or failed).
     */
    const handleSubmit = async (event) => {
        event.preventDefault();

        const listOfRooms = Object.values(roomObject);

        // The Area details from the form.
        // NOTE: allowEntry should always be 1, as the path finding algorithm used in the server doesn't support
        // unreachable nodes.
        const args = {
            def:newAreaID,
            allowEntry: 1,
            aName: newAreaName,
            building: destinationBuilding.chosenId,
            floor: newFloor,
            rooms: listOfRooms,
        };

        // The server request configuration.
        const requestOptions = {
            method: 'POST',
            withCredentials: 'true',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(args)
        };

        // Submitting the request in an async way so I will be able to wait for a response.
        const submitRequest = async (requestOptions) => {
            const response  = await fetch(APIFetchURI.CreateNewArea, requestOptions);
            return await response.json();
        };

        // Waiting for a response.
        const result = await submitRequest(requestOptions);
        if (result.key["message"]) {
            alert(result.key["message"]);
        }

    };

    return (
        <React.Fragment className="right-text">
            <form onSubmit={handleSubmit}>
                <h3>טופס הוספת אזור</h3>
                שם האזור החדש
                <br/>
                <TextInput
                    label="New Area Name"
                    textInput={newAreaName}
                    setTextInput={setNewAreaName}
                />
                <br/>
                בחר את הבניין בו האזור החדש
                <br/>
                <CustomSelectContainer
                    optionsObject={destinationBuilding}
                    setOptionsObject={setDestinationBuilding}
                    fetchURI={APIFetchURI.GetListOfBuilding}
                    disabled={false}
                    // onChange={handleBuildingSelect}
                />
                <br/>
                מזהה האזור החדש
                <br/>
                <TextInput label="New Area ID" textInput={newAreaID} setTextInput={setNewAreaID}/>
                <br/>
                מספר קומת האזור
                <br/>
                <RegexTextInput
                    label="Floor Number"
                    textInput={newFloor}
                    setTextInput={setNewFloor}
                    regex={onlyNumbersRegex}
                />
                <br/>
                הוספת חדרים באזור החדש
                <br/>
                <RoomAdder
                    roomObject={roomObject}
                    setRoomObject={setRoomObject}
                >
                </RoomAdder>
                <br/>
                <button type="submit">הוסף אזור!</button>
                <br/>
            </form>

            {/*{isMapAvailable &&*/}
            {/*    <NodesVisualization*/}
            {/*        map={map}*/}
            {/*        nodesFetchURI={APIFetchURI.GetBuildingNodesJSON}*/}
            {/*    />*/}
            {/*}*/}
        </React.Fragment>
    );
}

export default AddAreaForm;