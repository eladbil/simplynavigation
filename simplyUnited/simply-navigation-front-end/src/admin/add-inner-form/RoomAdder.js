import React, {useState} from "react";
import PropTypes from "prop-types";
import TextInput from "../../text-input/TextInput";


/**
 * This component is able to manage a given object and form to add wanted Rooms to Area.
 * 
 * NOTE: This component doesn't send the Rooms to the server nor having connection to Area.
 * It's simply to manage the logic of adding Rooms in QOL way.
 * 
 * Mandatory Props:
 * 1. roomObject - a react state object to save and manage the rooms.
 * 2. setRoomObject - a react set state function to change the object.
 */
const RoomAdder = (props) => {
    const roomObject = props.roomObject;
    const setRoomObject = props.setRoomObject;

    const [newRoomName, setNewRoomName] = useState("");
    const nameInputLabel = "New Room Name";


    /**
     * Logic function to remove a Room from the component.
     * The function is called once a Room button is clicked on, hence the passing in the creation of the buttons.
     * @param {*} param0 Event of mouse click on one of the Room buttons.
     */
    const handleRemove = ({target}) => {
        const {name} = target;
        const myKey = name;
        // Remove the clicked Button and item from the roomObject.
        const newRoomObject = Object.keys(roomObject).reduce((object, key) => {
            if (key !== myKey) {
                object[key] = roomObject[key];
            }
            return object;
        }, {});
        // Updating the roomObject.
        setRoomObject(newRoomObject);
    };

    // Creating the Directions buttons.
    let roomButtons = Object.keys(roomObject).map((key, index) => {
        return (
            <button key={key} name={key} value={index} onClick={handleRemove}>
                {"שם חדר: " + key}
            </button>
        );
    });


    // Add a Room to the roomObject.
    const addRoom = (event) => {
        event.preventDefault();
        setRoomObject({
            ...roomObject,
            [newRoomName]: newRoomName,
        });
        setNewRoomName("");
    };

    return (
        <React.Fragment>
            <TextInput textInput={newRoomName} setTextInput={setNewRoomName} label={nameInputLabel} />
            <br/>
            <button type="submit" onClick={addRoom}>
                הוסף חדר!
            </button>
            <br/>
            {roomButtons}
            <br/>
        </React.Fragment>
    );
};

RoomAdder.propTypes = {
    roomObject: PropTypes.object.isRequired,
    setRoomObject: PropTypes.func.isRequired,
};

export default RoomAdder;