import React, {useState} from "react";
import CustomSelectContainer from "../../custom-select/CustomSelectContainer";
import TextInput from "../../text-input/TextInput";
import RoomAdder from "../add-inner-form/RoomAdder";
import {APIFetchURI} from "../../assets/useful-data/APICalls";

/**
 * This component is a form for *updating* an Area (aka. Inner Vertex in the DB).
 * The logic to this form is found in the user instructions PDF.
 * @param {*} props No props needed nor used in this form.
 * @returns React.Fragment
 */
const UpdateInnerForm = (props) => {

    const [destinationBuilding, setDestinationBuilding] = useState({
        /** The chosen ID of the item selected. */
        chosenId: "",
        /** Indicates wether the select component is available or not. */
        isAvailable: true,
        /** A list to keep all the options in the select. */
        options : [],
    });


    const [destinationArea, setDestinationArea] = useState({
        /** The chosen ID of the item selected. */
        chosenId: "",
        /** Indicates wether the select component is available or not. */
        isAvailable: true,
        /** A list to keep all the options in the select. */
        options : [],
        areaFetchURIArgs: {},
    });

    const [newAreaName, setNewAreaName] = useState("");
    const [newBuildingName, setNewBuildingName] = useState("");
    const [newAreaID, setNewAreaID] = useState("");
    const [newFloor, setNewFloor] = useState("");
    const [roomObject, setRoomObject] = useState({});

    /**
     * This fucntion handles the selection of building event.
     * It`s purpose is to set-up the Destination Area object with the correct request options and 
     * the correct building ID to get the Areas from (in the request).
     * @param {string} buildingID The ID of the selected building.
     */
    const handleBuildingChange = (buildingID) => {
        setIsAreaAvailable(true);
        const requestOptions = {
            method: 'POST',
            withCredentials: 'true',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ building : buildingID })
        };
        setDestinationArea({
                ...destinationArea,
                areaFetchURIArgs: requestOptions,
            }
        );
    };

    const [isAreaAvailable, setIsAreaAvailable] = useState(false);

    const handleAreaSelect = (selectedAreaID) => {
        const requestOptions = {
            method: 'POST',
            withCredentials: 'true',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                building : destinationBuilding.chosenId,
                area: selectedAreaID,
            })
        };
        // Fetching from the server the Area details.
        fetch(APIFetchURI.GetAreaDetails, requestOptions).then(res => res.json())
            .then((result) => {
                // let jsonObj = JSON.parse(JSON.stringify(result));
                // console.log(jsonObj.key);
                const areaInfo = JSON.parse(JSON.stringify(result)).key;
                setNewAreaName(areaInfo["area"]);
                setNewBuildingName(areaInfo["building"]);
                // setArePatientsAllowed((!!areaInfo["entry"]));
                setNewAreaID(areaInfo["def"]);
                setNewFloor(areaInfo["floor"] + "");
                setRoomObject(areaInfo["rooms"].reduce((object, value) => ({...object, [value] : value}),{}));
            });
    };

    /**
     * Handle the submition of the UpdateInnerForm.
     * Send a POST request to the server including all the Area details. This request will tell the server 
     * to *update* this Area in the DB with the new details entered from the user.
     * The server may send a one time message to the user about the status of the request (success or failed).
     */
    const handleSubmit = async (event) => {
        event.preventDefault();

        const listOfRooms = Object.values(roomObject);
        // console.log(listOfRooms);

        const args = {
            oldBuilding: destinationBuilding.chosenId,
            oldArea: destinationArea.chosenId,
            def:newAreaID,
            allowEntry: 1,
            aName: newAreaName,
            building: destinationBuilding.chosenId,
            floor: newFloor,
            rooms: listOfRooms,
        };

        const requestOptions = {
            method: 'POST',
            withCredentials: 'true',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(args)
        };

        const submitRequest = async (requestOptions) => {
            const response  = await fetch(APIFetchURI.UpdateAreaDetails, requestOptions);
            return await response.json();
        };

        const result = await submitRequest(requestOptions);
        if (result.key["message"]) {
            alert(result.key["message"]);
        }
    };

    return (
        <>
            <form onSubmit={handleSubmit}>
                <h3>טופס עדכון אזור</h3>
                בחר בניין:
                <br/>
                <CustomSelectContainer
                    optionsObject={destinationBuilding}
                    setOptionsObject={setDestinationBuilding}
                    fetchURI={APIFetchURI.GetListOfBuilding}
                    disabled={false}
                    onChange={handleBuildingChange}
                />
                <br/>
                בחר אזור בבניין אותו תרצה לעדכן:
                <br/>
                <CustomSelectContainer
                    optionsObject={destinationArea}
                    setOptionsObject={setDestinationArea}
                    fetchURI={APIFetchURI.GetListOfAreas}
                    fetchURIArgs={destinationArea.areaFetchURIArgs}
                    disabled={!isAreaAvailable}
                    onChange={handleAreaSelect}
                />
                <h5>שינוי פרטי האזור:</h5>
                שם חדש לאזור:
                <br/>
                <TextInput
                    textInput={newAreaName}
                    setTextInput={setNewAreaName}
                    label={"New Area Name"}
                />
                <br/>
                שם חדש לבניין:
                <br/>
                <TextInput
                    textInput={newBuildingName}
                    setTextInput={setNewBuildingName}
                    label={"New Building Name"}
                />
                <br/>
                {/*<BinaryInput {...binaryInputProps}/>*/}
                מזהה חדש לאזור:
                <br/>
                <TextInput
                    textInput={newAreaID}
                    setTextInput={setNewAreaID}
                    label="New Area ID"
                />
                <br/>
                שינוי קומת האזור:
                <br/>
                <TextInput
                    textInput={newFloor}
                    setTextInput={setNewFloor}
                    label="Floor Number"
                />
                <br/>
                הוספת או הורדת חדרים באזור:
                <br/>
                <RoomAdder
                    roomObject={roomObject}
                    setRoomObject={setRoomObject}
                >
                </RoomAdder>
                <br/>
                <button type="submit">החל שינויים!</button>
            </form>
        </>
    );
};

export default UpdateInnerForm;