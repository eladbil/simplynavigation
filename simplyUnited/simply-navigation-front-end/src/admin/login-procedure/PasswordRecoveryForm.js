import TextInput from "../../text-input/TextInput";
import React, {useState} from "react";
import {useNavigate} from "react-router-dom";
import {APIFetchURI} from "../../assets/useful-data/APICalls";


const PasswordRecoveryForm = (props) => {

    const navigate = useNavigate();

    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");


    const handleRecoveryRequest = async (event) => {
        event.preventDefault();

        const requestBody = {
            firstName: firstName,
            lastName: lastName,
            email: email,
        };

        const requestOptions = {
            method: 'POST',
            withCredentials: 'true',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(requestBody),
        };

        const submitRequest = async (requestOptions) => {
            // console.log(requestOptions);
            return await fetch(APIFetchURI.PasswordRecovery, requestOptions);
        };

        let response = await submitRequest(requestOptions);

        let message = await response.json();
        // console.log(message);
        if (message) {
            console.log(message);
            alert(message["message"]);
        }

        // navigate("/login");
    }

    return (
        <>
            <h3>שחזור סיסמא:</h3>
            <form onSubmit={handleRecoveryRequest}>
                <h6>שם פרטי:</h6>
                <TextInput
                    textInput={firstName}
                    setTextInput={setFirstName}
                    label={"First Name"}
                />
                <h6>שם משפחה:</h6>
                <TextInput
                    textInput={lastName}
                    setTextInput={setLastName}
                    label={"Last Name"}
                />
                <h6>מייל:</h6>
                <TextInput
                    textInput={email}
                    setTextInput={setEmail}
                    label={"Email"}
                />
                <br/>
                <button type="submit">שחזר סיסמא!</button>
            </form>
        </>
    );

};

export default PasswordRecoveryForm;