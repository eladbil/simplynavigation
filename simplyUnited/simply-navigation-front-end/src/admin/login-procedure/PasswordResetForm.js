import React, {useState} from "react";
import TextInput from "../../text-input/TextInput";
import {useNavigate} from "react-router-dom";
import {APIFetchURI} from "../../assets/useful-data/APICalls";


const PasswordResetForm = (props) => {

    const navigate = useNavigate();


    const [code, setCode] = useState("");
    const [newPassword, setNewPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [email, setEmail] = useState("");

    const handleResetRequest = async (event) => {
        event.preventDefault();

        const requestBody = {
            code: code,
            newPassword: newPassword,
            confirmPassword: confirmPassword,
            email: email,
        };

        const requestOptions = {
            method: 'POST',
            withCredentials: 'true',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(requestBody),
        };

        const submitRequest = async (requestOptions) => {
            // console.log(requestOptions);
            return await fetch(APIFetchURI.ResetPassword, requestOptions);
        };

        let response = await submitRequest(requestOptions);

        let message = await response.json();
        if (message.key) {
            alert(message.key["message"]);
        }

    };

    return (
        <>
            <h3>אתחול סיסמא:</h3>
            <form onSubmit={handleResetRequest}>
                <h6>קוד שיחזור:</h6>
                <TextInput
                    textInput={code}
                    setTextInput={setCode}
                    label={"Recovery Code"}
                />
                <h6>מייל:</h6>
                <TextInput
                    textInput={email}
                    setTextInput={setEmail}
                    label={"Email"}
                />
                <br/>
                <h6>סיסמא חדשה:</h6>
                <TextInput
                    textInput={newPassword}
                    setTextInput={setNewPassword}
                    label={"New Password"}
                />
                <h6>וודא סיסמא:</h6>
                <TextInput
                    textInput={confirmPassword}
                    setTextInput={setConfirmPassword}
                    label={"Confirm Password"}
                />
                <br/>

                <button type="submit">אתחל סיסמא!</button>
            </form>
        </>
    );

};

export default PasswordResetForm;