import React, {useState} from "react";
import TextInput from "../../text-input/TextInput";
import { useNavigate } from "react-router-dom";
import {APIFetchURI} from "../../assets/useful-data/APICalls";

const CreatNewUser = (props) => {

    const navigate = useNavigate();

    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [password, setPassword] = useState("");
    const [email, setEmail] = useState("");

    const handleSubmit = async (event) => {
        event.preventDefault();

        const requestBody = {
            firstName: firstName,
            lastName: lastName,
            password: password,
            email: email,
        };

        const requestOptions = {
            method: 'POST',
            withCredentials: 'true',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(requestBody),
        };

        const submitRequest = async (requestOptions) => {
            // console.log(requestOptions);
            return await fetch(APIFetchURI.CreateUser, requestOptions);
        };

        let response = await submitRequest(requestOptions);

        let message = await response.json();
        // console.log(message);
        if (message) {
            console.log(message);
            alert(message["message"]);
        }

        navigate("/login");

    };

    return (
        <>
            <h3>יצירת משתמש חדש:</h3>
            <form onSubmit={handleSubmit}>
                <h6>שם פרטי:</h6>
                <TextInput
                    textInput={firstName}
                    setTextInput={setFirstName}
                    label={"First Name"}
                />
                <h6>שם משפחה:</h6>
                <TextInput
                    textInput={lastName}
                    setTextInput={setLastName}
                    label={"Last Name"}
                />
                <h6>סיסמא:</h6>
                <TextInput
                    textInput={password}
                    setTextInput={setPassword}
                    label={"Password"}
                />
                <h6>מייל:</h6>
                <TextInput
                    textInput={email}
                    setTextInput={setEmail}
                    label={"Email"}
                />
                <br/>
                <button type="submit">Submit!</button>
            </form>
        </>
    );

};

export default CreatNewUser;