import {useEffect} from "react";
import {APIFetchURI} from "../../assets/useful-data/APICalls";
import {useNavigate} from "react-router-dom";


const LogoutForm = (props) => {

    const setConnected = props.setAdminConnected

    const navigate = useNavigate();

    const callLogout = async () => {
        const requestOptions = {
            method: 'GET',
            withCredentials: 'true',
            credentials: 'include',
            headers: {'Content-Type': 'application/json'},
        };

        const submitRequest = async (requestOptions) => {
            const response = await fetch(APIFetchURI.Logout, requestOptions);
            return await response.json();
        };

        const result = await submitRequest(requestOptions);
        if (result.key["message"]) {
            alert(result.key["message"]);
        }
        setConnected(false);
    }

    callLogout();

    useEffect(() => {
        navigate("../");
    },[]);

    return (
        <>
        </>
    );
};

export default LogoutForm;