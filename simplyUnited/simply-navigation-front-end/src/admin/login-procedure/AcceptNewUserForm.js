import React, {useState} from "react";
import TextInput from "../../text-input/TextInput";
import {APIFetchURI} from "../../assets/useful-data/APICalls";


const AcceptNewUserForm = (props) => {

    const [superAdminEmail, setSuperAdminEmail] = useState("");
    const [superAdminPassword, setSuperAdminPassword] = useState("");
    const [newAdminEmail, setNewAdminEmail] = useState("");

    const handleAcceptRequest = async (event) => {
        event.preventDefault();

        const requestBody = {
            superAdminEmail,
            superAdminPassword: superAdminPassword,
            newAdminEmail: newAdminEmail,
        };

        const requestOptions = {
            method: 'POST',
            withCredentials: 'true',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(requestBody),
        };

        const submitRequest = async (requestOptions) => {
            // console.log(requestOptions);
            return await fetch(APIFetchURI.AcceptUser, requestOptions);
        };

        let response = await submitRequest(requestOptions);

        let message = await response.json();
        if (message.key) {
            alert(message.key["message"]);
        }

    };

    return (
        <>
            <h3>קבל משתמש חדש:</h3>
            <form onSubmit={handleAcceptRequest}>
                <h6>המייל שלך:</h6>
                <TextInput
                    textInput={superAdminEmail}
                    setTextInput={setSuperAdminEmail}
                    label={"Your Email"}
                />
                <h6>הסיסמא שלך:</h6>
                <TextInput
                    textInput={superAdminPassword}
                    setTextInput={setSuperAdminPassword}
                    label={"Your Password"}
                />
                <br/>
                <h6>המייל של המשתמש החדש:</h6>
                <TextInput
                    textInput={newAdminEmail}
                    setTextInput={setNewAdminEmail}
                    label={"New Admin Mail"}
                />
                <br/>
                <button type="submit">קבל משתמש!</button>
            </form>
        </>
    );
};

export default AcceptNewUserForm;