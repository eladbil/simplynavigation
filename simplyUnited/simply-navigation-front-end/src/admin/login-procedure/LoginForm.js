import TextInput from "../../text-input/TextInput";
import React, {useState} from "react";
import {Link, useNavigate} from "react-router-dom";
import {APIFetchURI} from "../../assets/useful-data/APICalls";


const LoginForm = (props) => {

    const setConnected = props.setAdminConnected;

    const navigate = useNavigate();

    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");


    const handleLoginRequest = async (event) => {
        event.preventDefault();

        const requestBody = {
            username: username,
            password: password,
        };

        const requestOptions = {
            method: 'POST',
            withCredentials: 'true',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            credentials: 'include',
            body: JSON.stringify(requestBody),
        };

        const submitRequest = async (requestOptions) => {
            const response  = await fetch(APIFetchURI.SignIn, requestOptions);
            return await response;
        };

        let result = await submitRequest(requestOptions);
        if (result.ok) {
            // console.log(result);
            setConnected(true);
            try {
                result = result.json();
                if (result["message"]) {
                    alert(result["message"]);
                }

            } catch (error) {}
        } else {
            setConnected(false);
            result = result.json();
            alert(result["message"]);
        }

        navigate("../");
    };

    return (
        <>
            <form onSubmit={handleLoginRequest}>
                <h4>התחברות:</h4>
                <h6>אימייל:</h6>
                <TextInput
                    textInput={username}
                    setTextInput={setUsername}
                    label={"Email"}
                />
                <h6>סיסמא:</h6>
                <TextInput
                    textInput={password}
                    setTextInput={setPassword}
                    label={"Password"}
                />
                <br/>
                <button type="submit">התחבר!</button>
            </form>
            <br/>
            <Link to={'create-new-user'}>יצירת משתמש חדש</Link>
            <br/>
            <Link to={'password-recovery'}>שכחתי סיסמא</Link>
        </>
    );

};

export default LoginForm;