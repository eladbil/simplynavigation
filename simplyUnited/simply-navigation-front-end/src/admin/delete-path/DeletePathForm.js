import React, {useState} from "react";
import CustomSelectContainer from "../../custom-select/CustomSelectContainer";
import {APIFetchURI} from "../../assets/useful-data/APICalls";



/**
 * This component is a form for *deleting* a Path (aka. Edge in the DB).
 * The logic to this form is found in the user instructions PDF.
 * @param {*} props No props needed or used in this form.
 * @returns React.Fragment
 */
const DeletePathForm = (props) => {

    const [newPathType, setNewPathType] = useState({
        /** The chosen ID of the item selected. */
        chosenId: "",
        /** Indicates wether the select component is available or not. */
        isAvailable: true,
        /** A list to keep all the options in the select. */
        options : [],
    });

    const [isTypePathAvailable, setIsTypePathAvailable] = useState(false);

    const [optionalBuilding, setOptionalBuilding] = useState({
        /** The chosen ID of the item selected. */
        chosenId: "",
        /** Indicates wether the select component is available or not. */
        isAvailable: true,
        /** A list to keep all the options in the select. */
        options : [],
    });
    const [isSrcReady, setIsSrcReady] = useState(false);

    const [srcPoint, setSrcPoint] = useState({
        /** The chosen ID of the item selected. */
        chosenId: "",
        /** Indicates wether the select component is available or not. */
        isAvailable: true,
        /** A list to keep all the options in the select. */
        options : [],
    });

    const [destPoint, setDestPoint] = useState({
        /** The chosen ID of the item selected. */
        chosenId: "",
        /** Indicates wether the select component is available or not. */
        isAvailable: true,
        /** A list to keep all the options in the select. */
        options : [],
    });

    /**
     * A function to handle the selection of a Building from the select.
     * This function sets up the server request configurations for the path points select components.
     * This function is passed to the CustomSelectContainer component in order to run once an option is selected.
     * @param {string} newBuilding The ID of the building selected.
     */
    const handleBuildingChange = (selectedBuildingId) => {
        setIsTypePathAvailable(true);
        const requestOptions = {
            method: 'POST',
            withCredentials: 'true',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                typePath : newPathType.chosenId,
                building: selectedBuildingId,
            })
        };
        setSrcPoint({
            ...srcPoint,
            fetchURIArgs: requestOptions,
        });
        setDestPoint({
            ...destPoint,
            fetchURIArgs: requestOptions,
        });
    };

    /**
     * Handle the submition of the DeletePathForm.
     * Send a POST request to the server including all the Path details. This request will tell the server 
     * *delete* the Path from the DB.
     * The server may send a one time message to the user about the status of the request (success or failed).
     */
    const handleSubmit = async (event) => {
        event.preventDefault();

        const requestBody = {
            type: newPathType.chosenId,
            building: optionalBuilding.chosenId,
            src: srcPoint.chosenId,
            dest: destPoint.chosenId,
        };

        const requestOptions = {
            method: 'POST',
            withCredentials: 'true',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(requestBody),
        };

        const submitRequest = async (requestOptions) => {
            const response  = await fetch(APIFetchURI.DeletePath, requestOptions);
            // Waiting for a response before returning.
            return await response.json();
        };

        const result = await submitRequest(requestOptions);
        if (result.key["message"]) {
            alert(result.key["message"]);
        }

    };

    return (
        <>
            <form onSubmit={handleSubmit}>
                <h3>טופס מחיקת מסלול</h3>
                בחר את סוג המסלול
                <br/>
                <CustomSelectContainer
                    optionsObject={newPathType}
                    setOptionsObject={setNewPathType}
                    fetchURI={APIFetchURI.GetPathTypes}
                    disabled={!newPathType.isAvailable}
                    // onChange={handlePathSelect}
                />
                <br/>
                בחר את הבניין בו המסלול נמצא
                <br/>
                <CustomSelectContainer
                    optionsObject={optionalBuilding}
                    setOptionsObject={setOptionalBuilding}
                    fetchURI={APIFetchURI.GetListOfBuilding}
                    disabled={false}
                    onChange={handleBuildingChange}
                />
                <br/>
                בחר את נקודת המוצא של המסלול
                <br/>
                <CustomSelectContainer
                    optionsObject={srcPoint}
                    setOptionsObject={setSrcPoint}
                    fetchURI={APIFetchURI.DeleteSetSrcPath}
                    fetchURIArgs={srcPoint.fetchURIArgs}
                    disabled={!isTypePathAvailable}
                />
                <br/>
                בחר את נקודת הסיום של המסלול
                <br/>
                <CustomSelectContainer
                    optionsObject={destPoint}
                    setOptionsObject={setDestPoint}
                    fetchURI={APIFetchURI.DeleteSetDestPath}
                    fetchURIArgs={destPoint.fetchURIArgs}
                    disabled={!isTypePathAvailable}
                />
                <br/>
                <button type="submit">מחק מסלול!</button>
            </form>
        </>
    );
};

export default DeletePathForm;