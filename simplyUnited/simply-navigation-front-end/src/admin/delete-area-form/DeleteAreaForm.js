import React, {useState} from "react";
import CustomSelectContainer from "../../custom-select/CustomSelectContainer";
import {APIFetchURI} from "../../assets/useful-data/APICalls";


/**
 * This component is a form for *deleting* an Area (aka. Inner Vertex in the DB).
 * The logic to this form is found in the user instructions PDF.
 * @param {*} props No props needed or used in this form.
 * @returns React.Fragment
 */
const DeleteAreaForm = (props) => {


    const [destinationBuilding, setDestinationBuilding] = useState({
        /** The chosen ID of the item selected. */
        chosenId: "",
        /** Indicates wether the select component is available or not. */
        isAvailable: true,
        /** A list to keep all the options in the select. */
        options : [],
    });


    const [destinationArea, setDestinationArea] = useState({
        /** The chosen ID of the item selected. */
        chosenId: "",
        /** Indicates wether the select component is available or not. */
        isAvailable: true,
        /** A list to keep all the options in the select. */
        options : [],
        areaFetchURIArgs: {},
    });

    const [isAreaAvailable, setIsAreaAvailable] = useState(false);


    /**
     * This fucntion handles the selection of building event.
     * It`s purpose is to set-up the Destination Area object with the correct request options and 
     * the correct building ID to get the Areas from (in the request).
     * @param {string} buildingID The ID of the selected building.
     */
    const handleBuildingChange = (buildingID) => {
        setIsAreaAvailable(true);
        const requestOptions = {
            method: 'POST',
            withCredentials: 'true',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ building : buildingID })
        };
        setDestinationArea({
            ...destinationArea,
            areaFetchURIArgs: requestOptions,
            }
        );
    };


    /**
     * Handle the submition of the DeleteAreaForm.
     * Send a POST request to the server including all the Area details. This request will tell the server 
     * to *remove* this Area from the DB along with all it`s connected paths.
     * The server may send a one time message to the user about the status of the request (success or failed).
     */
    const handleSubmit = async (event) => {
        event.preventDefault();

        const args = {
            building: destinationBuilding.chosenId,
            area: destinationArea.chosenId,
        };

        const requestOptions = {
            method: 'POST',
            withCredentials: 'true',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(args)
        };

        const submitRequest = async (requestOptions) => {
            const response  = await fetch(APIFetchURI.DeleteArea, requestOptions);
            // Waiting for response and only then return.
            return await response.json();
        };

        const result = await submitRequest(requestOptions);
        if (result.key["message"]) {
            alert(result.key["message"]);
        }
    };

    return (
        <React.Fragment>
            <form onSubmit={handleSubmit}>
                <h3>טופס מחיקת אזור בתוך בניין</h3>
                בחר את הבניין בו תרצה למחוק את האזור
                <br/>
                <CustomSelectContainer
                    optionsObject={destinationBuilding}
                    setOptionsObject={setDestinationBuilding}
                    fetchURI={APIFetchURI.GetListOfBuilding}
                    disabled={false}
                    onChange={handleBuildingChange}
                />
                <br/>
                בחר את האזור אותו תרצה למחוק
                <br/>
                <CustomSelectContainer
                    optionsObject={destinationArea}
                    setOptionsObject={setDestinationArea}
                    fetchURI={APIFetchURI.GetListOfAreas}
                    fetchURIArgs={destinationArea.areaFetchURIArgs}
                    disabled={!isAreaAvailable}
                />
                <br/>
                <button type="submit">מחק אזור!</button>
            </form>
        </React.Fragment>
    );
};

export default DeleteAreaForm;