import React, {useState} from "react";
import CustomSelectContainer from "../../custom-select/CustomSelectContainer";
import {APIFetchURI} from "../../assets/useful-data/APICalls";


const GetQRForm = (props) => {

    const [chosenBuilding, setChosenBuilding] = useState({
        chosenId: "",
        isAvailable: true,
        options : [],
    });

    const [chosenArea, setChosenArea] = useState({
        chosenId: "",
        isAvailable: false,
        options : [],
        areaFetchURIArgs: {},
    });

    const [isAreaAvailable, setIsAreaAvailable] = useState(false);

    const handleBuildingChange = (buildingID) => {
        setIsAreaAvailable(true);
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ building : buildingID })
        };
        setChosenArea({
                ...chosenArea,
                areaFetchURIArgs: requestOptions,
            }
        );
    };

    const handleAreaSelect = async (selectedAreaID) => {
        const requestOptions = {
            method: 'POST',
            withCredentials: 'true',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json',
            },
            responseType: 'blob',
            body: JSON.stringify({
                building : chosenBuilding.chosenId,
                areaName : selectedAreaID
            })
        };

        // fetch(APIFetchURI.GetQR, requestOptions)
        //     .then(response => {
        //         response.blob().then(blob => {
        //
        //         });
        //         //window.location.href = response.url;
        //     });

        const submitRequest = async (requestOptions) => {
            const response  = await fetch(APIFetchURI.GetQR, requestOptions);
            // const body = await response.json();
            // console.log("Body is:", body);
            // return body;
            return response;
        };

        const result = await submitRequest(requestOptions);

        result.blob().then((blob) => {
            let url = window.URL.createObjectURL(blob);
            let a = document.createElement('a');
            a.href = url;
            a.download = selectedAreaID + ".pdf";
            a.click();
        });
        // const fileName = (response.headers.get('content-disposition').split('filename*=UTF-8\'\'')[1]);
        // console.log(response.headers['Content-disposition'].split('filename=')[1]);


    };

    return (
        <>
            <h3>קבל את הQR</h3>
            בחר בניין:
            <CustomSelectContainer
                optionsObject={chosenBuilding}
                setOptionsObject={setChosenBuilding}
                fetchURI={APIFetchURI.GetListOfBuilding}
                disabled={false}
                onChange={handleBuildingChange}
            />
            <br/>
            בחר אזור:
            <CustomSelectContainer
                optionsObject={chosenArea}
                setOptionsObject={setChosenArea}
                fetchURI={APIFetchURI.GetListOfAreas}
                fetchURIArgs={chosenArea.areaFetchURIArgs}
                disabled={!isAreaAvailable}
                onChange={handleAreaSelect}
            />
            <br/>
        </>
    );
};

export default GetQRForm;