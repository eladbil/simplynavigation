import {useState} from "react";

import Graph from "react-vis-network-graph";
import CustomSelectContainer from "../../custom-select/CustomSelectContainer";
import React from "react";
import DrawEntireBuildingForm from "./DrawEntireBuildingForm";
import DrawCertainFloorForm from "./DrawCertainFloorForm";
import DrawAreaByNameForm from "./DrawAreaByNameForm";
import {graphOption} from "./GraphConfig";



const NodesVisualization = (props) => {

    const [visForm, setVisForm] = useState(<></>);

    const [visSubmitBody, setVisSubmitBody] = useState({});
    const [visSubmitAPICall, setVisSubmitAPICall] = useState('');

    const [mapOptionsArray, setMapOptionsArray] = useState([
        "wholeBuilding",
        "onlyFloor",
        "areaByName",
    ]);

    const [mapOptions, setMapOptions] = useState({
        chosenId: "",
        isAvailable: true,
        options: [
            {value: mapOptionsArray[0], label: "הצג את כל הבניין"},
            {value: mapOptionsArray[1], label: "הצג קומה מבניין"},
            {value: mapOptionsArray[2], label: "הצג אזור על ידי שם"},
        ],
    });

    const onMapOptionSelect = (optionSelectedID) => {
        if (optionSelectedID === mapOptionsArray[0]) {
            setVisForm(<DrawEntireBuildingForm
                setSubmitAPICall={setVisSubmitAPICall}
                submitOptions={visSubmitBody}
                setSubmitOptions={setVisSubmitBody}
            />);
        } else if (optionSelectedID === mapOptionsArray[1]) {
            setVisForm(<DrawCertainFloorForm
            setSubmitAPICall={setVisSubmitAPICall}
            submitOptions={visSubmitBody}
            setSubmitOptions={setVisSubmitBody}
            />)
        } else if (optionSelectedID === mapOptionsArray[2]) {
            setVisForm(<DrawAreaByNameForm
                setSubmitAPICall={setVisSubmitAPICall}
                submitOptions={visSubmitBody}
                setSubmitOptions={setVisSubmitBody}
            />)
        }
    }

    const [graph, setGraph] = useState({});

    const buildGraph = (mapNodesObject) => {
        setIsReady(false);
        const originalNodesArray = mapNodesObject["Nodes"];
        const originalEdgesArray = mapNodesObject["Relationships"];
        // console.log(originalNodesArray)
        // console.log(originalEdgesArray)
        let nodesArray = [];
        let edgesArray = [];

        originalNodesArray.forEach((node) => {
            let namePlace = 'name'
            if ( node["labels"][0] === "in_V") {
                namePlace = 'areaName'
            }

            let ourTitle = {
                alice: "alice",
                bob: "bob",
            }

            nodesArray.push({
                id: node["identity"].low,
                label: node["properties"][namePlace],
                title: ourTitle,
                title2: "hellos",
            });
        });

        console.log(originalEdgesArray)
        if (originalEdgesArray) {
            originalEdgesArray.forEach((edge) => {
                edgesArray.push({
                    _label: edge["identity"].low,
                    id: edge["identity"].low,
                    from: edge["start"].low,
                    to: edge["end"].low
                });
            });
        } else {
            edgesArray = [];
        }

        setGraph({
            nodes: nodesArray,
            edges: edgesArray,
        });
        setIsReady(true);
    };

    const [isReady, setIsReady] = useState(false);

    const sendRequest = async () => {

        const requestOptions = {
            method: 'POST',
            withCredentials: 'true',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(visSubmitBody),
        };

        const submitRequest = async (requestOptions) => {
            const response  = await fetch(visSubmitAPICall, requestOptions);
            return await response.json();
        };

        const result = await submitRequest(requestOptions);
        if (result.key["message"]) {
            alert(result.key["message"]);
        }
        buildGraph(result.key);
    }

    // const graph = {
    //     nodes: [
    //         { id: 1, label: "Node 1", title: "node 1 tootip text" },
    //         { id: 2, label: "Node 2", title: "node 2 tootip text" },
    //         { id: 3, label: "Node 3", title: "node 3 tootip text" },
    //         { id: 4, label: "Node 4", title: "node 4 tootip text" },
    //         { id: 5, label: "Node 5", title: "node 5 tootip text" }
    //     ],
    //     edges: [
    //         { from: 1, to: 2 },
    //         { from: 1, to: 3 },
    //         { from: 2, to: 4 },
    //         { from: 2, to: 5 }
    //     ]
    // };

    const events = {
        hoverEdge: function (event) {
            console.log(event);
            const selEdge = event.edge;
            const data = graph.edges.map((el) => {
                if (el.id === selEdge) return { ...el, label: el._label };
                else return el;
            });

            const temp = { ...graph };
            temp.edges = data;
            setGraph(temp);
        },
    };

    return (
        <>
            <h3>הצגת המפה בבסיס נתונים</h3>
            {
                !isReady &&
                <>
                    <h6>בחר את סוג המפה שתרצה</h6>
                    <CustomSelectContainer
                        optionsObject={mapOptions}
                        setOptionsObject={setMapOptions}
                        disabled={false}
                        onChange={onMapOptionSelect}
                    />
                    <br/>
                    {visForm}
                    <button onClick={sendRequest}>הצג גרף</button>
                    <br/>
                </>
            }

            {
                isReady &&
                <>
                    <Graph
                        id="nodes-map"
                        graph={graph}
                        options={graphOption}
                        events={events}
                    />
                </>
            }
        </>
    );

};

export default NodesVisualization;