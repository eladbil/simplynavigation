import React, {useState} from "react";
import CustomSelectContainer from "../../custom-select/CustomSelectContainer";
import {APIFetchURI} from "../../assets/useful-data/APICalls";


const DrawAreaByNameForm = (props) => {

    const setVisSubmitAPICall = props.setSubmitAPICall;
    const setIDChosen = props.setSubmitOptions;
    const submitOptions = props.submitOptions;

    const [destinationBuilding, setDestinationBuilding] = useState({
        chosenId: "",
        isAvailable: true,
        options : [],
    });


    const [destinationArea, setDestinationArea] = useState({
        chosenId: "",
        isAvailable: false,
        options : [],
        areaFetchURIArgs: {},
    });

    const [isAreaAvailable, setIsAreaAvailable] = useState(false);

    const handleBuildingChange = (buildingID) => {
        setIsAreaAvailable(true);
        const requestOptions = {
            method: 'POST',
            withCredentials: 'true',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ building : buildingID })
        };
        setDestinationArea({
                ...destinationArea,
                areaFetchURIArgs: requestOptions,
            }
        );
    };

    const handleAreaSelect = (areaID) => {
        setVisSubmitAPICall(APIFetchURI.DrawAreaByName);
        setIDChosen({
            building: destinationBuilding.chosenId,
            areaName: areaID,
        });
    };

    return (
        <>
            <br/>
            בחר את הבניין בו קיים האזור אותו תרצה להציג
            <CustomSelectContainer
                optionsObject={destinationBuilding}
                setOptionsObject={setDestinationBuilding}
                fetchURI={APIFetchURI.GetListOfBuilding}
                disabled={false}
                onChange={handleBuildingChange}
            />
            <br/>
            בחר את האזור אותו תרצה להציג
            <br/>
            <CustomSelectContainer
                optionsObject={destinationArea}
                setOptionsObject={setDestinationArea}
                fetchURI={APIFetchURI.GetListOfAreas}
                fetchURIArgs={destinationArea.areaFetchURIArgs}
                disabled={!isAreaAvailable}
                onChange={handleAreaSelect}
            />
            <br/>
        </>
    );

};

export default DrawAreaByNameForm;