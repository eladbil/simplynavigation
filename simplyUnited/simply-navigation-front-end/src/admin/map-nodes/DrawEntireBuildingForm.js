import React, {useState} from "react";
import CustomSelectContainer from "../../custom-select/CustomSelectContainer";
import {APIFetchURI} from "../../assets/useful-data/APICalls";
import PropTypes from "prop-types";


const DrawEntireBuildingForm = (props) => {

    const setVisSubmitAPICall = props.setSubmitAPICall;

    const setIDChosen = props.setSubmitOptions;
    const submitOptions = props.submitOptions;

    const [building, setBuilding] = useState({
        chosenId: "",
        isAvailable: true,
        options : [],
    });

    const handleBuildingChange = (selectedBuildingID) => {
        setVisSubmitAPICall(APIFetchURI.DrawEntireBuilding);
        setIDChosen({
            building: selectedBuildingID,
        });
    };

    return (
        <>
            בחר בניין אותו תרצה להציג
            <CustomSelectContainer
                optionsObject={building}
                setOptionsObject={setBuilding}
                fetchURI={APIFetchURI.GetListOfBuilding}
                disabled={false}
                onChange={handleBuildingChange}
            />
            <br/>
        </>
    );
};

export default DrawEntireBuildingForm;

DrawEntireBuildingForm.propTypes = {
    setSubmitOptions: PropTypes.func.isRequired,
};