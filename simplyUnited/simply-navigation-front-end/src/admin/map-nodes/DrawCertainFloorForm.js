import CustomSelectContainer from "../../custom-select/CustomSelectContainer";
import {APIFetchURI} from "../../assets/useful-data/APICalls";
import React, {useState} from "react";
import TextInput from "../../text-input/TextInput";


const DrawCertainFloorForm = (props) => {




    const setVisSubmitAPICall = props.setSubmitAPICall;

    const setIDChosen = props.setSubmitOptions;
    const submitOptions = props.submitOptions;

    const [newFloor, setNewFloor] = useState("");

    const [building, setBuilding] = useState({
        chosenId: "",
        isAvailable: true,
        options : [],
    });

    const handleBuildingChange = (selectedBuildingID) => {
        setVisSubmitAPICall(APIFetchURI.DrawCertainFloorInBuilding);
        setIDChosen({
            building: selectedBuildingID,
            floor: newFloor,
        });
    };

    const handleFloorChange = (floor) => {
        setNewFloor(floor);
        setIDChosen({
            building: building.chosenId,
            floor: floor,
        });
    };

    return (
        <>
            בחר בניין אותו תרצה להציג
            <CustomSelectContainer
                optionsObject={building}
                setOptionsObject={setBuilding}
                fetchURI={APIFetchURI.GetListOfBuilding}
                disabled={false}
                onChange={handleBuildingChange}
            />
            <h6>הכנס את מספר הקומה</h6>
            <TextInput
                textInput={newFloor}
                setTextInput={handleFloorChange}
                label="Floor Number"
            />
            <br/>
        </>
    );
};

export default DrawCertainFloorForm;