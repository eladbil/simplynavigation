

export const graphOption = {
    autoResize: true,
    edges: {
        arrows: {
            to: {
                enabled: true,
                // imageHeight: undefined,
                // imageWidth: undefined,
                scaleFactor: 1,
                // src: undefined,
                type: "arrow",
            },
            from: {
                enabled: false,
                // imageHeight: undefined,
                // imageWidth: undefined,
                scaleFactor: 1,
                // src: undefined,
                type: "arrow"
            }
        },
        color: 'green',
        font: '12px arial #ff0000',
        scaling:{
            label: true,
        },
        shadow: true,
        smooth: true,
    },
    height: "700px",
    physics: {
        enabled: true,
        // maxVelocity: 50,
        // minVelocity: 0.1,
        // solver: "barnesHut",
        stabilization: {
            enabled: true,
            iterations: 1000,
            updateInterval: 100,
            onlyDynamicEdges: false,
            fit: true
        }
    }
};