import React, {useState} from "react";
// import { Link } from "react-router-dom";
// import * as AiIcons from "react-icons/ai";
import {SideBarDataOptional} from "./SideBarData";
import "./NavigationBar.css"
import Navbar from "react-bootstrap/Navbar";
import Container from "react-bootstrap/Container";
import {Nav, NavDropdown, Offcanvas} from "react-bootstrap";

import icon from "../assets/Design/images/wolfson-icon.png";



const NavigationBar = (props) => {

    const isAdminConnected = props.isAdminConnected ? props.isAdminConnected : false;

    const formsDropdown = () => {
        return (
            <>
            <NavDropdown title={"טפסים לניהול המערכת"} className="navbar-dropdown ml-auto">
                {
                    SideBarDataOptional.map((item, index) => {
                        return <Nav.Link key={index} href={item.path} className="navbar-link">
                            {item.title}
                        </Nav.Link>
                    })
                }
            </NavDropdown>
            <Nav.Link key="stat1" href="/logviewweb" className="navbar-link">
                סטטיסטיקות ותגובות
            </Nav.Link>
            </>
        );
    };

    return (
        <>
            <Navbar variant="light" expand="lg" className="navbar" >
                <Container fluid>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
                    <Navbar.Brand href="/">
                        <img
                            alt=""
                            src={icon}
                            width="40"
                            height="40"
                            // className="d-inline-block align-top"
                        /> {'  '}
                        Simply Navigation
                    </Navbar.Brand>
                    <Navbar.Offcanvas placement="end">
                        <Offcanvas.Header closeButton>
                            <Offcanvas.Title>
                                תפריט
                            </Offcanvas.Title>
                        </Offcanvas.Header>
                        <Offcanvas.Body className="ml-auto">
                            <Nav>
                                <Nav.Link href="/search">התחל ניווט</Nav.Link>
                                {isAdminConnected ?
                                    <Nav.Link href="/logout">התנתק</Nav.Link> : <></>
                                }
                                {isAdminConnected ?
                                    <></> : <Nav.Link href="/login">התחבר כמנהל</Nav.Link>
                                }
                                {isAdminConnected ? formsDropdown() : <></>}
                            </Nav>
                        </Offcanvas.Body>
                    </Navbar.Offcanvas>
                </Container>
            </Navbar>
        </>
    );
};

export default NavigationBar;