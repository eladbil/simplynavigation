import React from "react";
import * as AiIcons from "react-icons/ai"
import * as FaIcons from "react-icons/fa"

export const SideBarDataOptional = [
    {
        title: 'הצגת המפה בצורת גרף',
        path: '/map-visualization',
        icon: <FaIcons.FaCrown/>,
        cName: 'nav-text',
    },
    {
        title: 'טופס הוספת אזור',
        path: '/add-area-form',
        icon: <FaIcons.FaCrown/>,
        cName: 'nav-text',
    },
    {
        title: 'טופס עדכון אזור',
        path: '/update-area-form',
        icon: <FaIcons.FaCrown/>,
        cName: 'nav-text',
    },
    {
        title: 'טופס מחיקת אזור',
        path: '/delete-area-form',
        icon: <FaIcons.FaCrown/>,
        cName: 'nav-text',
    },
    {
        title: 'טופס הוספת נקודת ציון',
        path: '/add-outer-form',
        icon: <FaIcons.FaCrown/>,
        cName: 'nav-text',
    },
    {
        title: 'טופס עדכון נקודת ציון',
        path: '/update-outer-form',
        icon: <FaIcons.FaCrown/>,
        cName: 'nav-text',
    },
    {
        title: 'טופס מחיקת נקודת ציון',
        path: '/delete-outer-form',
        icon: <FaIcons.FaCrown/>,
        cName: 'nav-text',
    },
    {
        title: 'טופס הוספת מסלול',
        path: '/add-edge-form',
        icon: <FaIcons.FaCrown/>,
        cName: 'nav-text',
    },
    {
        title: 'טופס עדכון מסלול',
        path: '/update-edge-form',
        icon: <FaIcons.FaCrown/>,
        cName: 'nav-text',
    },
    {
        title: 'טופס מחיקת מסלול',
        path: '/delete-edge-form',
        icon: <FaIcons.FaCrown/>,
        cName: 'nav-text',
    },
    {
        title: 'טופס קבלת QR',
        path: '/get-QR-form',
        icon: <FaIcons.FaCrown/>,
        cName: 'nav-text',
    },
]