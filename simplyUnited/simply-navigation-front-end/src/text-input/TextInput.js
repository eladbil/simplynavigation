import PropTypes from "prop-types";
import TextField from '@mui/material/TextField';
import {useEffect} from "react";
import {basicSanitizationRegex, noCommentRegex} from "../assets/useful-data/ParametersOrConstants";

const TextInput = (props) => {

    const textInput = props.textInput;
    const setTextInput = props.setTextInput;
    const label = props.label;

    const regex = basicSanitizationRegex;
    const secondRegex = noCommentRegex;

    const handleTextInputChange = (event) => {
        event.currentTarget.value = event.currentTarget.value.replace(regex, "");
        event.currentTarget.value = event.currentTarget.value.replace(secondRegex, "");
        const {value} = event.target;
        setTextInput(value);
    };

    // useEffect(() => {
    //     setTextInput("default-text");
    // }, []);

    return (
        <TextField
            label={label}
            variant="filled"
            onChange={handleTextInputChange}
            value={textInput}
        />
    );
};

TextInput.propTypes = {
    textInput: PropTypes.string.isRequired,
    setTextInput: PropTypes.func.isRequired,
    label: PropTypes.string.isRequired,
};


export default TextInput;