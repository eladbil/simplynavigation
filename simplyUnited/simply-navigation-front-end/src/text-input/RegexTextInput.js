import {TextField} from "@mui/material";
import PropTypes from "prop-types";


const RegexTextInput = (props) => {

    const regex = props.regex;
    const onChange = props.onChange;
    // const textInput = props.textInput;
    const setTextInput = props.setTextInput;
    const label = props.label;

    const handleChange = (event) => {
        event.currentTarget.value = event.currentTarget.value.replace(regex, "");
        setTextInput(event.currentTarget.value);
        onChange(event)
    }

    return (
        <TextField
            onChange={handleChange}
            label={label}
            variant="filled"
        />
    );
}

RegexTextInput.propTypes = {
    onChange: PropTypes.func,
    regex: PropTypes.instanceOf(RegExp).isRequired,
    textInput: PropTypes.string.isRequired,
    setTextInput: PropTypes.func.isRequired,
    label: PropTypes.string.isRequired,
};

export default RegexTextInput;
