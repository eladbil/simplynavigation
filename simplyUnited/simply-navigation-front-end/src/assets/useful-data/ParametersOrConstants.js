export const DeviceSize = {
    mobile: 768,
    tablet: 992,
    laptop: 1324,
    desktop: 2024,
}

export const onlyNumbersRegex =  new RegExp("[^0-9]*$");
// export const basicSanitizationRegex = new RegExp("(\\/\\/)\\w*|(['\":{}])*$");
export const basicSanitizationRegex = new RegExp("(['\":{}])");
export const noCommentRegex = new RegExp("(\\/\\/)");
