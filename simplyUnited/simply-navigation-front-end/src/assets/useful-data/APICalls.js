

const jsonData= require('../../configFiles/configAll.json');
export const backend_domain = "https://"+jsonData.domain +":" + jsonData.portServerDbAdmin;

// export const bagickend_domain = "https://10.100.102.18:5000";

export const APIFetchURI = {
    GetListOfBuilding : backend_domain + "/admin/updateJsGetListOfBuilding",
    GetListOfCheckpoints: backend_domain + "/admin/updateJsGetListOfOuterV",
    CreateNewCheckpoint: backend_domain + "/admin/addJsAddOuter",
    UpdateCheckpointDetails: backend_domain + "/admin/updateJsUpdateOuter",
    DeleteCheckpoint: backend_domain + "/admin/delJsDeleteOuter",
    GetListOfAreas: backend_domain + "/admin/updateJsGetListOfAreas",
    GetAreaDetails: backend_domain + "/admin/updateJsGetAreaDetails",
    CreateNewArea: backend_domain + "/admin/addJsCreateNewInnerV",
    UpdateAreaDetails: backend_domain + "/admin/updateJsUpdateInner",
    DeleteArea: backend_domain + "/admin/delJsDeleteInner",
    CreateNewPath: backend_domain + "/admin/addJsCreatePath",
    UpdatePathDetails: backend_domain + "/admin/updateJsUpdatePath",
    DeletePath: backend_domain + "/admin/delJsDeleteEdge",
    GetPathDetails: backend_domain + "/admin/updateJsGetPathDetails",
    GetPathTypes: backend_domain + "/admin/addJsGetPathTypes",
    GetDirectionTypes: backend_domain + "/admin/addJsGetDirTypes",
    GetDirectionClassification: backend_domain + "/admin/addJsGetDirClassifications",
    SetSrcPath: backend_domain + "/admin/addJsSetSrc",
    SetDestPath: backend_domain + "/admin/addJsSetDest",
    DeleteSetSrcPath: backend_domain + "/admin/delJsSetSrc",
    DeleteSetDestPath: backend_domain + "/admin/delJsSetDest",
    GetBuildingNodesJSON: backend_domain + "/admin/drawEntireBuilding",
    GetQR: backend_domain + "/admin/generateQR",
    DrawEntireBuilding: backend_domain + "/admin/drawEntireBuilding",
    DrawCertainFloorInBuilding: backend_domain + "/admin/drawCertainFloorCertainBuiding",
    DrawAreaByName:backend_domain + "/admin/drawSpecificAreaByName",
    SignIn: backend_domain + "/admin/signin",
    Logout: backend_domain + "/admin/logout",
    Refresh: backend_domain + "/admin/refresh",
    isAdmin: backend_domain + "/admin/isAdmin",
    PasswordRecovery: backend_domain + "/admin/passwordRecovery",
    CreateUser: backend_domain + "/admin/createNewUser",
    AcceptUser: backend_domain + "/admin/acceptNewUser",
    ResetPassword: backend_domain + "/admin/resetPasswordWithCode",
};