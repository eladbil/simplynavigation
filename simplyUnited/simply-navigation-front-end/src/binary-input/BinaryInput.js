import PropTypes from "prop-types";
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Radio from '@mui/material/Radio';


const BinaryInput = (props) => {

    const isTrue = props.isTrue;
    const setIsTrue = props.setIsTrue;
    const trueLabel = props.trueLabel;
    const falseLabel = props.falseLabel;
    const choiceQuestion = props.choiceQuestion;

    const handleChoice = ({target}) => {
        setIsTrue(target.value)
    };

    // Doing the init here because for future change of values.
    const trueValue = true;
    const falseValue = false;


    return (
        <RadioGroup
            aria-labelledby="demo-radio-buttons-group-label"
            defaultValue={falseValue}
            onChange={handleChoice}
        >
            {choiceQuestion}
            <FormControlLabel value={trueValue} control={<Radio />} label={trueLabel} />
            <FormControlLabel value={falseValue} control={<Radio />} label={falseLabel} />
        </RadioGroup>
    );
};

BinaryInput.propType = {
    isTrue: PropTypes.bool.isRequired,
    setIsTrue: PropTypes.func.isRequired,
    trueLabel: PropTypes.string.isRequired,
    falseLabel: PropTypes.string.isRequired,
    choiceQuestion: PropTypes.string.isRequired,
};

export default BinaryInput;