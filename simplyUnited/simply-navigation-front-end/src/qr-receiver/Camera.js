import {useState} from 'react';


// import Camera from 'react-html5-camera-photo';
// import 'react-html5-camera-photo/build/css/index.css';
// import jsQR from "jsqr";
// To use Html5QrcodeScanner (more info below)
// import {Html5QrcodeScanner} from "html5-qrcode"

// To use Html5Qrcode (more info below)
import {Html5Qrcode} from "html5-qrcode"
// CommonJS require
// const jsQR = require("jsqr");


function QR_scanner () {
  const [QrMessage , setQRMessage] = useState("")
    
    // <input type="file" accept="image/*" capture>
    // <input type="file" accept="image/*" capture="user">
    // <input type="file" accept="image/*" capture="environment">

    function handle_camera(e){
       
        if (e.target.files.length === 0) {
            
            return;
          }
          else{
            const imageFile = e.target.files[0];

              
              const html5QrCode = new Html5Qrcode(/* element id */ "reader");
              html5QrCode.scanFile(imageFile,  false)
              .then(qrCodeMessage => {
                // console.log("qrCodeMessage");
                  setQRMessage(qrCodeMessage)
                // console.log(QrMessage);
                // console.log(typeof(qrCodeMessage))
                // document.getElementById("ee").innerHTML = qrCodeMessage;
            })
          }
      

    }
  return (
    <div>
        {QrMessage}
        
        <div id="reader"></div>
    <input type="file" accept="image/*" capture="environment" onChange={handle_camera}/>

    
    {/* <input type="file" style="display:none;" id="file" name="file"/> */}
    

    {/* <style> 
    input[type=button], input[type=submit], input[type=reset] {
    background-color: #04AA6D;
    border: none;
    color: white;
    padding: 16px 32px;
    text-decoration: none;
    margin: 4px 2px;
    cursor: pointer;
    }
    </style> */}



    </div>
   
  );
}

export default QR_scanner;