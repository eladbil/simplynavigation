import React from "react";
import {Button,Container} from "react-bootstrap"
import "bootstrap/dist/css/bootstrap.min.css";
import {useNavigate  } from 'react-router-dom';
import { Tooltip } from '@mui/material';
import { useTranslation } from 'react-i18next';



import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';

/**
 * 
 * @author Yedidya Bachar
 * @version 1.0.0
 * @date 27/5/2022
 * @description  Used as a jump to the search page (firstscreen) a language must be entered
 * @param {*} props 
 * lang - Language we work with
 * value -  value of button
 * @returns 
 * A button that clicks on it leads to the search page
 */
export default function LoginLayouToFirstScreen(props) {
  const { t ,i18n} = useTranslation();

  let navigate = useNavigate(); 
  const routeChange = () =>{    
    handleClose();
    
       // get a callback when the server responds
  let path = '../'+props.lang; 
  navigate(path);
    
  }

  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button variant="outlined" onClick={handleClickOpen}>
      {props.value}
      </Button>
      {/* If the button is pressed then a dialog is created with the user that checks if he definitely wants to leave the navigation */}
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        {/* title */}
        <DialogTitle id="alert-dialog-title">
          {t("Stop the current navigation?")}
        </DialogTitle>
        {/* content */}
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
          {t("Clicking OK will stop the existing navigation")}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button style={{margin:'2px'}} onClick={handleClose}>{t('Cancel')}</Button>{'  '}
          <Button style={{margin:'2px'}} color='info' onClick={routeChange} autoFocus>{'  '}
          {t('OK')}
          
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}




    