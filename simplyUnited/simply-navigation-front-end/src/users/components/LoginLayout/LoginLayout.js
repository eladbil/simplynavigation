import React,{useState} from "react";

import "bootstrap/dist/css/bootstrap.min.css";
import {useNavigate  } from 'react-router-dom';
// import AlertDismissibleExample from "../Alert/Alertdis";
import ScreenRouteElasticCarousele from '../RouteScreen/ScreenRouteElasticCarousele'
import {LogRouteFromIdToId} from '../../LogFunction/logInfo';
import {logErrorReq} from '../../LogFunction/logError';
import { useTranslation } from 'react-i18next';
import {Button} from '@mui/material';
import DialogTT from "../Dialog/DialogTT";

/**
 * 
 * @author Yedidya Bachar
 * @version 1.0.0
 * @date 27/5/2022
 * @description The purpose of this component is to send data to the server for the purpose of loading a route and moving to the route page with the data that will come from the server
 * It can be used both when loading data for the first time and when you want to update the route
 * 
 * @param {isRefresh,oldDef,srcDef,setcoursleAndCompass,url,lang,typeInfo} props 
 * url - url of db (neo4j)
 * destDef - Target point code
 * defObj - Target point code
 * chosenBuilding - The chosen building
 * chosenArea - The chosen area
 * chosenRoom - The chosen room 
 * lang - The language with which On works
 * typeInfo - What kind of information we want to send to the log is an update or a search
 * setcoursleAndCompass - function set carrousel
 * @returns A button that clicks on the track page with the correct data
 * If there is no loading then there is an error alert
 * 
 * note:
 * This component is written in a slightly spaghetti way and should be combined with additional login layouts in the project in a smarter way
 * 
 */
export default function LoginLayout(props) {
  const [openDialog, setOpenDialog] = React.useState(false);
  const [valueDialog, setValueDialog] = React.useState('');
  const [titleDialog, setTitleDialog] = React.useState('');
  
  const handleClickOpen = () => {
    setOpenDialog(true);
  };

  //translating
  const { t ,i18n} = useTranslation();
  //use state Release the components by changing their key (common in react) 
  //You should change and use useEffect
  const [toRef, setToRef] = useState(0);

  //show Alert
  const [showAlert, setShowAlert] = useState(false);
  //Object navigation between pages
  let navigate = useNavigate(); 
  /**
  * Auxiliary function when the button is pressed
  * Sends navigation data to server
  * Receives the information for display and sends the information to the navigation page
  * @returns 
  */
  const routeChange = () =>{
    //If there is no renewal at the source point then there is no need to update
    //Intended for update
    if(props.isRefresh==true && props.oldDef==props.srcDef){
     
      setTitleDialog(t('The requested route has already been displayed'));
      setValueDialog(t(''))
      handleClickOpen();
      return;
    }
    
    //The objects that send the information to the server should be changed to fetch js

    /*
    First request converts the search fields to Lenk
    A second request sends the source point and destination target to get a route
    A more efficient form of exercise can be thought of
    */
    var xhr2 = new XMLHttpRequest()
    xhr2.addEventListener('load', () => {
      let destDef = xhr2.responseText;
      // console.log(props.defObj.def+'&destDef='+destDef);
      if(destDef == '[]'){
      
        setTitleDialog(t('Error loading route please make sure all fields are entered correctly'));
        setValueDialog(t(''))
        handleClickOpen();
        return

      }
        var xhr = new XMLHttpRequest()
        // get a callback when the server responds
        xhr.addEventListener('load', () => { 
          // update the state of the component with the result here
          let lists = JSON.parse(xhr.responseText).resultRoute;
          if(Object.keys(lists).length==0){
            fetch(`${props.url}doesIdExist?def=${props.defObj.def.toUpperCase()}`).then(data=>{
             

                if(!data.redirected){
            
                  setTitleDialog(t("The code is not recognized"));
                  setValueDialog(<>{t('An error occurred while loading the route, the identification code may not have been entered correctly.')}
                  <br/>
                    {t('The identification code entered is:  ')}
                <strong>
                    {props.defObj.def}
                    </strong>
                    <br/>
                   {t('Please check that this is the code that appears on the sign.')}</>)
                  handleClickOpen();
              // alert(` 
              // ${t('.התרחשה שגיאה בטעינת המסלול, ייתכן כי קוד הזיהוי לא הוכנס במדוייק\n')}
              // ${t('קוד הזיהוי שהוכנס הינו')} 
              // ${props.defObj.def} 
              // ${t('אנא וודא כי זה הקוד המופיע על השלט')} `);
              
              
             
              }
              else{
                setTitleDialog(t('Unknown error'));
                setValueDialog(t(''))
                handleClickOpen();
              }
            })
            return;
          }
        
          if(props.isRefresh){
            // props.setcoursleAndCompass(<></>)
            setToRef(toRef+1)
            props.setcoursleAndCompass(<ScreenRouteElasticCarousele key={toRef+1} routes={lists}/>)
            LogRouteFromIdToId('Update' , {oldDef:props.oldDef , srcDef:props.srcDef, destDef:props.destDef})
          }
          
       
         
          let relativePath = '../SecondScreen'; 
          //go to seconscreen  page
         //and Moves the data to the next page
          navigate(relativePath,{state:{nn:lists,url:props.url,destDef:destDef, defObj:props.defObj, chosenBuilding:props.chosenBuilding ,chosenArea:props.chosenArea, chosenRoom:props.chosenRoom ,lang:props.lang, compass:'props' , typeInfo:props.typeInfo}});
        })

        
        if(props.defObj.def.toUpperCase() == destDef){
          
          setTitleDialog(t('Your location and destination are identified as the same place'));
          setValueDialog(t(''))
          handleClickOpen();
          return;
        }
        // alert(props.defObj.def.toUpperCase())
        let newUrl = props.url+props.lang+'/getRouteIdToId'+'?srcDef='+props.defObj.def.toUpperCase()+'&destDef='+destDef
        xhr.onerror = function() { logErrorReq(newUrl) };
        xhr.open('GET', newUrl)
        
      //   https://localhost:3000/chooseLang?srcDef=OC101&destDef=OC102
        
        xhr.send()

      })
      if(props.chosenBuilding==undefined){
        // alert(t('Select a building'));


        setTitleDialog(t('Select a building'));
                setValueDialog(t('Select a building'))
                handleClickOpen();
        return;
      }
      if(props.chosenArea==undefined){
        setTitleDialog(t('You must select an area'));
        setValueDialog(t(''))
        handleClickOpen();
        return;
      }
      if(props.defObj.def==undefined){
        setTitleDialog(t('An identification code must be entered'));
        setValueDialog(t(''))
        handleClickOpen();
      
        return;
      }
      
      let newUrl = props.url+'fromAreaGetId?nameBuilding='+props.chosenBuilding+'&nameRegion='+props.chosenArea;
      xhr2.onerror = function() { logErrorReq(newUrl, ) };
      xhr2.open('GET', newUrl);
      xhr2.send()


    }
    

    return (<>
      
          
                <Button
                // variant="outlined" 
                variant="contained" 
                // color="secondary"
                // color="inherit"
                // color="primary"
                style={{'marginRight':'5px' , 'marginLeft':'5px', color:'black' , backgroundColor:'green'}}
               
                 size="medium"
                   onClick={() =>{
                routeChange(); 
              }}>
               
              {props.value}
              </Button>
              {/* <AlertDismissibleExample show={showAlert} setShow={setShowAlert} title='שגיאה בטעינת המסלול' text='אנא וודא כי כל השדות הוכנסו כראוי'/> */}
              <DialogTT 
              title={titleDialog} value={valueDialog}
                setOpen={setOpenDialog} open={openDialog}>
                </DialogTT>
                

    
              </>
    );
  }
    