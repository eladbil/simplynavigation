import React from "react";
import {Container , Row} from 'react-bootstrap'
import './RouteStart.css'
import { useTranslation } from 'react-i18next';
// const { t} = useTranslation();
// const { t ,i18n} = useTranslation();
/**
 * 
 * @author Yedidya Bachar
 * @version 1.0.0
 * @date 28/5/2022
 * @description An object that will be displayed at the start of the track
 * @param {*} props 
 * @returns 
 */
export default function RouteStart(props){
    //translating
    const {t} = useTranslation();
    return (
        <div className="RouteStart">
        <Container
        
        >
            <Row><div className="RouteStart">{t("Start navigation")}</div></Row>
            <Row><div className="RouteStart">{t("Navigation to")}{props.dst} </div> </Row>
            <Row><div className="RouteStart">{t('Click in the direction of the arrow')}<br/>{t('nextArrow')}</div></Row>
            <Row className="RouteStartArrow"><div className="RouteStartArrow"></div></Row>
            {/* <img src={rightArrow} /> */}
        </Container>
        </div>
        
    );

}