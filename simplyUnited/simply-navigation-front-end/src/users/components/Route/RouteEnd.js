import React from "react";
import './RouteEnd.css'
import { useTranslation } from 'react-i18next';
/**
 * @author Yedidya Bachar
 * @version 1.0.0
 * @date 27/5/2022
 * @description An object that will be displayed at the end of the track
 * @returns 
 */
export default function RouteEnd(){
    const { t } = useTranslation();
    return (
        <div className="RouteEnd">
        <div  style={{textAlign: "center"}}>{t("You've arrived")}</div>
        </div>
    );

}