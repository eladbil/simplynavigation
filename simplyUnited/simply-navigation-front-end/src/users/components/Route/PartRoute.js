import React from "react";
import './PartRoute.css';
import { useTranslation } from 'react-i18next';
import hall from './icons/hall.png';
import elevator from './icons/elevator.png'


import {Card,ListGroup,Row} from "react-bootstrap"
/**
 * 
 * @author Yedidya Bachar
 * @version 1.0.0
 * @date 28/5/2022
 * @description Part of a route
* At the end should enter as part of an array into a carousel or other display
* Will change to Card later
 * @param {*} props - Fields for display
 * @returns Card of part of the navigation with navigation instructions
 */
export default function PartRoute(props){



    const {t} = useTranslation();
    // let convertToIcon=  {"מסדרון":"hall","לובי":"hall","מעלית":'elevator',"قاعه":"hall","ردهة":"hall","مصعد":'elevator',"коридор":"hall","вестибюль":"hall","Лифт":'elevator'}
    let convertToIcon=  {'Hallway':hall,'Lobby':hall, 'Elevator':elevator, "מסדרון":hall,"לובי":hall,"מעלית":elevator,"قاعه":hall,"ردهة":hall,"مصعد":elevator,"коридор":hall,"вестибюль":hall,"Лифт":elevator}
    
    //Checks whether it is a number of steps or a number of meters
    function typeDirection(distance , obj){
        if(convertToIcon[obj] ==elevator ){
            return distance/100 + ' '+t('floors');
        }
        else{
            return distance + ' ' +t('meter');
        }
    }
    return(
        <Card variant='info' style={{ 'backgroundColor':'#DDA0DD', width: '100rem' }}>
        <ListGroup   variant='info'>
    <ListGroup.Item className="colorNew">{t('You are in the area:')} {props.area}</ListGroup.Item>
    <ListGroup.Item className="colorNew">{t('go to:')} {props.nextDst}</ListGroup.Item>
    <ListGroup.Item className="colorNew">{t('The instructions:')} {props.freeText}</ListGroup.Item>
    <ListGroup.Item className="colorNew">
        <Row>
    
        <div className="colorNew" style={{width:'30%'}}>{typeDirection(props.distance , props.obj)} </div>
        <div className="colorNew" style={{width:'30%'}}>{t('Direction')}: {props.direction}</div>
         <Card.Img className="colorNew" style={{width:'30%' }} variant="top" src={convertToIcon[props.obj]} />
        </Row>
    </ListGroup.Item>
    </ListGroup>
    </Card>  
    
    );

}