import {Html5Qrcode} from "html5-qrcode"
import {logErrorBasic} from '../../LogFunction/logError'
/**
 * @author Yedidya Bachar
 * @version 1.0.0
 * @date 28/5/2022
 * @description
  * Qr object
  * Pressing the button opens a camera
  * After the photo is taken, the qr is analyzed and returns a string of what is written in it
  * @param {*} props 
  * parentCallback - Function of the parent
    After reading, the function will not work in the parent
    In our case we will send her the string of the qr
  * @returns  A type of button that presses the camera 
*/
function QR_scanner (props) {
    //An auxiliary function that is activated after the image is taken
    function handle_camera(e){
      try{
      //If there is no picture you can return
      //Otherwise we will send the image to decode and send the result to the parent using a function from the parent
        if (e.target.files.length === 0) {
      //  console.log(e);
            return;
        }
           else{
         
            const imageFile = e.target.files[0];
              
              const html5QrCode = new Html5Qrcode("reader");
              html5QrCode.scanFile(imageFile,  false)
              .then(qrCodeMessage => {
              
                props.parentCallback(qrCodeMessage);
                
                  //Reset the input
                  e.target.value = ''
            })
          }
        }catch(err){
          logErrorBasic(err,'QR');
        }
    }
  return (
    <>
        
        <input   id="files"  style={{ visibility: "hidden" }} type="file" accept="image/*" capture="environment" onChange={handle_camera}/>
        {/* Part of the scanner's requirements is to have this div */}
        <div id="reader" width="0px"></div>
    </>
   
  );
}

export default QR_scanner;