import React from 'react';
import QrCodeScannerIcon from '@mui/icons-material/QrCodeScanner';
import {Button} from '@mui/material';



/**
 * 
 * @author Yedidya Bachar
 * @version 1.0.0
 * @date 27/5/2022
 * @description A button which, when pressed, causes the camera to open for QR scanning
 * @returns Button with icon of QR
 */
export default function ButtonScanner(){
    return(
        <Button
        size="large"
       
        style={{'marginRight':'5px' , 'marginLeft':'5px' , backgroundColor:'green'}}
         variant="light"
          onClick={()=>{
            //Activation of the image upload input object
             document.getElementById("files").click();
           }}>
             {/* <Image  src={scan} /> */}
             <QrCodeScannerIcon fontSize="large"/>
             </Button>
    );
}

