import React from "react";
import {NavDropdown} from "react-bootstrap"
import '../../CSS/BackgroundScreens.css'

// import language from "./icons/language.jpg";
/**
 * @author Yedidya Bachar
 * @version 1.0.0
 * @date 27/5/2022
 * @parm 
 * {
 * img - Image of the button on the screen
 * }
 * @description Select a language and jump to the custom address
 * @returns Drop down list of language selection
 */
export default function LanguageChange(props){
    return(
      <>

 
{/* <img style={{backgroundColor:'yellow'}} src={language} ></img> */}
<NavDropdown menuVariant="dark" 
title={ 
  props.img
 

   }
 id="basic-nav-dropdown">
  {/* <Image style={{background:'black' ,width:'87%' ,height:'100%'}} thumbnail={true} rounded={true} src={language} /> */}
   <NavDropdown.Item href="/he">עברית</NavDropdown.Item>
   <NavDropdown.Divider />
   <NavDropdown.Item href="/ar">عربيه</NavDropdown.Item>
   <NavDropdown.Divider />
   <NavDropdown.Item href="/en">English</NavDropdown.Item>
   <NavDropdown.Divider />
   <NavDropdown.Item href="/ru">русский</NavDropdown.Item>
 </NavDropdown> 
 </>
  );
}

