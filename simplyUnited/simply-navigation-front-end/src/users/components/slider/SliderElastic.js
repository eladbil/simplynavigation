import React,{useState,useRef, useEffect} from "react";
import Carousel,{ consts } from 'react-elastic-carousel'
import Compass5 from "../Compass/Compass5";
import "./SliderElastic.css";
import {InputGroup} from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import { Button } from '@mui/material';
/**
 * 
 * @author Yedidya Bachar
 * @version 1.0.0
 * @date 29/5/2022
 * @description Holds the compass and turnstile of the track
  The connection between them is that the image in the background of the compass varies depending on the data of the route
 * @param {*} props 
  arrayData - Information about the route that entered the carousel as an array
 * @returns 
  Compass
  Carousel
  buttons next and prev
 */
export default function SliderElastic(props){
//trunslation
  const {t} = useTranslation();
  const useRef1 = useRef();

  // Set the carousel arrows themselves to zero
  // In css I reduced the arrows so that they would not be seen
  function myArrow({ type, onClick, isEdge }) {
    const pointer = type === consts.PREV ? '' : ''                           // '👈' : '👉'
    return (
      <button className="todel" onClick={onClick} disabled={isEdge}>
        {pointer}
      </button>
    )
  }
  

//Checks the screen orientation
  function isRTL(){ 
    if(document.documentElement.dir=="rtl"){
      return true;
    }
    return false
  }
// "Northwest"
// "Northeast"
// "Southeast"
// "Southwest"

  //Map of directions by languages
  //Check out the option to move it to the translation area
    const directions = { "север":'North',"юг":"South","Восток":"East","Запад":"West","северо-Запад":"Northwest","К северо-востоку":"Northeast","юго-восток":"Southeast","Юго-запад":"Southwest",
                         "في":'North',"ملف":"South","مرو":"East","عقل":"West","الشمال الغربي":"Northwest","الشمال الشرقي":"Northeast","الجنوب الشرقي":"Southeast","جنوب غرب":"Southwest",
                         "צפון":'North',"דרום":"South","מזרח":"East","מערב":"West","צפון-מערב":"Northwest","צפון-מזרח":"Northeast","דרום-מזרח":"Southeast","דרום-מערב":"Southwest",
                         "North":'North',"South":"South","East":"East","West":"West","Northwest":"Northwest","Northeast":"Northeast","Southeast":"Southeast","Southwest":"Southwest"
                        }
//Image is not defined when there is no direction
    const [backGround , setBackGround] = useState('undefind.png');
  
    const [Carousel1 , setCarousel1] = useState(
      //Carousel of the track
   <Carousel 
  //  className="colorCarousel"
   style={{'marginTop':'10px',
  //  'background': 'linear-gradient(140deg,#740ba3, #3d1662,#3d1662,#740ba3)'
  //  'backgroundColor':'#DDA0DD',
  }} ref={useRef1}
   renderArrow={myArrow}
      isRTL={isRTL()}initialActiveIndex={0} itemsToShow={1} onChange={(currentItem, pageIndex) =>       
          { 
              var d = directions[currentItem.item.direction]
              setBackGround(d+'.png');
           }   
        }>
          {props.arrayData}
      </Carousel>);

    return(<> 
    
     <div style={{ 'marginRight' : '10px','marginLeft' : '10px' ,
    //  backgroundColor:'black',
    display: "flex" }}>
     
       <Button
        // style={{width:'30%'}}

        //  variant="outlined" 
        //  variant="contained" 
        //  color="secondary"
        //  color="inherit"
         color="primary"
         style={{
           width:'30%',
         'marginRight':'5px' ,
          'marginLeft':'5px',
          'fontSize': '100%',
          //  color:'black' ,
          //  backgroundColor:'green'
          }}
        
          size="medium"
         onClick={() =>  useRef1.current.slidePrev()}>{t('prev')}<br/>{t('prevArrow')}</Button>
    <Compass5 style={{ width:'40%'}} changeBackground={setBackGround} backgroundurl={backGround} />
    <Button 
    // style={{width:'30%'}}

        //  variant="outlined" 
        // variant="contained" 
        //  color="secondary"
        //  color="inherit"
         color="primary"
         style={{
           width:'30%',
         'marginRight':'5px' ,
          'marginLeft':'5px',
          'fontSize': '100%',
          //  color:'black' ,
          //  backgroundColor:'green'
          }}
        
          size="medium"
    onClick={() =>  useRef1.current.slideNext()}>{t('next')}<br/>{t('nextArrow')}</Button> 
    </div>
   {Carousel1}  
   
        
    </>);
}