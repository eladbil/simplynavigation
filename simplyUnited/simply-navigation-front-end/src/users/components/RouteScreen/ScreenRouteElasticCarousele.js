import React from "react";
import RouteStart from "../Route/RouteStart";
import RouteEnd from "../Route/RouteEnd";
import PartRoute from "../Route/PartRoute";
import SliderElastic from "../slider/SliderElastic";
/**
 * 
 * @author Yedidya Bachar
 * @version 1.0.0
 * @date 28/5/2022
 * @description Slider screen - Receives the information about a segment in the route and builds an array of cards from it, a card for each segment
 * @param {*} props 
 routes - Route information
 * @returns 
 */
export default function ScreenRouteElasticCarousele(props){
    let arrayData = []
     /**
     * Creating route components
     * @param {*} arrayData - An array of track information
     * @returns 
   
     SliderElastic - componenet of slider
     */
function createRoute(arrayData){
//  alert(props.routes.at(-1).name);
  let key=0;
    arrayData.push(<RouteStart key={key} dst={props.routes.at(-1).name}/>)
    key+=1;
   
    for (let index = 0; index < props.routes.length; index++) {
      
      for (let index2= 0; index2 < props.routes[index].directions.length; index2++) {
       
          var nextDst='';
          if(index2==props.routes[index].directions.length-1 && index ==props.routes.length-1 ){
            nextDst=props.routes.at(-1).name;
          }
          else if(index2==props.routes[index].directions.length-1){
            nextDst=props.routes[index+1].name;
          }
          else{
            nextDst = props.routes[index].name;
          }
          arrayData.push(<PartRoute key={key} nextDst={nextDst} area={props.routes[index].name }  freeText={props.routes[index].directions[index2][3]} distance={props.routes[index].directions[index2][2]} direction={props.routes[index].directions[index2][0]} obj={props.routes[index].directions[index2][1]}/>);       
          key+=1;
        }   
  }
    arrayData.push(<RouteEnd key={key}/>)

    return arrayData;
  }
  createRoute(arrayData);



    
    return(<>   
     <SliderElastic arrayData={arrayData} />
    </>);

    
}