import ReactStars from "react-rating-stars-component";
import React from "react";

 /**
  * @author Yedidya Bachar
  * @version 1.0.0
  * @date 27/5/2022
  * @description The rating component within the feedback components
  * @param {*} props 
  * {
  * setUpdating -  Updates the rating number at the parent component
  * }
  * @returns 
  */
export default function RatingChanged(props){


    const ratingChanged = (newRating) => {
        props.setUpdating(newRating);
       
      };
       
      return(
        <ReactStars
          count={5}
          onChange={ratingChanged}
          size={50}
          activeColor="#ffd700"

          isHalf={false}
          // emptyIcon={<i className="far fa-star"></i>}
          // halfIcon={<i className="fa fa-star-half-alt"></i>}
          // fullIcon={<i className="fa fa-star"></i>}
        />
       );
       
      
}
document.getElementById("where-to-render")