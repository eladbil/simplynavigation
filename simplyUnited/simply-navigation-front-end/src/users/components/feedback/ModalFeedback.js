import React,{useState,useRef} from 'react';
import {Modal,Button} from 'react-bootstrap';
import ContentFeedback from './ContentFeedback';
import { useTranslation } from 'react-i18next';
import '../../CSS/imgText.css'
import '../../CSS/BackgroundScreens.css'

// import { Button } from '@mui/material';
/**
 * 
 * @author Yedidya Bachar
 * @version 1.0.0
 * @date 27/5/2022
 * @description The feedback pop-up box
Inside the box is a content component
 * @returns 
 * A pop-up screen of feedback with content
 */
export default function ModalFeedback(props){
  //function trunslation
  const {t} = useTranslation();

  //call function of son
  const useRef1 = useRef();
  // set show of pop up window feedback
    const [show, setShow] = useState(false);

  //function Raises the pop-up screen
  const handleShow = () => setShow(true);
  //Closes the pop-up screen
  const handleClose = () => setShow(false);
  //send feedback to server log
  const handleSend = ()=>{
    //Enables the function of sending content components son
    useRef1.current.feedbackSend1();
    setShow(false)
  }
  return (
    <>
 
<>
        <Button onClick={handleShow} variant="white" 
                >{props.value}
                 
                {/* feedback<RateReviewIcon /> */}
                </Button>  
                 
     </>

 
      <Modal  show={show}
       onHide={handleClose}
        >
        {/* <Modal.Header closeButton> */}
        {/* Modal - title,body */}
        <Modal.Header>
          <Modal.Title>{t('feedback')}</Modal.Title>
        </Modal.Header>
        {/* content of feedback in another component */}
        <Modal.Body>
          <ContentFeedback ref={useRef1}></ContentFeedback>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
          {t('Close')} 
          </Button>
          <Button variant="primary" onClick={handleSend}>
          {t('Send')}
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}



