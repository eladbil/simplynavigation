import React,{useState,useEffect,useImperativeHandle,forwardRef} from 'react';
import {Button,Form,InputGroup,FormControl} from 'react-bootstrap';
import RatingChanged from './RatingChanged ';
import {feedbackSend} from '../../LogFunction/logInfo';
import { t } from 'i18next';


/**
 * @author Yedidya Bachar
 * @version 1.0.0
 * @date 27/5/2022
 * @description Contains the content of the feedback
It has 4 sections of options to fill detailed below.

 * @param {*} props 
 * @param {*} ref 
 * Allows you to call an internal function from a parent component
 * @returns 
 * The contents of the feedback
  * The fields you want to fill
 */
function ContentFeedback(props ,ref){
let radio = 'radio';
const [ratingRoute , setRatingRoute] = useState(null);
const [isFinishRoute , setIsFinishRoute]= useState(null);
const [isBack , setIsBack]= useState(null);
const [feedbackText , setFeedbackText] = useState(null);
useEffect(()=>{
 
},[ratingRoute,isFinishRoute,isBack,feedbackText])

useImperativeHandle(ref,() => ({
  feedbackSend1() {
    feedbackSend(ratingRoute,isFinishRoute,isBack,feedbackText);
  },
}))
    return(
        <Form >

  {/*part 1 -  User experience rating from 5 stars */}
    <Form.Label>{t('Rate the navigation experience')}</Form.Label>
    <RatingChanged setUpdating={setRatingRoute}></RatingChanged>

{/*part 2 -  Question whether the user will return to using the navigation system
Answer Yes or No */}
  <Form.Label>{t('Would you use the navigation system again if necessary?')}</Form.Label>
  <div key={`isFinish-${radio}`} className="mb-3">

  <Form.Check
  onChange={e=>{
    setIsFinishRoute(1);
  }}
  // on
    inline
    label={t('Yes')}
    name="group3"
    type={radio}
    id={`inline-${radio}-1`}
  />
  <Form.Check
   onChange={e=>{
    setIsFinishRoute(0);
  }}
    inline
    label={t('No')}
    name="group3"
    type={radio}
    id={`inline-${radio}-2`}
  />
  </div>
{/*part 3 -  Question whether the user has completed the route
Answer Yes or No */}
  <Form.Label>{t('Have you completed the route?')}</Form.Label>
  <div key={`isBack-${radio}`} className="mb-3">

  <Form.Check
  onChange={e=>{
    setIsBack(1);
  }}
  
    inline
    label={t('Yes')}
    name="group2"
    type={radio}
    id={`inline-${radio}-1`}
  />
  <Form.Check
  onChange={e=>{
    setIsBack(0);
  }}
    inline
    label={t('No')}
    name="group2"
    type={radio}
    id={`inline-${radio}-2`}
  />

  </div>
  
{/*part 4 -  Writing feedback in words */}
  <p> {t('We will be happy to receive short feedback in words from you')}</p>
  <InputGroup size="sm" className="mb-3">
    <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm" onChange={e=>{setFeedbackText(e.target.value)}} />
  </InputGroup>

</Form>

    );
}
export default forwardRef(ContentFeedback)