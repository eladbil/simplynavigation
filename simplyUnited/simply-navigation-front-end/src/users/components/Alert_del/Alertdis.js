import React,{useState} from "react";
import {Alert} from 'react-bootstrap';

/**
 * @author Yedidya Bachar
 * @version 1.0.0
 * @date 27/5/2022
 * 
 * @componentName AlertDismissibleExample return 
 * @param {*} props
 * setShow  - Which defines the visibility of the alert
 * @returns Alert of danger if show == true else return <></>
 */
export default function AlertDismissibleExample(props) {
    const [show, setShow] = useState(true);
  
    if (props.show) {
      return (
        <Alert variant="danger" onClose={() => props.setShow(false)} dismissible>
          <Alert.Heading>{props.title}</Alert.Heading>
          <p>
            {props.text}
          </p>
        </Alert>
      );
    }
    return (<></>);
    // return <Button onClick={() => props.setShow(true)}>Show Alert</Button>;
  }
  
