import React,{useEffect, useState} from "react";
import {useLocation} from 'react-router-dom';

import QR_scanner from '../QR/QR_scanner';
import { getAllUrlParams } from "../../functionHandle/FunctionUrl";
import {Image, Form,Alert,Container,ToggleButtonGroup} from "react-bootstrap"
import feedbackImg from '../feedback/feedback.png'
import '../ScreensSearch/SecondScreen.css'
import LoginLayouToFirstScreen from "../LoginLayout/LoginLayouToFirstScreen";
import WolfsonIcon from '../WolfsonButton/wolfson128.png'
import ScreenRouteElasticCarousele from "../RouteScreen/ScreenRouteElasticCarousele";
import InstNav from "../Instructions/InstNav";
import './RouteIdToId.css'
import { useTranslation } from 'react-i18next';
import LoginLayoutIdToId from "./LoginLayoutIdToId";
import {LogRouteFromIdToId} from '../../LogFunction/logInfo'
import ModalFeedback from "../feedback/ModalFeedback";
import returnImg from "../ScreensSearch/searchroute.png";
import ButtonScanner from "../QR/ButtonScanner";
import {Button,Stack  ,Divider   } from '@mui/material';
import CachedSharpIcon from '@mui/icons-material/CachedSharp';
import '../../CSS/BackgroundScreens.css'

/**
 
 * @author Yedidya Bachar
 * @version 1.0.0
 * @date 28/5/2022
 * @description Shows the route from point to point
 * Very similar to the secondscreen component
 * @param {*} props 
 * @returns 
 */
export default function RouteIdToId(props){
  const [toRef, setToRef] = useState(0);
   //translating
    const { t ,i18n} = useTranslation();
    
  //Get data from the previous page
    const location = useLocation();
    const [defObj2, setdef2] = useState({def:location.state.srcDef });
 
    //funcion update def and src from qr 
  const updateDefSrcQR = (childData) =>{
    try{
  
     let childDataString = String(childData)

    setdef2({def:String(getAllUrlParams(childDataString).id.toUpperCase())})
  
    }
    catch{
      alert(t('The picture is not good'))
     
    }
     // set_QR_content(childData)
 }
    //get list of route
    let pp = {
        routes : location.state.nn
        
        }

    const [lists] = useState(pp.routes);
    //coursleAndCompass obj
    let [coursleAndCompass,setcoursleAndCompass] = useState(<ScreenRouteElasticCarousele routes={pp.routes}/>)
    useEffect(() => {
      
      //log
      LogRouteFromIdToId(location.state.typeInfo ,  { srcDef: location.state.srcDef, destDef: location.state.destDef })
      document.documentElement.dir = props.heEn;
    //change Language
    //You should also add a side change
    //There may be a bug
      i18n.changeLanguage(props.lang);
  
      if(lists){
        setToRef(toRef+1)
      setcoursleAndCompass(<ScreenRouteElasticCarousele key={toRef+1} routes={lists}/>)
      }
    },[lists])
    const newNav = (<QR_scanner  height='50%' width='50%' parentCallback = {updateDefSrcQR} text={t('Take a barcode')}/>);
    let imgRef =  <CachedSharpIcon fontSize="large"/> // <Image rounded={true} style={{ height:'100%' , width:'100%'}} src={refreshImg} />;
    // const newNav = (<QR_scanner parentCallback = {updateDefSrcQR} text={t('Take a barcode')}/>);
    
    //nned to design
    const defSrc = (

    <Form.Group className="mb-3" controlId="formAreaID">
    <Form.Label>{t('Please enter an identification code')}</Form.Label>
  
    <div style={{ display: "flex" }}>
  
    
      <div style={{height:'33%' , width:'20%'}}>
        {/* button img scan and start scan */}
     
      <ButtonScanner/>
      </div>
      <div style={{height:'100%' , width:'60%'}}>
      <Form.Control size='lg'  style={{height:'100%' , width:'100%'}} type="text" placeholder={t('Please enter an identification code')} value={defObj2.def} onChange={e => {
          const newDefObj = { def: e.target.value };
         setdef2(newDefObj); // Now it works
      }}>
      </Form.Control>
      </div>
      <div style={{height:'100%' , width:'20%'}}>
        {/* button load new route */}
      <LoginLayoutIdToId setcoursleAndCompass={setcoursleAndCompass} className="defSrc" typeInfo='Update' isRefresh={true} value={imgRef} url={location.state.url} oldDef={location.state.srcDef} srcDef={defObj2.def} destDef={location.state.destDef} setdef={location.state.setdef2} lang={ location.state.lang} path={location.state.path}></LoginLayoutIdToId>
       {/* <LoginLayout oldDef={location.state.defObj.def} srcDef={defObj2.def}  destDef={location.state.destDef} setcoursleAndCompass={setcoursleAndCompass} typeInfo='RouteUpdate' isRefresh={true} className="defSrc" value={imgRef} url={location.state.url} defObj={defObj2} chosenBuilding={location.state.chosenBuilding} chosenArea={location.state.chosenArea} chosenRoom={location.state.chosenRoom} setdef={location.state.setdef2} lang={ location.state.lang}></LoginLayout> */}
      </div>
      </div>
      {/* </InputGroup> */}
    </Form.Group>
  );
    return (
          <div className="mainbackgroundColor">
         <div style={{ display: "flex" }}>

         <Stack 
        //  className="mainbackgroundColor"
// style={{'max-height': '100px'}}
direction="row"
  divider={<Divider orientation="vertical" flexItem />}
  spacing={2}
  >
<div 
className="shadowBtn"
 style={{width:'33%', maxHeight:'128px'}}
// style={{width:'35%' ,height:'100%'}}
>
  {/* button of search route */}
<LoginLayouToFirstScreen  value={<><Image 
style={{maxHeight:'128px'}} 
// className="mainbackgroundColor"
className="imgbtn" 
 thumbnail={true} rounded={true} src={returnImg}></Image>
{/* {t("return")} */}
{t('New route')}

</>} lang={ location.state.lang}/>
   
        </div>


{/* button of wolfson */}
  
  
<div 
className="shadowBtn"
      style={{width:'33%', maxHeight:'128px'}}
      // style={{maxHeight:'128px'}} 
      >
        
        <ModalFeedback
        value= {<><Image
          className="imgbtn"  
        style={{maxHeight:'128px'}} 
       // style={{height:'128px' , width:'128px'}}
        // className="mainbackgroundColor"
         thumbnail={true} rounded={true} src={feedbackImg}></Image>{t('feedback')}</>}
        ></ModalFeedback>
        </div>
        <div  
        className="shadowBtn"
        style={{width:'35%', maxHeight:'128px'}}
// style={{width:'35%' ,height:'100%'}}
>
  
  <Button 
  
  // style={{width:'100%' ,height:'100%'}} 
  variant="white"  >
    <Image  
    className="imgbtn"
    // className="mainbackgroundColor" 
    //  style={}} 
    style={{maxHeight:'128px'}}
    thumbnail={true} rounded={true} 
    // src='https://www.gov.il/BlobFolder/office/wolfson/he/WOLFSON4.png'
    src = {WolfsonIcon}
    // src = './refresh.png'
    ></Image></Button>
  </div>
        </Stack>
        </div>   
        <br/> 
        {/* list butoons top                                                                                                                                                                                         location.state.srcDef */}
            {defSrc}
         {/* coursle And Compass */}
         {coursleAndCompass}
            {/* instruction of navigation */}
        <InstNav></InstNav>
        {newNav}
            </div>
    )

 
}