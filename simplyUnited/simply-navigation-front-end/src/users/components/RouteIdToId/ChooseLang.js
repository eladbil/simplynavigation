
import React,{useState,useEffect} from "react";

import { loadArgFromCurrentUrl } from "../../functionHandle/FunctionUrl";

import {Button} from 'react-bootstrap';
import LoginLayoutIdToId from "./LoginLayoutIdToId";
import { useTranslation } from 'react-i18next';
import {logChangeLang} from '../../LogFunction/logInfo'
import '../../CSS/BackgroundScreens.css'
import './ChooseLang.css';
/**
 * @author Yedidya Bachar
 * @version 1.0.0
 * @date 28/5/2022
 * @description 
 * Designed for language selection before navigating to the next page
 * Accessing this page requires data in the url to send navigation data to the server later
 * @param {*} props 
 * lang - old lang 
 * url - The database address (neo4j)
 * @returns 
 * Buttons for selecting languages
 */
function ChooseLang(props) {
        //translating
    const {i18n} = useTranslation();
        //url db 
    const [url] = useState(props.url)
    
    //Start code and destination code
    const [srcDef, setSrcDef] = useState('');
    const [destDef, setDestDef] = useState(''); 
    
    //Checks if there is the information in the url
    const [loading, setLoading] = useState(false);
   
    //set lang
    const [lang, setLang] = useState(props.lang);

    useEffect(() => {
        const loadPost = async () => {
  
            // Till the data is fetch using API 
            // the Loading page will show.
            setLoading(true);
  
            // Await make wait until that 
            // promise settles and return its result
            const srcdef1 = await loadArgFromCurrentUrl('srcdef');
            const destDef1 = await loadArgFromCurrentUrl('destdef');
         
            // srcDef=y&destDef=2
            if(srcdef1==undefined | !destDef1==undefined){
                
                setLoading(false);
                // setLang(<RestartScreen></RestartScreen>)
                return

            }
            setSrcDef(srcdef1);
            setDestDef(destDef1);
            // Closed the loading page
            setLoading(false);
        }
  
        // Call the function
        loadPost();
        
    }, []);
  
    return (
        <div className="mainbackgroundColor">
           
                {loading ? (
                    <h4>Loading...</h4>) :
                    <>
                    {/* {lang} */}

                    <br></br> <br></br> 
                    <br></br> <br></br> 
                 
                   <Button variant="primary" className="btnChooseLang" onClick={ () => {
                        setLang('he');
                        i18n.changeLanguage('he');
                        logChangeLang('he');
                      
                    }}>עברית</Button>{' '}
                   <br></br> <br></br> 
                    <Button variant="primary" className="btnChooseLang" onClick={ () => {
                        setLang('en')
                        i18n.changeLanguage('en');
                        logChangeLang('en');
                      
                    }}>English</Button>{' '}
                    <br></br> <br></br> 
                    <Button variant="primary" className="btnChooseLang" onClick={ () => {
                        setLang('ar')
                        i18n.changeLanguage('ar');
                        logChangeLang('ar');
                      
                    }}>عربيه</Button>{' '}
                    <br></br> <br></br> 
                    <Button variant="primary" className="btnChooseLang" onClick={ () => {
                        setLang('ru');
                        i18n.changeLanguage('ru');
                        logChangeLang('ru');
                      
                    }}>Русский</Button>{'\n '}
                    <br></br> <br></br> 
                    
                    <LoginLayoutIdToId  typeInfo='RouteFromIdToId' isRefresh={false} value='loadRoute' path='/getRouteIdToId' lang ={lang} url ={url} srcDef={srcDef} destDef = {destDef}></LoginLayoutIdToId>                   
                  
                   
                    </>
}
          
                
        </div>
    );
}
export default ChooseLang;