import React,{useState,useEffect} from "react";
import {Button} from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import {useNavigate  } from 'react-router-dom';
import {LogRouteFromIdToId} from '../../LogFunction/logInfo';
import ScreenRouteElasticCarousele from '../RouteScreen/ScreenRouteElasticCarousele';
import { logErrorReq } from "../../LogFunction/logError";
import { useTranslation } from 'react-i18next';
import DialogTT from "../Dialog/DialogTT";
// import AlertDismissibleExample from "./Alertdis";
// const url = 'http://localhost';

/**
 * 
 * @author Yedidya Bachar
 * @version 1.0.0
 * @date 28/5/2022
 * @description Button to switch path based on id code to id code
  * Can be used either directly via url or by updating
  * Very similar to the component of loginlayout should be considered for one of them
 * @param {*} props 
 * 
 * Copied from login layout may not be accurate
 * url - url of db (neo4j)
 * destDef - Target point code
 * defObj - Target point code
 * chosenBuilding - The chosen building
 * chosenArea - The chosen area
 * chosenRoom - The chosen room 
 * lang - The language with which On works
 * typeInfo - What kind of information we want to send to the log is an update or a search
 * setcoursleAndCompass - function set carrousel
 * @returns A button that clicks on the track page with the correct data
 * If there is no loading then there is an error alert
 * 
 */
export default function LoginLayoutIdToId(props) {

  const [openDialog, setOpenDialog] = React.useState(false);
  const [valueDialog, setValueDialog] = React.useState('');
  const [titleDialog, setTitleDialog] = React.useState('');
  
  const handleClickOpen = () => {
    setOpenDialog(true);
  };
   //translating
  const { t } = useTranslation();
  const [showAlert, setShowAlert] = useState(false);
  const [lists, setLists] = useState(props.lists);
   //use state Release the components by changing their key (common in react) 
  //You should change and use useEffect
  const [toRef, setToRef] = useState(0);
  useEffect(()=>{
  //refresh coursleAndCompass
    if(lists){
      setToRef(toRef+1)
    props.setcoursleAndCompass(<ScreenRouteElasticCarousele key={toRef+1} routes={lists}/>)
    }
  },[lists])
  
  
  
    //go to seconscreen  page
    //and Moves the data to the next page
    let navigate = useNavigate(); 
    const routeChange = () =>{
      if(props.srcDef.toUpperCase() == props.destDef){
        setTitleDialog(t('Your location and destination are identified as the same place' ));
        setValueDialog('');
        handleClickOpen();
        return;
      }
      if(props.oldDef==props.srcDef){
        setTitleDialog(t('The requested route has already been displayed'));
        setValueDialog('');
        handleClickOpen();
        return;
      }
      // alert(props.srcDef)
      if(props.srcDef==undefined || props.srcDef==''){
        setTitleDialog(t('An identification code must be entered'));
        setValueDialog(t('In order to find a new route, a new identification code must be entered'))
        handleClickOpen();
      
        return;
      }
      
    
      /**
       *  request sends the source point and destination target to get a route
       *        */
      
      var xhr = new XMLHttpRequest()
  
      // get a callback when the server responds
      xhr.addEventListener('load', () => {
      
       
        // update the state of the component with the result here
        let lists = JSON.parse(xhr.responseText).resultRoute;
        
      
        if(Object.keys(lists).length==0){
          // alert(`${props.url}doesIdExist?def=${props.srcDef.toUpperCase()}`)
          fetch(`${props.url}doesIdExist?def=${props.srcDef.toUpperCase()}`).then(data=>{
          
            if(!data.redirected){
            
              setTitleDialog(t("The code is not recognized"));
              setValueDialog(<>{t('An error occurred while loading the route, the identification code may not have been entered correctly.')}
              <br/>
                {t('The identification code entered is:  ')}
            <strong>
                {props.srcDef}
                </strong>
                <br/>
               {t('Please check that this is the code that appears on the sign.')}</>)
               
              handleClickOpen();
          
             
        }
        else{
          setTitleDialog(t('Unknown error'));
          setValueDialog(t('Unknown error'))
          handleClickOpen();
         
        }
        
        
      
      })
      return;
    }
      // else{
     
        if(props.isRefresh){
         
          setLists(lists);
        //log
          LogRouteFromIdToId('Update' , {oldDef:props.oldDef ,srcDef:props.srcDef, destDef:props.destDef})
         }
       
        
       

        // {"resultRoute":[{"name":"אורתופדיה","directions":[["מזרח","מסדרון","15","לך 15 מטר מזרחה במסדרון"]]},{"name":"אורתופדיה,ילדים,מתבגרים,סטומה","directions":[["מזרח","לובי","6","לך מזרחה בלובי 6 מטר,עבור את הקבלה"]]},{"name":"מעלית גסטרואנטרולוגיה","directions":[["למעלה","מעלית","100","עלה במעלית קומה אחת קומות לקומה 2"]]},{"name":"מעלית בית מרקחת ציטוטוקסיקה","directions":[["מערב","לובי","10","מערבה 10 מטר בלובי"],["מערב","מסדרון","1","כנס למסדרון"]]}]}
        
        let relativePath = '/'+props.lang+props.path; 
     
        navigate(relativePath,{state:{nn:lists,url:props.url,lang:props.lang ,srcDef:props.srcDef, destDef:props.destDef, path:props.path, typeInfo:props.typeInfo}});
        // }
      })
      // alert(props.destDef,props.srcDef)
  
      let newUrl = props.url+props.lang+props.path+'?srcDef='+props.srcDef+'&destDef='+props.destDef;
      xhr.onerror = function() { logErrorReq(newUrl) };
      xhr.open('GET', newUrl);
      
      xhr.send()
    }
    return (<>
      
         
             

                <Button
                // variant="outlined" 
                variant="contained" 
                // color="secondary"
                // color="inherit"
                // color="primary"
                style={{'marginRight':'5px' , 'marginLeft':'5px', color:'black' , backgroundColor:'green'}}
               
                 size="medium"
                   onClick={() =>{
                routeChange(); 
              }}>
               
              {props.value}
              </Button>
              <DialogTT 
              title={titleDialog} value={valueDialog}
                setOpen={setOpenDialog} open={openDialog}>
                </DialogTT>
              </>
    );
  }
    