import React from 'react'
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { useTranslation } from 'react-i18next';
import {Button} from '@mui/material';
import './DialogTT.css'


/**
 * @author Yedidya Bachar,

 * @version 1.0.1
 * @date 09/06/2022
 * @param {*} props
 * open - Boolean Whether to open or close.
 * value - The content of the dialogue.
 * title - The title of the dialogue.
 * @description
 * Creating a dialogue with the user
 * The component gets a title and content
 * Returns a component that pops up on the screen and notifies the user
 * @returns 
 */
export default function DialogTT(props){

   const {t} = useTranslation();
    const handleClose = () => {
      try{

      props.setOpen(false);
      }catch(e){}
   
    };

return(

  <Dialog
  open={props.open}
  onClose={handleClose}
  aria-labelledby="alert-dialog-title"
  aria-describedby="alert-dialog-description"
>
  {/* title of dialog */}
  <DialogTitle id="alert-dialog-title">
    {props.title}
  </DialogTitle>
  {/* content of dialog */}
  <DialogContent>
    <DialogContentText id="alert-dialog-description">{props.value}
    </DialogContentText>
  </DialogContent>

  {/* button to close dialog */}
  <DialogActions>
    <Button className="okbtndialog" 
    color='info'
     onClick={handleClose} autoFocus>{'  '}
    {t('OK')}
    </Button>
    
  </DialogActions>
</Dialog>
);
}