import React,{ useEffect, useState} from "react";
import {useLocation} from 'react-router-dom';
import QR_scanner from '../QR/QR_scanner';
import { getAllUrlParams } from "../../functionHandle/FunctionUrl";
import {Form  } from "react-bootstrap"
import LoginLayout from "../LoginLayout/LoginLayout";
// import './SecondScreen.css'
import LoginLayouToFirstScreen from "../LoginLayout/LoginLayouToFirstScreen";
import ScreenRouteElasticCarousele from "../RouteScreen/ScreenRouteElasticCarousele";
import { useTranslation } from 'react-i18next';
import {LogRouteFromSearch} from '../../LogFunction/logInfo'
import InstNav from "../Instructions/InstNav";

import { Stack  ,Divider, Button} from '@mui/material';
// import CachedTwoToneIcon from '@mui/icons-material/CachedTwoTone';
import CachedSharpIcon from '@mui/icons-material/CachedSharp';
import Image from 'react-bootstrap/Image'
import WolfsonIcon from '../WolfsonButton/wolfson128.png'

import returnImg from "./searchroute.png";
import ModalFeedback from "../feedback/ModalFeedback";
import ButtonScanner from "../QR/ButtonScanner";

import '../../CSS/BackgroundScreens.css'
import { checkRtlLtr } from "../../functionHandle/rtlLtr";
import feedbackImg from '../feedback/feedback.png'
/**
 * 
 * @author Yedidya Bachar
 * @version 1.0.0
 * @date 28/5/2022
 * @description A screen that shows the navigation route
 * @returns 
 */
export default function SecondScreen(){
  // translating
  const { t ,i18n} = useTranslation();

  //location -  Get information from the previous page
    const location = useLocation();

    //set new src codem point
    const [defObj2, setdef2] = useState({def:'' });
   
   
    useEffect(() => {
      document.documentElement.dir = checkRtlLtr(location.state.lang)
     //set lang
      i18n.changeLanguage(location.state.lang);
      //log 
      LogRouteFromSearch(location.state.typeInfo, {srcId: location.state.defObj.def, dstId:location.state.destDef})
     
     }, []);
    
    // const [defObj, setdef] = useState({def: '' });
    //function of update information of src id after get string from scan
  const updateDefSrcQR = (childData) =>{
    try{
     let childDataString = String(childData)
    setdef2({def:String(getAllUrlParams(childDataString).id.toUpperCase())})
  
    }
    catch{
      alert(t('The picture is not good'))
     
    }
     // set_QR_content(childData)
 }
//  Information Requests information about the route
    let pp = {
        routes : location.state.nn
        
        }
    let [coursleAndCompass,setcoursleAndCompass] = useState(<ScreenRouteElasticCarousele routes={pp.routes}/>)
   

    //scan
    const newNav = (<QR_scanner   parentCallback = {updateDefSrcQR} text={t('Take a barcode')}/>);
  
    //img of refresh route
    let imgRef = <CachedSharpIcon fontSize="large"/> //<Image rounded={true} style={{ height:'100%' , width:'100%'}} src={refreshImg} />;
   

    //Manually fill in a code with a scan button
    const defSrc = (
    <Form.Group className="mb-3" controlId="formAreaID">
  <Form.Label>{t('Please enter an identification code')}</Form.Label>
 
  <div style={{ display: "flex" }}>
 
  
    <div style={{height:'33%' , width:'20%'}}>
      {/* button scan */}
 
      <ButtonScanner/>
    </div>
    {/* Manually fill in code */}
    <div style={{height:'100%' , width:'60%'}}>
    <Form.Control size='lg'  style={{height:'100%' , width:'100%'}} type="text" placeholder={t('Please enter an identification code')} value={defObj2.def} onChange={e => {
        const newDefObj = { def: e.target.value };
       setdef2(newDefObj); // Now it works
    }}>
    </Form.Control>
    </div>
    <div style={{height:'100%' , width:'20%'}}>
   {/* button refresh */}
     <LoginLayout oldDef={location.state.defObj.def} srcDef={defObj2.def}  destDef={location.state.destDef} setcoursleAndCompass={setcoursleAndCompass} typeInfo='RouteUpdate' isRefresh={true} className="defSrc" value={imgRef} url={location.state.url} defObj={defObj2} chosenBuilding={location.state.chosenBuilding} chosenArea={location.state.chosenArea} chosenRoom={location.state.chosenRoom} setdef={location.state.setdef2} lang={ location.state.lang}></LoginLayout>
    </div>
    </div>
  
  </Form.Group>
  );

    return (
<div className="mainbackgroundColor">
       




<div style={{ display: "flex" }}>

<Stack 

// className="mainbackgroundColor"

direction="row"
  divider={<Divider orientation="vertical" flexItem />}
  spacing={2}
  >


<div 
// className="mainbackgroundColor"
className="shadowBtn"
 style={{width:'33%', maxHeight:'128px'}}
// style={{width:'35%' ,height:'100%'}}
>
  {/* button of search route */}
<LoginLayouToFirstScreen  value={<div><div 
// className="shadowBtn"
><Image 
style={{
  // background:'none' ,border:'0',
  maxHeight:'128px'}} 
className="imgbtn"
 thumbnail={true}
  // rounded={true}
   src={returnImg}
 ></Image></div>
{/* {t("return")} */}
{t('New route')}

</div>} lang={ location.state.lang}/>
   
        </div>




{/* button fill feedback */}


      <div 
      // className="mainbackgroundColor"
      className="shadowBtn"
      style={{width:'33%', maxHeight:'128px'}}
      // style={{maxHeight:'128px'}} 
      >
        <ModalFeedback
        value= {<><div 
          // className="shadowBtn"
          ><Image 
        
        style={{maxHeight:'128px'}} 
       // style={{height:'128px' , width:'128px'}}
       className="imgbtn" 
       thumbnail={true} 
       rounded={true} 
       src={feedbackImg}
       ></Image></div>{t('feedback')}</>}
        ></ModalFeedback>
        </div>
{/*   
        <div style={{width:'35%' ,height:'100%'}}>
 
    <WolfsionButton  value={<><Image style={{width:'100%' ,height:'100%'}} thumbnail={true} rounded={true} src='https://www.gov.il/BlobFolder/office/wolfson/he/WOLFSON4.png'></Image></>}/>
  </div> */}

<div 
// className="mainbackgroundColor"
className="shadowBtn"
  style={{width:'35%', maxHeight:'128px'}}
// style={{width:'35%' ,height:'100%'}}
>
  
  <Button 
  // style={{width:'100%' ,height:'100%'}} 
  variant="white"  >
    <div   >
      <div  
      // className="shadowBtn"
      >
    <Image  className="imgbtn" 
    //  style={}} 
    style={{maxHeight:'128px'}}
    thumbnail={true} rounded={true} 
    // src='https://www.gov.il/BlobFolder/office/wolfson/he/WOLFSON4.png'
    src = {WolfsonIcon}
    // src = './refresh.png'
    ></Image></div></div></Button>
  </div>
        </Stack>
        </div>
        <br/>
        
        {defSrc}
        {coursleAndCompass}
      {/* Navigation instructions */}
        <InstNav></InstNav>
        {/* // </Container> */}

        {newNav}
</div>
            
      );
    }
      








