import { logErrorReq } from "../../../LogFunction/logError";
/**
 * @description The functions on this page are to send requests to a server that provides information such as names of buildings, areas and rooms ...
 * @author Yedidya Bachar
 * @version 1.0.0
 * @date 28/5/2022 
*/


/**
 * 
 * @param {*} url - url of server db
 * @param {*} lang - The language in which we want the input
 * @param {*} setTrunsBuildings - A function that saves a mapping between a building and its name for synchronization in case of translation
 * @param {*} setListBuildings - Defining a list of buildings
 */
export function loadBuilding(url,lang,setTrunsBuildings,setListBuildings){
 
    let xhr = new XMLHttpRequest();
    xhr.addEventListener('load', () => { 
      
      let lists = JSON.parse(xhr.responseText).userResults;
      
      setTrunsBuildings(lists)
      //add value and label per building
      const options =[]
      Object.keys(lists).sort().forEach(element=>options.push({value:element,label:element}))
     
      setListBuildings({buildings:options})
      
      // setListBuildings({buildings:<OptionLists list_options={Object.keys(lists).sort()}/>})
    })
    let newUrl =  url+lang+'/getBuildings'
    xhr.onerror = function() { logErrorReq(newUrl) };
    xhr.open('GET',newUrl)
   
    xhr.send()
  }
/**
 * Given the name of the building the function asks for the areas that are in it
 * @param {*} url - url of server db
 * @param {*} lang - The language in which we want the input
 * function like in loadBuilding
 * @param {*} setTrunsAreas 
 * @param {*} setListAreas 
 * //
 * @param {*} building  name of building which we seek his regions
 */
  export function reqAreas(url,lang,setTrunsAreas,setListAreas,building){
    var xhr = new XMLHttpRequest();
    xhr.addEventListener('load', () => {
      let lists = JSON.parse(xhr.responseText).userResults;
      setTrunsAreas(lists)

      const options =[]
      Object.keys(lists).sort().forEach(element=>options.push({value:element,label:element}))
   
      setListAreas({areas:options})

   
    })
    let newUrl =  url+lang+'/getRegions?nameBuilding='+building;
    xhr.onerror = function() { logErrorReq(newUrl) };
    xhr.open('GET',newUrl)
    xhr.send()
  }

  /**
   * 
   * @param {*} url 
   * @param {*} setListRooms set list of rooms
   * @param {*} building name chosen building 
   * @param {*} Area name chosen area 
   */
  export function reqRooms(url,setListRooms,building , Area){ 
    
    var xhr = new XMLHttpRequest();
    // get a callback when the server responds
    xhr.addEventListener('load', () => {
      let options= [];
      let lists = JSON.parse(xhr.responseText).userResults;
      lists.sort().forEach(element=>options.push({value:element,label:element}))
      setListRooms({areas:lists})
    })
    let newUrl =  url+'getRooms?nameBuilding='+building+'&nameRegion='+Area;
    xhr.onerror = function() { logErrorReq(newUrl) };
    xhr.open('GET',newUrl)
   
    xhr.send()
    }