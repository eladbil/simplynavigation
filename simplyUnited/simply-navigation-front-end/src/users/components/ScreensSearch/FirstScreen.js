import React,{ useState,useEffect } from "react";
import {Form,InputGroup} from "react-bootstrap"
import {Button,Stack,Divider   } from '@mui/material';
import "bootstrap/dist/css/bootstrap.min.css";
// import './FirstScreen.css';
import QR_scanner from '../QR/QR_scanner';
import {getAllUrlParams,loadArgFromCurrentUrl} from '../../functionHandle/FunctionUrl';

import LoginLayout from "../LoginLayout/LoginLayout";
import LanguageChange from "../LanguageChange/LanguageChange";
import { useTranslation } from 'react-i18next';
import {loadBuilding,reqRooms,reqAreas} from './HandleFunc/requestFunctions';

import Select from 'react-select';
import {LogEnterSearch,logChangeLang} from '../../LogFunction/logInfo';
import feedbackImg from '../feedback/feedback.png'
import ModalFeedback from "../feedback/ModalFeedback";
import Image from 'react-bootstrap/Image'
import NavigationIcon from '@mui/icons-material/Navigation';
import ButtonScanner from "../QR/ButtonScanner";
import WolfsonIcon from '../WolfsonButton/wolfson128.png'
import language from "../LanguageChange/icons/download2.png";
import InstSearch from "../Instructions/InstSearch";
/**
 * 
 * @author Yedidya Bachar
 * @version 1.0.0
 * @date 28/5/2022
 * @description 
 * Track Search Screen
  This screen has the scroll lists for search as well as buttons for feedback, translation, and route loading.
  At the end is a list of instructions
 * @param {*} props 
  lang - The language we work with
  heEn - Screen orientation to the right or left
 * @returns 
  buttons and scroll list as written in the description
 */
export default function FirstScreen(props) {
//url of db
  const [url , setUrl] = useState(props.url)
  //translating
  const { t ,i18n} = useTranslation();
  
//log lang
  logChangeLang(props.lang);
  //language
  const [lang , setLang] = useState(props.lang)
  //Dictionary match name of building to value to be sent to db
  const [trunsBuildings , setTrunsBuildings] = useState({})
  //Dictionary match name of area to value to be sent to db
  const [trunsAreas , setTrunsAreas] = useState({})
  //src code of point
  const [defObj, setdef] = useState({def: '' });
  //chosen building
  const [chosenBuilding, setChosenBuilding]  = useState({building: 'ff' });
  //chosen area
  const [chosenArea, setChosenArea]  = useState({area: '' });
  //chosen room
  const [chosenRoom, setChosenRoom]  = useState({room: '' });
  //list buildings 
  const [listBuildingsObj ,setListBuildings] = useState({buildings:[]  });
 //list areas 
  const [listAreasObj ,setListAreas] = useState({areas:[]});
 //list room
  const [listroomsObj ,setListRooms] = useState({rooms:[] });
  //content of qr data
  const [QR_content, set_QR_content] = useState('')
  
//row of src point code
  const defSrc = ( 
    <Form.Group className="mb-3" controlId="formAreaID">
  <Form.Label>{t('Please enter an identification code')}</Form.Label>
  <InputGroup className="mb-3">
  {/* Manually fill in the area code  */}
  <Form.Control type="text" placeholder={t('identification code')} 
  value={defObj.def}
   onChange={e => {
        const newDefObj = { def: e.target.value };
       setdef(newDefObj); // Now it works
       
    }}>
      
    </Form.Control>
    {/* A button that activates the camera for scanning */}
    
      <ButtonScanner/>
    </InputGroup>
  </Form.Group>
  );


  //A scrolling list of list of buildings
  //After the selection, a request is sent for the appropriate list of areas
const selectBuilding = (<Select placeholder={t('Select a building')+'...'} aria-label='select_building' onChange={e=>{
 
  setChosenBuilding({building: e.value });
  reqAreas(url,lang,setTrunsAreas,setListAreas,trunsBuildings[e.value])
  }} options={listBuildingsObj.buildings}
  >  
 
  </Select>);

  //A scrolling list of list of area according to the selected building
  //After the selection, a request is sent for the appropriate list of rooms
const selectAreas = (<Select placeholder={t('Select an area')+'...'} aria-label='select_building' onChange={e=>{
  setChosenArea({area: e.value });
  reqRooms(url ,setListRooms,trunsBuildings[chosenBuilding.building],trunsAreas[e.value] );
  }}
    options={listAreasObj.areas}
  > 
 
  </Select>);

//Room selection
//At the moment there is not much meaning to choosing a room and the above option can be downloaded
const selectRooms = (<Select placeholder={t('Choose a room')+'...'} aria-label='select_building' onChange={e=>{ 
  setChosenRoom({room: e.value } );
  }} >
    
  </Select>);

useEffect(() => {

  LogEnterSearch('d');
  i18n.changeLanguage(props.lang);
  document.documentElement.dir = props.heEn;

  //Initially, an automatic request is sent to load the buildings
  loadBuilding(url,lang ,setTrunsBuildings,setListBuildings); 

  setdef({def:loadArgFromCurrentUrl('id')})
 }, [props.heEn]);



/**
 * Obtaining the area code from the url
 * @param {*} childData - string of url 
 */
  const handleCallbackScannerQR = (childData) =>{
   
   try{
    let childDataString = String(childData)
    let srcID = String(getAllUrlParams(childDataString).id.toUpperCase())
   
    if(srcID==''){
      alert(t('the qr dont good'))
    }
   //in thr url need to be quary of 'srcID'
    setdef({def:srcID})
   }
   catch{
     alert(t('The picture is not good'))
   }
}
  return(<div className='mainbackgroundColor'
  // style={{backgroundImage: `url(${backGround})`}}
  >  


  

{/* buttons of change language, wolfson logo anf send feedback */}
<Stack

direction="row"
  divider={<Divider orientation="vertical" flexItem />}
  spacing={2}
  >


  <div 
  className="shadowBtn"
    style={{width:'35%' ,maxHeight:'128px'}}>
     <LanguageChange className="childLang" 
     img={ <Image
      className="imgbtn"  
      // className="mainbackgroundColor"
      style={{maxHeight:'128px'}} 
      thumbnail={true} rounded={true} src={language} />}
     />
  </div>

  <div style={{width:'35%' }}
  className="shadowBtn"
  >
 
        <ModalFeedback
        value= {<><Image 

        style={{maxHeight:'128px'}} 
       // style={{height:'128px' , width:'128px'}}
        // className="mainbackgroundColor"
        className="imgbtn" 
         thumbnail={true} rounded={true} src={feedbackImg}></Image>{t('feedback')}</>}
        ></ModalFeedback>
  </div>
<div 
style={{width:'35%' ,maxHeight:'128px'}}
className="shadowBtn"
>
  
  <Button 
  // style={{width:'100%' ,height:'100%'}} 
  variant="light" >
    <Image  
    // className="mainbackgroundColor" 
    className="imgbtn" 
     style={{maxHeight:'128px'}} 
    // style={{width:'128px' ,height:'90px'}}
    thumbnail={true} rounded={true} 
  
    src = {WolfsonIcon}
    // src = './refresh.png'
    ></Image></Button>
  </div>
  </Stack>
     
  <br/> 

  {QR_content}
  <div style={{margin: 'auto', padding: '10px'}}>
  {defSrc}
  <h4>{t('Select a building')}</h4>
  {selectBuilding}
  <h4>{t('Select an area')}</h4>
  {selectAreas}
  <h4>{t('Choose a room')}</h4>
  {selectRooms}
  </div>
<div style={{width:'40%' ,margin: 'auto', padding: '10px'}}>

<LoginLayout style={{'margin-left':'auto' ,'margin-rgiht':'auto', padding: '10px'}} typeInfo="RouteFromSearch" isRefresh={false} value= {<div><NavigationIcon sx={{ mr: 1 }} />{t(`Uploading a route`)}</div>} url={url} defObj={defObj} chosenBuilding={trunsBuildings[chosenBuilding.building]} chosenArea={trunsAreas[chosenArea.area]} chosenRoom={chosenRoom} setdef={setdef} lang={lang}></LoginLayout>
</div>

{/* Instructions to search */}
<InstSearch></InstSearch>
 

  {/* object of qr scanner - open camera and looad pictuer */}
  <QR_scanner parentCallback = {handleCallbackScannerQR} text={t('Take a barcode')}/>

  </div>);
}