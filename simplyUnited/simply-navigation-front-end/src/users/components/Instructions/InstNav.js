import React from "react";
import { useTranslation } from 'react-i18next';
import { Alert } from "react-bootstrap";
/**
 * 
 * @author Yedidya Bachar
 * @version 1.0.0
 * @date 27/5/2022
 * @description Instructions for route search
 * @returns <Alert> of Instructions
 */
export default function InstNav(){
  // Translation function
    const {t} = useTranslation();
    return(
      // Green instruction screen
    <Alert variant="success">
      {/* Title of the instructions */}
    <Alert.Heading>{t('The instructions:')}</Alert.Heading>
    {/* parts of the instructions */}
    <p>
  {t('Instructions for use Title')}
  </p>
  <hr />
  <p className="mb-0">
  {t('Instructions for use search1')}
  </p>
  <br/>
  <p>
  {t('Instructions for use search2')}
  </p>
  <p>
  {t('Instructions for use search3')}
  </p>
  <p className="mb-0">
  {t('Instructions for use search4')}
  </p>
  <p className="mb-0">
  {t('Instructions for use search5')}
  </p>
      </Alert>

    );
}