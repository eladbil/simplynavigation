import React from "react";
import { useTranslation } from 'react-i18next';
import { Alert } from "react-bootstrap";
/**
 * 
 * @author Yedidya Bachar
 * @version 1.0.0
 * @date 27/5/2022
 * @description Instructions for the navigation part
 * @returns <Alert> of Instructions
 */
export default function InstSearch(){
  // Translation function
    const {t} = useTranslation();
    return(
    // Green instruction screen
   <Alert variant="success">
     {/* Title of the instructions */}
  <Alert.Heading>{t('Welcome to the navigation system')}</Alert.Heading>
   {/* parts of the instructions */}
  <p>
  {t('Instructions for use Title')}
  </p>
  <hr />
  <p className="mb-0">
  {t('Instructions for use p1')}
  </p>
  <br/>
  <p>
  {t('Instructions for use p2')}
  </p>
  <p>
  {t('Instructions for use p3')}
  </p>
</Alert>

    );
}