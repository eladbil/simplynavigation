import React,{useEffect,useState , usr} from "react";
import './Compass5.css'
import West from './icons/West.png'
import East from './icons/East.png'
import South from './icons/South.png'
import North from './icons/North.png'
import Northwest from './icons/Northwest.png'
import Northeast from './icons/Northeast.png'
import Southeast from './icons/Southeast.png'
import Southwest from './icons/Southwest.png'
import noImg from './icons/noImg.png'
import cmpimg from './icons/compass.png'

/**
 * @author Yedidya Bachar,
 * A component containing the compass calculations
 * and returns the compass circle and images of arrows according to the compass direction.
 * Most of the code and calculations were taken from the Internet and adapted to react
 * @version 1.0.1
 * @date 09/06/2022
 * 
 * @param {*} props 
 * {
 * backgroundurl - src of background of the compass
 * }
 * @returns 
 */
export default function Compass5(props){  

  //Mapping between a direction and an image of an arrow in the appropriate direction and color
  const directionImg = {'Northwest.png':Northwest,'Northeast.png':Northeast,'Southeast.png':Southeast,'Southwest.png':Southwest,'West.png':West,'East.png':East,'South.png':South,'North.png':North , 'undefined.png':noImg}
 
 
  const [oo , setoo] = useState('strong')
 

   useEffect(() => {
    //start compass
      startCompass();
  });


    const compassCircle = document.querySelector(".compass-circle");
    // const myPoint = document.querySelector(".my-point");
    // const startBtn = document.querySelector(".start-btn");
    const isIOS =
      navigator.userAgent.match(/(iPod|iPhone|iPad)/) &&
      navigator.userAgent.match(/AppleWebKit/);
//       function init() {
//   startBtn.addEventListener("click", startCompass);
  
// }


/*
Checking whether the device is Android or Apple
And accordingly defines the api of the browser
*/
function startCompass() {
  setoo('ll')
  
  if (isIOS) {
    DeviceOrientationEvent.requestPermission()
      .then((response) => {
        if (response === "granted") {
          window.addEventListener("deviceorientation", handler, true);
        } else {
          alert("has to be allowed!");
        }
      })
      .catch(() => alert("not supported"));
  } else {
    window.addEventListener("deviceorientationabsolute", handler, true);
  }
}


//A function that defines the degrees in which the device is located
// Works with every movement of the device
function handler(e) {
  let compass = e.webkitCompassHeading || Math.abs(e.alpha - 360);
  // compassCircle.style.transform = `translate(-50%, -50%) rotate(${-compass}deg)`;
  try{
  compassCircle.style.transform = `translate(-50%, -50%) rotate(${-compass}deg)`;
  }
  catch(e){
    
  }
}


  // let pointDegree;

  // function locationHandler(position) {
  //   const { latitude, longitude } = position.coords;
  //   pointDegree = calcDegreeToPoint(latitude, longitude);

  //   if (pointDegree < 0) {
  //     pointDegree = pointDegree + 360;
  //   }
  // }

  //   function calcDegreeToPoint(latitude, longitude) {
  //     // Qibla geolocation
  //     const point = {
  //       lat: 21.422487,
  //       lng: 39.826206
  //     };

  //     const phiK = (point.lat * Math.PI) / 180.0;
  //     const lambdaK = (point.lng * Math.PI) / 180.0;
  //     const phi = (latitude * Math.PI) / 180.0;
  //     const lambda = (longitude * Math.PI) / 180.0;
  //     const psi =
  //       (180.0 / Math.PI) *
  //       Math.atan2(
  //         Math.sin(lambdaK - lambda),
  //         Math.cos(phi) * Math.tan(phiK) -
  //           Math.sin(phi) * Math.cos(lambdaK - lambda)
  //       );
  //     return Math.round(psi);
  //   }
  
 
   
    return(

        <>

        {/* <show/> */}
     
        <div className="compass" >
      <div className="arrow"></div>      
   
      <div className="compass-circle" style={{
        backgroundImage:  "url("+cmpimg+"), url("+directionImg[props.backgroundurl]+ ")"
        
        }}></div>
      <div 
       className="my-point" ></div>
     
    </div>
   
    {/* <button class="start-btn" onClick={startCompass}>Start compass</button> */}
    {/* <button onClick={refreshPage}>Click to reload!</button> */}
    </>

    );
  }




  