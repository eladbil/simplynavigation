import React from 'react';
import Chart from 'react-apexcharts'
/**
 * 
 * @author Yedidya Bachar
 * @version 1.0.0
 * @date 29/5/2022
 * @description
 * Displays a circle of percentages
 * @param {*} props 
 * labels
 * series
 * @returns 
 * ircle of percentages
 */
export default function PercentRoute(props){

    let state = {
      options: {labels: props.labels},
     
      series: props.series,//[44, 55, 41, 17, 15],
      labels: props.labels//['A', 'B', 'C', 'D', 'E']
    }

    return (
      <div className="donut">
        <Chart options={state.options} series={state.series} type="donut" width="380" />
      </div>
    );
  }


