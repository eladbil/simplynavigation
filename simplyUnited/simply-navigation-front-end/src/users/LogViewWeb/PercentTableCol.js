import React, { useState,useEffect,useImperativeHandle,forwardRef } from 'react';
import Chart from 'react-apexcharts'

/**
 * 
 * @param {*} props 
 * @param {*} ref 
 * @returns 
 */
function PercentSrcIdUpdate(props,ref) {
   
    const [keyToRefresh ,setKeyToRefresh ] = useState(0);
    const [graph ,setGraph ] = useState(<></>);
    const [series ,setSeries ] = useState([]);
    const [labels ,setLabels ] = useState([]);
    // <Chart options={this.state.options} series={this.state.series} type="donut" width="380" />
  
    let state = {
      options: 
        {
            chart: {
              width: 380,
              type: 'donut',
              dropShadow: {
                enabled: true,
                color: '#111',
                top: -1,
                left: 3,
                blur: 3,
                opacity: 0.2
              }
            },
            stroke: {
              width: 0,
            },
            plotOptions: {
              pie: {
                donut: {
                  labels: {
                    show: true,
                    total: {
                      showAlways: true,
                      show: true
                    }
                  }
                }
              }
            },
            labels: labels,//["Comedy", "Action", "SciFi", "Drama", "Horror"],
            dataLabels: {
              dropShadow: {
                blur: 3,
                opacity: 0.8
              }
            },
            fill: {
            type: 'pattern',
              opacity: 1,
              pattern: {
                enabled: true,
                style: ['verticalLines', 'squares', 'horizontalLines', 'circles','slantedLines'],
              },
            },
            states: {
              hover: {
                filter: 'none'
              }
            },
            theme: {
              palette: 'palette2'
            },
            title: {
              text: "Favourite Movie Type"
            },
            responsive: [{
              breakpoint: 480,
              options: {
                chart: {
                  width: 200
                },
                legend: {
                  position: 'bottom'
                }
              }
            }]
          },
        
        
        }
    
  
  function sendRequest(){
   
   
    let newUrl=props.urlServerLog;
    fetch(newUrl)
    .then(res => res.json(),
    
    setSeries([]),
    setLabels([])
    
    ).then(data =>{
      // console.log(data)
      
    
        data[0].forEach(item => {

          if(props.binary){
          if(item.label=='0'){
            item.label='No'
          }
          if(item.label=='1'){
            item.label='Yes'
          }
        }
          
            setSeries(oldArray => [...oldArray, item.count]);
            setLabels(oldArray => [...oldArray, item.label]);
            
        
        },
        );
        

    })
    }
    useEffect(() => {
      setGraph(<Chart options={state.options} key={keyToRefresh+1} labels={labels} series={series} type="donut" width="380" />)
    }, [labels,series]);
  
    useImperativeHandle(ref,() => ({
      sendRequest1() {
        sendRequest();
      },
    }))

  // fetchData() is called whenever data is updated.



    return (
      <div className="donut">
          <h2>{props.title}</h2>
      {graph}
      </div>
    );
  
}
export default forwardRef(PercentSrcIdUpdate);
