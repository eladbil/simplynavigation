import React,{useState,useEffect,useRef,useImperativeHandle,forwardRef} from "react";
import ReactApexChart from 'react-apexcharts';
import PercentRoute from "./PercentRoutes";
/**
 * 
 * @author Yedidya Bachar
 * @version 1.0.0
 * @date 29/5/2022
 * @description Graph of the number of route starts divided by route types
 * @param {*} props 
 * @param {*} ref 
 * @returns 
 */
function GraphRoutes(props,ref) {
 
  //Variable for refreshing the graph
  const [keyToRefresh ,setKeyToRefresh ] = useState(0);

  // Count the number of routes by type
  const [countIdToId ,setCountIdToId ] = useState(0);
  const [countSearch ,setCountSearch ] = useState(0);

//The names of the types
 const idToId = 'מנקודה לנקודה';
 const search = 'בעזרת חיפוש'

//  The days and values
  const [seriesRouteDates ,setSeriesRouteDates ] = useState([])
  const [categoriesRouteDates ,setCategoriesRouteDates ] = useState([])

  //the graph
  const [graph ,setGraph ] = useState(<></>)//<GraphRoutes series ={seriesRouteDates} categories={categoriesRouteDates}></GraphRoutes>
  const [percentRoute ,setPercentRoute ] = useState(<></>)
 
    let state = {
      series: seriesRouteDates,
      options: {
        chart: {
          type: 'bar',
          height: 350,
          stacked: true,
          toolbar: {
            show: true
          },
          zoom: {
            enabled: true
          }
        },
        responsive: [{
          breakpoint: 480,
          options: {
            legend: {
              position: 'bottom',
              offsetX: -10,
              offsetY: 0
            }
          }
        }],
        plotOptions: {
          bar: {
            horizontal: false,
            borderRadius: 10
          },
        },
        xaxis: {
          type: 'datetime',
          categories: categoriesRouteDates
          // categories: ['01/01/2011 GMT', '01/02/2011 GMT', '01/03/2011 GMT', '01/04/2011 GMT',
          //   '01/05/2011 GMT', '01/06/2011 GMT'
          // ],
        },
        legend: {
          position: 'right',
          offsetY: 40
        },
        fill: {
          opacity: 1
        }
      },
    
    
    };
   
    const timer = React.useRef();
    //     timer.current = window.setTimeout(() => {
             
        // }, 3000),
    /**
     * Sending a request to a log server to obtain the necessary information
     */
    function sendRequest(){
      
      
      //urls to request of the information
      let urlIdToId = props.urlServerLog1;
      let urlSearch = props.urlServerLog2;
     
    
      Promise.all([
          
            fetch(urlIdToId).then(res => res.json()),
            fetch(urlSearch).then(res => res.json()),
            setCountIdToId(0),
            setCountSearch(0),
          
           
        ]).then(([urlOneData, urlTwoData]) => {
        
            let arrayDates = []
            urlOneData[0].forEach(item => {
                    arrayDates.push(item.date)
          
                });
            urlTwoData[0].forEach(item => {
            arrayDates.push(item.date)
    
            });
            arrayDates = [...new Set(arrayDates)];
            let datesSort = arrayDates.sort();
            let RouteFromIdToIdDates = new Array(datesSort.length).fill(0);
            let RouteFromSearchDates = new Array(datesSort.length).fill(0);
            
            let temp1 = 0
            urlOneData[0].forEach(item => {
                if(datesSort.includes(item.date)){
                 
                    RouteFromIdToIdDates[datesSort.indexOf(item.date)] = item.count;
                    
                    temp1+=item.count;
                   
                }
            });
            let temp2 = 0
            urlTwoData[0].forEach(item => {
                if(datesSort.includes(item.date)){
                 
                    RouteFromSearchDates[datesSort.indexOf(item.date)] = item.count;
                    temp2+=item.count;
                }
                
        })
//set information
        setCountIdToId(temp1);
        setCountSearch(temp2);
        setCategoriesRouteDates(datesSort);
        setSeriesRouteDates([{name: idToId ,data: RouteFromIdToIdDates},{name: search ,data: RouteFromSearchDates}]);
      })

        }


      useImperativeHandle(ref,() => ({
        sendRequest1() {
          sendRequest();
        },
       
      }))

      useEffect(() => {
        setPercentRoute(<PercentRoute key={keyToRefresh+1} series={[countIdToId,countSearch]} labels={[idToId,search]}></PercentRoute>);
        setGraph( <ReactApexChart key={keyToRefresh+2} options={state.options} series={state.series} type="bar" height={350} />);
      }, [countIdToId,countSearch, seriesRouteDates, categoriesRouteDates]);

    
      return (
       <>
        <h2>כמות הניווטים שהתחילו כל יום </h2>
       {graph} 
       {percentRoute}
     </>
       
      );
      }

export default forwardRef(GraphRoutes)