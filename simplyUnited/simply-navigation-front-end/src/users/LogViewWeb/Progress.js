import * as React from 'react';
import Box from '@mui/material/Box';
import CircularProgress from '@mui/material/CircularProgress';
import { green } from '@mui/material/colors';
import Button from '@mui/material/Button';


/**
 * @author Yedidya Bachar
 * @version 1.0.0
 * @date 14/6/2022
 * @description Creates the spin effect on the circle button
 * @param {*} ref Allows to run functionality outside the class
 * @returns 
 */
function CircularIntegration(prop,ref) {
  const [loading, setLoading] = React.useState(false);
  const [success, setSuccess] = React.useState(false);
  const timer = React.useRef();

  const buttonSx = {
    ...(success && {
      bgcolor: green[500],
      '&:hover': {
        bgcolor: green[700],
      },
    }),
  };

  React.useEffect(() => {
  
    return () => {
      clearTimeout(timer.current);
    };
    
  }, []);


  React.useImperativeHandle(ref,() => ({
    ChangeT() {
        setSuccess(true);
        setLoading(false);
    },
    ChangeF() {
        setSuccess(false);
        setLoading(true);
    },
  }))

  return (
    <Box sx={{ display: 'flex', alignItems: 'center' }}>
     
      <Box sx={{ m: 1, position: 'relative',alignItems: 'center' }}>
        <Button
          variant="contained"
          sx={buttonSx}
          disabled={loading}
          onClick={prop.onClick}
        >
          {prop.title}
        </Button>
        {loading && (
          <CircularProgress
            size={24}
            sx={{
              color: green[500],
              position: 'absolute',
              top: '50%',
              left: '50%',
              marginTop: '-12px',
              marginLeft: '-12px',
            }}
          />
        )}
      </Box>
    </Box>
  );
}
export default React.forwardRef(CircularIntegration);