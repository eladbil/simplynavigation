import React, {Component} from "react";
import {Table} from 'react-bootstrap'
/**
 * @author Yedidya Bachar
 * @version 1.0.1
 * @date 25/6/2022
 * @param {*} props 
 * {
 * heading  - title
 * body - body of table
 * }
 * @description A simple table for displaying data
 * @returns <Alert> of Instructions
 */
class TableSimple extends Component {
    render() {
        var heading = this.props.heading;
        var body = this.props.body;
        let keyToRef = 1000
        return (
        <> <Table 
            striped bordered hover
            >
                <thead>
                    <tr>
                        {heading.map(head => <th key={++keyToRef}>{head}</th>)}
                    </tr>
                </thead>
                <tbody>
                    {body.map(row => <TableRow key={++keyToRef} row={row} />)}
                </tbody>
            </Table>
            </>
        );
    }
}
  
class TableRow extends Component {
    render() {
        let keyToRef2 = 0
        var row = this.props.row;
        return (
            <tr>
                {row.map(val => <td key={++keyToRef2}>{val}</td>)}
            </tr>
          
        )
    }
}

export default TableSimple;
  