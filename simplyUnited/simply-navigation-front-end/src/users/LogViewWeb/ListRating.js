
import React, { useState,useEffect,useImperativeHandle,forwardRef } from 'react';
import Box from '@mui/material/Box';
import ListItem from '@mui/material/ListItem';
import { FixedSizeList } from 'react-window';
import ReactStars from "react-rating-stars-component";
import './ListRating.css';
import moment from "moment";
import VerticalSlider from './VerticalSlider';

/**
 * @author Yedidya Bachar
 * @version 1.0.0
 * @date 27/5/2022
 * @description 
 * Create a row in a table
 * @param {*} props 
 * data - An array of all the information
 * index - The index to the line we want to produce
 * style - The style of the line
 * @returns 
 * ListItem of infromation
 * rate , date, comment
 */

function renderRow(props) {

  const {data, index, style } = props;
 
  return (
    <ListItem key={index} style={style} component="div" disablePadding>
       
       <div style={{  width:'60%' ,height:'50%'}} >
        <ReactStars
        color={'#ffd700'	}
          count={data[index].Rating}
          edit={false}
          size={20}
          activeColor="#ffd700"

          isHalf={false}
      
        /></div>
      
    

<div>{moment(data[index].OrderDate).format('DD/MM/YYYY')}</div>

<div className="myBox">
{data[index].Feedbacktext}
</div>

        
   
    </ListItem>
  );
}

/**
 * List of ratings and comments.
 * Each comment with a rating has a line within a list.
 * @param {*} props 
 * urlServerLog - url of log db
 * title
 * @param {*} ref 
 * Link to the function of the father
 * @returns 
 * graph and title
 */
function VirtualizedList(props,ref) {
const [ref2 ,setRef2 ] = useState(0);
const [graph ,setGraph ] = useState(<></>);
const [data ,setData ] = useState([]);
const [OrginalData ,setOrginalData ] = useState([]);

/**
 * send request to log db
 */
function sendRequest(){
   
 
    let newUrl=props.urlServerLog;
    console.log(newUrl);
    fetch(newUrl)
    .then(res => res.json(),
    setData([])
    
    ).then(data =>{
      data[0].sort((firstItem, secondItem) => new Date(firstItem.OrderDate) -new Date(secondItem.OrderDate) );
      data[0].sort((firstItem, secondItem) =>  firstItem.Rating - secondItem.Rating);
      // const newArr = data[0].filter(object => {
      //   return object.Rating == 3;
      // });
    
        setData(data[0]);    
        setOrginalData(data[0]);
        
        
    }) 
    }
    useEffect(() => {
      setRef2(ref2+1)
      setGraph( <Box key={ref2+1}
        sx={{ width: '100%', height: 400, maxWidth: '100%', bgcolor: 'background.paper' }}
      >
        <FixedSizeList
          height={400}
          width='100%'
          itemSize={100}
          itemCount={data.length}
          overscanCount={5}
          itemData= {data}
        >
          {renderRow}
        </FixedSizeList>
        {/* <p>lk</p> */}
      </Box>)
    }, [data]);
  
    useImperativeHandle(ref,() => ({
      sendRequest1() {
        sendRequest();
      },
    }))


    function changeRangeStar(a,b){
      console.log(a,b)
      const newArr = OrginalData.filter(object => {
        return (object.Rating >=a && object.Rating <=b);
      });
      // console.log(newArr);
    
      setData(newArr);    
    }

  return (
    <div className="donut">
          <h2>{props.title}</h2>
          <VerticalSlider changeRangeStar={changeRangeStar}></VerticalSlider>
      {graph}
      </div>
  );
}
export default forwardRef(VirtualizedList);