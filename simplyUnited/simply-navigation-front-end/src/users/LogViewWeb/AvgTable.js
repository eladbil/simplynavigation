import React, { useState,useEffect,useRef,forwardRef,useImperativeHandle } from 'react';
import TableSimple from './TableSimple';


/**
 * 
 * @author Yedidya Bachar
 * @version 1.0.0
 * @date 29/5/2022
 * @description Create a table with feedback data
 * @param {*} props 
 * urlServerLog - url of db log
 * title of table
 * @param {*} ref 
 * @returns 
 */
function AvgTable(props,ref) {


    const [keyToRefresh ,setKeyToRefresh ] = useState(0);
    //the table
    const [graph ,setGraph ] = useState(<></>);
    const [series ,setSeries ] = useState([]);
  
  /*
   * Data request what log
  avg_enter - Average hits
  avgroute - Average routes per user
  avgupdate - Average updates per user per track
   */
  function sendRequest(){
    let newUrl=props.urlServerLog;
    fetch(newUrl).then(res => res.json(),
    setSeries([])
    ).then(data =>{
        // avg_enter: 17.333333333333332, avgroute: 14.333333333333334, avgupdate: 14.5
        if (data[0][0].avgupdate == null || data[0][0].avgroute==0){
          data[0][0].avgupdate=0;
        }
        if(data[0][0].avg_enter==null){
          data[0][0].avg_enter=0;
        }
        if(data[0][0].avgroute==null){
          data[0][0].avgroute=0;
        }


        console.log([data[0][0].avg_enter,data[0][0].avgroute,data[0][0].avgupdate])
        setSeries([(data[0][0].avg_enter).toFixed(2),data[0][0].avgroute.toFixed(2),data[0][0].avgupdate.toFixed(2),(data[0][0].avgupdate/data[0][0].avgroute).toFixed(2)]);
      
       
    })
    .then(
    setKeyToRefresh(keyToRefresh+1),
    setGraph(<TableSimple heading={['ממוצע כניסות למשתמש','ממוצע מסלולים למשתמש','ממוצע עדכונים למשתמש שמשתמש במסלולים','ממוצע עדכונים למסלול']} body={[series]}/>)
    )

 
    }
//Helps activate the function sendRequest through the parent
  useImperativeHandle(ref,() => ({
    sendRequest1() {
      sendRequest();
    },
   
  }))
 //Initializes the table according to the data
   useEffect(() => {
    setGraph(<TableSimple heading={['ממוצע כניסות למשתמש','ממוצע מסלולים למשתמש','ממוצע עדכונים למשתמש שמשתמש במסלולים','ממוצע עדכונים למסלול']} body={[series]}/>)
  }, [series]);

    return (
      <div className="donut">
          <h2>{props.title}</h2>
      {graph}
      </div>
    );
  
}

export default forwardRef(AvgTable);