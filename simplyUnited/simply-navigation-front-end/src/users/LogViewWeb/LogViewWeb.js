import React,{useState,useRef, useReducer} from "react";
import { Button  } from '@mui/material';
import VirtualizedList from "./ListRating";
import GraphRoutes from "./GraphRoutes";
import RangeCal from "./RangeCal";
import PercentTableCol from "./PercentTableCol";
import './LogViewWeb.css'
import DivisionHours from "./DivisionHours";
import AvgTable from "./AvgTable";
import {getUrlserverDbAdmin} from '../functionHandle/urls'
import CircularIntegration from './Progress'


/* https://localhost:3000/LogViewWeb */

/**
 * 
 * @author Yedidya Bachar
 * @version 1.0.1
 * @date 15/8/2022
 *  Log display screen
 * @returns
 * Components that visually represent the log data
 */
export default function LogViewWeb(){
 
  const [change] = React.useState(0);
//   let requestOptions = {
//     // method: 'GET',
//     withCredentials: 'true',
//     // headers: {
//     //     Accept: 'application/json',
//     //     'Content-Type': 'application/json',
//     // },
//     credentials: 'include',
//     //body: JSON.stringify(requestBody),
// }; 
  //Screen orientation
  document.documentElement.dir = 'rtl';
  //Allows child components to add functions that will be activated when an event happens to the parent
  const useRef1 = useRef();
  const useRef2 = useRef();
  const useRef3 = useRef();
  const useRef4 = useRef();
  const useRef5 = useRef();
  //feedback
  const useRef6 = useRef();
  const useRef7 = useRef();
  const useRef8 = useRef();
  const useRef9 = useRef();
  const useRef10 = useRef();
//When will the components be refreshed
  const [toRefresh,setToRefresh] = useState(0);
  //Date range selected to extract information from the log
  const [momentDate1,setMomentDate1] = useState(null);
  const [momentDate2,setMomentDate2] = useState(null);
  //Add the date range to the query
  let dateRange =`?date1=${momentDate1}&date2=${momentDate2}`;
  //url of log server
    let urlServerLog = getUrlserverDbAdmin() + 'api/'
    const [refresh, doRefresh] = useReducer(x=>x+1,0);
    const timer = React.useRef();

    function onclickLoad(){
        useRef10.current.ChangeF();
        useRef1.current.sendRequest1();
        useRef2.current.sendRequest1();
        useRef3.current.sendRequest1();
        useRef4.current.sendRequest1();
        useRef5.current.sendRequest1();
        useRef6.current.sendRequest1();
        useRef7.current.sendRequest1();
        useRef8.current.sendRequest1();
        useRef9.current.sendRequest1();
        const promise1 = new Promise((resolve, reject) => {        
          if(momentDate1===null |momentDate1==='Invalid date' |momentDate2===null |momentDate2==='Invalid date'){
          
            resolve(false);
            return
          }
          resolve(true);
        });
        promise1.then((value) => {
          if(!value){
            return
          }
          doRefresh();
        }).then(
            timer.current = window.setTimeout(() => {
              setToRefresh(toRefresh+1)
              useRef10.current.ChangeT()
        }, 3000),            
        );
    }

    //After selecting a date range all requests will be sent to the server
    // let b = <Button variant="contained" className="btnlogmain" onClick={onclickLoad} >טען סטטיסטיקות בתאריכים</Button>

//The graphs that will be displayed eventually
    let a = <RangeCal className='objlogmain' setMomentDate2={setMomentDate2} setMomentDate1={setMomentDate1} ></RangeCal>
    let c = <GraphRoutes className='objlogmain' ref={useRef1} momentDate1={momentDate1} momentDate2={momentDate2} urlServerLog1={urlServerLog + 'CountRoutesByDateAndType/Info_Basic_Log/RouteFromIdToId'+dateRange} urlServerLog2={urlServerLog + 'CountRoutesByDateAndType/Info_Basic_Log/RouteFromSearch'+dateRange} refresh={refresh}></GraphRoutes>
    let avgTable = <AvgTable className='objlogmain'  ref={useRef2} momentDate1={momentDate1} momentDate2={momentDate2} title='טבלת ממוצעים' urlServerLog={urlServerLog + 'getAverages'+dateRange} refresh={refresh}></AvgTable>
    let countSrcIdUpdate = <PercentTableCol className='objlogmain' ref={useRef3} momentDate1={momentDate1} momentDate2={momentDate2} binary={false} title='פילוג הנקודות בהם עודכן המסלול' urlServerLog={urlServerLog + 'getCountingValuesInColumn/Info_Update/srcDef'+dateRange} refresh={refresh}></PercentTableCol>
    let countSrcIdStart = <PercentTableCol className='objlogmain' ref={useRef4} momentDate1={momentDate1} momentDate2={momentDate2} binary={false} title='פילוג משתמשים שהתחילו מסלול בחלוקה לפי שפות' urlServerLog={urlServerLog + 'getColumnFromStartRoute/Info_Basic_Log/Lang'+dateRange} refresh={refresh}></PercentTableCol>
    let countIsFinish = <PercentTableCol className='objlogmain' ref={useRef6} momentDate1={momentDate1} momentDate2={momentDate2} binary={true} title='האם סיימת מסלול' urlServerLog={urlServerLog + 'getCountingValuesInColumn/Feedback1/IsFinish'+dateRange} refresh={refresh}></PercentTableCol>
    let countIsBack = <PercentTableCol  className='objlogmain' ref={useRef7} momentDate1={momentDate1} momentDate2={momentDate2} binary={true} title='האם תשתמש שוב במערכת' urlServerLog={urlServerLog + 'getCountingValuesInColumn/Feedback1/Isback'+dateRange} refresh={refresh}></PercentTableCol>
    let countRating = <PercentTableCol className='objlogmain' ref={useRef8} momentDate1={momentDate1} momentDate2={momentDate2} binary={false} title='דירוגים' urlServerLog={urlServerLog + 'getCountingValuesInColumn/Feedback1/Rating'+dateRange} refresh={refresh}></PercentTableCol>
    let divisionHours =  <DivisionHours className='objlogmain' ref={useRef5} momentDate1={momentDate1} momentDate2={momentDate2}  title='פילוג משתמשים שהתחילו מסלול לפי שעות היום' urlServerLog={urlServerLog + 'getRoutesHoursCount'+dateRange} refresh={refresh}></DivisionHours>
    let ratingDateTxt =  <VirtualizedList  className='objlogmain' ref={useRef9} momentDate1={momentDate1} momentDate2={momentDate2}  title='תגובות משתמשים' urlServerLog={urlServerLog + 'getFeedbackDateRatingTxt'+dateRange} refresh={refresh}></VirtualizedList>

    return(
      <div className="logViewWeb">
        <div className="center" >
          <h1 className="h1objlogmain">סטטיסטיקות ונתונים על מערכת הניווט</h1>
        <div id='root2'></div>
  <div className='objlogmain'>
        <div style={{width:'100%'}}  >
        <h2 className='h2objlogmain'>בחר טווח תאריכים</h2>
          <div className="centerlogmain">
        {a}
        <br/><br/>
        {/* {b} */}
    
<CircularIntegration ref={useRef10} change={change} onClick={onclickLoad} title='טען סטטיסטיקות בתאריכים'></CircularIntegration>
{/* <Button onClick={() => {useRef10.current.ChangeT();}}>change</Button> */}
        </div>
        </div>
        </div>
        <div className='objlogmain'>
        {c}
        </div>
        <div className='objlogmain'>
        {countSrcIdUpdate}
        </div>
        <div className='objlogmain'>
        {countSrcIdStart}
        </div>
        <div className='objlogmain'>
        {divisionHours}
        </div>
        <div className='objlogmain'>
        {avgTable}
        </div>

          <h1 className="h1objlogmain">תגובות משתמשים</h1>

          <div className='objlogmain'>
          {countIsFinish}
        </div>
        <div className='objlogmain'>
        {countIsBack}
        </div>
        <div className='objlogmain'>
        {countRating}
        </div>
{ratingDateTxt}

       {/* </div> */}
        </div>
        <div>
          <h2>
            אתחול מערכת
          </h2>
          {/* אתחול מערכת */}
          <div  className='restart'>
          {/* 
        */}
          <Button color='warning' variant="contained" onClick={()=>{ 
           
           fetch(urlServerLog+'doesItExistDB')
           .then(function(response) {
            return response.json();
          })
          .then(function(data) {
           if(data.result === true){
            // console.log(' יש לשאול האם הוא רוצה למחוק ואז לעשות מחיקה ויצירה של הטבלאות') 

            if (window.confirm("המערכת מחוברת לבסיס הנתונים, לחיצה על אישור תאתחל את בסיס")) {
              // alert('winner');
              fetch(urlServerLog+'restartTables')
           .then(function(response) {
            return response.json();
          })
          .then(function(data) {
            if(data.result === 'success'){
              alert('הפעולה מתבצעת, יש לחכות מס דקות לטעינת הבסיס נתונים.')
            }
            else{
              alert(data.result)
            }

           });
              
           }
           }
           else if(data.result === false){

            if (window.confirm('לא קיים בסיס נתונים, לחיצה על אישור תאפס את הנתונים הקיימים.')) {
              // alert('loser');

              fetch(urlServerLog+'CreateNewDB')
              .then(function(response) {
               return response.json();
             })
             .then(function(data) {
              if(data.result === 'success'){
                alert('הפעולה מתבצעת, יש לחכות מס דקות לטעינת הבסיס נתונים.')
              }
              else{
                alert(data.result)
              }

             });
                 
              
           }
            
           }
           else{
            alert('שגיאה לא ידועה, לא ניתן לאתחל את המערכת')
           }
          })
          }
          }>אתחל תגובות משתמשים</Button>
          </div>
         
        </div>
        </div>
    );
}

