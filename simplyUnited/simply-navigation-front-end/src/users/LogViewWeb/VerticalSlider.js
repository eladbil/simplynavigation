import * as React from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Slider from '@mui/material/Slider';



function valueLabelFormat(value) {
  const units = ['Star'];

  return `${value} ${units[0]}`;
}

function calculateValue(value) {
  
  return  value;
}

const marks = [
   
    {
      value: 1,
      label: '1',
    },
    {
      value: 2,
      label: '2',
    },
    {
      value: 3,
      label: '3',
    },
    ,
    {
      value: 4,
      label: '4',
    },
    ,
    {
      value: 5,
      label: '5',
    },
  ];

  function valuetext(value) {
    return `${value}°C`;
  }
  
/**
 * @author Yedidya Bachar
 * @version 1.0.0
 * @date 27/5/2022
 * @description A line for choosing the range of stars that the user wants to see in the comments
 * @param {*} props 
 * @returns 
 */
export default function VerticalSlider(props) {
  const [fromvalue, setFromvalue] = React.useState(1);
  const [toValue, setToValue] = React.useState(5);

  const handleChange = (event,range) => {
   
    if (typeof range[0] === 'number') {
        setFromvalue(range[0]);
        setToValue(range[1])
        props.changeRangeStar(range[0],range[1])
    }
  };



  return (
    <Box
    //  sx={{ width: 250 }}
     >
      <Typography id="non-linear-slider" gutterBottom>
        range: {calculateValue(fromvalue)} - {calculateValue(toValue)}
      </Typography>
     
      <Slider
      valueLabelDisplay="auto"
        // track="inverted"
        // aria-labelledby="track-inverted-range-slider"
        aria-labelledby="non-linear-slider"
        // getAriaValueText={valuetext}
        // onChange={alert(value)}
        min={1}
        step={1}
        max={5}
        scale={calculateValue}
        getAriaValueText={valueLabelFormat}
        valueLabelFormat={valueLabelFormat}
        defaultValue={[1, 5]}
        onChange={handleChange}
        marks={marks}
      />
    </Box>
  );
}
