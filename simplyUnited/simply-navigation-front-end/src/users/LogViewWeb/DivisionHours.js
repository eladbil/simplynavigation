import React,{useState,useImperativeHandle,forwardRef,useEffect} from "react";
import ReactApexChart from "react-apexcharts";
/**
 * 
 * @author Yedidya Bachar
 * @version 1.0.0
 * @date 29/5/2022
 * @description Distribution of data by daytime hours
 * Represented by a continuous graph
 * @param {*} props 
 * title -  title of graph
 * urlServerLog - url of log db 
 * @param {*} ref - Sent from the father to activate an internal function within the component
 * @returns 
 * graph of ReactApexChart of division into hours
 */
function DivisionHours(props,ref){


  //Designed to interpret the component in case the information is updated
  const [keyToRefresh ,setKeyToRefresh ] = useState(0);
  // The graph shown
    const [graph ,setGraph ] = useState(<></>);
    // The information displayed (series) per hour (labels)
    const [series ,setSeries ] = useState([]);
    const [labels ,setLabels ] = useState([]);
    
      let state = {
      
        series: [{
            name: "משתמשים",
            data: series //[10, 41, 35, 51, 49, 62, 69, 91, 148]
        }],
        options: {
          chart: {
            height: 350,
            type: 'line',
            zoom: {
              enabled: false
            }
          },
          dataLabels: {
            enabled: false
          },
          stroke: {
            curve: 'straight'
          },
          title: {
            text: 'Product Trends by Month',
            align: 'left'
          },
          grid: {
            row: {
              colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
              opacity: 0.5
            },
          },
          xaxis: {
            categories: labels//['00:00', '01:00', '02:00', '03:00', '05:00', '06:00', '07:00', '08:00', '09:00'],
          }
        },
      
      
      };
/**
 * Sending a request to the log server for the required information
 */
      function sendRequest(){
        let newUrl=props.urlServerLog;
        fetch(newUrl).then(res => res.json(),
        //Boot the information arrays
        setSeries([]),
        setLabels([])
        ).then(data =>{
            
            data[0].forEach(item => {
                setSeries(oldArray => [...oldArray, item.count]);
                setLabels(oldArray => [...oldArray, item.label]);
            },
            );
            setKeyToRefresh(keyToRefresh+1)
           
        })
       
    
     
        }
   //Registration to activate the function from the father
        useImperativeHandle(ref,() => ({
          sendRequest1() {
            sendRequest();
          },
         
        }))
      //Initialize the graph
        useEffect(() => {
          setGraph(<ReactApexChart options={state.options} key={keyToRefresh} series={state.series} type="line" height={350} />)
        }, [labels,series]);
  

  
      return (
        <div 
        // className="backgrouncomp"
        // style={{
          // background:'linear-gradient(120deg,#835c91, #b97ef0,#cf9bfe,#ead1f5)'}} striped bordered hover
          // background:'linear-gradient(120deg,#ead1f5, #FFE4C4,#FFE4C4,#ead1f5)'}} 
        id="chart">
            <h2>{props.title}</h2>
            {graph}
        </div>
      );
  }
export default forwardRef(DivisionHours);

