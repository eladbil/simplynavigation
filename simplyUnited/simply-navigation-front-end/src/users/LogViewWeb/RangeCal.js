import React,{useState,useEffect} from 'react';
import './RangeCal.css';
import DateRangePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import moment from "moment";
// const d = moment(date).format('YYYY-MM-DD')
//https://www.youtube.com/watch?v=tojwQEdI-QI

/**
 * 
 * @author Yedidya Bachar
 * @version 1.0.0
 * @date 29/5/2022
 * @description 
 * Select a date range
 * @param {*} props 
 * setMomentDate1 - set Start Date
 * setMomentDate2 - set End Date
 * 
 * @returns 
 * Calendar of date selection
 */
function RangeCal(props) {
  const [selectedDate1,setSelectedDate1] = useState(null);
  const [selectedDate2,setSelectedDate2] = useState(null);
  return (

    <div >
      <br/>
      <h4>מתאריך</h4>

      {/* <div {...{ display: "flex", justifyContent: "space-between" }}> */}
      <DateRangePicker className='child' 
      // showTimeSelect
     
      opens='left'
      selected={selectedDate1}
      onChange={date =>{ 
        setSelectedDate1(date)
        props.setMomentDate1(moment(date).format('YYYY-MM-DD'))
      }
      }
      dateFormat='dd-MM-yyyy'
      maxDate={new Date()}
      isClearable={true}
        ></DateRangePicker>
        <br/><br/>
        <h4>עד תאריך</h4>
      <DateRangePicker className='child' 
      
      selected={selectedDate2}
      onChange={date => {
        date.setDate(date.getDate() + 1);
        setSelectedDate2(date)
        props.setMomentDate2(moment(date).format('YYYY-MM-DD'))

      }
    }
      dateFormat='dd-MM-yyyy'
      minDate={selectedDate1}
      maxDate={new Date()}
      isClearable={true}
      ></DateRangePicker>
    </div>
  );
}

export default RangeCal;