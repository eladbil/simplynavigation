import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
/**
 * 
 * @author Yedidya Bachar
 * @version 1.0.0
 * @date 29/5/2022
 
 * Translation into 4 languages
 * English, Arabic, Hebrew and Russian
 */
const resources = {
  en: {
    translation: {
        "Language": "English",
        'identification code':'identification code',
        'The picture is not good':'Image is bad',
        'Welcome to Navigation':'Welcome to navigation',
        'Select a building':'Select building',
        'Choose a room':'Choose a room',
        'Select an area':'Select area',
        'Welcome to the navigation system':'Welcome to the navigation system',
        'Uploading a route':'Loading route',
        'Please enter an identification code':'Enter identification code',
        'Take a barcode':'Take a barcode',

        "Enter identification code":'Enter identification code',
        "return":"return",
        "Start navigation":"Start navigation",
        "Navigation to":"Navigation to",
        'Click in the direction of the arrow':'Click in the direction of the arrow',
        "You've arrived":"You've arrived",
        'Update route':'Update route',
        'You are in the area:':'You are in the area:',
        'go to:':'go to:',
        'The instructions:':'Instructions:',


        'Instructions for use Title':'Instructions for use',
        'Instructions for use p1':'1. The barcode should be scanned in the area where you are after the scan and the code will appear in the code box\nAlternatively the code can be written manually in the appropriate place, the code appears above the barcode.',
        'Instructions for use p2':'2. Search for the desired building name in the "Building Search" box, you can also search by partial name.\nThen look for the name of the department / region you want to reach.\nYou can also enter the room number but this does not affect the search.',
        'Instructions for use p3':'3. Click the Upload Track button.\nNote During the route a route can be recalculated.\nNote the instructions on the next screen.',
        'Instructions for use p4':'',
        'Instructions for use p5':'',


                  
        'Instructions for use search1': 'When you move the carousel to the right, you will receive the navigation instructions for the next section on the way to the destination.',
        'Instructions for use search2': 'The rotating arrow will point you in the direction you need to go.',
        'Instructions for use search3': 'Please note that the hospital is full of signs, please note that what is written on the signs is in accordance with the instructions in the navigation section. ',
        'Instructions for use search4': 'Please note that our system works on the basis of compass sensors located on your device and therefore the device should be held vertically while navigating.',
        'Instructions for use search5': 'If the arrow does not rotate, your device may not support the feature in sliding mode.',

//elastic
'next':'next',
'👉':'👉',
'prev':'prev',
'👈':'👈',
'prevArrow':'👈',
'nextArrow':'👉',

'Direction':'Direction',
'New route':'New route',

//feedback
'feedback':'Feedback',
'Close':'Close',
'Send':'Send',
'Rate the navigation experience':'Rate the navigation experience',
'Would you use the navigation system again if necessary?':'Would you use the navigation system again if necessary?',
'No':'No',
'Yes':'Yes',
'Have you completed the route?':'Have you completed the route?',
'We will be happy to receive short feedback in words from you':'We will be happy to receive short feedback in words from you',

'floor':'floor',
'meter':'meter',
'Clicking OK will stop the existing navigation':'Clicking OK will stop the existing navigation',

"Stop the current navigation?":"Stop the current navigation?",
'Cancel':'Cancel',
'OK':'OK',
//dialog
'You must select a building':'You must select a building',
'You must select an area':'You must select an area',
'An identification code must be entered':'An identification code must be entered',
'Your location and destination are identified as the same place' : 'Your location and destination are identified as the same place',
'The requested route has already been displayed':'The requested route has already been displayed',
"Error loading route please make sure all fields are entered correctly":"Error loading route please make sure all fields are entered correctly",
'In order to find a new route, a new identification code must be entered':'In order to find a new route, a new identification code must be entered',
'The code is not recognized':'The code is not recognized', 
'An error occurred while loading the route, the identification code may not have been entered correctly.':'An error occurred while loading the route, the identification code may not have been entered correctly.',
'The identification code entered is:  ':'The identification code entered is:  ',
'Please check that this is the code that appears on the sign.':'Please check that this is the code that appears on the sign.',
'Unknown error':'Unknown error',

        
    }
  },
  ar: {
    translation: {
        "Language": "عربيه",
        'identification code': "كود التعريف",
        'The picture is not good': "الصورة سيئة" ,
        'Welcome to Navigation': "مرحبًا بك في التنقل" ,
        'Select a building': "تحديد مبنى",
        'Choose a room': "اختر غرفة" ,
        'Select an area': "تحديد المنطقة" ,
        'Welcome to the navigation system': "مرحبًا بك في نظام الملاحة" ,
        'Uploading a route': "مسار التحميل" ,
        'Please enter an identification code': "أدخل رمز التعريف",
        'Take a barcode':'خذ الرمز الشريطي',

        "Enter identification code":"أدخل رمز التعريف",
        "return":"عودة",
        "Start navigation":"بدء التنقل",
        "Navigation to":"التنقل إلى",
        'Click in the direction of the arrow':"انقر في اتجاه السهم" ,
        "You've arrived":"لقد وصلت",
        'Update route':'تحديث الطريق',
        'You are in the area:':"أنت في المنطقة:",
        'go to:':'اذهب إلى:',
        'The instructions:':"التعليمات:",
        
        
        'Instructions for use Title':"تعليمات الاستخدام",
        'Instructions for use p1':"1. أدخل رمز المنطقة حيث تكون في المربع الذي تريده ، \ n يظهر الرمز فوق الرمز الشريطي ، \ n وبدلاً من ذلك يمكن كتابة الرمز يدويًا في المكان المناسب.",
        'Instructions for use p2': '2. ابحث عن اسم المبنى المطلوب في مربع "بحث عن المبنى" ، ويمكنك أيضًا البحث بالاسم الجزئي. \ n ثم ابحث عن اسم القسم / المنطقة التي تريد الوصول إليها. \ n يمكنك أيضًا إدخال رقم الغرفة ولكن هذا لا تؤثر على البحث. ',
        'Instructions for use p3':'3. انقر فوق الزر تحميل المسار. \ n ملاحظة أثناء التوجيه يمكن إعادة حساب المسار. \ n لاحظ الإرشادات الموجودة على الشاشة التالية.',
        'Instructions for use p4':'',
        'Instructions for use p5':'',

        // alerts
        'The requested route has already been displayed':"المسار المطلوب تم عرضه بالفعل",
        "Error loading route please make sure all fields are entered correctly":"خطأ في تحميل المسار ، يرجى التأكد من إدخال جميع الحقول بشكل صحيح",

        'Instructions for use search1': "مع كل حركة للعرض الدائري في السهم الأيسر ، ستتلقى إرشادات التنقل للقسم التالي في الطريق إلى الوجهة.",
        'Instructions for use search2': "سيوجهك السهم الدوار في الاتجاه الذي تريد الذهاب إليه.",
        'Instructions for use search3': "يرجى ملاحظة أن المستشفى مليء باللافتات ، يرجى ملاحظة أن ما هو مكتوب على اللافتات يتوافق مع التعليمات الموجودة في قسم التنقل. ",
        'Instructions for use search4': "يرجى ملاحظة أن نظامنا يعمل على أساس مستشعرات البوصلة الموجودة على جهازك ، وبالتالي يجب حمل الجهاز عموديًا أثناء التنقل.",
        'Instructions for use search5': "إذا لم يتم تدوير السهم ، فقد لا يدعم جهازك الميزة في وضع التمرير",
        'next':'التالي',
        '👉':'👈',
        'prev':'السابق',
        '👈':'👉',
        'prevArrow':'👉',
        'nextArrow':'👈',

        'Direction':'الاتجاه',
        'New route':'طريق جديد',
        




//feedback
'feedback':'ردود الفعل',
'Close':'إغلاق',
'Send':'إرسال',
'Rate the navigation experience':'قيم تجربة الملاحة',
'Would you use the navigation system again if necessary?':'هل يمكنك استخدام نظام الملاحة مرة أخرى إذا لزم الأمر؟',
'No':'لا لا',
'Yes':'نعم نعم',
'Have you completed the route?':'هل أكملت الطريق؟',
'We will be happy to receive short feedback in words from you':'يسعدنا تلقي تعليقات قصيرة بالكلمات منك',


'floors':'طوابق',
'meter':'متر',
'Clicking OK will stop the existing navigation':'سيؤدي النقر فوق "موافق" إلى إيقاف التنقل الحالي',
"Stop the current navigation?":"هل تريد إيقاف التنقل الحالي؟",
'Cancel':'يلغي',
'OK':'نعم',


//dialog
'You must select a building':'يجب عليك تحديد مبنى',
'You must select an area':'يجب عليك تحديد منطقة',
'An identification code must be entered':'يجب إدخال رمز التعريف',
'Your location and destination are identified as the same place' : 'تم تحديد موقعك ووجهتك على أنهما نفس المكان',
'The requested route has already been displayed':'المسار المطلوب تم عرضه بالفعل',
"Error loading route please make sure all fields are entered correctly":"خطأ في تحميل المسار ، يرجى التأكد من إدخال جميع الحقول بشكل صحيح",
'In order to find a new route, a new identification code must be entered':'للعثور على مسار جديد ، يجب إدخال رمز تعريف جديد',
'The code is not recognized':'لم يتم التعرف على الرمز', 
'An error occurred while loading the route, the identification code may not have been entered correctly.':'حدث خطأ أثناء تحميل المسار ، ربما لم يتم إدخال رمز التعريف بشكل صحيح.',
'The identification code entered is:  ':'كود التعريف الذي تم إدخاله هو:  ',
'Please check that this is the code that appears on the sign.':'يرجى التحقق من أن هذا هو الرمز الذي يظهر على اللافتة.',
'Unknown error':'خطأ غير معروف',
// حوار







    }},
he: {
    translation: {
        "Language": "עברית",
        'identification code':'קוד זיהוי',
        'The picture is not good':'תמונה גרועה',
        'Welcome to Navigation':'ברוכים הבאים לניווט',
        'Select a building':'בחר בניין',
        'Choose a room':'בחר חדר',
        'Select an area':'בחר אזור',
        'Welcome to the navigation system':'ברוכים הבאים למערכת הניווט',
        'Uploading a route':'טען מסלול',
        'Please enter an identification code': 'הזן קוד זיהוי',
        'Take a barcode':'צלם ברקוד',


        "Enter identification code": 'הזן קוד זיהוי',
         "return": "חזור",
         "Start navigation": "התחל ניווט",
         "Navigation to":"ניווט אל",
         'Click in the direction of the arrow': 'לחץ בכיוון החץ',
         "You've arrived": "הגעת ליעד",
         'Update route':'עדכן מסלול',

         'You are in the area:':'הינך נמצא באזור:',
         'go to:':'לך אל:',
         'The instructions:':'הוראות:',
         'go to2:':'לך אל:',

         'Instructions for use Title':'הוראות שימוש',
         'Instructions for use p1':'1. יש לסרוק את הברקוד באזור בו אתה נמצא.\n לאחר הסריקה הקוד יופיע בתיבת הקוד לחילופין ניתן לכתוב את הקוד באופן ידני במקום המתאים, הקוד מופיע מעל הברקוד.',
         'Instructions for use p2':'2. חפש את שם הבניין הרצוי בתיבת "חיפוש בניין" , ניתן לחפש גם באמצעות שם חלקי.\n לאחר מכן חפש את שם המחלקה/אזור אליו אתה רוצה להגיע.\r\nניתן להכניס גם את מספר החדר אך זה אינו משפיע על החיפוש.',
         'Instructions for use p3':'3. לחץ על כפתור טעינת המסלול. שים לב במהלך המסלול ניתן לחשב מסלול מחדש.  שים/י לב להוראות במסך הבא.',
         'Instructions for use p4':'',
         'Instructions for use p5':'',
      
         
         'Instructions for use search1':'בכל הזזה של הקרוסלה בחץ השמאלי תקבלו את הוראות הניווט למקטע הבא בדרך אל היעד.',
         'Instructions for use search2':'החץ המסתובב יורה לכם את הכיוון אליו אתם צריכים לפנות בדרך.',
         'Instructions for use search3':' שימו לב כי בית החולים מלא בשלטים, שימו לב כי מה שרשום בשלטים תואם לרשום בהוראות של מקטע הניווט. ',
         'Instructions for use search4':'שימו לב כי המערכת שלנו עובדת על בסיס  חיישני מצפן שנמצאים על המכשיר שלכם  ועל כן יש להחזיק את המכשיר בצורה אנכית בזמן הניווט.',
         'Instructions for use search5':'אם החץ איננו מסתובב ייתכן כי מכשירכם איננו תומך בתכונה במצב זזה יש ללכת בעקבות הצבע שמופיע על החץ ומסומן בנקודות בדרך על יד הברקודים.',
         'next':'הבא',
        '👉':'👈',
        'prev':'הקודם',
        '👈':'👉',

        'prevArrow':'👉',
        'nextArrow':'👈',
        'Direction':'כיוון',
        'New route':'מסלול חדש',
        


//feedback
'feedback':'משוב',
'Close':'סגור',
'Send':'שלח',
'Rate the navigation experience':'דרג/י את חווית הניווט',
'Would you use the navigation system again if necessary?':'האם היית משתמש שוב במערכת הניווט במקרה הצורך?',
'No':'לא',
'Yes':'כן',
'Have you completed the route?':'האם סיימת את המסלול?',
'We will be happy to receive short feedback in words from you':'נשמח למשוב קצר במילים',

'floors':'קומות',
'meter':'מטר',
'Clicking OK will stop the existing navigation':'לחיצה על אישור תפסיק את הניווט הקיים',
"Stop the current navigation?":"האם לעצור את הניווט הנוכחי?",
'Cancel':'ביטול',
'OK':'אישור',

//check layout
'You must select a building':'עליך לבחור בניין',
'You must select an area':'עליך לבחור אזור',
'An identification code must be entered':'יש להכניס קוד זיהוי',
'Your location and destination are identified as the same place' : 'מיקומך והיעד מזוהים כאותו מקום',
'The requested route has already been displayed':'המסלול שאתם מבקשים כבר מוצג',
"Error loading route please make sure all fields are entered correctly":"שגיאה בטעינת המסלול אנא וודאו שכל השדות הוכנסו כראוי",
'In order to find a new route, a new identification code must be entered':'על מנת למצוא מסלול חדש יש להזין קוד זיהוי חדש',
'The code is not recognized':"הקוד איננו מזוהה", 
'An error occurred while loading the route, the identification code may not have been entered correctly.':'התרחשה שגיאה בטעינת המסלול, ייתכן כי קוד הזיהוי לא הוכנס במדוייק.',
'The identification code entered is:  ':'קוד הזיהוי שהוכנס הינו:  ',
'Please check that this is the code that appears on the sign.':'אנא בדוק כי זה הקוד המופיע על השלט.',
'Unknown error':'שגיאה לא ידועה',




//dialog layout


       
        
    }
  },
  ru: {
    translation: {
        "Language": 'русский',
        'identification code':'идентификационный код',
        'The picture is not good':'Изображение плохое',
        'Welcome to Navigation':'Добро пожаловать в навигацию',
        'Select a building':'Выберите здание',
        'Choose a room':'Выберите комнату',
        'Select an area':'Выберите область',
        'Welcome to the navigation system':'Добро пожаловать в навигационную систему',
        'Uploading a route':'Загрузка маршрута',
        'Please enter an identification code':'Введите идентификационный код',
        'Take a barcode':'Возьмите штрих-код',

        "Enter identification code":'Введите идентификационный код',
        "return": 'возврат',
        "Start navigation":"Начать навигацию",
        "Navigation to":"Навигация к",
        'Click in the direction of the arrow':'Нажмите в направлении стрелки',
        "You've arrived": "Вы прибыли",
        'Update route':"Обновить маршрут",

        'You are in the area:':'Вы находитесь в районе:',
        'go to:':'перейти к:',
        'The instructions:':'Инструкции',

        'Instructions for use Title':'Instructions for use',
        'Instructions for use p1':'Штрих-код должен быть отсканирован в той области, где вы находитесь после сканирования, и код появится в поле кода.\nКроме того, код можно написать вручную в соответствующем месте, код появится над штрих-кодом.',
        'Instructions for use p2':'2. Найдите нужное название здания в поле «Поиск здания», вы также можете выполнить поиск по частичному имени.\nЗатем найдите название отдела/региона, к которому вы хотите обратиться.\nВы также можете ввести номер комнаты, но это не повлиять на поиск.',
        'Instructions for use p3':'3. Нажмите кнопку «Загрузить трек».\nПримечание. Во время маршрута маршрут можно пересчитать.\nОбратите внимание на инструкции на следующем экране.',
        'Instructions for use p4':'',
        'Instructions for use p5':'',

        // alerts
        'The requested route has already been displayed':'Запрошенный маршрут уже отображен',
        "Error loading route please make sure all fields are entered correctly":"Ошибка загрузки маршрута, убедитесь, что все поля заполнены правильно",

        
        'Instructions for use search1': 'При перемещении карусели вправо вы получите инструкции по навигации для следующего раздела по пути к месту назначения.',
        'Instructions for use search2': 'Вращающаяся стрелка укажет вам направление, в котором вам нужно идти.',
        'Instructions for use search3': 'Обратите внимание, что в больнице полно вывесок, обратите внимание, что то, что написано на вывесках, соответствует инструкциям в разделе навигации. ',
        'Instructions for use search4': 'Обратите внимание, что наша система работает на основе датчиков компаса, расположенных на вашем устройстве, поэтому во время навигации устройство следует держать вертикально.',
        'Instructions for use search5': 'Если стрелка не вращается, возможно, ваше устройство не поддерживает функцию в скользящем режиме»',
        'next':'следующий',
'nextArrow':'👉',
'prev':'предыдущий',
'prevArrow':'👈',

'prevArrow':'👈',
'nextArrow':'👉',
'Direction':'Направление',
'New route':'Новый маршрут',


//feedback
'feedback':'Обратная связь',
'Close':'Закрыть',
'Send':'Отправить',
'Rate the navigation experience':'Оцените навигацию',
'Would you use the navigation system again if necessary?':'Вы бы воспользовались навигационной системой снова, если это необходимо?',
'No':'Нет',
'Yes':'Да',
'Have you completed the route?':'Вы завершили маршрут?',
'We will be happy to receive short feedback in words from you':'Мы будем рады получить от вас краткий словесный отзыв',   
  


'floors':'этажи',
'meter':'метр',
'Clicking OK will stop the existing navigation':'Нажатие OK остановит существующую навигацию.',

"Stop the current navigation?":"Остановить текущую навигацию?",
'Cancel':'Отмена',
'OK':'ХОРОШО',



//dialog
'You must select a building':'Вы должны выбрать здание',
'You must select an area':'Вы должны выбрать область',
'An identification code must be entered':'Необходимо ввести идентификационный код',
'Your location and destination are identified as the same place' : 'Ваше местоположение и пункт назначения определены как одно и то же место',
'The requested route has already been displayed':'Запрошенный маршрут уже отображался',
"Error loading route please make sure all fields are entered correctly":"Ошибка загрузки маршрута, убедитесь, что все поля заполнены правильно",
'In order to find a new route, a new identification code must be entered':'Чтобы найти новый маршрут, необходимо ввести новый идентификационный код',
'The code is not recognized':'Код не распознан', 
'An error occurred while loading the route, the identification code may not have been entered correctly.':'При загрузке маршрута произошла ошибка, возможно неверно введен идентификационный код.',
'The identification code entered is:  ':'Введен идентификационный код: ',
'Please check that this is the code that appears on the sign.':'Пожалуйста, убедитесь, что этот код указан на табличке.',
'Unknown error':'Неизвестная ошибка',

    }
}
};

i18n
  .use(initReactI18next)
  .init({
    resources,
    lng: 'he',
    fallbackLng: 'en'
  });
  

export default i18n;
