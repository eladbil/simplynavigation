import { trace } from "loglevel";
import moment from "moment";
// const url=  'http://localhost:5000';
import { getUrlserverDbAdmin } from "../functionHandle/urls";
// 'http://localhost:28938/api/RestartDB'
import {logErrorReq} from './logError'

let urlbasic = getUrlserverDbAdmin() + 'api/'; //'https://10.100.102.18:5000/api';

// const urlbasic = 'http://localhost:28938/api';
const urlLogInfo =  urlbasic+'addInfoRow';
const urlfeedback = urlbasic+'feedback';


/**
 * 
 * @author Yedidya Bachar
 * @version 1.0.2
 * @date 15/7/2022
 * @description This page has the functions that send messages to the log for statistics
 */

//Defines identification tax for each unique user idClient 
var idClient;
idClient = localStorage.getItem("name");
if(localStorage.getItem("name")=='null' || localStorage.getItem("name")==null ){
    let temp =Math.random();
    
    localStorage.setItem("name", temp);
    idClient = localStorage.getItem("name");
}

//Searches which language the user uses and if there is not one he defines locally for Hebrew
var LangSimpleNav;
LangSimpleNav = localStorage.getItem("LangSimpleNav");

if(LangSimpleNav==null || LangSimpleNav=='null'){
    let temp ='he';
    localStorage.setItem("LangSimpleNav", temp);
    LangSimpleNav = localStorage.getItem("LangSimpleNav");
    }

/**
 * 
 * @returns the current time Israel's for logging
 */
export function retTime(){

    return moment().utcOffset("+03:00").format('YYYY-MM-DD HH:mm:ss')
}

let previousLocations = [];
let infoObj = {
    timestamp: retTime(),
    lang:localStorage.getItem("LangSimpleNav"),
    level: 'Info',
    typeInfo: '',
    idClient:localStorage.getItem("name"),
    data:{}

}

/**
 * Changing the language locally
 * @param {*} lang - The language we want to change to
 */
export function logChangeLang(lang){

    localStorage.setItem("LangSimpleNav", lang);
    infoObj.lang = lang;
    LangSimpleNav= lang;
}

/**
 * Sending a log when logging in to the app via the search page
 * @param {*} data 
 */
export function LogEnterSearch(data){
    let infoObj2 = Object.assign({}, infoObj);
    infoObj2['typeInfo'] = 'EnterSearch';
    infoObj2['timestamp']= retTime();
    fetch(urlLogInfo , {
        method: 'Post' ,
        headers: {
            "Content-Type": "application/json;charset=UTF-8",
        },
        body: JSON.stringify(infoObj2),
    }).catch(err=>{
        logErrorReq(err);
    })
}

/**
 *  Sending a log when logging in to the app via the idToId page
 * @param {*} data 
 */
export function LogEnterIdtoId(data){
    let infoObj2 = Object.assign({}, infoObj);
    infoObj2['typeInfo'] = 'EnterIdtoId';
    infoObj2['timestamp']= retTime();
    fetch(urlLogInfo , {
        method: 'Post' ,
        headers: {
            "Content-Type": "application/json;charset=UTF-8",
        },
        body: JSON.stringify(infoObj2),
    }).catch(err=>{
        logErrorReq(err);
    })
}

/**
 * Click a log when entering the route through the search page
 * @param {*} typeInfo Type of information sent
 * @param {*} data the data to send
 */
export function LogRouteFromSearch(typeInfo ,data){
    previousLocations.push(data.src);
    let infoObj2 = Object.assign({}, infoObj);
    infoObj2['typeInfo'] = typeInfo;//'RouteFromSearch';
    infoObj2['timestamp']= retTime();
    infoObj2['data']=data;
    fetch(urlLogInfo , {
        method: 'Post' ,
        headers: {
            "Content-Type": "application/json;charset=UTF-8",
        },
        body: JSON.stringify(infoObj2),
    }).catch(err=>{
        logErrorReq(err);  
    })
}

/**
 * Click a log when entering the route through the idToId page
 * @param {*} typeInfo Type of information sent
 * @param {*} data the data to send
 */
export function LogRouteFromIdToId(typeInfo , data){
   
    
    let infoObj2 = Object.assign({}, infoObj);
    infoObj2['typeInfo'] = typeInfo;
    infoObj2['timestamp']= retTime();
    infoObj2['data']=data;

    fetch(urlLogInfo , {
        method: 'Post' ,
        headers: {
            "Content-Type": "application/json;charset=UTF-8",
        },
        body: JSON.stringify(infoObj2),
    }).catch(err=>{
        logErrorReq(err);
    })
}

/**
 * send feedback msg
 * @param {*} rateRoute User rating
 * @param {*} isBack Will the user return to using the system
 * @param {*} isFinish Will the user return finished the route if started
 * @param {*} feedbackText Short feedback in words
 */
export function feedbackSend(rateRoute , isBack , isFinish , feedbackText){

    fetch(urlfeedback+`?rateRoute=${rateRoute}&isBack=${isBack}&isFinish=${isFinish}&feedbackText=${feedbackText}&timestamp=${retTime()}`).catch(err=>{
        logErrorReq(err);
    })
}

