import React from 'react';
import { Alert } from 'react-bootstrap';


import {logErrorBasic} from './logError';
/**
 * 
 * @author Yedidya Bachar
 * @version 1.0.1
 * @date 29/8/2022
 
 * Sends an error message to the screen and log if necessary
When the app crashes
Functions as an error catcher
 */

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error) {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }

  componentDidCatch(error, errorInfo) {
    // You can also log the error to an error reporting service
    // logErrorToMyService(error, errorInfo);
    logErrorBasic(errorInfo, error);
  }

  render() {
    if (this.state.hasError) {
      // You can render any custom fallback UI
      return <><h1>Something went wrong.</h1>
      
      <Alert>
התרחשה שגיאה בשרת אנו השגיאה
אנו מצטערים על אי הנוחות הזמנית.
      </Alert>
      
      </>;
    }

    return this.props.children; 
  }
}
export default ErrorBoundary;

















// export default function ErrorBoundary() {
//   const [error,setError]=  useState(null)
//   const [errorInfo,setErrorInfo]=  useState(null);
 


// // static getDerivedStateFromError(error) {
// //   // Update state so the next render will show the fallback UI.
// //   return { hasError: true };
// // }

//   componentDidCatch(error, info){
//     // log the error to our server with loglevel
  
//     setError(error);
//     setErrorInfo(info);

//   }

// if (this.state.errorInfo) {
//   // You can render any custom fallback UI
//   return <h1>Something went wrong.</h1>;
//  }



   


//  return this.props.children;



// }

