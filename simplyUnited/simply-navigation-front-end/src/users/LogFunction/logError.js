import log from 'loglevel'
import { getUrlserverDbAdmin } from "../functionHandle/urls";
import {retTime} from './logInfo'
/**
 * 
 * @author Yedidya Bachar
 * @version 1.0.0
 * @date 29/5/2022
 * 
 * Sends the error message to the log
 
 * @param {*} url - message error
 */



let urlbasic = getUrlserverDbAdmin() + 'logError'
export function logErrorBasic(url , typeError='Unknown'){
    // The error fields sent
    let infoObj2 = {}
    infoObj2['Timestamp']= retTime();
    infoObj2['TypeError'] = typeError;
    infoObj2['ErrorDescription']=url;
    // xhr2.send(JSON.stringify(infoObj2))
    
    // alert(urlbasic)
    fetch(urlbasic , {
        method: 'Post' ,
        headers: {
            "Content-Type": "application/json;charset=UTF-8",
        },
        body: JSON.stringify(infoObj2),
    })
    // log.error('Request error: ' ,url)
}


export function logErrorReq(url , typeError='Request'){
    
    logErrorBasic(url ,typeError )
    // log.error('Request error: ' ,url)
}

// alert(urlbasic)
