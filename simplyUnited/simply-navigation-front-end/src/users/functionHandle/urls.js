/**
 * @author Yedidya Bachar
 * @version 1.0.2
 * @date 15/7/2022
 * @description This file is used to define the addresses of the servers used in this application within functions so that they can be used from other files

 */





const jsonData= require('../../configFiles/configAll.json');

const ip = jsonData.domain;
const portServerDbAdmin = jsonData.portServerDbAdmin;
const portServerDbUsers = jsonData.portServerDbUsers;
const portServerNav = jsonData.portServerNav;

/**
 * @returns URL of the backend (proxy - admin) server
 */
export function getUrlserverDbAdmin(){
    return `https://${ip}:${portServerDbAdmin}/`;
}

/**
 * @returns URL of the backend (proxy - users) server
 */
export function getUrlserverDbUsers(){
    return `https://${ip}:${portServerDbUsers}/`;
}

/**
 * 
 * @returns URL of the fronted server
 */
export function getUrlOrigin(){
    return `https://${ip}:${portServerNav}/`;
}
