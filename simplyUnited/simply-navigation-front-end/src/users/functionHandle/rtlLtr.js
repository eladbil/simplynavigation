/**
 * @author Yedidya Bachar
 * @version 1.0.1
 * @date 31/6/2022
 * @param {*} lang 
 * @description This function adjusts a language to its appropriate rtl or ltr direction
 * @returns 
 */
export function checkRtlLtr(lang){
    const rtl = "rtl";
    const ltr ="ltr";
    switch(lang) {
        case 'en':
          return ltr;
        case 'ru':
            return ltr;
        case 'ar':
            return rtl;

        case 'he':
            return rtl;
        default:
            return rtl;
      }
}