
import {Route, Routes} from "react-router-dom";
import NavigationBar from "./navigation-bar/NavigationBar";
import SideBarData from "./navigation-bar/SideBarData";
import React, {useEffect, useState} from "react";
import HomePage from "./home-page/HomePage";
import "./App.css"



import UpdateInnerForm from "./admin/update-inner/UpdateInnerForm";
import AddAreaForm from "./admin/add-inner-form/AddAreaForm";
import DeleteAreaForm from "./admin/delete-area-form/DeleteAreaForm";
import AddOuterVertexForm from "./admin/add-outer-vertex-form/AddOuterVertexForm";
import UpdateOuterForm from "./admin/update-outer/UpdateOuterForm";
import DeleteOuterForm from "./admin/delete-outer/DeleteOuterForm";



import './users/Trunslate/i18n';
import FirstScreen from "./users/components/ScreensSearch/FirstScreen";
import SecondScreen from './users/components/ScreensSearch/SecondScreen';
import ChooseLang from './users/components/RouteIdToId/ChooseLang';
import RestartScreen from './users/components/ScreensSearch/RestartScreen';
import RouteIdToId from './users/components/RouteIdToId/RouteIdToId';
import LogViewWeb from './users/LogViewWeb/LogViewWeb';
import log from 'loglevel'
import AddEdgeForm from "./admin/add-edge-form/AddEdgeForm";
import UpdateEdgeForm from "./admin/update-edge/UpdateEdgeForm";
import DeletePathForm from "./admin/delete-path/DeletePathForm";
import LoginForm from "./admin/login-procedure/LoginForm";
import CreatNewUser from "./admin/login-procedure/CreatNewUser";
import PasswordRecoveryForm from "./admin/login-procedure/PasswordRecoveryForm";
import PasswordResetForm from "./admin/login-procedure/PasswordResetForm";
import AcceptNewUserForm from "./admin/login-procedure/AcceptNewUserForm";
import GetQRForm from "./admin/get-qr-form/GetQRForm";
import NodesVisualization from "./admin/map-nodes/NodesVisualization";

import { useCookies } from "react-cookie";
import LogoutForm from "./admin/login-procedure/LogoutForm";

import {getUrlserverDbUsers} from './users/functionHandle/urls';
import ErrorBoundary from './users/LogFunction/ErrorBoundary';
import {APIFetchURI} from "./assets/useful-data/APICalls";

// import ErrorBoundary from "./users/LogFunction/ErrorBoundary";

function App() {
 
 
  log.enableAll();

  const rtl = "rtl";
  const ltr ="ltr";
  // document.documentElement.dir = rtl;


  const url = getUrlserverDbUsers();

  const [cookies, setCookie, removeCookie] = useCookies(['session_token']);
    // console.log(cookies);
  const [isAdminConnected, setIsAdminConnected] = useState(false);

    const getAdminStatus = async () => {
      const requestOptions = {
          withCredentials: true,
          credentials: 'include',
      };

      const submitRequest = async (requestOptions) => {
        const response = await fetch(APIFetchURI.isAdmin, requestOptions);
        return await response;
      };

      const result = await submitRequest(requestOptions);

      if (result.ok) {
            let body = await result.json();
            let answer = JSON.parse(JSON.stringify(body));
            setIsAdminConnected(answer.key);
      }
    }

    useEffect(()=> {
        getAdminStatus();
    },[isAdminConnected]);




    return (
      
    <div className="App">
      <ErrorBoundary>
        <NavigationBar isAdminConnected={isAdminConnected}/>
        <Routes>
            <Route path='/' element={<HomePage className="full-height"/>}/>
            <Route path='/get-QR-form' element={<GetQRForm className="full-height"/>}/>
            <Route path='/map-visualization' element={<NodesVisualization className="full-height"/>}/>
            <Route path='/add-area-form' element={<AddAreaForm className="full-height"/>}/>
            <Route path='/update-area-form' element={<UpdateInnerForm/>}/>
            <Route path='/delete-area-form' element={<DeleteAreaForm/>}/>
            <Route path='/add-outer-form' element={<AddOuterVertexForm/>}/>
            <Route path='/update-outer-form' element={<UpdateOuterForm/>}/>
            <Route path='/delete-outer-form' element={<DeleteOuterForm/>}/>
            <Route path='/add-edge-form' element={<AddEdgeForm/>}/>
            <Route path='/update-edge-form' element={<UpdateEdgeForm/>}/>
            <Route path='/delete-edge-form' element={<DeletePathForm/>}/>
            <Route path='/login' element={<LoginForm setAdminConnected={setIsAdminConnected}/>}/>
            <Route path='/login/create-new-user' element={<CreatNewUser/>}/>
            <Route path='/login/password-recovery' element={<PasswordRecoveryForm/>}/>
            <Route path='/login/password-reset' element={<PasswordResetForm/>}/>
            <Route path='/login/accept-new-user' element={<AcceptNewUserForm/>}/>
            <Route path='/logout' element={<LogoutForm setAdminConnected={setIsAdminConnected}/>}/>

      {/* yedidya */}
      
      <Route path='/SecondScreen' element={<SecondScreen/>} />
      <Route path='/search' element={<FirstScreen url ={url} lang='he' heEn = {rtl}/>} />
      <Route path='/he' element={<FirstScreen url ={url} lang='he' heEn = {rtl}/>} />
      <Route path='/en' element={<FirstScreen url ={url} lang='en' heEn = {ltr}/>} />
      <Route path='/ar' element={<FirstScreen url ={url} lang='ar' heEn = {rtl}/>} />
      <Route path='/ru' element={<FirstScreen url ={url} lang='ru' heEn = {ltr}/>} />
      
      {/* <Route path='/ChooseLang' element={<ChooseLang url ={url} lang='he' heEn = {rtl}/>} /> */}
      <Route path='/ChooseLang' element={<ChooseLang lang='he' url ={url} heEn = {rtl}/>} />
      <Route path='/he/getRouteIdToId'  element={<RouteIdToId lang='he' heEn = {rtl}/>} />
      <Route path='/en/getRouteIdToId'  element={<RouteIdToId lang='en' heEn = {ltr}/>} />
      <Route path='/ar/getRouteIdToId'  element={<RouteIdToId lang='ar' heEn = {rtl}/>} />
      <Route path='/ru/getRouteIdToId'  element={<RouteIdToId lang='ru' heEn = {ltr}/>} />
      {/* <Route path='/he/getRouteIdToId' element={<chooseLang heEn = {rtl}/>} /> */}
      {/* <Route path='/he/getRouteIdToId' element={<chooseLang heEn = {rtl}/>} /> */}
      <Route path='/errorNav' element={<RestartScreen url ={url} heEn = {rtl}/>} />

      {/* https://localhost:3000/LogViewWeb */}
      <Route path='/LogViewWeb' element={<LogViewWeb id='logViewWeb'/>} />
      
   
      </Routes>
      </ErrorBoundary>
    </div>
    );
}

export default App;
