/*
This file is in charge of all the connections to the Neo4j database, querys and everything database related.
It starts by opening up a connection to the database with the connection information in config.json and then
maintains an open connection, waiting for functions to be called to be executed.
*/

const neo4j = require('neo4j-driver')
// const fs = require('fs');


// const conFile = require('path').resolve(__dirname, 'config.json');
var details = require('../configFiles/configAll.json');

// var uName = '' //'neo4j'
// var pass = '' //'password';

// var neo4jport = '' //7687;
// var uri = '' //'bolt://localhost:' + neo4jport;
// var ip = ''
// var myDriver = '' //= neo4j.driver(uri, neo4j.auth.basic(uName, pass))


//connect to neo4j
// try {
//     //read the configuration json
//     data = fs.readFileSync(conFile, 'utf8')
// } catch (err) {
//     console.log("Can not read config.json\
//         \nerror message: \n" + err);
// }
//read what's in the json
// var details = JSON.parse(data);
// console.log(constAddresses)
// console.log(details)
// console.log(conFile)
const uName = details.usernameneo4j;
const pass = details.passwordneo4j;
const ip = details.ipneo4j;
const neo4jport = details.portneo4j;
const uri = 'bolt://' + ip + ':' + neo4jport;
const myDriver = neo4j.driver(uri, neo4j.auth.basic(uName, pass))



//check to make sure neo4j connected
verifyConnected()
async function verifyConnected() {
    try {
        await myDriver.verifyConnectivity()
    } catch (err) {
        
// console.log(uName)
// console.log(pass)
// console.log(ip)
// console.log(neo4jport)
        console.log("Can not connect to neo4j database, try restarting the server and checking config.json\
    \nerror message: \n" + err)
    }
}

//closes connection with database when invoked
async function dbCloseConn() {
    //close connection
    // await session.close()
    await myDriver.close()
}
async function testing() {
    var x = 0
    await dbListBuildings().then(ans => {
        x = ans
    })
    console.log(x)
    // console.log(x)
    // await dbGetInBuilding('building 2').then(ans => {
    //     console.log(ans)
    // })
    // await dbGetAreaRooms('building 2', 'מרקחת').then(ans => {
    //     console.log(ans)
    // })
    // dbDijkstraFromDefToName('A01', 'ניתוח פלסטי', 'building 1').then(ans => {
    //     console.log(ans)
    // })
    return x

}
//checks if unique ID exists already or not. If it doesnt exist returns true, else returns false
async function dbCheckDef(def) {
    const session = myDriver.session()
    await session.run("Match (n:in_V {def: \'" + def + "\'})\nreturn n").then(function (result) {
        ans = (result.records.length == 0)
    }).catch(function (err) {
        console.log(err)
        //check later
    }).finally(async function () {
        //close session
        await session.close()
    })
    //return buildings
    return ans;
}
//checks if this areaName is unique inside building. returns true if unique, false if already exists
async function dbUniqueAreaName(building, name) {
    const session = myDriver.session()
    await session.run("match (n:in_V)\
    where n.areaName = \'"+ name + "\' and n.building = '" + building + "'\
    return n").then(function (result) {
        // console.log(result.records.length)
        ans = (result.records.length == 0)
    }).catch(function (err) {
        console.log(err)
        //check later
    }).finally(async function () {
        //close session
        await session.close()
    })
    //return buildings
    return ans;
}
//checks if this outerV is unique. returns true if unique, false if already exists
async function dbUniqueOuterName(name) {
    const session = myDriver.session()
    await session.run("match (n:out_V)\
    where n.name = '"+ name + "'\
    return n").then(function (result) {
        // console.log(result.records.length)
        ans = (result.records.length == 0)
    }).catch(function (err) {
        console.log(err)
        //check later
    }).finally(async function () {
        //close session
        await session.close()
    })
    //return buildings
    return ans;
}

//returns the name of the in_V given it's unique ID
async function dbGetNameWithDef(def) {
    let name = []

    const session = myDriver.session()
    await session.run("Match (n:in_V {def: \'" + def + "\'})\nreturn n.areaName").then(function (result) {
        name = result.records[0]._fields[0]
    }).catch(function (err) {
        console.log(err)
        //check later
    }).finally(async function () {
        //close session
        await session.close()

    })
    //return buildings
    return name;
}
//returns the def of a given area
async function dbGetDefOfArea(building, areaName) {
    let name = []

    const session = myDriver.session()
    await session.run("Match (n:in_V {building: \'" + building + "\', areaName: '" + areaName + "'})\nreturn n.def").then(function (result) {
        name = result.records[0]._fields[0]
    }).catch(function (err) {
        console.log(err)
        //check later
    }).finally(async function () {
        //close session
        await session.close()

    })
    //return buildings
    return name;
}

//returns list of buildings
async function dbListBuildings() {
    //buildings list to return
    let buildings = []
    //connect to neo4j

    const session = myDriver.session()
    //send query
    await session.run("match (n:in_V) return distinct(n.building)").then(function (result) {
        result.records.forEach(
            //get the record
            function (record) {
                // console.log("Good Hello");
                // console.log(record);
                //get its field and for each field
                buildings.push(record._fields[0]);
            }
        )
    }).catch(function (err) {
        // console.log("Bad Hello");
        console.log(err);
        //check later
    }).finally(async function () {
        //close session
        await session.close()
    })
    buildings.sort()
    return buildings;

}
//get list of areas with buildings
async function dbGetInBuilding(build) {
    //areas in building list to return
    let areas = []
    //connect to neo4j
    const session = myDriver.session()
    //send query
    await session.run("match (n:in_V) \
    where n.building = \'"+ build + "\'\
    return n.areaName").then(function (result) {
        result.records.forEach(
            //get the record
            function (record) {
                //get its field and for each field
                areas.push(record._fields[0])
            }
        )
    }).catch(function (err) {
        console.log(err)
        //check later
    }).finally(async function () {
        //close session
        await session.close()

    })
    areas.sort()
    //return areas
    return areas
}
//get list of rooms in area
async function dbGetAreaRooms(building, area) {
    //areas in building list to return
    let rooms = []
    //connect to neo4j
    const session = myDriver.session()
    //send query
    await session.run("match (n:in_V) \
    where n.areaName = \'"+ area + "\' and n.building = '" + building + "' \
    return n.rooms"
    ).then(function (result) {
        result.records.forEach(
            //get the record
            function (record) {

                //get its field and for each field
                rooms = (record._fields[0])
            }
        )
    }).catch(function (err) {
        console.log(err)
        //check later
    }).finally(async function () {
        //close session
        await session.close()
    })
    //return areas
    return rooms
}
//returns list of outer Vs names
async function dbGetOuterVs() {
    //outerVs list to return
    let outers = []
    //connect to neo4j

    const session = myDriver.session()
    //send query
    await session.run("Match (n:out_V) return n.name").then(function (result) {
        result.records.forEach(
            //get the record
            function (record) {
                //get its field and for each field
                outers.push(record._fields[0])
            }
        )
    }).catch(function (err) {
        console.log(err)
        //check later
    }).finally(async function () {
        await session.close()


    })
    //return buildings
    return outers

}
//gets a specific outer v and returns all results
async function dbGetInnerV(building, area) {
    let inner = []
    //connect to neo4j

    const session = myDriver.session()
    //send query
    await session.run("Match (n:in_V) where n.areaName = '" + area + "' and n.building = '" + building + "' \
    return n").then(function (result) {
        inner = result.records[0]._fields[0].properties
    }).catch(function (err) {
        console.log(err)
        //check later
    }).finally(async function () {
        //close session
        await session.close()


    })
    //return buildings
    return inner
}
async function dbGetPath(query) {
    let inner = []
    //connect to neo4j

    const session = myDriver.session()
    //send query
    await session.run(query).then(function (result) {
        inner = result.records[0]._fields[0].properties
    }).catch(function (err) {
        console.log(err)
        //check later
    }).finally(async function () {
        //close session
        await session.close()

    })
    //return buildings
    return inner
}
/*
returns shortest path from source to target
given:
source_id (unique area id)
target_area (destination building name)
target_build (destination area name)


returns shortest path to destination in the following array format

[
    {
        "name":"Start area","directions":
        [
            ["direction","classifiaction","distance in meters","free text"]
            ["direction","classifiaction","distance in meters","free text"]
        ]
    },
    {
        "name":"Start area","directions":
        [
            ["direction","classifiaction","distance in meters","free text"]
        ]
    }
]
example


[
    {
        "name":"Pediatric surgery","directions":[
            ["East","Hallway","5","Head east and exit the hallway"]
            ["Up","Elevator","2","Go two stories up the elevator"]
        ]
    },
    {
        "name":"Orthopedics, children, adolescents, stoma","directions":
        [
            ["East","Lobby","8","Go east in the 8 meter lobby, for the reception"]
        ]
    }
]

*/
async function dbDijkstraFromDefToName(source_id, target_area, target_build) {
    let path = []
    //connect to neo4j
    const session = myDriver.session()
    //var driver = neo4j.driver('bolt://localhost', neo4j.auth.basic('neo4j', 'password'))
    /*
    Original
    console.log("MATCH (source:in_V {def: '" + source_id + "'}), \
    (target:in_V{areaName:'"+ target_area + "', building:'" + target_build + "'}) \
    CALL apoc.algo.dijkstra(source,target, '>', 'weight')\
    YIELD path, weight \
    RETURN relationships(path) as path, weight")
    */
    /*
    New
    "MATCH (source:in_V {def: '" + source_id + "'}), \
     (target:in_V{areaName:'"+ target_area + "', building:'" + target_build + "'}) \
     CALL apoc.algo.dijkstra(source,target, '>', 'weight')\
     YIELD path, weight \
     RETURN path as path, weight"
    */
    await session.run("MATCH (source:in_V {def: '" + source_id + "'}), \
    (target:in_V{areaName:'"+ target_area + "', building:'" + target_build + "'}) \
    CALL apoc.algo.dijkstra(source,target, '>', 'weight')\
    YIELD path, weight \
    RETURN path as path, weight").then(function (result) {
        result.records.forEach(
            //get the record
            function (record) {
                //get its field and for each field
                Object.values(record._fields[0])[2].forEach(function (seg) {
                    var name = seg.start.properties.areaName
                    var rel = seg.relationship
                    //delete the weight property
                    delete rel.properties['weight']
                    //put results of dijkstra into paths keeping the order
                    var props = Object.values(rel.properties)
                    //if it's an outer V, don't save its name
                    if (name == null) {
                        var tempDir = path[path.length - 1]["directions"]
                        tempDir = tempDir.concat(props)
                        path[path.length - 1]["directions"] = tempDir
                        console.log(tempDir)
                        //but if its in innerv, save it's name
                    } else {
                        path = path.concat({ "name": name, "directions": props })
                    }
                })
            }
        )
    }).catch(function (err) {
        //check later
        console.log(err)
    }).finally(async function () {
        //close session
        await session.close()
    })
    //return path
    // console.log("final")
    // console.log(path)
    return path
}

/*
returns shortest path from source to target
given:
source_id (unique area id of source)
source_id (unique area id of target)
*/
async function dbDijkstraFromDefToDef(source_id, target_id) {
    let path = []
    //connect to neo4j
    const session = myDriver.session()
    await session.run("MATCH (source:in_V {def: '" + source_id + "'}), \
    (target:in_V{def: '" + target_id + "'}) \
    CALL apoc.algo.dijkstra(source,target, '>', 'weight')\
    YIELD path, weight \
    RETURN path as path, weight").then(function (result) {
        result.records.forEach(
            //get the record
            function (record) {
                //get its field and for each field
                Object.values(record._fields[0])[2].forEach(function (seg) {
                    var name = seg.start.properties.areaName
                    var rel = seg.relationship
                    //delete the weight property
                    delete rel.properties['weight']
                    //put results of dijkstra into paths keeping the order
                    var props = Object.values(rel.properties)
                    //if it's an outer V, don't save its name
                    if (name == null) {
                        var tempDir = path[path.length - 1]["directions"]
                        tempDir = tempDir.concat(props)
                        path[path.length - 1]["directions"] = tempDir
                        console.log(tempDir)
                        //but if its in innerv, save it's name
                    } else {
                        path = path.concat({ "name": name, "directions": props })
                    }
                })
            }
        )
    }).catch(function (err) {
        //check later
        console.log(err)
    }).finally(async function () {
        //close session
        await session.close()
    })
    //return path
    // console.log("final")
    // console.log(path)
    return path
}
//adds an outer v, returns true if succeded, else false
async function dbAddOuterV(name, allowEntry) {
    var worked = true

    const session = myDriver.session()
    //create the string of rooms
    //send query
    await session.run("Create (n:out_V {allowEntry: " + allowEntry + ", name: \"" + name + "\"})").then(function (result) {
        //ans = (result.records.length == 0)
    }).catch(function (err) {
        console.log(err)
        worked = false
        //check later
    }).finally(async function () {
        //close session
        await session.close()

    })
    return worked
}
//gets if you're allowed entry to an outer V
async function dbGetAllowEntryOutV(name) {
    //if allow entry or not
    let allow = 0
    //connect to neo4j

    const session = myDriver.session()
    //send query
    await session.run("Match (n:out_V) \
     where n.name = '"+ name + "' \
     return n.allowEntry").then(function (result) {
        //get if allowed entry
        allow = result.records[0]._fields[0].low
    }).catch(function (err) {
        console.log(err)
        //check later
    }).finally(async function () {
        //close session
        await session.close()
    })
    //return if allowed entry
    return allow
}
//adds an inner v, returns true if succeded, else false
async function dbCreateInV(def, allowEntry, aName, building, floor, rooms) {
    var worked = true

    const session = myDriver.session()
    //create the string of rooms
    let roomStr = "["
    for (let i = 0; i < rooms.length; i++) {
        if (i != 0) {
            roomStr = roomStr + ","
        }
        roomStr = roomStr + "\"" + rooms[i] + "\""
    }
    roomStr = roomStr + "]"
    //send query
    await session.run("CREATE (n:in_V {rooms: " + roomStr + "," +
        "areaName:\"" + aName + "\"," +
        "def: \'" + def + "\'," +
        "floor:" + floor + "," +
        "allowEntry:" + allowEntry + "," +
        "building:\"" + building + "\"})").then(function (result) {
            //ans = (result.records.length == 0)
        }).catch(function (err) {
            console.log(err)
            worked = false
            //check later
        }).finally(async function () {
            //close session
            await session.close()

        })
    return worked
}
//create edge based on query
async function dbCreateEdge(query) {
    var worked = true

    const session = myDriver.session()
    //send query
    await session.run(query).then(function (result) {
    }).catch(function (err) {
        console.log(err)
        worked = false
        //check later
    }).finally(async function () {
        //close session
        await session.close()

    })
    return worked
}
//deletes outer V and all its edges
async function dbDeleteOuterV(outer) {
    let relIn = "MATCH ()-[r]->(n:out_V{name: '" + outer + "'}) delete r"
    let relOut = "MATCH (n:out_V{name: '" + outer + "'})-[r]->() delete r"
    let delOut = "match (n:out_V{name: '" + outer + "'}) delete n"
    var worked = true

    const session = myDriver.session()

    //delete all entering edges
    await session.run(relIn).then(function (result) {
    }).catch(function (err) {
        console.log(err)
        worked = false
    })
    //delete all exiting edges
    if (worked) {
        await session.run(relOut).then(function (result) {
        }).catch(function (err) {
            console.log(err)
            worked = false
        })
    }
    //delete V
    if (worked) {
        await session.run(delOut).then(function (result) {
        }).catch(function (err) {
            console.log(err)
            worked = false
        })
    }


    return worked
}
//deletes inner v and all its edgers
async function dbDeleteInnerV(building, area) {
    let relIn = "MATCH ()-[r]->(n:in_V{areaName: '" + area + "', building: '" + building + "'}) delete r"
    let relOut = "MATCH (n:in_V{areaName: '" + area + "', building: '" + building + "'})-[r]->() delete r"
    let delIn = "MATCH (n:in_V{areaName: '" + area + "', building: '" + building + "'}) delete n"
    var worked = true

    const session = myDriver.session()

    //delete all entering edges
    await session.run(relIn).then(function (result) {
    }).catch(function (err) {
        console.log(err)
        worked = false
    })
    //delete all exiting edges
    if (worked) {
        await session.run(relOut).then(function (result) {
        }).catch(function (err) {
            console.log(err)
            worked = false
        })
    }
    //delete V
    if (worked) {
        await session.run(delIn).then(function (result) {
        }).catch(function (err) {
            console.log(err)
            worked = false
        })
    }


    return worked
}
async function dbDeleteEdge(query) {
    var worked = true

    const session = myDriver.session()
    //send query
    await session.run(query).then(function (result) {
    }).catch(function (err) {
        console.log(err)
        worked = false
        //check later
    }).finally(async function () {
        //close session

    })
    return worked
}
/*
returns if the query returns anything or not
0 means returned nothing
1 means returned something
-1 means error
*/
async function dbDoEdgesExist(query) {
    var ret = 1

    const session = myDriver.session()
    //send query
    await session.run(query).then(function (result) {
        //check if theres a path
        if (result.records.length == 0) {
            ret = 0
        }
    }).catch(function (err) {
        console.log(err)
        ret = -1
        //check later
    }).finally(async function () {
        //close session

    })
    return ret
}
//update query for outer, returns if the update was a success of not
async function dbUpdateOuter(ogName, newName, allowEntry) {
    var worked = true

    const session = myDriver.session()
    //send query
    await session.run("Match (n:out_V) \
    where n.name = '"+ ogName + "' \
    set n.name = '"+ newName + "' \
    set n.allowEntry = "+ allowEntry + "").then(function (result) {
    }).catch(function (err) {
        console.log(err)
        worked = false
    }).finally(async function () {
        //close session

    })
    return worked
}
//updates innerV
async function dbUpdateInV(oldArea, oldBuilding, def, allowEntry, aName, building, floor, rooms) {
    var worked = true
    const session = myDriver.session()
    let roomStr = "["
    for (let i = 0; i < rooms.length; i++) {
        if (i != 0) {
            roomStr = roomStr + ","
        }
        roomStr = roomStr + "\"" + rooms[i] + "\""
    }
    roomStr = roomStr + "]"
    //send query
    await session.run("Match (n:in_V {areaName: '" + oldArea + "', building: '" + oldBuilding + "'}) \
    set n.def = '"+ def + "'\n\
    set n.areaName = '"+ aName + "'\n\
    set n.building = '"+ building + "'\n\
    set n.floor = "+ floor + "\n\
    set n.rooms = "+ roomStr + "\n\
    set n.allowEntry = "+ allowEntry + "").then(function (result) {
    }).catch(function (err) {
        console.log(err)
        worked = false
    }).finally(async function () {
        //close session
        await session.close()
    })
    return worked
}
/*
builds query for creating an edge. Returns query to check if edge exists, and query to create edge
recives string type (of path, entrance, exit, trail, path), 
string src (name/area name)
string dest (name/area name)
int weight (of path)
string building
list string directions[] (directions, up down right, left for each microdirection)
list string classification[] (hallway, stairs, loby... for each microdirection)
list int distances[] (5, 10, 20 for each microdirection)
list string freeText[] (go left fo 20 meters at hallway, ... for each microdirection)
*/
function dbBuildCreateEdgeQuery(type, src, dest, weight, building, directions, classification, distances, freeText) {
    let match = ""
    let where = ""
    switch (type) {
        case "path":
            match = "MATCH (a:in_V), (b:in_V) "
            where = "WHERE a.areaName = \"" + src + "\" AND b.areaName = \"" + dest + "\" " +
                "AND a.building = \"" + building + "\" AND b.building = \"" + building + "\" "
            break
        case "trail":
            match = "MATCH (a:out_V), (b:out_V) "
            where = "WHERE a.name = \"" + src + "\" AND b.name = \"" + dest + "\" "
            break
        case "entrance":
            match = "MATCH (a:out_V), (b:in_V) "
            where = "WHERE a.name = \"" + src + "\" AND b.areaName = \"" + dest + "\" " +
                "AND b.building = \"" + building + "\" "
            break
        case "exit":
            match = "MATCH (a:in_V), (b:out_V) "
            where = "WHERE a.areaName = \"" + src + "\" AND b.name = \"" + dest + "\" " +
                "AND a.building = \"" + building + "\" "
            break
    }
    let create = "CREATE (a)-[r:" + type + " {"
    for (let i = 0; i < directions.length; i++) {
        if (i != 0) {
            create += ", "
        }
        let str = "p" + i + ": [\'" + directions[i] + "\', \"" + classification[i] + "\", " + "\"" + distances[i] + "\", " + "\"" + freeText[i] + "\""
        str = str + "]"
        create += str
    }
    create += "}]->(b) "
    let setWeight = "set r.weight = " + weight + " "

    let query = match + where + create + setWeight

    //check if there is an edge there already, if so, do not add an edge
    match = ""
    let r = ""
    let part2 = ""
    switch (type) {
        case "path":
            match = "MATCH (n:in_V {areaName: '" + src + "', building: '" + building + "'})"
            part2 = "(m:in_V {areaName: '" + dest + "', building: '" + building + "'}) "
            break
        case "trail":
            match = "MATCH (n:out_V {name: '" + src + "'})"
            part2 = "(m:out_V {name: '" + dest + "'}) "
            break
        case "entrance":
            match = "MATCH (n:out_V {name: '" + src + "'})"
            part2 = "(m:in_V {areaName: '" + dest + "', building: '" + building + "'}) "
            break
        case "exit":
            match = "MATCH (n:in_V {areaName: '" + src + "', building: '" + building + "'})"
            part2 = "(m:out_V {name: '" + dest + "'}) "
            break
    }
    r = "-[r:" + type + "]->"
    let exists = match + r + part2 + "return r"
    return [exists, query]

}
/*
builds queries for deleting edges
recives string type (of path, entrance, exit, trail, path), 
string src (name/area name)
string dest (name/area name)
string building (empty string if its a trail (between two outers))
returns exists checks if there exists an edge alredy, query is the delete query
*/

function dbBuildDeleteEdgeQuery(type, src, dest, building) {
    let match = ""
    let r = ""
    let part2 = ""
    let del = "delete r"
    switch (type) {
        case "path":
            match = "MATCH (n:in_V {areaName: '" + src + "', building: '" + building + "'})"
            part2 = "(m:in_V {areaName: '" + dest + "', building: '" + building + "'}) "
            break
        case "trail":
            match = "MATCH (n:out_V {name: '" + src + "'})"
            part2 = "(m:out_V {name: '" + dest + "'}) "
            break
        case "entrance":
            match = "MATCH (n:out_V {name: '" + src + "'})"
            part2 = "(m:in_V {areaName: '" + dest + "', building: '" + building + "'}) "
            break
        case "exit":
            match = "MATCH (n:in_V {areaName: '" + src + "', building: '" + building + "'})"
            part2 = "(m:out_V {name: '" + dest + "'}) "
            break
    }
    r = "-[r:" + type + "]->"

    let query = match + r + part2 + del
    let exists = match + r + part2 + "return r"
    return [exists, query]
}
//builds the query to get a path
function dbBuildFillInEdgeQuery(type, building, source, dest) {
    var match = ""
    switch (type) {
        case "path":
            //(n:in_V {areaName: 'סרטן'})-[r:path]->()
            match = "MATCH (a:in_V {areaName: '" + source + "', building: '" + building + "'})-[r:path]-> \
            (b:in_V {areaName: '"+ dest + "',  building: '" + building + "'}) \
            return r"
            break
        case "trail":
            match = "MATCH (a:out_V {name:  '" + source + "'}) -[r:trail]-> (b:out_V {name:'" + dest + "'})\
            return r"
            break
        case "entrance":
            match = "MATCH (a:out_V {name:  '" + source + "'}) -[r:entrance]-> \
            (b:in_V {areaName: '"+ dest + "',  building: '" + building + "'}) \
            return r"
            break
        case "exit":
            match = "MATCH (a:in_V {areaName: '" + source + "', building: '" + building + "'})-[r:exit]-> \
            (b:out_V {name:'"+ dest + "'})\
            return r"
            break
    }
    return match
}
/*
builds queries neccessary for  updating edges
recieves:
string type (exit, entrance, trail, path)
string src (src vertex name)
string dest (destination vertex name)
string building (the building of the path, empty string if trail)
int weight (of path),
list string directions[] (directions, up down right, left for each microdirection),
list string classification[] (hallway, stairs, loby... for each microdirection),
list int distances[] (5, 10, 20 for each microdirection),
list string freeText[] (go left fo 20 meters at hallway, ... for each microdirection)

return list string [deleteQuery, addQuery]
*/
function dbBuildUpdateEdgeQuery(type, src, dest, building, weight, directions, classification, distances, freeText) {
    let match = ""
    let where = ""
    switch (type) {
        case "path":
            match = "MATCH (a:in_V), (b:in_V) "
            where = "WHERE a.areaName = \"" + src + "\" AND b.areaName = \"" + dest + "\" " +
                "AND a.building = \"" + building + "\" AND b.building = \"" + building + "\" "
            break
        case "trail":
            match = "MATCH (a:out_V), (b:out_V) "
            where = "WHERE a.name = \"" + src + "\" AND b.name = \"" + dest + "\" "
            break
        case "entrance":
            match = "MATCH (a:out_V), (b:in_V) "
            where = "WHERE a.name = \"" + src + "\" AND b.areaName = \"" + dest + "\" " +
                "AND b.building = \"" + building + "\" "
            break
        case "exit":
            match = "MATCH (a:in_V), (b:out_V) "
            where = "WHERE a.areaName = \"" + src + "\" AND b.name = \"" + dest + "\" " +
                "AND a.building = \"" + building + "\" "
            break
    }
    let create = "CREATE (a)-[r:" + type + " {"
    for (let i = 0; i < directions.length; i++) {
        if (i != 0) {
            create += ", "
        }
        let str = "p" + i + ": [\'" + directions[i] + "\', \"" + classification[i] + "\", " + "\"" + distances[i] + "\", " + "\"" + freeText[i] + "\""
        str = str + "]"
        create += str
    }
    create += "}]->(b) "
    let setWeight = "set r.weight = " + weight + " "

    let addQuery = match + where + create + setWeight

    //delete old edge
    let r = ""
    let part2 = ""
    let del = "delete r"
    switch (type) {
        case "path":
            match = "MATCH (n:in_V {areaName: '" + src + "', building: '" + building + "'})"
            part2 = "(m:in_V {areaName: '" + dest + "', building: '" + building + "'}) "
            break
        case "trail":
            match = "MATCH (n:out_V {name: '" + src + "'})"
            part2 = "(m:out_V {name: '" + dest + "'}) "
            break
        case "entrance":
            match = "MATCH (n:out_V {name: '" + src + "'})"
            part2 = "(m:in_V {areaName: '" + dest + "', building: '" + building + "'}) "
            break
        case "exit":
            match = "MATCH (n:in_V {areaName: '" + src + "', building: '" + building + "'})"
            part2 = "(m:out_V {name: '" + dest + "'}) "
            break
    }
    r = "-[r:" + type + "]->"

    let deleteQuery = match + r + part2 + del

    return [deleteQuery, addQuery]
}
/*
Creates query to display
rels, level: boolean, if we are doing a query for levels and relationships
build and floor are the building and the floor, if floor is relevant
*/
function dbQueryForDisplay(rels, level, building, floor) {
    if (rels &&level) {
        return "Match((n:in_V)-[r]->(m:in_V)) where n.floor = " + floor + " and n.building = '" + building + "' and m.floor = " + floor + " and m.building = '" + building + "' return r";
    } else if (!rels && level) {
        return "Match(n:in_V) where n.floor = " + floor + " and n.building = '" + building + "' return n"
    }
    else if (rels && !level){
        return "Match((n:in_V)-[r]->(m:in_V)) where n.building = '" + building + "' and m.building = '" + building + "' return r";
    } else if (!rels && !level) {
        return "Match(n:in_V) where n.building = '" + building + "' return n"
    }
}

/*
recieves query and runs it for the display
*/
async function dbForDisplayRunner(query) {
    let inner = []
    //connect to neo4j

    const session = myDriver.session()
    //send query
    await session.run(query).then(function (result) {
        inner = result.records
    }).catch(function (err) {
        console.log(err)
        //check later
    }).finally(async function () {
        //close session
        await session.close()

    })
    //return buildings
    return inner
}
/*
get specific inner by name and building for display
*/
async function dbForDisplaySpecificInnerByName(areaName, building) {
    var query = "Match(n:in_V) where n.building = '" + building + "' and n.areaName = '" + areaName + "' return n"
    let inner = []
    //connect to neo4j

    const session = myDriver.session()
    //send query
    await session.run(query).then(function (result) {
        inner = result.records
    }).catch(function (err) {
        console.log(err)
        //check later
    }).finally(async function () {
        //close session
        await session.close()

    })
    //return buildings
    return inner
}
/*
get specific inner by def for display
*/
async function dbForDisplaySpecificInnerByDef(def) {
    var query = "Match(n:in_V) where n.def = '" + def + "' return n"
    let inner = []
    //connect to neo4j

    const session = myDriver.session()
    //send query
    await session.run(query).then(function (result) {
        inner = result.records
    }).catch(function (err) {
        console.log(err)
        //check later
    }).finally(async function () {
        //close session
        await session.close()

    })
    //return buildings
    return inner
}
/*
get specific outerV for display
*/
async function dbForDisplaySpecificOuterV(name) {
    var query = "Match(n:out_V) where n.name = '" + name + "' return n"
    let inner = []
    //connect to neo4j

    const session = myDriver.session()
    //send query
    await session.run(query).then(function (result) {
        inner = result.records
    }).catch(function (err) {
        console.log(err)
        //check later
    }).finally(async function () {
        //close session
        await session.close()

    })
    //return buildings
    return inner
}

//recives type of relationship, source, dest, building, if relevant, and rels which is true if we're
//looking for the relationships, and false if we're looking for the node
async function dbForDisplayTwoNodesWithRels(type, src, dest, building, rels) {
    var match = ''
    var where = ''
    between = ', '
    if(rels == true) {
        between = "<-[r]->"
    }
    switch (type) {
        case "path":
            match = "MATCH (a:in_V)"+between+"(b:in_V) "
            where = "WHERE a.areaName = \"" + src + "\" AND b.areaName = \"" + dest + "\" " +
                "AND a.building = \"" + building + "\" AND b.building = \"" + building + "\" "
            break
        case "trail":
            match = "MATCH (a:out_V)"+between+"(b:out_V) "
            where = "WHERE a.name = \"" + src + "\" AND b.name = \"" + dest + "\" "
            break
        case "entrance":
            match = "MATCH (a:out_V)"+between+"(b:in_V) "
            where = "WHERE a.name = \"" + src + "\" AND b.areaName = \"" + dest + "\" " +
                "AND b.building = \"" + building + "\" "
            break
        case "exit":
            match = "MATCH (a:in_V)"+between+"(b:out_V) "
            where = "WHERE a.areaName = \"" + src + "\" AND b.name = \"" + dest + "\" " +
                "AND a.building = \"" + building + "\" "
            break
    }
    var query = match+where+"return *"
    // console.log(query)
    let ans = []
    //connect to neo4j

    const session = myDriver.session()
    //send query
    await session.run(query).then(function (result) {
        ans = result.records
    }).catch(function (err) {
        console.log(err)
        //check later
    }).finally(async function () {
        //close session
        await session.close()

    })
    //return buildings
    return ans

}
//returns json 
async function dbDownloadAsJson() {
    let myJson = ''
    const session = myDriver.session()
    await session.run("CALL apoc.export.json.all(null, \
        {useTypes:True, stream: true} )\
        YIELD file, nodes, relationships, properties, data \
        RETURN file, nodes, relationships, properties, data \
    ").then(result => {
        //.records[0]._fields
        myJson = result.records[0]._fields[4]
    }).catch(function (err) {
        console.log(err)
        //check later
    }).finally(async function () {
        //close session

    })
    return myJson

}
/*
async function dbUploadJson(jsonFile) {
    let myJson = ''
    const session = myDriver.session()
    await session.run("CALL apoc.import.json("+JSON.parse(jsonFile)+")"
    ).then(res => {
        //.records[0]._fields
        console.log(res)
    }).catch(function (err) {
        console.log(err)
        //check later
    }).finally(async function () {
        //close session

    })
    return myJson
}
*/
// testing2()
async function testing2() {
    // await dbUniqueAreaName("building 1", "fakePlc").then(res => {
    //     console.log(res)
    // })
    // await dbUniqueOuterName("fakeOuter").then(res => {
    //     console.log(res)
    // })
    await dbDownloadAsJson().then(res => {
        console.log(res)
    })
    // var fs = require('fs')
    // var file = fs.readFileSync('../viewModel/hospitalMap', 'utf8');
    // console.log(file)
    // await dbUploadJson(file)
}

module.exports = {
    dbCloseConn, dbCheckDef, dbUniqueAreaName, dbUniqueOuterName,
    dbGetNameWithDef, dbGetDefOfArea,
    dbListBuildings, dbGetInBuilding, dbGetAreaRooms,
    dbGetOuterVs, dbGetInnerV, dbGetPath, dbDijkstraFromDefToName,
    dbDijkstraFromDefToDef,
    dbAddOuterV, dbGetAllowEntryOutV, dbCreateInV,
    dbCreateEdge, dbDeleteOuterV, dbDeleteInnerV,
    dbDeleteEdge, dbDoEdgesExist, dbUpdateOuter, dbUpdateInV,
    dbBuildCreateEdgeQuery, dbBuildDeleteEdgeQuery,
    dbBuildFillInEdgeQuery, dbBuildUpdateEdgeQuery, 
    dbQueryForDisplay,dbForDisplayRunner,
    dbForDisplaySpecificInnerByName,dbForDisplaySpecificInnerByDef,
    dbForDisplaySpecificOuterV, dbForDisplayTwoNodesWithRels,
    dbDownloadAsJson
};
