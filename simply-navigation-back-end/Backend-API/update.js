/*
Functions used for updating information in the database, like checkpoints, areas and paths.
*/
var dataConn = require('../DB-Interface/basic_db_queries')

tester()
async function tester() {
    // let map ={"hello": 1}
    // console.log(map["hello"])
    // console.log("here")
    // console.log(await jsGetListOfBuilding())
    // console.log(await jsAddOuter('fakeOuter', 0))
    // console.log(await jsSetSrc('exit', "building 1"))
    // console.log(await jsSetDest('exit', "building 1"))
    // console.log(await jsGetOuterByName("fakeOuter"))
    // console.log(await jsUpdateOuter("fakeOuterby", "fakeOuter", 1))
    // console.log(await jsGetListOfAreas("building 1"))
    // console.log(await jsGetAreaDetails("building 1", "fakePlc"))
    // console.log(await jsUpdateInner("building 1", "fakePlc", "building 1", "fakePlc", 1, 'fake', 1, [ 'room 9', 'room 10' ]))
    // console.log(await jsGetPathDetails("building 1", "path", "fakePlc", "fake"))
    // console.log(await jsUpdatePath("path","fakePlc", "fake","building 1" ,200 , ["right", "left"], ["hallway", "elevator"], [10, 12],  [ 'right hallway 20 meters', 'down elevator 10 mtrs' ]))
    // console.log(await jsGetPathDetails("building 1", "path", "fakePlc", "fake"))


    //unsyncronized tests
    /*
        jsGetListOfBuilding().then(succ => {
    console.log(succ)
    }
        )
    jsSetSrc('exit', "building 1").then(succ => {
        console.log(succ)
        }
            )
    jsSetDest('exit', "building 1").then(succ => {
        console.log(succ)
        }
            )
   jsGetOuterByName("fakeOuter").then(succ => {
    console.log(succ)
    }
        )
    */
}

//returns list of buildings
async function jsGetListOfBuilding(map) {
    let areas = []
    await dataConn.dbListBuildings().then(function (res) {
        areas = res
        console.log(areas)
    })
    return areas
}

/*
*gets list of places for sources in pathbuilder
* recives typePath which is the type of path it is
    meaning entrance, exit, path, trail
    and building which is either the building name or an empty string
*/
async function jsSetSrc(map) {
    let typePath = map["typePath"]
    let building = map["building"]


    var areas = []
    //if were starting from inside a building
    if (typePath == "path" || typePath == 'exit') {
        //get a list of areas inside that building
        await dataConn.dbGetInBuilding(building).then(res => {
            res.forEach(thing => {
                areas.push(thing)
            })
        })
        //if it's an outer v, get list of outer v names
    } else {
        await dataConn.dbGetOuterVs().then(res => {
            res.forEach(thing => {
                areas.push(thing)
            })
        })
    }
    //set options to be this
    return areas
}

/*
gets list of of destinations
* recives typePath which is the type of path it is
    meaning entrance, exit, path, trail
    and building which is either the building name or an empty string
*/
async function jsSetDest(map) {
    let typePath = map["typePath"]
    let building = map["building"]

    var areas = []
    //if were ending from inside a building
    if (typePath == "path" || typePath == 'entrance') {
        //get a list of areas inside that building
        await dataConn.dbGetInBuilding(building).then(res => {
            res.forEach(thing => {
                areas.push(thing)
            })
        })
        //if it's an outer v, get list of outer v names
    } else {
        await dataConn.dbGetOuterVs().then(res => {
            res.forEach(thing => {
                areas.push(thing)
            })
        })
    }
    //set options to be this
    return areas
}
//translate selected words
function translater(map) {
    let word = map['word']

    switch (word) {
        case "Up":
            return "למעלה"
        case "Down":
            return "למטה"
        case "Right":
            return "ימינה"
        case "Left":
            return "שמאלה"
        case "Hallway":
            return "מזדרון"
        case "Elevator":
            return "מעלית"
        case "Stairs":
            return "מדרגות"
        case "Loby":
            return "לובי"
    }
}
//list of outer vs
async function jsGetListOfOuterV(map) {

    let areas = []
    await dataConn.dbGetOuterVs().then(res => {
        res.forEach(thing => {
            areas.push(thing)
        })
    })
    return areas
}

/*
gets information about specific outerV by name
recives string name of outerV, returns information about it

returns map {"name": outerName, "entry": allow}

such that:
name = outerVname, entry = int allowEntry (1 or 0)]
*/
async function jsGetOuterByName(map) {
    let outerName = map['outerName']
    let allow = 0
    //get whether you are allowed entry into that outerV
    await dataConn.dbGetAllowEntryOutV(outerName).then(res => {
        allow = res
    })
    return { "name": outerName, "entry": allow }
}
/*
updates outer v
recives:
string ogName (the original name of the outer v)
string newName
int allowEntry (1 or 0)
returns message if it worked or not
*/
async function jsUpdateOuter(map) {
    let ogName = map['ogName']
    let newName = map['newName']
    let allowEntry = map['allowEntry']
    let ret = {}
    let unique = true
    //assuming we are changing the name, check to see that the new name is unique
    if (newName != ogName) {
        await dataConn.dbUniqueOuterName(newName).then(res => {
            unique = res
        })
    }
    //if it has the same name/doesnt exist another one with that name, change it
    if (unique) {
        await dataConn.dbUpdateOuter(ogName, newName, allowEntry).then(worked => {
            if (worked) {
                ret['message'] = "העידכון בוצע בהצלחה"
            } else {
                ret['message'] = "הפעולה נכשלה, העידכון לא בוצע"
            }
        })
    //if there already exists an outer with the same name, error
    } else {
        ret['message'] = 'קיים כבר נקודת ציון עם עם שם זה, נסה שם אחר'
    }
    return ret
}
//gets list of areas from within a building
//recieves string building which is the name of building we want the areas of
async function jsGetListOfAreas(map) {
    let building = map['building']

    var areas = []
    await dataConn.dbGetInBuilding(building).then(res => {
        res.forEach(thing => {
            areas.push(thing)
        })
    })
    return areas
}

/*
get info about specific innerV.
recieves:
string building (building the area is in)
string area (name of the area)

returns map as so:
{"building": building, "area":area, "entry": allowEntry, "def":def, "floor":floor, "rooms":rooms}
types in map:
{
string building,
string area,
int allowEntry (1 or 0),
string def (the unique ID decided by admin),
int floor,
list string rooms (the rooms on the floor)
}

Comment:
original name was onClickInner
*/
async function jsGetAreaDetails(map) {
    let building = map["building"]
    let area = map["area"]
    console.log(building)
    console.log(area)
    let allowEntry = 0
    let def = 0
    let floor = 0
    let rooms = []
    //get the information about this area
    await dataConn.dbGetInnerV(building, area).then(res => {
        allowEntry = res['allowEntry'].low
        def = res['def']
        floor = res['floor'].low
        rooms = res['rooms']
    })
    return { "building": building, "area": area, "entry": allowEntry, "def": def, "floor": floor, "rooms": rooms }
}
/*
updates inner V

recieves:
string oldBuilding (old building name)
string oldArea (old area name)
string building (new building name)
string area (new area Name)
int allowEntry (1 or 0),
string def (the unique ID decided by admin),
int floor,
list string rooms (the rooms on the floor)

returns message (of success or not)
*/
async function jsUpdateInner(map) {
    let oldBuilding = map['oldBuilding']
    let oldArea = map['oldArea']
    let building = map['building']
    let aName = map['aName']
    let allowEntry = map['allowEntry']
    let def = map['def']
    let floor = map['floor']
    let rooms = map['rooms']

    let cont = false
    let ret = {}
    await dataConn.dbGetNameWithDef(def).then(res => {
        //check that the ID (def) the admin inputed is unique
        //if the name we got is the old name, then we're good, if it doesn't exist yet, then also good
        if (res == oldArea || res.length == 0) {
            cont = true
        } else {
            ret['message'] = "כבר קיים איזור אחר עם ID כזה, נסה ID אחר"
        }
    })
    //if we changed the area name, check that the new name is unique inside the building
    if (aName != oldArea) {
        //check we are not creating an area with a duplicate name
        await dataConn.dbUniqueAreaName(building, aName).then(res => {
            if (res == false) {
                ret['message'] = "כבר קיים שם זה לאיזור אחר בתוך הבניין, אנא נסה שם אחר"
                cont = false
            }
        })
    }
    //if the ID is unique, and if the new name (if changed) is unique, continue
    if (cont) {
        //create the vertex and alert the admin if the vertex was created or not
        await dataConn.dbUpdateInV(oldArea, oldBuilding, def, allowEntry, aName, building, floor, rooms).then(worked => {
            if (worked) {
                ret['message'] = "האיזור עודכן בהצלחה"
            } else {
                ret['message'] = "הפעולה נכשלה, האיזור לא עודכן"
            }
        })
    }
    return ret
}

/*
gets details about specific path
string building (the building of the path, empty string if trail)
string type (exit, entrance, trail, path)
string src (src vertex name)
string dest (destination vertex name)

returns either error message or map as so:
return {"weight":weight, "directions":directions, "classifications":classification, 
    "distances":distances, "freeText": freeText}
such that types are:
{
int weight (of path),
list string directions[] (directions, up down right, left for each microdirection),
list string classification[] (hallway, stairs, loby... for each microdirection),
list int distances[] (5, 10, 20 for each microdirection),
list string freeText[] (go left fo 20 meters at hallway, ... for each microdirection)
}
*/
async function jsGetPathDetails(map) {
    let building = map['building']
    let type = map['type']
    let src = map['src']
    let dest = map['dest']
    let ret = {}
    //get the query to find the edge
    let query = dataConn.dbBuildFillInEdgeQuery(type, building, src, dest)
    //ask db for info about the edge and save it in pathDets
    let pathDets = ""
    await dataConn.dbGetPath(query).then(res => {
        pathDets = res
    })
    //if there's no path like this
    if (pathDets.length == 0) {
        ret['message'] = "לא קיים מסלול כזה"
        return ret;
    }
    //get the weight and delete it from the map
    let weight = pathDets["weight"].low
    delete pathDets["weight"]
    // document.getElementById("weight").value = weight

    //create arrays of each type of direction
    let directions = []
    let classification = []
    let distances = []
    let freeText = []
    //go through map and extract this information
    for (const [key, value] of Object.entries(pathDets)) {
        directions.push(value[0])
        classification.push(value[1])
        distances.push(value[2])
        freeText.push(value[3])
    }
    return {
        "weight": weight, "directions": directions, "classifications": classification,
        "distances": distances, "freeText": freeText
    }
}
/*
updates the path by deleting old one and creating new one
recieves:
string type (exit, entrance, trail, path)
string src (src vertex name)
string dest (destination vertex name)
string building (the building of the path, empty string if trail)
int weight (of path),
list string directions[] (directions, up down right, left for each microdirection),
list string classification[] (hallway, stairs, loby... for each microdirection),
list int distances[] (5, 10, 20 for each microdirection),
list string freeText[] (go left fo 20 meters at hallway, ... for each microdirection)

returns if succeded or not

*/
async function jsUpdatePath(map) {
    let type = map['type']
    let src = map['src']
    let dest = map['dest']
    let building = map['building']
    let weight = map['weight']
    let directions = map['directions']
    let classification = map['classification']
    let distances = map['distances']
    let freeText = map['freeText']

    let ret = {}


    tup = dataConn.dbBuildUpdateEdgeQuery(type, src, dest, building, weight, directions, classification, distances, freeText);
    let deleteQuery = tup[0]
    let addQuery = tup[1]

    let suc = false
    //use delete query to delete old edge(s) between them
    await dataConn.dbDeleteEdge(deleteQuery).then(worked => {
        suc = worked
    })
    //if we couldnt delete, error
    if (!suc) {
        ret['message'] ="הפעולה נכשלה, המסלול לא עודכן"
        return ret
    }
    // send the query to create edge and print if succeded or not
    await dataConn.dbCreateEdge(addQuery).then(worked => {
        if (worked) {
            ret['message'] = "המסלול עודכן בהצלחה"
        } else {
            ret['message'] = "תקלה, ייתכן והמסלול נמחק"
        }
    })
    return ret
}
module.exports = {
    updateJsUpdatePath: jsUpdatePath,
    updateJsGetPathDetails: jsGetPathDetails,
    updateJsUpdateInner: jsUpdateInner,
    updateJsGetAreaDetails: jsGetAreaDetails,
    updateJsGetListOfAreas: jsGetListOfAreas,
    updateJsUpdateOuter: jsUpdateOuter,
    updateJsGetOuterByName: jsGetOuterByName,
    updateJsGetListOfOuterV: jsGetListOfOuterV,
    updateJsSetDest: jsSetDest,
    updateJsSetSrc: jsSetSrc,
    updateJsGetListOfBuilding: jsGetListOfBuilding,
}