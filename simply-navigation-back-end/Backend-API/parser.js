/*
Takes requests from the server and calls the appropriate functions. This is used for the following function
files:
1) add.js
2) delete.js
3) update.js
4) fromJsonToGraph.js
5) genereateQR.js
6) displayGraph.js

Note: jsonFormat.json is a json with the agreed parameters needed to call the functions
*/

var add = require('./add.js')
var del = require('./delete.js')
var update = require('./update.js')
var derulo = require('./fromJsonToGraph')
var qr = require('./generateQR')
var draw = require("./displayGraph.js")

const auth = require('./jsonFormat.json')
// tests()
function tests() {
    var obj = { type: "json", functionName: "downloadAsJson", args: {} }
    runRequest(obj).then(res => {
        console.log(res)
    })
}
//"objJson":{"type": "string", "functionName":"string", "args":{}}
async function runRequest(objJson) {
    var nameFunc = objJson.functionName
    var type = objJson.type
    var args = objJson.args

    ret = ''
    switch (type) {
        case "add":
            await add[nameFunc](args).then(res => {
                // console.log(res)
                ret = res
            })
            break;

        case "del":
            await del[nameFunc](args).then(res => {
                // console.log(res)
                ret = res
            })
            break;
        case "update":
            await update[nameFunc](args).then(res => {
                // console.log(res)
                ret = res
            })
            break;
        case "draw":
            await draw[nameFunc](args).then(res => {
                // console.log(res)
                ret = res
            })
            break;
        case "json":
            await derulo[nameFunc](args).then(res => {
                // console.log(res)
                ret = res
            })
            break;
        case "qr":
            await qr[nameFunc](args).then(res => {
                // console.log(res)
                ret = res
            })
            break;
    }
    return ret
    // console.log(nameFunc)
    // console.log(funcJsons.functionNames[nameFunc])//.then(res => {
    // console.log(res)
    // })
}

module.exports = {
    runRequest
}