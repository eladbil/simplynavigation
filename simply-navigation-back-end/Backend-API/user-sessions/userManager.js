/*
File in charge of managing admins, used to create and accept admins, as long as changing passwords and so on
*/
const constAddresses = require('../../configFiles/configAll.json')
const userData = './user-sessions/users.json'
const fs = require('fs')
const bcrypt = require('bcrypt')
const nodemailer = require('nodemailer');
const recoveryEmailAddr = "wolfson.navigation@hotmail.com"
const recoveryEmailPassword = "yaishsh3"
const superAdminAccepter = "wolfson.navigation@hotmail.com"
const newUserAcceptTime = 10 * 24 * 60 * 60 * 1000 //10 days
// const accepterLink = "https://localhost:5000/admin/accept-new-user"
const accepterLink = constAddresses.protocolfronted+constAddresses.domain+":"+constAddresses.portServerNav+"/admin/accept-new-user"
const recoveryCodeValidTime = 600 * 1000 //10 min
const emailServiceName = "Hotmail"

var passwordRecoveryCodes = {}
// test()
// async function test(){
// await sendYouWereExceptedMail("bob", "bob@gmail.com").then(res=> {
//   console.log(res)
// })
// }
//send an email saying to new user that they were excepted
async function sendYouWereExceptedMail(firstName, newAdminEmail) {

  //send the mail from the mailing address above
  var transporter = nodemailer.createTransport({
    service: emailServiceName,
    auth: {
      user: recoveryEmailAddr,
      pass: recoveryEmailPassword
    }
  });

  //email content
  var format = "<html dir=\"rtl\" lang=\"he\"> <meta charset=\"utf-8\"> \
  <p dir=\"rtl\">שלום"+ " " + firstName + ", <br> \
 המנהל קיבל את בקשתך לחשבון הוולפסון ניווט, נפתחה לך חשבון מנהל. \
  <br> בברכה," + " <br>" + "צוות וולפסון ניווט" + "<br> \
<br><br><br>" + "נא לא להשיב למייל זה" + " </p> </html>"

  var mailOptions = {
    from: superAdminAccepter,
    to: newAdminEmail,
    subject: 'חשבון אדמינסטרציה אושר',
    html: format
  };

  //send the mail
  return new Promise((resolve, reject) => {
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        // console.log(error)
        resolve(0);
      } else {
        resolve(1);
      }
    });
  })
}
//send an email to main admin asking them to accept new user
async function sendAcceptenceRequestEmail(newFname, newLname, newUserEmail) {
  var sendTo = superAdminAccepter;


  //send the mail from the mailing address above
  var transporter = nodemailer.createTransport({
    service: emailServiceName,
    auth: {
      user: recoveryEmailAddr,
      pass: recoveryEmailPassword
    }
  });

  //email content
  var format = "<html dir=\"rtl\" lang=\"he\"> <meta charset=\"utf-8\"> \
  <p dir=\"rtl\">שלום,"+ " <br> \
  המשתמש "+ newFname + " " + newLname + " אם המייל " + newUserEmail + " מבקש ליצור חשבון אדמניסטרציה למערכת ניווט וולפסון. חשבון כזה נותן לו הרשאות ניהול ואפשרויות לראות ולערוך את המפה של מערכת הניווט. \
  בכדי לאשר לו/ה הרשאות אלא, נא ללחוץ על הקישור ולהיכנס למערכת. " + "<br>" + accepterLink + "?newAdmin=" + newUserEmail + " \
  \ <br>"+ "שמו לב, ניתן להוסיף אותו רק 10 ימים מקבלת מייל זה! \
  <br> בברכה," + " <br>" + "צוות וולפסון ניווט" + "<br> \
<br><br><br>" + "נא לא להשיב למייל זה" + " </p></html>"

  var mailOptions = {
    from: superAdminAccepter,
    to: sendTo,
    subject: 'בקשה חדשה לחשבון אדמינסטרציה',
    html: format
  };

  //send the mail
  return new Promise((resolve, reject) => {
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        resolve(0);
      } else {
        resolve(1);
      }
    });
  })
}
/*
creates new user.
TODO- Send email to master user to confirm the creating of new user is valid
recieves
{
    "firstName": "string",
    "lastName": "string",
    "password": "string",
    "email": "string"
 }
 example:
 {
    "firstName": "bob",
    "lastName": "jackson",
    "password": "12345",
    "email": "bob@gmail.com"
 }
*/
async function createNewUser(req, res) {
  //take the username, password and email and create json to add
  const fName = req.body.firstName
  const lName = req.body.lastName
  const pword = req.body.password
  const userEmail = req.body.email
  //hash the password
  const hashedPassword = await bcrypt.hash(pword, 10)
  //give the user a certain amount of time he needs to be accepted
  const now = new Date()
  const expiresAt = new Date(+now + newUserAcceptTime)
  var toAdd = { firstName: fName, lastName: lName, password: hashedPassword, email: userEmail, accepted: "false", ttl: expiresAt }
  var data = {}
  try {
    data = fs.readFileSync(userData, 'utf8')
  } catch (err) {
    return res.json({ message: err })
  }
  //read what's in the json
  var obj = JSON.parse(data);
  //goes through the users and removes the ones who weren't added whos ttls expired
  var admins = Object.keys(obj.users);
  for (var i in admins) {
    // if the user is never supposed to expire, leave him, move on
    if (obj.users[admins[i]].ttl == "never") {
      continue;
    }
    //if the user is, check if he expired
    var userTtl = new Date(obj.users[admins[i]].ttl)
    if (obj.users[admins[i]].accepted == "false" && userTtl < (new Date())) {
      delete obj.users[admins[i]]
    }
  }


  //make sure user doesn't exist already
  if (obj.users[userEmail] != null) {
    return res.json({ message: "המנהל כבר קיים" })
  }
  //add the new person as the email
  obj.users[userEmail] = toAdd

  //send the mail asking the super admin to accept the new user
  var sent = -2
  await sendAcceptenceRequestEmail(fName, lName, userEmail).then(retu => {
    sent = retu
  })
  if (!sent) {
    return res.json({ message: "קרתה שגיאה, נא לנסות שינית" });
  }


  //turn json into string to write to file
  var strFile = JSON.stringify(obj);
  //wrtie newJson to file
  try {
    fs.writeFileSync(userData, strFile)
    var p1 = "נשלחה מייל לכתובת"
    var p2 = " " + superAdminAccepter + " ";
    var p3 = "אם בקשה להוספתך"
    res.json({ message: p1 + p2 + p3 }).end()
  }
  catch (err) {
    // console.log(err)
    return res.json({ message: "קרתה שגיאה, נא לנסות שינית" });
  }
}
/*
Adds the new admin to system
recieves:
admin thats accepting email and password along with the admin to accept's email
{
  "superAdminEmail": "string"
  "superAdminPassword": "string"
  "newAdminEmail": "string"
}
*/
async function acceptNewUser(req, res) {
  superAdminEmail = req.body.superAdminEmail;
  superAdminPassword = req.body.superAdminPassword;
  newAdminEmail = req.body.newAdminEmail;
  var data = {}
  try {
    data = fs.readFileSync(userData, 'utf8')
  } catch (err) {
    return res.json({ message: err })
  }
  //read what's in the json
  var obj = JSON.parse(data);
  if (obj.users[superAdminEmail] == null) {
    res.status(401)
    return res.json({ message: "שם משתמש ן/או סיסמה שגויה" }).end()
  }
  //check we have the right password
  if (await bcrypt.compare(superAdminPassword, obj.users[superAdminEmail].password)) {
    let cont = true;
  } else {
    res.status(401)
    return res.json({ message: "שם משתמש ן/או סיסמה שגויה" }).end()
  }
  if (obj.users[superAdminEmail].accepted == "false") {
    res.status(401)
    return res.json({ message: "אין לך חשבון מאושר. נא לנסות לרשום אותו עם חשבון מאושר" }).end()
  }
  //check new admin is on list
  if (obj.users[newAdminEmail] == null) {
    res.status(401)
    return res.json({ message: "המנהל לא נרשם" }).end()
  }
  //make sure the ttl didn't expire
  if (obj.users[newAdminEmail].ttl < (new Date())) {
    res.status(401)
    return res.json({ message: "זמן ההרשמה של המשתמש החדש פג תוקף" }).end()
  }
  obj.users[newAdminEmail].accepted = "true"
  obj.users[newAdminEmail].ttl = "never"

  var strFile = JSON.stringify(obj);
  //wrtie newJson to file
  try {
    fs.writeFileSync(userData, strFile)
    await sendYouWereExceptedMail(obj.users[newAdminEmail].firstName, newAdminEmail)
    return res.json({ message: "המנהל הוסף בהצלחה" }).end()
  }
  catch (err) {
    return res.json({ message: err });
  }
}

/*
when a password is lost, it sends a new password to the users email and he has 10 minutes to
change his password.
recieves:
{
    "firstName": "string",
    "lastName": "string",
    "email": "string"
}
example:
{
    "firstName": "bob",
    "lastName": "jackson",
    "email": "bob@gmail.com"
}
*/
async function passwordRecovery(req, res) {

  var sendTo = req.body.email;
  var fname = req.body.firstName;
  var lname = req.body.lastName;
  //read the page with details
  try {
    var data = fs.readFileSync(userData, 'utf8')
  } catch (err) {
    return res.json({ message: err }).end()
  }

  var obj = JSON.parse(data);
  //make sure user doesn't exist already
  if (obj.users[sendTo].firstName.toUpperCase() != fname.toUpperCase() || obj.users[sendTo].lastName.toUpperCase() != lname.toUpperCase()) {
    return res.json({ message: "שם פרטי או שם משפחה שגויות" }).end();
  }

  //send the mail from the mailing address above
  var transporter = nodemailer.createTransport({
    service: emailServiceName,
    auth: {
      user: recoveryEmailAddr,
      pass: recoveryEmailPassword
    }
  });

  // const emailTransporter = nodemailer.createTransport({
  //   host: 'smtp.mail.yahoo.com',
  //   port: 465,
  //   service:'yahoo',
  //   secure: false,
  //   auth: {
  //     user: recoveryEmailAddr,
  //     pass: recoveryEmailPassword
  //   },
  //   debug: false,
  //   logger: true
  // });


  //get a 6 digit recovery number
  var recoveryNum = Math.floor(Math.random() * 1000000);
  var neverEnding = 0;
  while (recoveryNum < 100000 && neverEnding < 100) {
    recoveryNum = Math.floor(Math.random() * 1000000)
    neverEnding++;
  }
  //add recovery number for user and make it valid for 10 min
  var now = new Date()
  var expiresAt = new Date(+now + recoveryCodeValidTime)
  passwordRecoveryCodes[sendTo] = { num: recoveryNum, expires: expiresAt }
  //email content
  var format = "<html dir=\"rtl\" lang=\"he\"> <meta charset=\"utf-8\">\
<p dir=\"rtl\">שלום "+ fname + ", \
<br>הקוד שיחזור שלך למערכת ניווט וולפסון הינו "+ recoveryNum + ". \
הקוד הינו תקף ל10 דקות בלבד! \
אם לא את/ה ביקשת קוד זה, נא לעדכן את המנהלים בהקדם האפשרי."+ " \
<br>בברכה,<br>"+ "צוות וולפסון ניווט" + " \
<br><br><br> " + "נא לא להשיב למייל זה" + "</html>"

  var mailOptions = {
    from: superAdminAccepter,
    to: sendTo,
    subject: 'שחזור סיסמא',
    html: format
  };

  //send the mail
  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      res.json({ message: error });
    } else {
      var p1 = "נשלחה מייל אם קוד חד פעמית לכתובת"
      var p2 = sendTo + "\n";
      var p3 = "נא להיכנס למייל ולהיכנס דרך הסיסמא החד פעמית"
      res.json({ message: p1 + p2 + p3 }).end()
    }
  });

}

/*
after recovery code is sent, this is used to reset the password.
recieves:
code is the 6 digit recovery code
newpassword is the new password,
confirm password is the new password again to make sure there are no errors
and email.

{
    "code": "string",
    "newPassword": "string",
    "confirmPassword": "string",
    "email": "string"
}
example.
{
    "code": "532912",
    "newPassword": "abcd",
    "confirmPassword": "abcd",
    "email": "bob@gmail.com"
}

*/
async function resetPasswordWithCode(req, res) {
  var code = req.body.code;
  var newPassword = req.body.newPassword;
  var email = req.body.email;
  var confirmPassword = req.body.confirmPassword;
  //check to make sure the newpassword and the confirmed password are the same
  if (confirmPassword != newPassword) {
    return res.json({ message: "סיסמה ווידוי סיסמא לא תואמות" }).end();
  }
  //if the 6 digit code is wrong, error
  if (passwordRecoveryCodes[email] == null || code != passwordRecoveryCodes[email].num) {
    return res.json({ message: "מייל או קוד שגוי" }).end();
  }
  //if the code expired, delete the session
  else if (passwordRecoveryCodes[email].expires < (new Date())) {
    delete passwordRecoveryCodes[email]
    return res.json({ message: "הקוד פג תוקף נסה לקבל קוד חדש" }).end()
  }
  //if the code is good, change the password
  delete passwordRecoveryCodes[email]
  try {
    data = fs.readFileSync(userData, 'utf8')
  } catch (err) {
    return res.json({ message: err })
  }
  //read what's in the json
  var obj = JSON.parse(data);
  const hashedPassword = await bcrypt.hash(newPassword, 10)
  obj.users[email].password = hashedPassword
  //turn json into string to write to file
  var strFile = JSON.stringify(obj);
  //wrtie newJson to file
  try {
    fs.writeFileSync(userData, strFile)
    return res.json({ message: "הסיסמה שונתה בהצלחה" }).end()
  }
  catch (err) {
    return res.json({ message: err }).end();
  }

}
module.exports = {
  createNewUser, passwordRecovery, resetPasswordWithCode, acceptNewUser
}