/*
In charge of cookies and opening up connections to the admin section of the code by checking user
authentication when logging in and saving the cookies when the user logs out
*/
// Getting the path of the user data file.
const userData = require('path').resolve('Backend-API','user-sessions', 'users.json')
// const userData = './user-sessions/users.json'
const { cookie } = require('express/lib/response')
const uuid = require('uuid')
const bcrypt = require('bcrypt')
const fs = require('fs')

//set the session length to be 1 hour
const sessionLength = 10*60*60*100
//refresh if less than an 45 min is left
const refreshWithLess = 1000 *60*45

const cookieError = { message: "Please sign in" }

function signedIn(req, res, next) {
    if (req.query.user == null) {
        res.status(403)
        console.log("in error")
        return res.json({ message: "Please sign in" })
    }
    next()
}
// each session contains the username of the user and the time at which it expires
class Session {
    constructor(username, expiresAt) {
        this.username = username
        this.expiresAt = expiresAt
    }

    // we'll use this method later to determine if the session has expired
    isExpired() {
       return this.expiresAt < (new Date())
    }
}

// this object stores the users sessions. For larger scale applications, you can use a database or cache for this purpose
const sessions = {}
/*
used to sign in and create a sessionLength session
recieves json:
{
    "username":"string",
    "password": "string"
}
*/
const signinHandler = async (req, res) => {
    //delete expired sessions
    deleteExpiredSessions()
    //if there's already a cookie, delete it.
    if (req.cookies && req.cookies['session_token'] && sessions[req.cookies['session_token']]) {
        delete sessions[req.cookies['session_token']]
    }
    // get users credentials from the JSON body
    const { username, password } = req.body

    if (!username || !password) {
        // If the username isn't present, return an HTTP unauthorized code
        res.status(401)
        return res.json({ message: "שם משתמש או סיסמא חסרים" }).end()
    }

    // validate the password against our data
    // if invalid, send an unauthorized code
    var data = {}
    try {
        data = fs.readFileSync(userData, 'utf8')
    } catch (err) {
        console.log(error)
        res.status(500)
        return res.json({ message: err }).end()
    }
    var jsonOfInfo = JSON.parse(data)
    //with a certain probability, delete expired ttl new admins that weren't added
    jsonOfInfo = deleteOverTtlNewAdmins(jsonOfInfo)

    //check this user exist in the system
    if (jsonOfInfo.users[username] == null) {
        res.status(401)
        return res.json({ message: "שם משתמש ן/או סיסמה שגויה" }).end()
    }
    //check we have the right password
    if (await bcrypt.compare(password, jsonOfInfo.users[username].password)) {
        let cont = true;
    } else {
        res.status(401)
        return res.json({ message: "שם משתמש ן/או סיסמה שגויה" }).end()
    }
    //check to make sure their email is confirmed
    if (jsonOfInfo.users[username].accepted == "false") {
        res.status(401)
        return res.json({ message: "אין לך חשבון מאושר" }).end()
    }

    // generate a random UUID as the session token
    const sessionToken = uuid.v4()

    // set the expiry time as session lengths after the current time
    const now = new Date()
    const expiresAt = new Date(+now + sessionLength)

    // create a session containing information about the user and expiry time
    const session = new Session(username, expiresAt)
    // add the session information to the sessions map
    sessions[sessionToken] = session
    // In the response, set a cookie on the client with the name "session_cookie"
    // and the value as the UUID we generated. We also set the expiry time
    // res.setHeader('Access-Control-Allow-Credentials', 'true');
    res.cookie("session_token", sessionToken, { expires: expiresAt })
    // console.log(sessions)
    res.end()
}
/*
const welcomeHandler = (req, res) => {
    // if this request doesn't have any cookies, that means it isn't
    // authenticated. Return an error code.
    if (!req.cookies) {
        console.log("error")
        res.status(401).end()
        return
    }

    // We can obtain the session token from the requests cookies, which come with every request
    const sessionToken = req.cookies['session_token']
    if (!sessionToken) {
        // If the cookie is not set, return an unauthorized status
        res.status(401).end()
        return
    }

    // We then get the session of the user from our session map
    // that we set in the signinHandler
    userSession = sessions[sessionToken]
    if (!userSession) {
        // If the session token is not present in session map, return an unauthorized error
        res.status(401).end()
        return
    }
    // if the session has expired, return an unauthorized error, and delete the 
    // session from our map
    if (userSession.isExpired()) {
        delete sessions[sessionToken]
        res.status(401).end()
        return
    }

    // If all checks have passed, we can consider the user authenticated and
    // send a welcome message
    res.send(`Welcome  ${userSession.username}!`).end()
}
*/
//checks if an admin is connected based on the cookie, if so returns true, else returns fals
const isAdminConnected = (req, res) =>{
     // (BEGIN) The code from this point is the same as the first part of the welcomeHandler
     if (!req.cookies) {
        res.json({key: false})
        return
    }

    const sessionToken = req.cookies['session_token']
    if (!sessionToken) {
        res.json({key: false})
        return
    }

    let userSession = sessions[sessionToken]
    if (!userSession) {
        res.json({key: false})
        return
    }
    if (userSession.isExpired()) {
        delete sessions[sessionToken]
        res.json({key: false})
        return
    }
    res.json({key: true})
    return

}
//refreshes current session
const refreshHandler = (req, res) => {
    // (BEGIN) The code from this point is the same as the first part of the welcomeHandler
    if (!req.cookies) {
        res.status(401).end()
        return
    }

    const sessionToken = req.cookies['session_token']
    if (!sessionToken) {
        res.status(401).end()
        return
    }

    let userSession = sessions[sessionToken]
    if (!userSession) {
        res.status(401).end()
        return
    }
    if (userSession.isExpired()) {
        delete sessions[sessionToken]
        res.status(401).end()
        return
    }
    // (END) The code until this point is the same as the first part of the welcomeHandler

    // create a new session token
    const newSessionToken = uuid.v4()

    // renew the expiry time
    const now = new Date()
    const expiresAt = new Date(+now + sessionLength)
    const session = new Session(userSession.username, expiresAt)

    // add the new session to our map, and delete the old session
    sessions[newSessionToken] = session
    delete sessions[sessionToken]

    // set the session token to the new value we generated, with a
    // renewed expiration time
    res.cookie("session_token", newSessionToken, { expires: expiresAt })
    res.end()
}

//refreshes session and continues on to function that was supposed to be executes.
//Used mainly to see if we are an admin before displaying a page which only admins have access to.
const refreshOtherCalls = (req, res, next) => {
    console.log(req.cookies);
    // (BEGIN) The code from this point is the same as the first part of the welcomeHandler
    if (!req.cookies) {
        res.status(401)
        // console.log("Im error 1");
        return res.json(cookieError).end()
    }

    const sessionToken = req.cookies['session_token']
    if (!sessionToken) {
        res.status(401)
        // console.log("Im error 2");
        return res.json(cookieError).end()
    }

    let userSession = sessions[sessionToken]
    if (!userSession) {
        res.status(401)
        // console.log("Im error 3");
        return res.json(cookieError).end()
    }
    if (userSession.isExpired()) {
        delete sessions[sessionToken]
        // console.log("Im error 4");
        res.status(401)
        return res.json(cookieError).end()
    }
    // (END) The code until this point is the same as the first part of the welcomeHandler
    //if its not time to refresh the cookie yet, move on.
    if (!(userSession.expiresAt - new Date() < refreshWithLess)) {
        next();
    } else {
    // create a new session token
    const newSessionToken = uuid.v4()

    // renew the expiry time
    const now = new Date()
    const expiresAt = new Date(+now + sessionLength)
    const session = new Session(userSession.username, expiresAt)

    // add the new session to our map, and delete the old session
    sessions[newSessionToken] = session
    delete sessions[sessionToken]

    // set the session token to the new value we generated, with a
    // renewed expiration time
    res.cookie("session_token", newSessionToken, { expires: expiresAt })
    next()
    }
    // res.end()

}

//logs out of session
const logoutHandler = (req, res) => {
    if (!req.cookies) {
        res.status(401).end()
        return
    }

    const sessionToken = req.cookies['session_token']
    if (!sessionToken) {
        res.status(401).end()
        return
    }

    delete sessions[sessionToken]

    // res.cookie("session_token", "", { expires: new Date() })
    res.end()
}


//in a 1/15 chance, it goes through the users and removes the ones who weren't added whos ttls expired
function deleteOverTtlNewAdmins(jsonOfInfo) {
    // one out of 15 times do this
    var rand = Math.floor(Math.random() * 15);
    if (rand != 7) {
        return jsonOfInfo;
    }
    var admins = Object.keys(jsonOfInfo.users);
    for (var i in admins) {
        // if the user is never supposed to expire, leave him, move on
        if(jsonOfInfo.users[admins[i]].ttl == "never") {
            continue;
        }
        //if the user is, check if he expired
        var userTtl = new Date(jsonOfInfo.users[admins[i]].ttl)
        if (jsonOfInfo.users[admins[i]].accepted == "false" &&  userTtl< (new Date())) {
            delete jsonOfInfo.users[admins[i]]
        }
    }
    var strFile = JSON.stringify(jsonOfInfo);
    //wrtie newJson to file
    try {
        fs.writeFileSync(userData, strFile)
        return jsonOfInfo
    }
    catch (err) {
        return jsonOfInfo
    }
}
//deletes expired sessions
function deleteExpiredSessions() {
   for (let key of Object.keys(sessions)) {
       const curr = sessions[key];
        if (sessions[key].isExpired()) {
            delete sessions[key];
        }
    }
}

module.exports = {
    signedIn, signinHandler, refreshHandler, refreshOtherCalls, logoutHandler, isAdminConnected
}