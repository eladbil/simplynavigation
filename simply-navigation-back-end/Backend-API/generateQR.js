//Generates QR pdf file with the area QR code and instructions on how to use the app
// https://stackoverflow.com/questions/26392735/pdfkit-custom-fonts-fs-readfilesync-is-not-a-function
/*
    Got hebrew font from here:
    https://fonts.google.com/specimen/Rubik?preview.text=%D7%A9%D7%9C%D7%95%D7%9D%20%D7%90%D7%9C%D7%99%D7%9B%D7%9D&preview.text_type=custom#standard-styles
    With help from here:
    https://stackoverflow.com/questions/18718559/how-to-output-euro-symbol-in-pdfkit-for-nodejs
*/
const fs = require('fs')
// const constAddresses = JSON.parse(
//     fs.readFileSync(require('path').resolve('../configFiles', 'configAll.json'))
//     );


const constAddresses = require('../configFiles/configAll.json');
const dataConn = require('../DB-Interface/basic_db_queries');
const qr = require('qrcode');
const PDFDocument = require('pdfkit');
const hebFont=  "./Hebrew-Font/Rubik-Regular.ttf";
const instructFont = "./Hebrew-Font/IBMPlexSansHebrew-Light.ttf"
// const startURL = "https://localhost:3000/user?id=" 
const startURL = constAddresses.protocol+constAddresses.domain+':'+constAddresses.portServerNav+"/user?id=" 
// pdfMake = require('pdfmake')

// generateQR({building: "מרפאות חוץ", areaName: "ראומטולוגיה"}).then(x=> {
//     x[0].pipe(fs.createWriteStream('areaQR.pdf'));
// })





/*
generates Qr code
recives {"building": "string", "areaName": "string"}
returns list:
[PDFdocument, "name_Of_Document"]
*/
async function generateQR(map) {
    // map = {building: "מרפאות חוץ", areaName: "ראומטולוגיה"}
    var building = map["building"]
    var areaName = map["areaName"]
    //get the def from the database
    var def = '';
    await dataConn.dbGetDefOfArea(building, areaName).then(res => {
        def = res;
    })
    //generate the qr code
    var url = startURL + def;
    var areaQr = ''
    //make sure to await before using areaQr as so: await areaQr
    areaQr = new Promise(function (resolve, reject) {
        qr.toDataURL(url, function (err, code) {
            if (err) {
                reject(err)
                return err;
            }

            resolve(code)
            return code;
        })
    })
    
    var docName = areaName +"_"+def+ ".pdf";
    // create pdf with info
    var doc = new PDFDocument({ size: 'A4'});
    // doc.pipe(fs.createWriteStream('areaQR.pdf'));
    doc.info['Title'] = docName;
    doc.font(hebFont)
    doc.fontSize(40).text("ניווט פנימי וולפסון", {
        align: 'center',
        //right to left
        features: ['rtla']
    })
    doc.fontSize(27).text("בניין: " + building, {
        align: 'center',
        features: ['rtla']
    })
    doc.fontSize(27).text("איזור: " + areaName, {
        align: 'center',
        features: ['rtla']
    })

    doc.moveDown();
    doc.fontSize(27).text("מזהה: ", {
        align: 'center',
        features: ['rtla']
    })
    doc.fontSize(27).font('Helvetica-Bold').text(def, {
        align: 'center'
    })

    doc.image(await areaQr, {
        fit: [449, 300],
        align: 'center'
        // valign: 'bottom'
    });

    doc.font(instructFont)
    doc.fontSize(20).text(
        "1( סרוק את הקוד",
         {
        align: 'right',
        features: ['rtla']
    })
    doc.text(
        "2( בחר יעד",
         {
        align: 'right',
        features: ['rtla']
    })
    doc.text(
        "3( עקוב אחרי הכיוונים",
         {
        align: 'right',
        features: ['rtla']
    })
    doc.text(
        "4( הגעת ליעד!",
         {
        align: 'right',
        features: ['rtla']
    })

    // Finalize PDF file
    doc.end();
    //return the pdf
    return [doc, docName];
}

module.exports = {
    generateQR
}