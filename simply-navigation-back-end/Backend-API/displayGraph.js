const { range } = require('express/lib/request');
const dataConn = require('../DB-Interface/basic_db_queries')
/*
Functions for displaying the neo4j graph. Passes graph information along. 
You are able to get the following things:
1) look at a certain floor in certain building
2) look at entire certain building
3) look at specific inV by name
4) look at inV by def
5) look at specific outV
6) look at two specific Vs (to see their relationship)


var allowEntry = map["allowEntry"];
    var includeOutVs= map["includeOutV"];
    var specificArea = map["specificAreaName"];
*/

/*
look at a certain floor in certain building.
Recives map {
    floor: "integer",
    building:"string"
}

Test call:
certainFloorCertainBuiding({floor:1, building:"מרפאות חוץ"});
*/
async function certainFloorCertainBuiding(map) {
    var floor = map["floor"];
    var building = map["building"];
    var rels = []
    var nodes = []
    //get the query
    var queryForRels = dataConn.dbQueryForDisplay(true, true, building, floor)
    var queryForNodes = dataConn.dbQueryForDisplay(false, true, building, floor)
    //run the relationship query
    await dataConn.dbForDisplayRunner(queryForRels).then(res => {
        for (var i = 0; i < res.length; i++) {
            rels.push(res[i]._fields[0])
        }
    })
    //run the node query
    await dataConn.dbForDisplayRunner(queryForNodes).then(res => {
        for (var i = 0; i < res.length; i++) {
            nodes.push(res[i]._fields[0])
        }
    })
    return { Nodes: nodes, Relationships: rels }
}

/*
look at a certain building.
Recives map {
    building:"string"
}

Test call:
entireBuilding({building: "מרפאות חוץ"})
*/
async function entireBuilding(map) {
    var building = map["building"];

    var floor = map["floor"];
    var building = map["building"];
    var rels = []
    var nodes = []
    //get the relationships
    var queryForRels = dataConn.dbQueryForDisplay(true, false, building, '')
    var queryForNodes = dataConn.dbQueryForDisplay(false, false, building, '')
    //run the relationship query
    await dataConn.dbForDisplayRunner(queryForRels).then(res => {
        for (var i = 0; i < res.length; i++) {
            rels.push(res[i]._fields[0])
        }
    })
    //run the node query
    await dataConn.dbForDisplayRunner(queryForNodes).then(res => {
        for (var i = 0; i < res.length; i++) {
            nodes.push(res[i]._fields[0])
        }
    })
    // console.log({Nodes: nodes, Relationships: rels})
    return { Nodes: nodes, Relationships: rels }
}

/*
returns a specific area node by building and name
Recives map {
    building:"string",
    areaName:"string"
}

returns {Nodes: nodes}

example call:
specificAreaByName({areaName:"אשפוז יום", building:"מרפאות חוץ"})
*/
async function specificAreaByName(map) {
    var areaName = map["areaName"];
    var building = map["building"];
    var nodes = []
    //run the node query
    await dataConn.dbForDisplaySpecificInnerByName(areaName, building).then(res => {
        for (var i = 0; i < res.length; i++) {
            nodes.push(res[i]._fields[0])
        }
    })
    return { Nodes: nodes }
}

/*
returns a specific area node by def
Recives map {
    def: "string"
}

returns {Nodes: nodes}

example call:
specificAreaByDef({def:"OC201"})
*/
async function specificAreaByDef(map) {
    var def = map["def"]
    var nodes = []
    //run the node query
    await dataConn.dbForDisplaySpecificInnerByDef(def).then(res => {
        for (var i = 0; i < res.length; i++) {
            nodes.push(res[i]._fields[0])
        }
    })
    // console.log({Nodes: nodes})
    return { Nodes: nodes }
}

/*
returns a specific outer V
Recives map {
    name: "string"
}

returns {Nodes: "list_of_nodes"}

example call:
specificOuterV({name:"temp"})
*/
async function specificOuterV(map) {
    var name = map["name"]
    var nodes = []
    //run the node query
    await dataConn.dbForDisplaySpecificOuterV(name).then(res => {
        for (var i = 0; i < res.length; i++) {
            nodes.push(res[i]._fields[0])
        }
    })
    // console.log({ Nodes: nodes })
    return { Nodes: nodes }
}
/*
returns two nodes and the relationships (if any) between them
Recives map {
    type: "string",
    srcName: "string",
    destName:"string",
    building:"string"
}

returns {Nodes: "list_of_nodes", Relationships: "list_of_relationshiph"}

example calls:
// betweenTwoPlaces({ type: "path", srcName: "המכון לבריאות השד (ממוגרפיה)", destName: "אשפוז יום", building: "מרפאות חוץ" })
// betweenTwoPlaces({type:"path", srcName:"המכון לבריאות השד (ממוגרפיה)", destName:"מכון הלב", building:"מרפאות חוץ"})
// betweenTwoPlaces({type:"exit", srcName:"המכון לבריאות השד (ממוגרפיה)", destName:"מעבר בין בניין אישפוז למרפאות חוץ קומת כניסה", building:"מרפאות חוץ"})
// betweenTwoPlaces({type:"entrance", destName:"המכון לבריאות השד (ממוגרפיה)", srcName:"מעבר בין בניין אישפוז למרפאות חוץ קומת כניסה", building:"מרפאות חוץ"})
// betweenTwoPlaces({type:"trail", srcName:"temp", destName:"מעבר בין בניין אישפוז למרפאות חוץ קומת כניסה"})
*/
async function betweenTwoPlaces(map) {
    var nodes = [];
    var rels = [];
    var type = map["type"];
    var src = map["srcName"];
    var dest = map["destName"];
    var building = '';
    //if it's not a trail then get the building, else, no need for building
    if (type != "trail") {
        building = map["building"]
    }
    await dataConn.dbForDisplayTwoNodesWithRels(type, src, dest, building, false).then(res => {
            nodes = res[0]._fields
    })
    await dataConn.dbForDisplayTwoNodesWithRels(type, src, dest, building, true).then(res => {
        for (var i = 0; i < res.length; i++) {
            rels.push(res[i]._fields[2])
        }
    })
    return { Nodes: nodes, Relationships: rels}
}

module.exports ={
    drawCertainFloorCertainBuiding: certainFloorCertainBuiding,
    drawEntireBuilding: entireBuilding,
    drawSpecificAreaByName: specificAreaByName,
    drawSpecificAreaByDef: specificAreaByDef,
    drawSpecificOuterV: specificOuterV,
    drawBetweenTwoPlaces : betweenTwoPlaces
}