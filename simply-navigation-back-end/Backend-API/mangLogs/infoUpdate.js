class InfoUpdate{
    constructor(OrderData, Lang, TypeInfo ,IdClient, OldDef,SrcDef,DestDef , Ip){
        this.OrderData = OrderData;
        this.Lang = Lang;
        this.TypeInfo = TypeInfo;
        this.IdClient = IdClient;
        this.OldDef = OldDef;
        this.SrcDef = SrcDef;
        this.DestDef = DestDef;
        this.Ip = Ip;
    }
}
module.exports = InfoUpdate;