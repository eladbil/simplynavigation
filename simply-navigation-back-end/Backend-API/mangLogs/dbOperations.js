var config = require('./dbconfig').config;
var configStart = require('./dbconfig').configStart;
const sql = require('mssql');
function retRange(date1,date2){
    return `OrderDate >= '${date1}' and OrderDate <= '${date2}'`;
}
async function getInfoBasic(){
    try{
        let pool = await sql.connect(config);
        let products = await pool.request().query("SELECT * from Info_Basic_Log")
       
        return products.recordsets;
    }
    catch(error){
        console.log(error)

    }
}

//return info basic abut id of user
async function getInfoBasicId(orderId){
    try{
        let pool = await sql.connect(config);
        let products = await pool.request()
        .input('input_parameter' ,sql.Float,orderId )
        .query("SELECT * from Info_Basic_Log where IdClient = @input_parameter")
        return products.recordsets;
    }
    catch(error){
        console.log(error)

    }
}
//Add row of update to Info_Basic_Log table
async function addInfoBasicRow(Info) {  
    
    try {
      
        let pool = await sql.connect(config);
        let insertProduct = await pool.request()
            .input('OrderDate', sql.DateTime,  Info.OrderDate)
            .input('Lang', sql.VarChar, Info.Lang)
            .input('TypeInfo', sql.NVarChar, Info.TypeInfo)
            .input('IdClient', sql.Float, Info.IdClient)
            .input('Data', sql.NVarChar, Info.Data)
            .input('Ip', sql.VarChar, Info.Ip)
            .query("insert into Info_Basic_Log values(@OrderDate,@Lang,@TypeInfo,@IdClient, @Data, @Ip);")
            // .execute('InsertInfo_Basic_Log');
            // insert into Info_Basic_Log values('2008-11-11 13:23:44','he','update',0.747289239285126, '{srcId:"OC101", dstId:"OC201", oldId:"OC001"}', '127.0.0.1');
            // {
            //     OrderDate: '13-5-2022 7:39:06',
            //     Lang: 'he',
            //     TypeInfo: 'EnterSearch',
            //     IdClient: '0.747289239285126',
            //     Data: '{}',
            //     Ip: '127.0.0.1'
            //   }
           
        return insertProduct.recordsets;
    }
    catch (err) {
        console.log(err);
    }
}

//Add row of update to Info_Update table
async function addInfoUpdateRow(Info) {  
    try {
       
        let pool = await sql.connect(config);
        let insertProduct = await pool.request()
            .input('OrderDate', sql.DateTime, Info.OrderDate)
            .input('Lang', sql.VarChar, Info.Lang)
            .input('TypeInfo', sql.NVarChar, Info.TypeInfo)
            .input('IdClient', sql.Float, Info.IdClient)
            .input('OldDef', sql.NVarChar, Info.OldDef)
            .input('SrcDef', sql.NVarChar, Info.SrcDef)
            .input('DestDef', sql.NVarChar, Info.DestDef)
            .input('Ip', sql.VarChar, Info.Ip)
            .query("insert into Info_Update values(@OrderDate,@Lang,@TypeInfo,@IdClient, @OldDef, @SrcDef, @DestDef, @Ip);")
            // .execute('InsertInfo_Basic_Log');
            // insert into Info_Basic_Log values('2008-11-11 13:23:44','he','update',0.747289239285126, '{srcId:"OC101", dstId:"OC201", oldId:"OC001"}', '127.0.0.1');

        return insertProduct.recordsets;
    }
    catch (err) {
        console.log(err);
    }
}



//All-time request of the requested routes
async function getOrderDateOfRoutes(date1 ,date2) {  
    try {
        // console.log(date1 ,date2,'getOrderDateOfRoutes')
        let pool = await sql.connect(config);
        let insertProduct = await pool.request()
            .query(`select OrderDate from Info_Basic_Log where TypeInfo = 'RouteFromSearch' and OrderDate >= ${date1} and OrderDate <= ${date2};`)
           
            // .execute('InsertInfo_Basic_Log');
            // insert into Info_Basic_Log values('2008-11-11 13:23:44','he','update',0.747289239285126, '{srcId:"OC101", dstId:"OC201", oldId:"OC001"}', '127.0.0.1');

        return insertProduct.recordsets;
    }
    catch (err) {
        console.log(err);
    }
}


//ספירת עמודות כניסה לפי תאריך
async function getCountRoutesByDateAndType(table,col,date1 ,date2) {  
    try {

    //   console.log(`SELECT FORMAT(OrderDate, 'yyyy-MM-dd') AS date, COUNT(OrderDate ) AS count FROM ${table} where TypeInfo = '${col}' and OrderDate >= '${date1}' and OrderDate <= '${date2}' GROUP BY FORMAT(OrderDate, 'yyyy-MM-dd');`)
    //    console.log(table,col)
    // console.log(`hii ${retRange(date1,date2)}`)
        let pool = await sql.connect(config);
        let insertProduct = await pool.request()
            // .input('Table', sql.NVarChar(1000), table)
            // .input('col', sql.NVarChar, col)
            .query(`SELECT FORMAT(OrderDate, 'yyyy-MM-dd') AS date, COUNT(OrderDate ) AS count FROM ${table} where TypeInfo = '${col}' and ${retRange(date1,date2)} GROUP BY FORMAT(OrderDate, 'yyyy-MM-dd');`)
   
            return insertProduct.recordsets;
    }
    catch (err) {
        console.log(err);
    }
}

//ספירת ערכים בעמודה עפי שם עמודה וטבלה
 //     SELECT srcDef, count(srcDef) 
        //     FROM Info_Update as count
        //    GROUP by srcDef;
async function getCountingValuesInColumn(table,col,date1 ,date2) {  
//  console.log(date1 ,date2)
    try {
    //    console.log(table,col,date1 ,date2)
        let pool = await sql.connect(config);
        // console.log(`SELECT ${col} as label, count(${col}) as count FROM ${table} where ${retRange(date1,date2)} GROUP by ${col};`)
        let insertProduct = await pool.request()
            // .input('Table', sql.NVarChar(1000), table)
            // .input('col', sql.NVarChar, col)
            .query(`SELECT ${col} as label, count(${col}) as count FROM ${table} where ${retRange(date1,date2)} and ${col} IS NOT NULL GROUP by ${col};`)
            
            return insertProduct.recordsets;
    }
    catch (err) {
      
        console.log(err);
    }
}


//ספירת ערכים בעמודה בשורות בהן התחילו מסלול
async function getColumnFromStartRoute(table , col,date1 ,date2) {  
    try {
       
        let pool = await sql.connect(config);
        let insertProduct = await pool.request()
            // .input('Table', sql.NVarChar(1000), table)
            // .input('col', sql.NVarChar, col)
            .query(`SELECT ${col} as label, count(${col}) as count FROM ${table} Where (TypeInfo='RouteFromSearch' OR TypeInfo='RouteFromIdToId') and ${retRange(date1,date2)} GROUP by ${col};`)
       
            return insertProduct.recordsets;
    }
    catch (err) {
        console.log(err);
    }
}
//SELECT FORMAT(OrderDate, 'HH') AS date , COUNT(OrderDate) AS count FROM Info_Basic_Log Where TypeInfo='RouteFromSearch' OR TypeInfo='RouteFromIdToId' GROUP BY FORMAT(OrderDate, 'HH')
//ספירת שעות ביום בהם התחיל מסלול
async function getRoutesHoursCount(date1 ,date2) {  
    try {
       
        let pool = await sql.connect(config);
        let insertProduct = await pool.request()
            .query(`SELECT FORMAT(OrderDate, 'HH') AS label , COUNT(OrderDate) AS count FROM Info_Basic_Log Where (TypeInfo='RouteFromSearch' OR TypeInfo='RouteFromIdToId') and ${retRange(date1,date2)} GROUP BY FORMAT(OrderDate, 'HH');`);
            return insertProduct.recordsets;
    }
    catch (err) {
        console.log(err);
    }
}


//SELECT FORMAT(OrderDate, 'HH') AS date , COUNT(OrderDate) AS count FROM Info_Basic_Log Where TypeInfo='RouteFromSearch' OR TypeInfo='RouteFromIdToId' GROUP BY FORMAT(OrderDate, 'HH')
//טבלת עדכונים וכניסות פר משתמש
async function getAverages(date1 ,date2) {  
    try {
       
        let pool = await sql.connect(config);
        let insertProduct = await pool.request()
            .query(`SELECT AVG(Cast(ll.countenter as Float)) as avg_enter, AVG(Cast(ll.countroute as float)) as avgroute, AVG(cast(NULLIF(ll.countupdate, 0) AS float)) as avgupdate
            FROM (
            SELECT zz.*, isnull(tt.Routes1, 0) as countroute, isnull(kk.countupdate, 0) as countupdate
            FROM
            (select idClient , Count(TypeInfo) AS Routes1 FROM Info_Basic_Log where (TypeInfo='RouteFromSearch' OR TypeInfo='RouteFromIdToId') and ${retRange(date1,date2)} GROUP BY idClient) tt
            full JOIN
            (select idClient , Count(TypeInfo) AS countenter FROM Info_Basic_Log where TypeInfo='EnterSearch' and ${retRange(date1,date2)} GROUP BY idClient) zz
            ON tt.IdClient = zz.IdClient
            full JOIN
            (select idClient , Count(TypeInfo) AS countupdate FROM Info_Update where TypeInfo='Update' and ${retRange(date1,date2)} GROUP BY idClient) kk
            ON kk.IdClient = zz.IdClient
            ) ll
            ;`);
            return insertProduct.recordsets;
    }
    catch (err) {
        console.log(err);
    }
}

function convertto01(str){
    if(str=='0'){
        return '\0';
    }
    if(str=='1'){
        return '\1';
    }
    return null;   
}
function checknull(inty){
    if(inty=='null'){
        return null;
    }
   return inty;
}
//feedback
async function insertFeedback(query) {  
    try {
    //    console.log(query);
     
       let pool = await sql.connect(config);
       let insertProduct = await pool.request()
            .input('OrderDate', sql.DateTime, query.timestamp)
           .input('Rating', sql.Int, checknull(query.rateRoute))
           .input('Isfinish', sql.VarChar(4),   checknull(query.isFinish)) //(query.isFinish)
           .input('Isback', sql.VarChar(4), checknull(query.isBack)) //parseInt(query.isBack)
           .input('Feeadbacktext', sql.NVarChar(255), query.feedbackText)//checknull(query.feedbackText))
        //    .query('insert into Feedback1 values("2022-05-25 21:17:37",null,null,null," ");');
           .query("insert into Feedback1 values(@OrderDate,@Rating,@Isfinish,@Isback, @Feeadbacktext);")
           // .execute('InsertInfo_Basic_Log');
           // insert into Info_Basic_Log values('2008-11-11 13:23:44','he','update',0.747289239285126, '{srcId:"OC101", dstId:"OC201", oldId:"OC001"}', '127.0.0.1');

           
           
        //    fetch(urlfeedback+`?rateRoute=${rateRoute}&isBack=${isBack}&isFinish=${isFinish}&feedbackText=${feedbackText}&timestamp=${retTime()}`)
       return insertProduct.recordsets;
    }
    catch (err) {
        // console.log(3);
        console.log(err);
    }
}


//feedback get information
async function getInformationFeedback(date1 ,date2) {  
    try {
       
        let pool = await sql.connect(config);
        let insertProduct = await pool.request()
            .query(`SELECT AVG(Cast(ll.countenter as Float)) as avg_enter, AVG(Cast(ll.countroute as float)) as avgroute, AVG(cast(NULLIF(ll.countupdate, 0) AS float)) as avgupdate
            FROM (
            SELECT zz.*, isnull(tt.Routes1, 0) as countroute, isnull(kk.countupdate, 0) as countupdate
            FROM
            (select idClient , Count(TypeInfo) AS Routes1 FROM Info_Basic_Log where (TypeInfo='RouteFromSearch' OR TypeInfo='RouteFromIdToId') and ${retRange(date1,date2)} GROUP BY idClient) tt
            full JOIN
            (select idClient , Count(TypeInfo) AS countenter FROM Info_Basic_Log where TypeInfo='EnterSearch' and ${retRange(date1,date2)} GROUP BY idClient) zz
            ON tt.IdClient = zz.IdClient
            full JOIN
            (select idClient , Count(TypeInfo) AS countupdate FROM Info_Update where TypeInfo='Update' and ${retRange(date1,date2)} GROUP BY idClient) kk
            ON kk.IdClient = zz.IdClient
            ) ll
            ;`);
            return insertProduct.recordsets;
    }
    catch (err) {
        console.log(err);
    }
}

// select CAST(OrderDate AS DATE) as OrderDate, Rating, Feedbacktext from Feedback1
//feedback get rating
async function getFeedbackDateRatingTxt(date1 ,date2) {  
    try {
    //    console.log(`select CAST(OrderDate AS DATE) as OrderDate, Rating, Feedbacktext from Feedback1 where (OrderDate IS NOT NULL and Rating IS NOT NULL) and (Feedbacktext IS NOT NULL) and ${retRange(date1,date2)};`)
        let pool = await sql.connect(config);
        let insertProduct = await pool.request()
            .query(`select CAST(OrderDate AS DATE) as OrderDate, Rating, Feedbacktext from Feedback1 where (OrderDate IS NOT NULL and Rating IS NOT NULL) and (Feedbacktext != 'null') and ${retRange(date1,date2)};`);
            return insertProduct.recordsets;
    }
    catch (err) {
        console.log(err);
    }
}

async function RestartDB() {  
    try {
    //    console.log(`select CAST(OrderDate AS DATE) as OrderDate, Rating, Feedbacktext from Feedback1 where (OrderDate IS NOT NULL and Rating IS NOT NULL) and (Feedbacktext IS NOT NULL) and ${retRange(date1,date2)};`)
    console.log('RestartDB');
    let pool = await sql.connect(configStart);
        let insertProduct = await pool.request()
            .query(`
            DROP TABLE Feedback1;
            DROP TABLE Info_Basic_Log;
            DROP TABLE Info_Update;
            `);
            return insertProduct.recordsets;
    }
    catch (err) {
        console.log(err);
    }
}
// DROP DATABASE newDatabase5;


async function CreateDb() {  
    try {
    //    console.log(`select CAST(OrderDate AS DATE) as OrderDate, Rating, Feedbacktext from Feedback1 where (OrderDate IS NOT NULL and Rating IS NOT NULL) and (Feedbacktext IS NOT NULL) and ${retRange(date1,date2)};`)
    console.log('CreateDb');
    let pool = await sql.connect(configStart);
        let insertProduct = await pool.request()
            .query(`
            CREATE DATABASE SimpleNav;            
            `);
            return insertProduct.recordsets;
    }
    catch (err) {
        console.log(err);
    }
}

async function CreateTabels() { 
    console.log('CreateTabels'); 
    try {
    //    console.log(`select CAST(OrderDate AS DATE) as OrderDate, Rating, Feedbacktext from Feedback1 where (OrderDate IS NOT NULL and Rating IS NOT NULL) and (Feedbacktext IS NOT NULL) and ${retRange(date1,date2)};`)

    let pool = await sql.connect(config);
    // console.log(config)
        let insertProduct = await pool.request()
            .query(`
   
            
            CREATE TABLE Feedback1 (
                OrderDate datetime NOT NULL,
                Rating int,
                Isfinish varchar(1),
                Isback varchar(1),
                Feedbacktext nvarchar(255),              
            );
          
            CREATE TABLE Info_Basic_Log (
                OrderDate datetime NOT NULL,
                Lang varchar(2),  
                TypeInfo nvarchar(20), 
                IdClient float,
                Data nvarchar(max),
                Ip varchar(20),      
               
            );
            
            CREATE TABLE Info_Update (
                OrderDate datetime NOT NULL,
                Lang varchar(2),  
                TypeInfo nvarchar(20), 
                IdClient float,
                oldDef nvarchar(10),
                srcDef nvarchar(10),
                destDef nvarchar(10),
                Ip varchar(20), 
            );
            
            `);
            return insertProduct.recordsets;
    }
    catch (err) {
        console.log(err);
    }
}

async function doesItExistDB(nameDB='SimpleNav') { 
    
    try {
    //    console.log(`select CAST(OrderDate AS DATE) as OrderDate, Rating, Feedbacktext from Feedback1 where (OrderDate IS NOT NULL and Rating IS NOT NULL) and (Feedbacktext IS NOT NULL) and ${retRange(date1,date2)};`)

    let pool = await sql.connect(configStart);
    // console.log(config)
        let insertProduct = await pool.request()
            .query(`
            SELECT count(*) as result from sys.databases WHERE name = '${nameDB}'; 
            `);
            return insertProduct.recordsets;
    }
    catch (err) {
        console.log(err);
    }
}

async function doesItExistTableFeedback1(nameTable='Feedback1') { 
    
    try {
    //    console.log(`select CAST(OrderDate AS DATE) as OrderDate, Rating, Feedbacktext from Feedback1 where (OrderDate IS NOT NULL and Rating IS NOT NULL) and (Feedbacktext IS NOT NULL) and ${retRange(date1,date2)};`)

    let pool = await sql.connect(config);
    // console.log(config)
        let insertProduct = await pool.request()
            .query(`
            SELECT count(*) as result FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '${nameTable}';
            `);
            return insertProduct.recordsets;
    }
    catch (err) {
        console.log(err);
    }
}

async function doesItExistTableInfo_Basic_Log(nameTable='Info_Basic_Log') { 
    return doesItExistTableFeedback1(nameTable)
}

async function doesItExistTableInfo_Update(nameTable='Info_Update') { 
    return doesItExistTableFeedback1(nameTable)
}






async function DropTableFeedback1(nameTable='Feedback1') { 
    
    try {
    //    console.log(`select CAST(OrderDate AS DATE) as OrderDate, Rating, Feedbacktext from Feedback1 where (OrderDate IS NOT NULL and Rating IS NOT NULL) and (Feedbacktext IS NOT NULL) and ${retRange(date1,date2)};`)
    // DROP TABLE ${nameTable};
    let pool = await sql.connect(config);
    // console.log(config)
        let insertProduct = await pool.request()
            .query(`
            DELETE FROM ${nameTable};
            `);
            return insertProduct.recordsets;
    }
    catch (err) {
        console.log(err);
    }
}

async function DropTableInfo_Basic_Log(nameTable='Info_Basic_Log') { 
    return DropTableFeedback1(nameTable)
}

async function DropTableInfo_Update(nameTable='Info_Update') { 
    return DropTableFeedback1(nameTable)
}


async function CreateTableFeedback1(nameTable='Feedback1') { 
    
    try {
    //    console.log(`select CAST(OrderDate AS DATE) as OrderDate, Rating, Feedbacktext from Feedback1 where (OrderDate IS NOT NULL and Rating IS NOT NULL) and (Feedbacktext IS NOT NULL) and ${retRange(date1,date2)};`)

    let pool = await sql.connect(config);
    // console.log(config)
        let insertProduct = await pool.request()
            .query(`
                CREATE TABLE ${nameTable} (
                OrderDate datetime NOT NULL,
                Rating int,
                Isfinish varchar(1),
                Isback varchar(1),
                Feedbacktext nvarchar(255),              
            );
            `);
            return insertProduct.recordsets;
    }
    catch (err) {
        console.log(err);
    }
}




async function CreateTableInfo_Basic_Log(nameTable='Info_Basic_Log') { 
    try {
        //    console.log(`select CAST(OrderDate AS DATE) as OrderDate, Rating, Feedbacktext from Feedback1 where (OrderDate IS NOT NULL and Rating IS NOT NULL) and (Feedbacktext IS NOT NULL) and ${retRange(date1,date2)};`)
    
        let pool = await sql.connect(config);
        // console.log(config)
            let insertProduct = await pool.request()
                .query(`
                    CREATE TABLE ${nameTable} (
                    OrderDate datetime NOT NULL,
                    Lang varchar(2),  
                    TypeInfo nvarchar(20), 
                    IdClient float,
                    Data nvarchar(max),
                    Ip varchar(20),      
                   
                );
                `);
                return insertProduct.recordsets;
        }
        catch (err) {
            console.log(err);
        }
}

async function CreateTableInfo_Update(nameTable='Info_Update') { 
    try {
        //    console.log(`select CAST(OrderDate AS DATE) as OrderDate, Rating, Feedbacktext from Feedback1 where (OrderDate IS NOT NULL and Rating IS NOT NULL) and (Feedbacktext IS NOT NULL) and ${retRange(date1,date2)};`)
    
        let pool = await sql.connect(config);
        // console.log(config)
            let insertProduct = await pool.request()
                .query(`
                CREATE TABLE ${nameTable} (
                    OrderDate datetime NOT NULL,
                    Lang varchar(2),  
                    TypeInfo nvarchar(20), 
                    IdClient float,
                    oldDef nvarchar(10),
                    srcDef nvarchar(10),
                    destDef nvarchar(10),
                    Ip varchar(20), 
                );
                `);
                return insertProduct.recordsets;
        }
        catch (err) {
            console.log(err);
        }
}







module.exports={
    getInfoBasic: getInfoBasic,
    getInfoBasicId: getInfoBasicId,
    addInfoBasicRow:addInfoBasicRow,
    addInfoUpdateRow:addInfoUpdateRow,

    //feedback
    insertFeedback:insertFeedback,
    getFeedbackDateRatingTxt:getFeedbackDateRatingTxt,



    getOrderDateOfRoutes:getOrderDateOfRoutes,

    //ספירה של תאריך
    getCountRoutesByDateAndType:getCountRoutesByDateAndType,
     //ספירה 
    getCountingValuesInColumn:getCountingValuesInColumn,
    getColumnFromStartRoute:getColumnFromStartRoute,
    getRoutesHoursCount:getRoutesHoursCount,

    //ממוצע
    getAverages:getAverages,

    RestartDB:RestartDB,
    CreateDb:CreateDb,
    CreateTabels:CreateTabels,



    
doesItExistDB:doesItExistDB,
doesItExistTableFeedback1:doesItExistTableFeedback1,
doesItExistTableInfo_Basic_Log:doesItExistTableInfo_Basic_Log,
doesItExistTableInfo_Update:doesItExistTableInfo_Update,
DropTableFeedback1:DropTableFeedback1,
DropTableInfo_Basic_Log:DropTableInfo_Basic_Log,
DropTableInfo_Update:DropTableInfo_Update,
CreateTableFeedback1:CreateTableFeedback1,
CreateTableInfo_Basic_Log:CreateTableInfo_Basic_Log,
CreateTableInfo_Update:CreateTableInfo_Update,
}

