
const addressConsts = require('../../configFiles/configAll.json')
const mssqlPort = addressConsts.portsqlserver;
const mssqlIP = addressConsts.ipsqlserver;
const mssqlPassword = addressConsts.passwordsqlserver;

const config = {
    user : 'sa',
    password : mssqlPassword,
    server: mssqlIP,
    database:'SimpleNav',
    options:{
        trustedconnection: true,
        enableArithAbort : true,
        instancename : mssqlIP + ',' + mssqlPort,
        // instancename : '127.0.0.1,50388',
        encrypt: false
    },
    port : parseInt(mssqlPort)
}
// port : 50387
// database: 'SimpleNav',
//password : 'password',
// instancename : 'SQLEXPRESS01',
// server: 'localhost',

const config_all={

    port : parseInt(mssqlPort),
    user : 'sa',
    password : mssqlPassword,
    server: mssqlIP,
    database:'SimpleNav',
    options:{
        trustedconnection: true,
        enableArithAbort : true,
        instancename : mssqlIP + ',' + mssqlPort,
        // instancename : '127.0.0.1,50388',
        encrypt: false
        },

}
// instancename : 'localhost,1433',

// const config = {
//     user : `${config_all.user}`,
//     password : `${config_all.password}`,
//     server: `${config_all.server}`,
//     database: `${config_all.database}`,
//     options:config_all.options,
//     port : config_all.port_all
// }

const configStart = {
    user : `${config_all.user}`,
    password : `${config_all.password}`,
    server: `${config_all.server}`,
    options:config_all.options,
    port : config_all.port
}



// http://localhost:28938/api/op
// docker run -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=yourStrong(!)Password" -e "MSSQL_PID=Express" -p 1440:1433 -d mcr.microsoft.com/mssql/server:2019-latest 
//docker run -e "ACCEPT_EULA=Y" -e "MSSQL_SA_PASSWORD=Adminxyz22#" -p 1440:1433 --name SQLEXPRESS01 --hostname SQLEXPRESS01 -d mcr.microsoft.com/mssql/server:2019-latest
//docker run -e "ACCEPT_EULA=Y" -e "MSSQL_SA_PASSWORD=Adminxyz22#" -e "MSSQL_PID=Express" -p 1433:1433 -d mcr.microsoft.com/mssql/server:2019-latest



module.exports = {
    config:config,
    configStart:configStart
}