/*
Functions used for deleting information from the database, like checkpoints, areas and paths.
*/
var dataConn = require('../DB-Interface/basic_db_queries')
//adds list of outerVs
tester()
async function tester() {
    // console.log(await jsGetListOfOuterV());
    // console.log(await jsDeleteOuter("thisFake"))
    // console.log(await jsGetListOfBuilding())
    // console.log(await jsGetListOfAreas("building 1"))
    // console.log(await jsDeleteInner("test", "delete"))
    // console.log(await jsDeleteEdge("path", "fakePlc", "fake", "building 1"))
}
//returns list of outer Vs
async function jsGetListOfOuterV(map) {
    let areas = []
    await dataConn.dbGetOuterVs().then(res => {
        res.forEach(thing => {
            areas.push(thing)
        })
    })
    return areas
}

//deletes outerV
//recives string with name of outer V
async function jsDeleteOuter(map) {
    outer = map["outer"]
    let ret = {}
    await dataConn.dbDeleteOuterV(outer).then(succ => {
        if (succ) {
            ret['message'] = 'נמחק בהצלחה'
        } else {
            ret['message'] = 'קרתה שגיאה, יתכן וחלק מהקשתות נמחקו'
        }
    })
    return ret
}
//returns a list of buildings
async function jsGetListOfBuilding(map) {
    var build = []
    await dataConn.dbListBuildings().then(function (res) {
        build = res
    })
    return build
}

//gets list of areas from within a building
// recieves string building which is the name of building we want the areas of
async function jsGetListOfAreas(map) {
    let building = map["building"]
    var areas = []
    await dataConn.dbGetInBuilding(building).then(res => {
        res.forEach(thing => {
            areas.push(thing)
        })
    })
    return areas
}

/*
*gets list of places for sources in pathbuilder
* recives typePath which is the type of path it is
    meaning entrance, exit, path, trail
    and building which is either the building name or an empty string
*/
async function jsSetSrc(map) {
    let typePath = map["typePath"]
    let building = map["building"]

    var areas = []
    //if were starting from inside a building
    if (typePath == "path" || typePath == 'exit') {
        //get a list of areas inside that building
        await dataConn.dbGetInBuilding(building).then(res => {
            res.forEach(thing => {
                areas.push(thing)
            })
        })
        //if it's an outer v, get list of outer v names
    } else {
        await dataConn.dbGetOuterVs().then(res => {
            res.forEach(thing => {
                areas.push(thing)
            })
        })
    }
    //set options to be this
    return areas
}
/*
gets list of of destinations
* recives typePath which is the type of path it is
    meaning entrance, exit, path, trail
    and building which is either the building name or an empty string
*/
async function jsSetDest(map) {
    let typePath = map["typePath"]
    let building = map["building"]

    var areas = []
    //if were ending from inside a building
    if (typePath == "path" || typePath == 'entrance') {
        //get a list of areas inside that building
        await dataConn.dbGetInBuilding(building).then(res => {
            res.forEach(thing => {
                areas.push(thing)
            })
        })
        //if it's an outer v, get list of outer v names
    } else {
        await dataConn.dbGetOuterVs().then(res => {
            res.forEach(thing => {
                areas.push(thing)
            })
        })
    }
    //set options to be this
    return areas
}
/*
deletes inner v and all its edges
string area (name of inner area)
string building (name of building)

returns if succeeded or not
*/
async function jsDeleteInner(map) {

    let area = map["area"]
    let building = map["building"]
    
    let ret = {}
    //delete, let admin know if it was deleted or not
    await dataConn.dbDeleteInnerV(building, area).then(succ => {

        if (succ) {
            ret['message'] = 'נמחק בהצלחה'
        } else {
            ret['message'] = 'קרתה שגיאה, יתכן וחלק מהקשתות נמחקו'
        }
    })
    return ret
}
/*
deletes edge
recives string type (of path, entrance, exit, trail, path), 
string src (name/area name)
string dest (name/area name)
string building (empty string if its a trail (between two outers))
*/
async function jsDeleteEdge(map) {

    let type = map["type"]
    let src = map["src"]
    let dest = map["dest"] 
    let building = map["building"]

    //get the queries, exists checks if there exists an edge alredy, query is the delete query
    let tup = dataConn.dbBuildDeleteEdgeQuery(type, src, dest, building)
    let exists = tup[0]
    let query = tup[1]
    ret = {}
    let canProceed = true
    //check if this edge exists
    await dataConn.dbDoEdgesExist(exists).then(res => {
        if (res == -1) {
            ret['message'] = "הפעולה נכשלה, המסלול לא נמחק"
            canProceed = false
            //if the edge doesnt exist, write appropriate message
        } else if (res == 0) {
            ret['message'] = "לא קיים מסלול כזה מלכתחילה, שום פעולה לא בוצע"
            canProceed = false
        }
    })
    //if this edge exists, delete it
    if (canProceed) {
        //send the query to create edge and print if succeded or not
        await dataConn.dbDeleteEdge(query).then(worked => {
            if (worked) {
                ret['message'] = "המסלול נמחק בהצלחה"
            } else {
                ret['message'] = "הפעולה נכשלה, המסלול לא נמחק"
            }
        })
        return ret
    }
}
module.exports= {
    delJsDeleteEdge: jsDeleteEdge,
    delJsDeleteInner: jsDeleteInner,
    delJsSetDest: jsSetDest, 
    delJsSetSrc: jsSetSrc,
    delJsGetListOfAreas:jsGetListOfAreas,
    delJsGetListOfBuilding: jsGetListOfBuilding,
    delJsDeleteOuter:jsDeleteOuter,
    delJsGetListOfOuterV: jsGetListOfOuterV
}