/*
This is the admin server API with all the calls the frontend needs for the admin to be able to operate the
system
*/
const express = require("express");
var bodyParser = require('body-parser');
const app = express()
app.use(bodyParser.json());
var parser = require('./parser.js')

const userManager = require('./user-sessions/userManager.js')
var fs = require('fs');



// const constAddresses = JSON.parse(
//     fs.readFileSync(require('path').resolve('configFiles', 'configAll.json'))
//     // fs.readFileSync(require('path').resolve(__dirname, 'Backend-API/configFiles/ConfigAll.json'))
//     );
const constAddresses = require('../configFiles/configAll.json');
// const constAddresses = JSON.parse(require('path').resolve(__dirname, 'configFiles/ConfigAll.json'));
console.log(constAddresses);

// const constAddresses = require('./consts.json')



//added for cookies
// app.use(express.json())
const auth = require('./user-sessions/authentication.js')
// var bob = "three"
const cookieParser = require('cookie-parser')
const uuid = require('uuid')
// Invoking this server port from the config.
const port = constAddresses.portServerDbAdmin;
const frontendAddress = constAddresses.protocol + constAddresses.domain + ":" + constAddresses.portServerNav;
app.use(cookieParser())

// var http = require('http');
var https = require('https');
const path_private_key = require('path').resolve(__dirname, 'https-keys/admin_key.pem')
const path_private_cert = require('path').resolve(__dirname, 'https-keys/admin_cert.pem')

var privateKey  = fs.readFileSync(path_private_key, 'utf8');
var certificate = fs.readFileSync(path_private_cert, 'utf8');


// DELETE THIS
console.log(privateKey)
console.log(certificate)


var credentials = {key: privateKey, cert: certificate};

var cors = require('cors')

console.log(frontendAddress)
console.log(port)

app.use(cors({
    // origin: 'https://localhost:3000',
    origin: frontendAddress,
    credentials: true,
}))
// app.use(cors())


this._baseUrl = 'https://' + constAddresses.domain + ':' + port + '/';

/*
used to sign in and create a 15 minute session
recieves json:
{
    "username":"string",
    "password": "string"
}
*/
app.post('/admin/signin', auth.signinHandler)

/*
refreshes the session for another 15 min
recieves nothing
*/
app.post('/admin/refresh', auth.refreshHandler)

//checks if an admin is connected, if so returns true, else returns fals
app.get('/admin/isAdmin', auth.isAdminConnected)
/*
logs out of session
Recieves nothing
*/
app.get('/admin/logout', auth.logoutHandler)



//added for create users
/*
creates new user.
Sends email to master user to confirm the creating of new user is valid
recieves
{
    "firstName": "string",
    "lastName": "string",
    "password": "string",
    "email": "string"
 }
 example:
 {
    "firstName": "bob",
    "lastName": "jackson",
    "password": "12345",
    "email": "bob@gmail.com"
 }
*/
app.post('/admin/createNewUser', userManager.createNewUser)

/*
accepts the new admin to system
recieves:
admin thats accepting email and password along with the admin to accept's email
{
  "superAdminEmail": "string",
  "superAdminPassword": "string",
  "newAdminEmail": "string"
}
*/
app.post('/admin/acceptNewUser', userManager.acceptNewUser)

/*
when a password is lost, it sends a new password to the users email and he has 10 minutes to
change his password.
recieves:
{
    "firstName": "string",
    "lastName": "string",
    "email": "string"
}
example:
{
    "firstName": "bob",
    "lastName": "jackson",
    "email": "bob@gmail.com"
}
*/
app.post('/admin/passwordRecovery', userManager.passwordRecovery)

/*
after recovery code is sent, this is used to reset the password.
recieves:
code is the 6 digit recovery code
newpassword is the new password,
confirm password is the new password again to make sure there are no errors
and email.

{
    "code": "string",
    "newPassword": "string",
    "confirmPassword": "string",
    "email": "string"
}
example.
{
    "code": "532912",
    "newPassword": "abcd",
    "confirmPassword": "abcd",
    "email": "bob@gmail.com"
}

*/
app.post('/admin/resetPasswordWithCode', userManager.resetPasswordWithCode)

// app.listen(port, () => {
//     console.log("Application started and Listening on port " + port);
// });

app.get('/testing', (req, res) => {
    res.send("hey Billy!")
});

/*
"addJsCreateNewInnerV":{"def":"string", "allowEntry":"int", "aName":"string", "building":"string", "floor":"int", "rooms":"list_of_strings"}

creates inner v
 recives string def (= areaID), int allowEntry (1 or 0), string aName (=areaName),
  string building, int floor, list string rooms[], 
*/
// app.post('/admin/addJsCreateNewInnerV', auth.refreshOtherCalls, (req, res) => {
app.post('/admin/addJsCreateNewInnerV', auth.refreshOtherCalls, (req, res) => {
    var thisJson = { type: "add", functionName: "addJsCreateNewInnerV", args: req.body }
    //send the json
    parser.runRequest(thisJson).then(retVal => {
        res.json({ key: retVal })
    })
});


/*
    "addJSetDest":{"typePath":"string", "building":"string"},

    sends list of of destinations
    recives typePath which is the type of path it is
    meaning entrance, exit, path, trail
    and building which is either the building name or an empty string
*/
app.post('/admin/addJsSetDest', auth.refreshOtherCalls, (req, res) => {
    var thisJson = { type: "add", functionName: "addJsSetDest", args: req.body }
    //send the json
    parser.runRequest(thisJson).then(retVal => {
        res.json({ key: retVal })
    })
});
/*
"addJsSetSrc":{"typePath":"string", "building":"string"}

    returns list of places for sources in pathbuilder
    recives typePath which is the type of path it is
    meaning entrance, exit, path, trail
    and building which is either the building name or an empty string
*/
app.post('/admin/addJsSetSrc', auth.refreshOtherCalls, (req, res) => {
    var thisJson = { type: "add", functionName: "addJsSetSrc", args: req.body }
    //send the json
    parser.runRequest(thisJson).then(retVal => {
        res.json({ key: retVal })
    })

});

/*
"addJsAddOuter":{"name":"string", "allowEntry":"int"}

adds an outer vertex on submit, recives string name and 0, 1 allowEntry
*/
app.post('/admin/addJsAddOuter', auth.refreshOtherCalls, (req, res) => {
    var thisJson = { type: "add", functionName: "addJsAddOuter", args: req.body }
    //send the json
    parser.runRequest(thisJson).then(retVal => {
        res.json({ key: retVal })
    })
});

/*
"addJsGetListOfBuilding":{}

returns list of buildings
*/

app.get('/admin/addJsGetListOfBuilding', auth.refreshOtherCalls, (req, res) => {
    var thisJson = { type: "add", functionName: "addJsGetListOfBuilding", args: {} }
    //send the json
    parser.runRequest(thisJson).then(retVal => {
        res.json({ key: retVal });
    });

});

/*
    "addJsCreatePath":{"type":"string", "src":"string", "dest":"string", "weight":"int", "building":"string", "directions":"list_of_strings", "classification":"list_of_strings", "distances":"list_of_ints", "freeText":"list_of_strings"}

    creates path in inner vertex
recives string type (of path, entrance, exit, trail, path), 
string src (name/area name)
string dest (name/area name)
int weight (of path)
string building (empty string if its a trail (between two outers))
list string directions[] (directions, up down right, left for each microdirection)
list string classification[] (hallway, stairs, loby... for each microdirection)
list int distances[] (5, 10, 20 for each microdirection)
list string freeText[] (go left fo 20 meters at hallway, ... for each microdirection)
*/
app.post('/admin/addJsCreatePath', auth.refreshOtherCalls, (req, res) => {
    var thisJson = { type: "add", functionName: "addJsCreatePath", args: req.body }
    //send the json
    parser.runRequest(thisJson).then(retVal => {
        res.json({ key: retVal })
    })
});

/*
"addJsGetPathTypes":{}

returns the path types
example:
"entrance", "exit", "path", "trail"
*/
app.get('/admin/addJsGetPathTypes', auth.refreshOtherCalls, (req, res) => {
    var thisJson = {type : "add", functionName: "addJsGetPathTypes", args: {}}
    //send the json
    parser.runRequest(thisJson).then(retVal => {
        res.json({ key: retVal })
    })
});

/*
"addJsGetDirClassifications":{}

returns direction classifications
example:
"מסדרון", "מעלית", "מדרגות", "לובי"
*/
app.post('/admin/addJsGetDirClassifications', auth.refreshOtherCalls, (req, res) => {
    var thisJson = { type: "add", functionName: "addJsGetDirClassifications", args: {} }
    //send the json
    parser.runRequest(thisJson).then(retVal => {
        res.json({ key: retVal })
    })
});
/*
"addJsGetDirTypes":{}

returns direction types
example:
 "למעלה", "ימינה", "למטה", "שמאלה", "ישר", "צפון", "דרום", "מזרח", "מערב"
*/
app.post('/admin/addJsGetDirTypes', auth.refreshOtherCalls, (req, res) => {
    var thisJson = { type: "add", functionName: "addJsGetDirTypes", args: {} }
    //send the json
    parser.runRequest(thisJson).then(retVal => {
        res.json({ key: retVal })
    })
});

/*
"delJsDeleteEdge":{"type":"string", "src":"string", "dest":"string","building":"string"}

deletes edge
recives string type (of path, entrance, exit, trail, path), 
string src (name/area name)
string dest (name/area name)
string building (empty string if its a trail (between two outers))
*/
app.post('/admin/delJsDeleteEdge', auth.refreshOtherCalls, (req, res) => {
    var thisJson = { type: "del", functionName: "delJsDeleteEdge", args: req.body }
    //send the json
    parser.runRequest(thisJson).then(retVal => {
        res.json({ key: retVal })
    })
});

/*
"delJsDeleteInner":{"area":"string", "building":"string"}

deletes inner v and all its edges
string area (name of inner area)
string building (name of building)

returns if succeeded or not
*/
app.post('/admin/delJsDeleteInner', auth.refreshOtherCalls, (req, res) => {
    var thisJson = { type: "del", functionName: "delJsDeleteInner", args: req.body }
    //send the json
    parser.runRequest(thisJson).then(retVal => {
        res.json({ key: retVal })
    })
});

/*
"delJsSetDest":{"typePath":"string", "building":"string"}

returns list of of destinations
recives typePath which is the type of path it is
meaning entrance, exit, path, trail
and building which is either the building name or an empty string
*/
app.post('/admin/delJsSetDest', auth.refreshOtherCalls, (req, res) => {
    var thisJson = { type: "del", functionName: "delJsSetDest", args: req.body }
    //send the json
    parser.runRequest(thisJson).then(retVal => {
        res.json({ key: retVal })
    })
});

/*
"delJsSetSrc":{"typePath":"string", "building":"string"}

returns list of places for sources in pathbuilder
recives typePath which is the type of path it is
meaning entrance, exit, path, trail
and building which is either the building name or an empty string
*/
app.post('/admin/delJsSetSrc', auth.refreshOtherCalls, (req, res) => {
    var thisJson = { type: "del", functionName: "delJsSetSrc", args: req.body }
    //send the json
    parser.runRequest(thisJson).then(retVal => {
        res.json({ key: retVal })
    })
});

/*
"delJsGetListOfAreas":{"building":"string"}

gets list of areas from within a building
recieves string building which is the name of building we want the areas of
*/
app.post('/admin/delJsGetListOfAreas', auth.refreshOtherCalls, (req, res) => {
    var thisJson = { type: "del", functionName: "delJsGetListOfAreas", args: req.body }
    //send the json
    parser.runRequest(thisJson).then(retVal => {
        res.json({ key: retVal })
    })
});

/*
"delJsGetListOfBuilding":{}

returns a list of buildings
*/
app.post('/admin/delJsGetListOfBuilding', auth.refreshOtherCalls, (req, res) => {
    var thisJson = { type: "del", functionName: "delJsGetListOfBuilding", args: {} }
    //send the json
    parser.runRequest(thisJson).then(retVal => {
        res.json({ key: retVal })
    })
});

/*
"delJsDeleteOuter":{"outer":"string"}

deletes outerV
recives string with name of outer V
*/
app.post('/admin/delJsDeleteOuter', auth.refreshOtherCalls, (req, res) => {
    var thisJson = { type: "del", functionName: "delJsDeleteOuter", args: req.body }
    //send the json
    parser.runRequest(thisJson).then(retVal => {
        res.json({ key: retVal })
    })
});

/*
"delJsGetListOfOuterV":{}

returns list of outer Vs
*/
app.post('/admin/delJsGetListOfOuterV', auth.refreshOtherCalls, (req, res) => {
    var thisJson = { type: "del", functionName: "delJsGetListOfOuterV", args: {} }
    //send the json
    parser.runRequest(thisJson).then(retVal => {
        res.json({ key: retVal })
    })
});

/*
"updateJsUpdatePath":{"type":"string", "src":"string", "dest":"string","building":"string","weight":"int","directions":"list_of_strings", "classification":"list_of_strings", "distances":"list_of_ints", "freeText":"list_of_strings"}

updates the path by deleting old one and creating new one
recieves:
string type (exit, entrance, trail, path)
string src (src vertex name)
string dest (destination vertex name)
string building (the building of the path, empty string if trail)
int weight (of path),
list string directions[] (directions, up down right, left for each microdirection),
list string classification[] (hallway, stairs, loby... for each microdirection),
list int distances[] (5, 10, 20 for each microdirection),
list string freeText[] (go left fo 20 meters at hallway, ... for each microdirection)

returns if succeded or not
*/
app.post('/admin/updateJsUpdatePath', auth.refreshOtherCalls, (req, res) => {
    var thisJson = { type: "update", functionName: "updateJsUpdatePath", args: req.body }
    //send the json
    parser.runRequest(thisJson).then(retVal => {
        res.json({ key: retVal })
    })
});

/*
"updateJsGetPathDetails":{"building":"string", "type":"string", "src":"string", "dest":"string"}

gets details about specific path
string building (the building of the path, empty string if trail)
string type (exit, entrance, trail, path)
string src (src vertex name)
string dest (destination vertex name)

returns either error message or map as so:
return {"weight":weight, "directions":directions, "classifications":classification, 
    "distances":distances, "freeText": freeText}
such that types are:
{
int weight (of path),
list string directions[] (directions, up down right, left for each microdirection),
list string classification[] (hallway, stairs, loby... for each microdirection),
list int distances[] (5, 10, 20 for each microdirection),
list string freeText[] (go left fo 20 meters at hallway, ... for each microdirection)
*/
app.post('/admin/updateJsGetPathDetails', auth.refreshOtherCalls, (req, res) => {
    var thisJson = { type: "update", functionName: "updateJsGetPathDetails", args: req.body }
    //send the json
    parser.runRequest(thisJson).then(retVal => {
        res.json({ key: retVal })
    })
});

/*
"updateJsUpdateInner":{"oldBuilding": "string", "oldArea":"string", "building":"string", "aName":"string", "allowEntry":"int", "def":"string", "floor":"int", "rooms":"list_of_strings"}

updates inner V

recieves:
string oldBuilding (old building name)
string oldArea (old area name)
string building (new building name)
string area (new area Name)
int allowEntry (1 or 0),
string def (the unique ID decided by admin),
int floor,
list string rooms (the rooms on the floor)

returns message (of success or not)
*/
app.post('/admin/updateJsUpdateInner', auth.refreshOtherCalls, (req, res) => {
    var thisJson = { type: "update", functionName: "updateJsUpdateInner", args: req.body }
    //send the json
    parser.runRequest(thisJson).then(retVal => {
        res.json({ key: retVal })
    })
});

/*
"updateJsGetAreaDetails":{"building":"string", "area":"string"}

get info about specific innerV.
recieves:
string building (building the area is in)
string area (name of the area)

returns map as so:
{"building": building, "area":area, "entry": allowEntry, "def":def, "floor":floor, "rooms":rooms}
types in map:
{
string building,
string area,
int allowEntry (1 or 0),
string def (the unique ID decided by admin),
int floor,
list string rooms (the rooms on the floor)
}
*/
app.post('/admin/updateJsGetAreaDetails', auth.refreshOtherCalls, (req, res) => {
    var thisJson = { type: "update", functionName: "updateJsGetAreaDetails", args: req.body }
    //send the json
    parser.runRequest(thisJson).then(retVal => {
        res.json({ key: retVal })
    })
});

/*
"updateJsGetListOfAreas":{"building":"string"}

gets list of areas from within a building
recieves string building which is the name of building we want the areas of
*/
app.post('/admin/updateJsGetListOfAreas', auth.refreshOtherCalls, (req, res) => {
    var thisJson = { type: "update", functionName: "updateJsGetListOfAreas", args: req.body }
    //send the json
    parser.runRequest(thisJson).then(retVal => {
        res.json({ key: retVal })
    })
});

/*
"updateJsUpdateOuter":{"ogName":"string", "newName":"string", "allowEntry":"int"}

updates outer v
recives:
string ogName (the original name of the outer v)
string newName
int allowEntry (1 or 0)
returns message if it worked or not
*/
app.post('/admin/updateJsUpdateOuter', auth.refreshOtherCalls, (req, res) => {
    var thisJson = { type: "update", functionName: "updateJsUpdateOuter", args: req.body }
    //send the json
    parser.runRequest(thisJson).then(retVal => {
        res.json({ key: retVal })
    })
});

/*
"updateJsGetOuterByName":{"outerName":"string"}

gets information about specific outerV by name
recives string name of outerV, returns information about it

returns map {"name": outerName, "entry": allow}

such that:
name = outerVname, entry = int allowEntry (1 or 0)]
*/
app.post('/admin/updateJsGetOuterByName', auth.refreshOtherCalls, (req, res) => {
    var thisJson = { type: "update", functionName: "updateJsGetOuterByName", args: req.body }
    //send the json
    parser.runRequest(thisJson).then(retVal => {
        res.json({ key: retVal })
    })
});

/*
"updateJsGetListOfOuterV":{}

returns list of outer vs
*/
app.post('/admin/updateJsGetListOfOuterV', auth.refreshOtherCalls, (req, res) => {
    var thisJson = { type: "update", functionName: "updateJsGetListOfOuterV", args: {} }
    //send the json
    parser.runRequest(thisJson).then(retVal => {
        res.json({ key: retVal })
    })
});

/*
"updateJsSetDest":{"typePath":"string", "building":"string"}

returns list of of destinations
recives typePath which is the type of path it is
meaning entrance, exit, path, trail
and building which is either the building name or an empty string
*/
app.post('/admin/updateJsSetDest', auth.refreshOtherCalls, (req, res) => {
    var thisJson = { type: "update", functionName: "updateJsSetDest", args: req.body }
    //send the json
    parser.runRequest(thisJson).then(retVal => {
        res.json({ key: retVal })
    })
});

/*
"updateJsSetSrc":{"typePath":"string", "building":"string"}

returns list of places for sources in pathbuilder
recives typePath which is the type of path it is
meaning entrance, exit, path, trail
and building which is either the building name or an empty string
*/
app.post('/admin/updateJsSetSrc', auth.refreshOtherCalls, (req, res) => {
    var thisJson = { type: "update", functionName: "updateJsSetSrc", args: req.body }
    //send the json
    parser.runRequest(thisJson).then(retVal => {
        res.json({ key: retVal })
    })

});

/*
"updateJsGetListOfBuilding":{}

returns list of buildings
*/
// app.get('/admin/updateJsGetListOfBuilding',(req, res) => {
app.get('/admin/updateJsGetListOfBuilding', auth.refreshOtherCalls, (req, res) => {
    var thisJson = { type: "update", functionName: "updateJsGetListOfBuilding", args: {} }
    //send the json
    parser.runRequest(thisJson).then(retVal => {
        console.log(retVal);
        res.json({ key: retVal })
    })
});


/*
look at a certain floor in certain building.
Recives map {
    floor: "integer",
    building: "string"
}

Test call:
certainFloorCertainBuiding({floor:1, building:"מרפאות חוץ"});
*/
app.post('/admin/drawCertainFloorCertainBuiding',(req, res) => {
    var thisJson = { type: "draw", functionName: "drawCertainFloorCertainBuiding", args: req.body }
    //send the json
    parser.runRequest(thisJson).then(retVal => {
        res.json({ key: retVal })
    })
});

/*
look at a certain building.
Recives map {
    building:"string"
}

Test call:
entireBuilding({building: "מרפאות חוץ"})
*/
app.post('/admin/drawEntireBuilding',(req, res) => {
    var thisJson = { type: "draw", functionName: "drawEntireBuilding", args: req.body }
    //send the json
    parser.runRequest(thisJson).then(retVal => {
        res.json({ key: retVal })
    })
});

/*
returns a specific area node by building and name
Recives map {
    building:"string",
    areaName:"string"
}

returns {Nodes: nodes}

example call:
specificAreaByName({areaName:"אשפוז יום", building:"מרפאות חוץ"})
*/
app.post('/admin/drawSpecificAreaByName',(req, res) => {
    var thisJson = { type: "draw", functionName: "drawSpecificAreaByName", args: req.body }
    //send the json
    parser.runRequest(thisJson).then(retVal => {
        res.json({ key: retVal })
    })
});

/*
returns a specific area node by def
Recives map {
    def: "string"
}

returns {Nodes: nodes}

example call:
specificAreaByDef({def:"OC201"})
*/
app.post('/admin/drawSpecificAreaByDef',(req, res) => {
    var thisJson = { type: "draw", functionName: "drawSpecificAreaByDef", args: req.body }
    //send the json
    parser.runRequest(thisJson).then(retVal => {
        res.json({ key: retVal })
    })
});


/*
returns a specific outer V
Recives map {
    name: "string"
}

returns {Nodes: "list_of_nodes"}

example call:
specificOuterV({name:"temp"})
*/
app.post('/admin/drawSpecificOuterV',(req, res) => {
    var thisJson = { type: "draw", functionName: "drawSpecificOuterV", args: req.body }
    //send the json
    parser.runRequest(thisJson).then(retVal => {
        res.json({ key: retVal })
    })
});

/*
returns two nodes and the relationships (if any) between them
Recives map {
    type: "string",
    srcName: "string",
    destName:"string",
    building:"string"
}

returns {Nodes: "list_of_nodes", Relationships: "list_of_relationshiph"}

example calls:
// betweenTwoPlaces({ type: "path", srcName: "המכון לבריאות השד (ממוגרפיה)", destName: "אשפוז יום", building: "מרפאות חוץ" })
// betweenTwoPlaces({type:"path", srcName:"המכון לבריאות השד (ממוגרפיה)", destName:"מכון הלב", building:"מרפאות חוץ"})
// betweenTwoPlaces({type:"exit", srcName:"המכון לבריאות השד (ממוגרפיה)", destName:"מעבר בין בניין אישפוז למרפאות חוץ קומת כניסה", building:"מרפאות חוץ"})
// betweenTwoPlaces({type:"entrance", destName:"המכון לבריאות השד (ממוגרפיה)", srcName:"מעבר בין בניין אישפוז למרפאות חוץ קומת כניסה", building:"מרפאות חוץ"})
// betweenTwoPlaces({type:"trail", srcName:"temp", destName:"מעבר בין בניין אישפוז למרפאות חוץ קומת כניסה"})
*/
app.post('/admin/drawBetweenTwoPlaces',(req, res) => {
    var thisJson = { type: "draw", functionName: "drawBetweenTwoPlaces", args: req.body }
    //send the json
    parser.runRequest(thisJson).then(retVal => {
        res.json({ key: retVal })
    })
});



/*
"downloadAsJson":{}

returns a json of the graph
*/
app.get('/admin/downloadAsJson',(req, res) => {
    var thisJson = { type: "json", functionName: "downloadAsJson", args: {} }
    //send the json
    parser.runRequest(thisJson).then(retVal => {
        res.json(retVal)
    })

})

/*
generates Qr code on PDF document
recives {"building": "string", "areaName": "string"}
sends back pdf document of area with qr code
*/
app.post('/admin/generateQR',(req, res) => {

    var thisJson = { type: "qr", functionName: "generateQR", args: req.body }
    // var thisJson = {type : "qr", functionName: "generateQR", args: {building: "building 1", areaName: "Formula 1 Sim Room"}}
    //send the json
    parser.runRequest(thisJson).then(file => {
        var fileName = encodeURIComponent(file[1])
        // res.json({
        //     bob: fileName
        // });
        res.setHeader('Content-disposition', 'inline; filename*=UTF-8\'\'' + fileName)
        // file[0].pipe(fs.createWriteStream('temp.pdf'))
        file[0].pipe(res)
    })
});
// app.get('/gettest',(req, response) => {
  
//     response.json({'hello':'test'});
    
//   });


////////////////////////////////mangoLog///////////////////

function ErrorLogOfServer(e,response=null) {
    fs.appendFile("ErrorLogOfServer", JSON.stringify(e)+"\r\n", 'utf8', function (err) {
     
        //    });
    }); 
    console.log(e);

}
/**
 * Log management server
 */
 var InfoBasic = require('./mangLogs/infoBasic');
 const dbOperations = require('./mangLogs/dbOperations');





 var router = express.Router();
 
 app.use(bodyParser.urlencoded({ extended: true }));
 //app.use(bodyParser.json());
 //app.use(cors());
 
 app.use('/api', router);
 //set ip and port 
 //this._baseUrl = 'http://localhost:28938/';
 
 //Sender check
 router.use((req,res,next)=>{
    //  console.log('middlewarw');
    //  console.log(req)
     next();
 })
 
 //http://localhost:5000/api/infoBasic
 router.route('/infoBasic').get((request,response)=>{
    try{
 
     dbOperations.getInfoBasic().then(result =>{
        response.json(result[0]);
     })
    }catch(e){
        ErrorLogOfServer(e)
     }
 
 })
 
 
 //http://localhost:5000/api/infoBasicId/0.1
 router.route('/infoBasicId/:id').get((request,response)=>{
    try{
 
     dbOperations.getInfoBasicId(request.params.id).then(result => {
         response.json(result[0]);
       
     })
    }catch(e){
        ErrorLogOfServer(e,response)
     }
 
 })
 
 //בקשת כל הזמנים של תחילת מסלול
 // http://localhost:5000/api/getOrderDateOfRoutes
 router.route('/getOrderDateOfRoutes').get((request,response)=>{
    try{
 
     dbOperations.getOrderDateOfRoutes(request.query.date1,request.query.date2).then(result => {
         response.json(result[0]);
     })
    }catch(e){
        ErrorLogOfServer(e,response)
     }
 
 })
 
 
 //ספירת העמודות עפי ''תאריך''  עם שם טבלה ושם עמודה
 //  // http://localhost:5000/api/CountRoutesByDateAndType/Info_Basic_Log/RouteFromIdToId
 /**
  * 
  */
 router.route('/CountRoutesByDateAndType/:table/:col').get((request,response)=>{
    try{
     dbOperations.getCountRoutesByDateAndType(request.params.table,request.params.col,request.query.date1,request.query.date2).then(result => {
      
         response.json(result);
     })
    }catch(e){
        ErrorLogOfServer(e,response)
     }
 
 })
 
 //ספירת העמודות עפי עם שם טבלה ושם עמודה
 
 // http://localhost:28938/api/getCountingValuesInColumn/Info_Update/srcDef?date1=2022-05-01&date2=2022-05-02
 // http://localhost:28938/api/getCountingValuesInColumn/Feedback1/IsFinish?date1=2022-05-01&date2=2022-05-26
 router.route('/getCountingValuesInColumn/:table/:col').get((request,response)=>{
    try{
    
    
     dbOperations.getCountingValuesInColumn(request.params.table,request.params.col ,request.query.date1,request.query.date2).then(result => {
         response.json(result);
     })
    }catch(e){
        ErrorLogOfServer(e,response)
     }
 
 })
 
 
 //ספירת העמודות עפי עם שם טבלה ושם עמודה בהם התחיל מסלול
 // http://localhost:28938/api/getColumnFromStartRoute/Info_Basic_Log/Lang
 router.route('/getColumnFromStartRoute/:table/:col').get((request,response)=>{
    try{
    
     dbOperations.getColumnFromStartRoute(request.params.table,request.params.col,request.query.date1,request.query.date2).then(result => {
         response.json(result);
     })
    }catch(e){
        ErrorLogOfServer(e,response)
     }
 })
 
 //ספירת שעות של תחילת מסלול
 // http://localhost:28938/api/getRoutesHoursCount
 router.route('/getRoutesHoursCount').get((request,response)=>{
    try{
     dbOperations.getRoutesHoursCount(request.query.date1,request.query.date2).then(result => {
         // console.log(result)
         response.json(result);
     })
    }catch(e){
        ErrorLogOfServer(e,response)
     }
 
 })
 
 //ממוצע
 //http://localhost:28938/api/getAverages
 router.route('/getAverages').get((request,response)=>{
    try{
     dbOperations.getAverages(request.query.date1,request.query.date2).then(result => {
         // console.log(result)
         response.json(result);
     })
    }catch(e){
        ErrorLogOfServer(e,response)
     }
 })
 
 
 
 
 router.route('/addInfoRow').post((request,response)=>{
     request.body['ip'] = request.headers['x-forwarded-for'] || request.socket.remoteAddress;
     let order = {...request.body}
     // console.log(order);
     try{
 
         if (order.typeInfo === 'Update'){
          
             let toInsert = {
             "OrderDate":order.timestamp,
             "Lang":order.lang,
             "TypeInfo":order.typeInfo,
             "IdClient":order.idClient,
             "OldDef":order.data.oldDef,
             "SrcDef":order.data.srcDef,
             "DestDef":order.data.destDef,
             "Ip":order.ip};
             
             dbOperations.addInfoUpdateRow(toInsert).then(result => {
                 response.status(201).json(result);
             })
         }
         
         else {
             
             let toInsert = {
                 "OrderDate":order.timestamp,
                 "Lang":order.lang,
                 "TypeInfo":order.typeInfo,
                 "IdClient":order.idClient,
                 "Data":JSON.stringify(order.data),
                 "Ip":order.ip};
             dbOperations.addInfoBasicRow(toInsert).then(result => {
                 response.status(201).json(result);
             })
         }
         
        }catch(e){
            ErrorLogOfServer(e,response)
         }
    
     
 })
 //feedback
 app.get('/api/feedback', (req, res) => {
    try{
     dbOperations.insertFeedback(req.query);
     // console.log(req.query)
     // res.send({ express: 'Hello From Express' });
    }catch(e){
        ErrorLogOfServer(e,response)
     }
   });
 
   // http://localhost:28938/api/getFeedbackDateRatingTxt
   app.get('/api/getFeedbackDateRatingTxt', (req, res) => {
    try{
     dbOperations.getFeedbackDateRatingTxt(req.query.date1,req.query.date2).then(result => {
         res.status(201).json(result);
     })
     // console.log(req.query)
     // res.send({ express: 'Hello From Express' });
    }catch(e){
        ErrorLogOfServer(e,response)
     }
   });
 
 
 app.use(bodyParser.urlencoded({ extended: true }));
 
 
 
 
 //this._baseUrl = 'http://localhost:5000/';
 // this._baseUrl= 'http://192.168.137.1:5000/'
 
 app.get('/api/hello', (req, res) => {
   res.send({ express: 'Hello From Express' });
 });
 
 app.post('/logInfo', (req, res) => {
    try{
   req.body['ip'] = req.headers['x-forwarded-for'] || req.socket.remoteAddress;
  
   fs.appendFile("InfoLog", JSON.stringify(req.body)+"\r\n", 'utf8', function (err) {
     if (err) {
         console.log("An error occured while writing JSON Object to File.");
         return console.log(err);
     }
  
     console.log("JSON file has been saved.");
 });
   res.send(
     `I received your POST request. This is what you sent me: ${req.body.post}`,
   );
}catch(e){
    ErrorLogOfServer(e,response)
 }
 });
 
 
 app.post('/logError',(req, res) => {
  
//    req.body.logs.forEach(element => {
//      // console.log(element)
//      element['ip'] = req.headers['x-forwarded-for'] || req.socket.remoteAddress;
//      fs.appendFile("ErrorLog", JSON.stringify(element)+"\r\n", 'utf8', function (err) {
     
//    });
//    }); 

fs.appendFile("ErrorLogUsers", JSON.stringify(req.body)+"\r\n", 'utf8', function (err) {
     
    //    });
}); 
 });
 







 //RestartDB
 
 
//  router.route('/RestartDB1').get(auth.refreshOtherCalls , (request,response)=>{
     
//      request.body['ip'] = request.headers['x-forwarded-for'] || request.socket.remoteAddress;
//      let order = {...request.body}
//      // console.log(order);
//      try{
//             //  dbOperations.RestartDB().then(result => {
//             //      response.status(201).json(result);
//             //  })
//             console.log('win')
//      }
//      catch{
//          return;
//      }
    
     
//  })

//  app.get('/api/RestartDBorgin',auth.refreshOtherCalls , (request,response)=>{
//     console.log('restart');
     
//     request.body['ip'] = request.headers['x-forwarded-for'] || request.socket.remoteAddress;
//     let order = {...request.body}
//     // console.log(order);
//     try{
//            //  dbOperations.RestartDB().then(result => {
//            //      response.status(201).json(result);
//            //  })
//            console.log('win')
//     }
//     catch{
//         return;
//     }
// })



// router.route('/RestartDB').get((request,response)=>{
//     dbOperations.RestartDB().then(result => {
//         // console.log(result)
//         response.json(result);
//     })

// })

// router.route('/createdb').get((request,response)=>{
//     dbOperations.CreateDb().then(result => {
//         // console.log(result)
//         response.json(result);
//     })

// })

// router.route('/CreateTabels').get((request,response)=>{
    
//     dbOperations.CreateTabels().then(result => {
//         // console.log(result)
//         response.json(result);
//     })

// })
 
 // https://localhost:5000/api/gettest
 // app.('/gettest').get((request,response)=>{
    
    
 //         response.json({'hello':'test'});
     
 
 // })
 
 // https://localhost:5000/gettest
 app.get('/gettest',(req, response) => {
    try{
   
     response.json({'hello':'test'});
    }catch(e){
        ErrorLogOfServer(e,response)
     }
   });

    // https://localhost:5000/api/gettest
router.route('/gettest').get((request,response)=>{
    try{
    response.json({'hello':'test'});
}catch(e){
    ErrorLogOfServer(e,response)
 }

});
 
 //var port = process.env.PORT || 28938;
 //app.listen(port);
 //console.log('Order API is runnning at ' + port);


 router.route('/doesItExistDB').get((request,response)=>{
    try{
    dbOperations.doesItExistDB().then(result => {
        if (result[0][0].result ==1){
           
            response.json({'result':true});
        }
        else if(result[0][0].result ==0){
        response.json({'result':false});
        }
        else{
            response.json({'result':result});
        }
    })
}
    catch(e){
        ErrorLogOfServer(e,response);
    }

});

function createDBwithTabels(response){
    // console.log('createDBwithTabels');
    dbOperations.doesItExistDB().then(result => {
        if (result[0][0].result ==1){

                dbOperations.doesItExistTableFeedback1().then(result=>{
                    if (result[0][0].result ==1){
                        dbOperations.CreateTableFeedback1();
                    }
                });
                dbOperations.doesItExistTableInfo_Basic_Log().then(result=>{
                    if (result[0][0].result ==1){
                        dbOperations.CreateTableInfo_Basic_Log();
                    }
                });
                dbOperations.doesItExistTableInfo_Update().then(result=>{
                    if (result[0][0].result ==1){
                        dbOperations.CreateTableInfo_Update();
                    }
                });
             
     
            
        }
        else if(result[0][0].result ==0){
            dbOperations.CreateDb().then(()=>{
                dbOperations.CreateTableFeedback1();
                dbOperations.CreateTableInfo_Basic_Log();
                dbOperations.CreateTableInfo_Update();
            }).then(()=>{response.json({'result':'success'})});
            
        }
        else{
            response.json({'result':result});
        }
    })

}



router.route('/CreateNewDB').get((request,response)=>{
    try{
    createDBwithTabels(response);
    }catch(e){
        ErrorLogOfServer(e,response);
    }
});



router.route('/restartTables').get((request,response)=>{
    try{
    dbOperations.doesItExistDB().then(result => {
        if (result[0][0].result ==1){
            // dbOperations.DropTableFeedback1();
            // console.log(1);
            // dbOperations.DropTableInfo_Basic_Log();
            // console.log(2);
            // dbOperations.DropTableInfo_Update();
            // console.log(3);
            

            dbOperations.doesItExistTableFeedback1().then(result=>{
                console.log(result);
                if (result[0][0].result ==1){
                    dbOperations.DropTableFeedback1();
                }
                else{
                    dbOperations.CreateTableFeedback1();
                }
            });
            dbOperations.doesItExistTableInfo_Basic_Log().then(result=>{
                if (result[0][0].result ==1){
                    dbOperations.DropTableInfo_Basic_Log();
                }
                else{
                    dbOperations.CreateTableInfo_Basic_Log();
                }
            });
            dbOperations.doesItExistTableInfo_Update().then(result=>{
                if (result[0][0].result ==1){
                    dbOperations.DropTableInfo_Update();
                }
                else{
                    dbOperations.CreateTableInfo_Update();
                }
            });
            response.json({'result':'success'});
        }
        else{
            response.json({'result':'No success'});
        }
    })
}catch(e){
    ErrorLogOfServer(e,response);
}
    

});




////////////////////////////////mangoLog////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////2050////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

const jsonData= require('../configFiles/configAll.json');
const modelDb = require("../visitorNavigation/caching_middle.js");
const translator = require("../visitorNavigation/translator.js");
app.use("/static", express.static('../visitorNavigation/static/'));



//loads html to display
app.get("/", (req, res) => {
    res.sendFile(__dirname + '/client.html');
});



/*
Hebrew:
gets buildings

returns json in format
{"userResults":{"name for user to see":"value in hebrew"},"error":false}

example (see english for clearer example):
{"userResults":{"מרפאות חוץ":"מרפאות חוץ"},"error":false}
*/
app.get("/he/getBuildings", (req, res) => {
   
    modelDb.getBuildings().then(ans => {
      
        res.send(ans)
    })
});



// console.log(jsonData.portServerDbUsers);
app.get("/testing", (req, res) => {
   console.log(constAddresses);
    res.send({'test':'test'});
});
// https://localhost:2050/testing
/*
Hebrew:
gets areas given a building.
nameBuilding is the name of the building

returns json in the format
{"userResults":{"name for user to see":"value in hebrew"},"error":false}

example (see english for clearer example):
{"userResults":{"אולטראסאונד וגניקולגי":"אולטראסאונד וגניקולגי"}, "error":false}

*/
app.get("/he/getRegions", (req, res) => {
    const nameBuilding = req.query.nameBuilding;
    if (hasIlligalChar(nameBuilding)) {
        res.send('Error')
    } else {
        modelDb.getRegions(nameBuilding).then(ans => {
            res.send(ans)
        })
    }
});

//getRooms
app.get("/getRooms", (req, res) => {
    const nameBuilding = req.query.nameBuilding;
    const nameRegion = req.query.nameRegion
    if (hasIlligalChar(nameBuilding) || hasIlligalChar(nameRegion)) {
        res.send('Error')
    } else {
        modelDb.getRooms(nameBuilding, nameRegion).then(ans => {
            res.send(ans)
        });
    }
});
/*
Hebrew:

given:
def (unique area id)
nameBuilding (destination building name)
nameRegion (destination area name)

returns shortest path to destination in the following json format
{
    "resultRoute":
    [
        {
            "name":"Start area","directions":
            [
                ["direction","classifiaction","distance in meters","free text"]
            ]
        },
        {
            "name":"Start area","directions":
            [
                ["direction","classifiaction","distance in meters","free text"]
            ]
        }
    ]
}
example (see english for clearer example)
{"resultRoute":[{"name":"כירורגיה ילדים","directions":[["מזרח","מסדרון","5","לך לכיוון מזרח וצא מהמסדרון "]]},{"name":"אורתופדיה,ילדים,מתבגרים,סטומה","directions":[["מזרח","לובי","8","לך מזרחה בלובי 8 מטר,עבור את הקבלה"]]}]}
*/
app.get("/he/getRoute", (req, res) => {
    // getRoute(srcId, destBuilding, destRegion)
    const def = req.query.def;
    const nameBuilding = req.query.nameBuilding;
    const nameRegion = req.query.nameRegion;
    if (hasIlligalChar(nameBuilding) || hasIlligalChar(nameRegion) || hasIlligalChar(def)) {
        res.send('Error')
    } else {
        // console.log(def, nameBuilding, nameRegion)
        modelDb.getRouteDefToName(def, nameBuilding, nameRegion).then(ans => {
            res.send(ans)
        })
    }
});
/*
Hebrew
this gets the route the same as the other ones do, but from id to id
srcDef is the source id
destDef is the destination id
*/
app.get("/he/getRouteIdToId", (req, res) => {
    const srcDef = req.query.srcDef;
    const destDef = req.query.destDef
    if (hasIlligalChar(destDef) || hasIlligalChar(srcDef)) {
        res.send('Error')
    } else {
        // console.log(def, nameBuilding, nameRegion)
        modelDb.getRouteDefToDef(srcDef,destDef).then(ans => {
            res.send(ans)
        })
    }
});

/*
English:
gets buildings in english

returns json in format
{"userResults":{"name for user to see":"value in hebrew"},"error":false}

example:
{"userResults":{"Clinics":"מרפאות חוץ"},"error":false}
*/
app.get("/en/getBuildings", (req, res) => {
    modelDb.getBuildings().then(ans => {
        translator.translateBuildings(ans, 'en').then(build => {
            res.send(build)
        })
        
    })
});

//gets buildings in russian
app.get("/ru/getBuildings", (req, res) => {
    modelDb.getBuildings().then(ans => {
        translator.translateBuildings(ans, 'ru').then(build => {
            res.send(build)
        })
        
    })
});

//gets buildings in arabic
app.get("/ar/getBuildings", (req, res) => {
    modelDb.getBuildings().then(ans => {
        translator.translateBuildings(ans, 'ar').then(build => {
            res.send(build)
        })
        
    })
});

/*
English:

returns areas given a building name.

Building name must be the name it is saved as in the database, the way the admin labeled it!!

nameBuilding is the name of the building

returns json in the format:
{"userResults":{"name for user to see":"value in hebrew"},"error":false}

example:
{
    "userResults":{"Orthopedics, children, adolescents, stoma":"אורתופדיה,ילדים,מתבגרים,סטומה","orthopedics":"אורתופדיה","Endocrinology":"אנדוקרינולוגיה"},
    "error":false}
}
*/
app.get("/en/getRegions", (req, res) => {
    const nameBuilding = req.query.nameBuilding;
    if (hasIlligalChar(nameBuilding)) {
        res.send('Error')
    } else {
        modelDb.getRegions(nameBuilding).then(ans => {
            translator.translateRegions(nameBuilding, ans, 'en').then(area => {
                res.send(area)
            })
        })
    }
});

//gets areas in arabic given a building.
// Building must be in same language as in database
app.get("/ar/getRegions", (req, res) => {
    const nameBuilding = req.query.nameBuilding;
    if (hasIlligalChar(nameBuilding)) {
        res.send('Error')
    } else {
        modelDb.getRegions(nameBuilding).then(ans => {
            translator.translateRegions(nameBuilding, ans, 'ar').then(area => {
                res.send(area)
            })
        })
    }
});

//gets areas given a building
//Building must be in same language as in database
app.get("/ru/getRegions", (req, res) => {
    const nameBuilding = req.query.nameBuilding;
    if (hasIlligalChar(nameBuilding)) {
        res.send('Error')
    } else {
        modelDb.getRegions(nameBuilding).then(ans => {
            translator.translateRegions(nameBuilding, ans, 'ru').then(area => {
                res.send(area)
            })
        })
    }
});

/*
English:

given:
def (unique area id)
nameBuilding (destination building name)
nameRegion (destination area name)

returns shortest path to destination in the following json format
{
    "resultRoute":
    [
        {
            "name":"Start area","directions":
            [
                ["direction","classifiaction","distance in meters","free text"]
                ["direction","classifiaction","distance in meters","free text"]
                ["direction","classifiaction","distance in meters","free text"]
            ]
        },
        {
            "name":"Start area","directions":
            [
                ["direction","classifiaction","distance in meters","free text"]
                ["direction","classifiaction","distance in meters","free text"]
            ]
        }
    ]
}
example

{
    "resultRoute":
    [
        {
            "name":"Pediatric surgery","directions":[
                ["East","Hallway","5","Head east and exit the hallway"]
            ]
        },
        {
            "name":"Orthopedics, children, adolescents, stoma","directions":
            [
                ["East","Lobby","8","Go east in the 8 meter lobby, for the reception"]
            ]
        }
    ]
}
*/
app.get("/en/getRoute", (req, res) => {
    // getRoute(srcId, destBuilding, destRegion)
    const def = req.query.def;
    const nameBuilding = req.query.nameBuilding;
    const nameRegion = req.query.nameRegion;
    if (hasIlligalChar(nameBuilding) || hasIlligalChar(nameRegion) || hasIlligalChar(def)) {
        res.send('Error')
    } else {
        // console.log(def, nameBuilding, nameRegion)
        modelDb.getRouteDefToName(def, nameBuilding, nameRegion).then(route => {
            translator.translateRoute(route, 'en').then(trans => {
                res.send(trans)
            })
        })
    }
});



//returns route in russian
app.get("/ru/getRoute", (req, res) => {
    // getRoute(srcId, destBuilding, destRegion)
    const def = req.query.def;
    const nameBuilding = req.query.nameBuilding;
    const nameRegion = req.query.nameRegion;
    if (hasIlligalChar(nameBuilding) || hasIlligalChar(nameRegion) || hasIlligalChar(def)) {
        res.send('Error')
    } else {
        // console.log(def, nameBuilding, nameRegion)
        modelDb.getRouteDefToName(def, nameBuilding, nameRegion).then(route => {
            translator.translateRoute(route, 'ru').then(trans => {
                res.send(trans)
            })
        })
    }
});

//returns route in arabic
app.get("/ar/getRoute", (req, res) => {
    // getRoute(srcId, destBuilding, destRegion)
    const def = req.query.def;
    const nameBuilding = req.query.nameBuilding;
    const nameRegion = req.query.nameRegion;
    if (hasIlligalChar(nameBuilding) || hasIlligalChar(nameRegion) || hasIlligalChar(def)) {
        res.send('Error')
    } else {
        // console.log(def, nameBuilding, nameRegion)
        modelDb.getRouteDefToName(def, nameBuilding, nameRegion).then(route => {
            translator.translateRoute(route, 'ar').then(trans => {
                res.send(trans)
            })
        })
    }
});


/*
English
these get the route the same as the other ones do, but from id to id
srcDef is the source id
destDef is the destination id
*/
app.get("/en/getRouteIdToId", (req, res) => {
    const srcDef = req.query.srcDef;
    const destDef = req.query.destDef
    if (hasIlligalChar(destDef) || hasIlligalChar(srcDef)) {
        res.send('Error')
    } else {
        // console.log(def, nameBuilding, nameRegion)
        modelDb.getRouteDefToDef(srcDef,destDef).then(route => {
            translator.translateRoute(route, 'en').then(trans => {
                res.send(trans)
            })
        })
    }
});


/*
//returns route in russian
this get the route the same as the other ones do, but from id to id
srcDef is the source id
destDef is the destination id
*/
app.get("/ru/getRouteIdToId", (req, res) => {
    const srcDef = req.query.srcDef;
    const destDef = req.query.destDef
    if (hasIlligalChar(destDef) || hasIlligalChar(srcDef)) {
        res.send('Error')
    } else {
        // console.log(def, nameBuilding, nameRegion)
        modelDb.getRouteDefToDef(srcDef,destDef).then(route => {
            translator.translateRoute(route, 'ru').then(trans => {
                res.send(trans)
            })
        })
    }
});

/*
//returns route in russian
this get the route the same as the other ones do, but from id to id
srcDef is the source id
destDef is the destination id
*/
app.get("/ar/getRouteIdToId", (req, res) => {
    const srcDef = req.query.srcDef;
    const destDef = req.query.destDef
    if (hasIlligalChar(destDef) || hasIlligalChar(srcDef)) {
        res.send('Error')
    } else {
        // console.log(def, nameBuilding, nameRegion)
        modelDb.getRouteDefToDef(srcDef,destDef).then(route => {
            translator.translateRoute(route, 'ar').then(trans => {
                res.send(trans)
            })
        })
    }
});
//given an area defined by building and areaname, it returns the def of the area
app.get("/fromAreaGetId", (req, res) => {
    const nameBuilding = req.query.nameBuilding;
    const nameRegion = req.query.nameRegion
    if (hasIlligalChar(nameBuilding) || hasIlligalChar(nameRegion)) {
        res.send('Error')
    } else {
        modelDb.getAreaId(nameBuilding, nameRegion).then(ans => {
            res.send(ans)
        });
    }
})

//checks if def exists in the datyabase, if so, returns true, else returns fals
app.get("/doesIdExist", (req, res) => {
    const def = req.query.def;
    if (hasIlligalChar(def)) {
        res.send('Error')
    } else {
        modelDb.doesAreaExist(def).then(ans => {
            res.send(ans)
        });
    }
})


//checks if there is an illigal char that can hurt the database in code, if so returns true, else false
function hasIlligalChar(text) {
    return (text.includes('\'') || text.includes('\"') || text.includes(':')
        || text.includes('{') || text.includes('}')
        || text.includes('//'))
}


////////////////////////////////2050////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////


























// var httpServer = http.createServer(app);
var httpsServer = https.createServer(credentials, app);

// httpServer.listen(8080);
httpsServer.listen(port);
module.exports={app}