//Returns the graph in json format
var dataConn = require('../DB-Interface/basic_db_queries')

//returns a json of the graph
async function downloadAsJson(args) {
    let ret = ''
    await dataConn.dbDownloadAsJson().then(res => {
        ret = res
    })
    return ret
}

module.exports = {
    downloadAsJson: downloadAsJson,
}