/*
Functions used for adding information to the database, like checkpoints, areas and paths.
*/
var dataConn = require('../DB-Interface/basic_db_queries')

// tester()
async function tester() {
    // console.log(await jsGetListOfBuilding())
    // console.log(await jsAddOuter('fakeOuter', 0))
    //console.log(await jsSetSrc('exit', "building 1"))
    // console.log(await jsSetDest('exit', "building 1"))
    //console.log(await createNewInnerV('fake', 0, "fakePlc", "fake building", 1, ["room 1", "room 2"]))
    //console.log(await jsCreatePath("path", "fakePlc", "fake", 100, "building 1", ["up", "down"], ["stairs", "elevator"], [2, 3], ["up stairs 20 meters", "down elevator 5 mtrs"]))
}
//returns list of buildings
async function jsGetListOfBuilding(map) {
    let areas = []
    await dataConn.dbListBuildings().then(function (res) {
        areas = res
        //areas.push("הוספת בניין חדש")

    });
    return areas;
};

//adds an outer vertex on submit, recives string name and 0, 1 allowEntry
async function jsAddOuter(map) {
    let name = map["name"]
    let allowEntry = map["allowEntry"]

    let ret = {}
    //if he didnt put a name, tell him to put a name
    if (name == '') {
        ret['message'] = 'Error \n נא להכניס שם לאיזור'
    } else {
        //check that there doesnt exist another outer V with same name
        let unique = false
        await dataConn.dbUniqueOuterName(name).then(res => {
            unique = res
        })
        //if the name is unique, add the outer
        if (unique) {
            //add the variable. check if it was sucessfull
            await dataConn.dbAddOuterV(name, allowEntry).then(succ => {

                if (succ) {
                    ret['message'] = 'עודכן בהצלחה'
                } else {
                    ret['message'] = 'קרתה שגיאה, הנקודת ציון לא נוספה למאגר'
                }
            })
            //if the name is not unique, don't add the outer
        } else {
            ret['message'] = 'קיים כבר נקודת ציון עם עם שם זה, נסה שם אחר'
        }
    }
    return ret
}

/*
*gets list of places for sources in pathbuilder
* recives typePath which is the type of path it is
    meaning entrance, exit, path, trail
    and building which is either the building name or an empty string
*/
async function jsSetSrc(map) {
    let typePath = map["typePath"]
    let building = map["building"]

    var areas = []
    //if were starting from inside a building
    if (typePath == "path" || typePath == 'exit') {
        //get a list of areas inside that building
        await dataConn.dbGetInBuilding(building).then(res => {
            res.forEach(thing => {
                areas.push(thing)
            })
        })
        //if it's an outer v, get list of outer v names
    } else {
        await dataConn.dbGetOuterVs().then(res => {
            res.forEach(thing => {
                areas.push(thing)
            })
        })
    }
    //set options to be this
    return areas
}
/*
gets list of of destinations
* recives typePath which is the type of path it is
    meaning entrance, exit, path, trail
    and building which is either the building name or an empty string
*/
async function jsSetDest(map) {

    let typePath = map["typePath"]
    let building = map["building"]

    var areas = []
    //if were ending from inside a building
    if (typePath == "path" || typePath == 'entrance') {
        //get a list of areas inside that building
        await dataConn.dbGetInBuilding(building).then(res => {
            res.forEach(thing => {
                areas.push(thing)
            })
        })
        //if it's an outer v, get list of outer v names
    } else {
        await dataConn.dbGetOuterVs().then(res => {
            res.forEach(thing => {
                areas.push(thing)
            })
        })
    }
    //set options to be this
    return areas
}

/*creates inner v
 recives string def (= areaID), int allowEntry (1 or 0), string aName (=areaName),
  string building, int floor, list string rooms[], 
 */
async function createNewInnerV(map) {

    let def = map["def"]
    let allowEntry = map["allowEntry"]
    let aName = map["aName"]
    let building = map["building"]
    let floor = map["floor"]
    let rooms = map["rooms"]


    ret = {}
    let cont = false
    //check that the ID (def) the admin inputed is unique
    await dataConn.dbCheckDef(def).then(res => {
        if (res == false) {
            ret['message'] = "כבר קיים ID כזה, נסה ID אחר"
        } else {
            cont = true
        }
    })
    //check we are not creating an area with a duplicate name
    await dataConn.dbUniqueAreaName(building, aName).then(res => {
        if (res == false) {
            ret['message'] = "כבר קיים שם זה לאיזור אחר בתוך הבניין, אנא נסה שם אחר"
            cont = false
        }
    })
    //if the ID and name in building is unique
    if (cont) {
        //create the vertex and alert the admin if the vertex was created or not
        await dataConn.dbCreateInV(def, allowEntry, aName, building, floor, rooms).then(worked => {
            if (worked) {
                ret['message'] =  "האיזור הוסף בהצלחה"
            } else {
                ret['message'] =  "הפעולה נכשלה, האיזור לא הוסף"
            }
        })
    }
    return ret
}
//translate selected words
function translater(word) {
    switch (word) {
        case "Up":
            return "למעלה"
        case "Down":
            return "למטה"
        case "Right":
            return "ימינה"
        case "Left":
            return "שמאלה"
        case "Hallway":
            return "מזדרון"
        case "Elevator":
            return "מעלית"
        case "Stairs":
            return "מדרגות"
        case "Loby":
            return "לובי"
    }
}
/*creates path in inner vertex
recives string type (of path, entrance, exit, trail, path), 
string src (name/area name)
string dest (name/area name)
int weight (of path)
string building (empty string if its a trail (between two outers))
list string directions[] (directions, up down right, left for each microdirection)
list string classification[] (hallway, stairs, loby... for each microdirection)
list int distances[] (5, 10, 20 for each microdirection)
list string freeText[] (go left fo 20 meters at hallway, ... for each microdirection)

*/
async function jsCreatePath(map) {

    let type = map["type"]
    let src = map["src"]
    let dest = map["dest"]
    let weight = map["weight"]
    let building = map["building"]
    let directions = map["directions"]
    let classification = map["classification"]
    let distances = map["distances"]
    let freeText = map["freeText"]

    //exists is if there is an existing edge query, query is the create edge query

    let tup = dataConn.dbBuildCreateEdgeQuery(type, src, dest, weight, building, directions, classification, distances, freeText)
    let exists = tup[0]
    let query = tup[1]
    let canProceed = true
    ret = {}
    //check if this edge exists
    await dataConn.dbDoEdgesExist(exists).then(res => {
        if (res == -1) {
            ret['message'] =  "הפעולה נכשלה, המסלול לא הוסף"
            canProceed = false
            //if there is an existing edge, do not allow to create new one
        } else if (res == 1) {
            ret['message'] = "קיים כבר מסלול בין שני המקומות. נא למחוק את המסלול הקיים לפני הוספת מסלול חדש"
            canProceed = false
        }
    })
    if (!canProceed) {
        return ret;
    }
    //if this is a unique path from a to b, add it
    //send the query to create edge and print if succeded or not
    await dataConn.dbCreateEdge(query).then(worked => {
        if (worked) {
            ret['message'] = "המסלול הוסף בהצלחה"
        } else {
            ret['message'] = "הפעולה נכשלה, המסלול לא הוסף"
        }
    })
    return ret
}
//returns the path types
async function jsGetPathTypes() {
    return ["entrance", "exit", "path", "trail"]
}
//returns direction classifications
async function jsGetDirClassifications(){
    return ["מסדרון", "מעלית", "מדרגות", "לובי"]
}
//returns direction types
async function jsGetDirTypes() {
    return ["למעלה", "למטה", "צפון", "דרום", "מזרח", "מערב", "צפון-מערב", "צפון-מזרח", "דרום-מזרח", "דרום-מערב"]
}
module.exports = {
    addJsCreateNewInnerV: createNewInnerV,
    addJsSetDest: jsSetDest,
    addJsSetSrc: jsSetSrc,
    addJsAddOuter: jsAddOuter,
    addJsGetListOfBuilding: jsGetListOfBuilding,
    addJsCreatePath: jsCreatePath,
    addJsGetPathTypes: jsGetPathTypes,
    addJsGetDirClassifications: jsGetDirClassifications,
    addJsGetDirTypes: jsGetDirTypes

}