/*
File in charge of speaking with the database api (basic_db_queries.js) asking for what it needs
and processing the information to return it the format format described by each function, which 
is agreed upon by the front end and the backend
*/
var dataConn = require('../DB-Interface/basic_db_queries')
/*
returns building as map, buildings mapped to one annother

returns json in format
{"userResults":{"name for user to see":"value in hebrew"},"error":false}

example:
{"userResults":{"Clinics":"מרפאות חוץ"},"error":false}

*/
async function getBuildings() {
    let buildings = []
    await dataConn.dbListBuildings().then(res => {
        buildings = res
    })
    let buildingsMap = {}
    for (let i = 0; i < buildings.length; i++) {
        buildingsMap[buildings[i]] = buildings[i]
    }
    return { userResults: buildingsMap, error: false }
};
/*
returns areas inside buildings as map, areas mapped to one another


nameBuilding is the name of the building

returns json in the format:
{"userResults":{"name for user to see":"value in hebrew"},"error":false}

example:
{
    "userResults":{"Orthopedics, children, adolescents, stoma":"אורתופדיה,ילדים,מתבגרים,סטומה","orthopedics":"אורתופדיה","Endocrinology":"אנדוקרינולוגיה"},
    "error":false}
}
*/
async function getRegions(nameBuilding) {
    let listRegions = []
    await dataConn.dbGetInBuilding(nameBuilding).then(res => {
        listRegions = res
    })
    //save value in map
    let regionsMap = {}
    for (let i = 0; i < listRegions.length; i++) {
        regionsMap[listRegions[i]] = listRegions[i];
    }
    return { userResults: regionsMap, error: false }

};
//returns rooms inside buildings
async function getRooms(nameBuilding, nameRegion) {
    let rooms = ''
    await dataConn.dbGetAreaRooms(nameBuilding, nameRegion).then(res => {
        
        rooms = res
    })
    return { userResults: rooms, error: false }
};
/*
returns route from source id to area, given a source id, destination building and destination area

given:
srcId (unique area id)
destBuilding (destination building name)
destRegion (destination area name)

returns shortest path to destination in the following json format
{
    "resultRoute":
    [
        {
            "name":"Start area","directions":
            [
                ["direction","classifiaction","distance in meters","free text"]
            ]
        },
        {
            "name":"Start area","directions":
            [
                ["direction","classifiaction","distance in meters","free text"]
            ]
        }
    ]
}
example

{
    "resultRoute":
    [
        {
            "name":"Pediatric surgery","directions":[
                ["East","Hallway","5","Head east and exit the hallway"]
            ]
        },
        {
            "name":"Orthopedics, children, adolescents, stoma","directions":
            [
                ["East","Lobby","8","Go east in the 8 meter lobby, for the reception"]
            ]
        }
    ]
}
*/
async function getRouteDefToName(srcId, destBuilding, destRegion) {
    let route1 = ''
    await dataConn.dbDijkstraFromDefToName(srcId, destRegion, destBuilding).then(res => {
        route1 = res
    })
    return {'resultRoute':route1}
    
};

async function getRouteDefToDef(srcId, destId) {
    let route1 = ''
    await dataConn.dbDijkstraFromDefToDef(srcId, destId).then(res => {
        route1 = res
    })
    return {'resultRoute':route1}
}

//given a route, it changes it to json format
function toJsonRoute(value, index, array) {
    if (value.length == 4) {
        return { direction: value[0], obj: value[1], amount: value[2], freeText: value[3], 'error': false }
    }
    else {
        return { direction: '', obj: '', amount: '', freeText: '', 'error': true, infoError: { type: "missingData", data: value } }
    }

}

//given an area defined by building and areaname, it returns the def of the area
async function getAreaId(building, areaName){
    var def =''
    await dataConn.dbGetDefOfArea(building, areaName).then(res => {
        def = res;
    })
    return def
}
//given an aread id, it returns true if the area id exists in the database or not
async function doesAreaExist(def){
    var ans 
    await dataConn.dbCheckDef(def).then(res=>{
        ans = !res
    })
    return ans;
}

module.exports = {
    getBuildings: getBuildings,
    getRegions, getRegions,
    getRooms: getRooms,
    getRouteDefToName,
    getRouteDefToDef,
    getAreaId,
    doesAreaExist
};