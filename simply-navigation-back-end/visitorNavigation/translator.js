/*
In charge of translating through google translate all the information needed for visitor backend.
The translation is done through google translate
Note: This translation is not limited the four languages (russian, english, hebrew, and arabic),
it can be expanded to any langauge google translate supports.
There is also a cache so that things that were recently translated don't need to be translated again through
Note: Everything translated is sent back in the same format it was recieved, but now it's translated
google and can just be pulled from the cache.
*/
// const res = require('express/lib/response');
const https = require('https');
const NodeCache = require('node-cache');
var myCache = new NodeCache({ stdTTL: 1000 });
var ttl_long = 5
var ttl_short = 5

/*
Made possible by here!
https://stackoverflow.com/questions/2246017/using-google-translate-in-c-sharp
https://dev.to/isalevine/three-ways-to-retrieve-json-from-the-web-using-node-js-3c88
Demo call:
https.get('https://translate.googleapis.com/translate_a/single?client=gtx&sl=en&tl=he&dt=t&q=hello', res => {
*/
// googleTranslate('he', 'en', 'שלום עליכם מלאכי השרת')
// tester()
async function tester() {
    // googleTranslate('he', 'en', 'שלום עליכם מלאכי השרת').then(res => {
    //     console.log(res)
    // })
    /*
    googleTranslate('en', 'he', 'I want to go').then(res => {
        console.log(res)
    })
    
    translateFromHebrew('en', 'שלום עליכם מלאכי השרת').then(res => {
        console.log(res)
    })
    
   translateFromHebrewToArabic( 'שלום עליכם מלאכי השרת').then(res => {
    console.log(res)
})

translateFromHebrewToRussian( 'שלום עליכם מלאכי השרת').then(res => {
    console.log(res)
})

    translateFromHebrewToEnglish('שלום עליכם מלאכי השרת').then(res => {
        console.log(res)
    })
    */
}

/*
translates direction keywords from hebrew to english (en), russian (ru), or arabic (ar)
*/
function keyWordTranslate(destLang, keyword) {
    //if translate it english
    if (destLang == 'en') {
        switch (keyword) {
            case "למעלה":
                return "Up"
            case "למטה":
                return "Down"
            case "ימינה":
                return "Right"
            case "שמאלה":
                return "Left"
            case "ישר":
                return "Straight"
            case "צפון":
                return "North"
            case "דרום":
                return "South"
            case "מזרח":
                return "East"
            case "מערב":
                return "West"
            case "מסדרון":
                return "Hallway"
            case "מעלית":
                return "Elevator"
            case "מדרגות":
                return "Stairs"
            case "לובי":
                return "Lobby"
            case "צפון-מערב":
                return "Northwest"
            case "צפון-מזרח":
                return "Northeast"
            case  "דרום-מזרח":
                return "Southeast"
            case "דרום-מערב":
                return "Southwest"
            default:
                return "Not a keyword"
        }
        //if to russuan
    } else if (destLang == 'ru') {
        switch (keyword) {
            case "למעלה":
                return "Вверх"
            case "למטה":
                return "Вниз"
            case "ימינה":
                return "направо"
            case "שמאלה":
                return "слева"
            case "ישר":
                return "прямо "
            case "מסדרון":
                return "коридор"
            case "מעלית":
                return "Лифт"
            case "מדרגות":
                return "Лестница"
            case "לובי":
                return "вестибюль"
            case "צפון":
                return "север"
            case "דרום":
                return "юг"
            case "מזרח":
                return "Восток"
            case "מערב":
                return "Запад"
            case "צפון-מערב":
                return "северо-Запад"
            case "צפון-מזרח":
                return "К северо-востоку"
            case  "דרום-מזרח":
                return "юго-восток"
            case "דרום-מערב":
                return "Юго-запад"
            default:
                return "Not a keyword"
        }
        //if to arabic
    } else if (destLang == 'ar') {
        switch (keyword) {
            case "למעלה":
                return "أعلى"
            case "למטה":
                return "أسفل"
            case "ימינה":
                return "حق"
            case "שמאלה":
                return "غادر"
            case "ישר":
                return "مستقيم "
            case "מסדרון":
                return "قاعه"
            case "מעלית":
                return "مصعد"
            case "מדרגות":
                return "درج"
            case "לובי":
                return "ردهة"
            case "צפון":
                return "في"
            case "דרום":
                return "ملف"
            case "מזרח":
                return "مرو"
            case "מערב":
                return "عقل"
            case "צפון-מערב":
                return "الشمال الغربي"
            case "צפון-מזרח":
                return "الشمال الشرقي"
            case  "דרום-מזרח":
                return "الجنوب الشرقي"
            case "דרום-מערב":
                return "جنوب غرب"
            default:
                return "Not a keyword"
        }
    } else {
        return 'error translating keyword'
    }
}
async function translateFromHebrewToArabic(text) {
    return new Promise((resolve, reject) =>

        googleTranslate('he', 'ar', text).then(res => {
            resolve(res);
        })
    )
}

async function translateFromHebrewToRussian(text) {
    return new Promise((resolve, reject) =>
        googleTranslate('he', 'ru', text).then(res => {
            resolve(res);
        })
    )
}
async function translateFromHebrewToEnglish(text) {
    return new Promise((resolve, reject) =>
        googleTranslate('he', 'en', text).then(res => {
            resolve(res);
        })
    )
}
//translates text from hebrew to destination language
async function translateFromHebrew(destLang, text) {
    return new Promise((resolve, reject) =>
        googleTranslate('he', destLang, text).then(res => {
            resolve(res);
        })
    )
}
//recives string input language, string destination language, and the text to translate, returns translation
async function googleTranslate(srcLang, destLang, text) {
    return new Promise((resolve, reject) =>
        https.get('https://translate.googleapis.com/translate_a/single?client=gtx&sl=' + srcLang + '&tl=' + destLang + '&dt=t&q=' + text, async function (res) {
            let body = "";
            res.on("data", async function (chunk) {
                body += chunk;
            });

            res.on("end", async function () {
                try {
                    let json = JSON.parse(body);
                    // console.log(json)
                    // console.log(json[0][0][0])
                    //let translated equal the translation
                    let translated = json[0][0][0]
                    resolve(translated);
                    // do something with JSON
                } catch (error) {
                    reject("failed")
                    console.error(error.message);
                };
            });
        })

    )
}
/*
returns buildings in language translated to
translation is the key, hebrew is the value
*/
async function translateBuildings(buildings, destLang) {
    let transBuild = myCache.get(destLang + 'Buildings');
    // console.log(buildings)
    if (transBuild == null) {
        buildings = buildings.userResults
        buildings = Object.keys(buildings)
        transBuild = {};
        promises = [];
        //asynchronisly get all the buildings in the other language as promises
        for (let i = 0; i < buildings.length; i++) {
            //translate the string
            promises.push(new Promise(function (resolve, reject) {
                translateFromHebrew(destLang, buildings[i]).then(ans => {
                    // console.log(ans)
                    resolve(ans)
                })
            }).then(ans => {
                transBuild[ans] = buildings[i]
            }))
        }
        //wait for regions
        await Promise.all(promises)
        myCache.set(destLang + 'Buildings', transBuild, ttl_long);
    }
    //return awaited buildings
    return { userResults: transBuild, error: false }
}

/*
returns regions in language translated to
translation is the key, hebrew is the value
*/
async function translateRegions(building, regions, destLang) {
    //check if it's in cache
    let transReg = myCache.get(destLang + 'Regions' + building);
    if (transReg == null) {
        regions = regions.userResults
        regions = Object.keys(regions);
        transReg = {};
        promises = []
        //asynchronisly get all the areas in the other language as promises
        for (let i = 0; i < regions.length; i++) {
            //translate the string
            promises.push(new Promise(function (resolve, reject) {
                translateFromHebrew(destLang, regions[i]).then(ans => {
                    // console.log(ans)
                    resolve(ans)
                })
            }).then(ans => {
                transReg[ans] = regions[i]
            }))
        }
        //wait for regions
        await Promise.all(promises)
        myCache.set(destLang + 'Regions' + building, transReg, ttl_long);
    }
    //return awaited areas
    return { userResults: transReg, error: false }
}
async function translateRoute(route, destLang){
    
    route = route.resultRoute
    promises = []
    //send translation requests
    for (let i = 0; i <route.length; i++) {
        promises.push(new Promise(function (resolve, reject) {
            translateFromHebrew(destLang, route[i].name).then(ans => {
                // console.log(ans)
                resolve(ans)
            })
        }).then(ans => {
            route[i].name = ans
        }))
        //get all direction details
        for(let j = 0; j< route[i].directions.length; j++) {
            //translate keywords
            route[i].directions[j][0] = keyWordTranslate(destLang, route[i].directions[j][0])
            route[i].directions[j][1] = keyWordTranslate(destLang, route[i].directions[j][1])
            promises.push(new Promise(function (resolve, reject) {
                translateFromHebrew(destLang, route[i].directions[j][3]).then(ans => {
                    // console.log(ans)
                    resolve(ans)
                })
            }).then(ans => {
                route[i].directions[j][3] = ans
            }))
        }
    }
    //wait for all responces from google
    await Promise.all(promises)
    return {'resultRoute':route}
}
module.exports = {
    translateFromHebrew: translateFromHebrew,
    translateFromHebrewToArabic: translateFromHebrewToArabic,
    translateFromHebrewToRussian: translateFromHebrewToRussian,
    translateFromHebrewToEnglish: translateFromHebrewToEnglish,
    googleTranslate: googleTranslate,
    keyWordTranslate: keyWordTranslate,
    translateBuildings, translateRegions,
    translateRoute

}