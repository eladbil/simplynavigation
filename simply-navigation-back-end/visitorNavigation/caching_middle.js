/*
This is the cache that sits in between the visitor server and the database calls. If 
there are multiple attemps to access the same information in the database it will be
saved in this cache to avoid overworking the database and save response time.
All function calls in here are identical to the calls in the database file, that way the
server won't know the difference between the cache calls and the database and it's the same call.
Overall, when the server makes a requests, it will go to here, the function being called will check if
the information is already in the cache. If so, it will return the inforation, if not it will ask the
customSideCallN4j function that will get the neccisary information.
*/
var dataConn = require('./customSideCallN4j')

const NodeCache = require('node-cache');
var myCache = new NodeCache({ stdTTL: 1000 });
var ttl_long = 5
var ttl_short = 5

//returns building
async function getBuildings() {
    let buildings = myCache.get('Buildings');
    // console.log(buildings)
    if (buildings == null) {
        await dataConn.getBuildings().then(res => {
            buildings = res
            myCache.set('Buildings', buildings, ttl_long);
        })
    }
    return buildings
};
//returns areas inside buildings
async function getRegions(nameBuilding) {
    key = 'Regions'+nameBuilding
    let regions = myCache.get(key);
    // console.log("bobby!"+regions);
    if (regions == null) {
        await dataConn.getRegions(nameBuilding).then(res => {
            regions = res
            myCache.set(key, regions, ttl_long);
        })
    }
    return regions
};



//returns rooms inside buildings
async function getRooms(nameBuilding, nameRegion) {
    key = 'Rooms'+nameBuilding+nameRegion;
    let rooms = myCache.get(key);
    // console.log(rooms);
    if (rooms == null) {
        await dataConn.getRooms(nameBuilding, nameRegion).then(res => {
            rooms = res;
            myCache.set(key, rooms, ttl_long);
        })

    }
    return rooms;
};
//returns route from source id to area, given a source id, destination building and destination area
async function getRouteDefToName(srcId, destBuilding, destRegion) {
    key = 'Route'+srcId+destBuilding+destRegion
    let route1 =  myCache.get(key);
    // console.log(route1);
    if(route1 == null){
        await dataConn.getRouteDefToName(srcId, destBuilding, destRegion).then(res => {
            route1 = res
            myCache.set(key, route1, ttl_short);
        });

    }
    return route1

};

async function getRouteDefToDef(srcId,destId) {
    key = 'Route'+srcId+destId
    let route1 =  myCache.get(key);
    // console.log(route1);
    if(route1 == null){
        await dataConn.getRouteDefToDef(srcId, destId).then(res => {
            route1 = res
            myCache.set(key, route1, ttl_short);
        });

    }
    return route1

};
//given an area defined by building and areaname, it returns the def of the area
async function getAreaId(building, areaName){
    var cacheKey = "id_of"+building+","+areaName;
    var def = myCache.get(cacheKey)
    if (def == null){
        await dataConn.getAreaId(building, areaName).then(res => {
           def = res;
        })
        myCache.set(cacheKey, def);
    }
    return def
}
//given an aread id, it returns true if the area id exists in the database or not
async function doesAreaExist(def){
    var ans 
    await dataConn.doesAreaExist(def).then(res=>{
        ans =res
    })
    return ans;
}



function flushAll(){
    myCache.flushAll();
}
function getStats(){
    return myCache.getStats();
}
function flushStats(){
    myCache.flushStats();
}

function close(){
    if(myCache==null){
        return
    }
    myCache.close()
}

function open(short , long){
    if(myCache!=null){
        close()
    }

    myCache = new NodeCache({ stdTTL: 1000 });
    ttl_long = short
    ttl_short = long
    
    }
function open(){
    if(myCache!=null){
        close()
    }
    myCache = new NodeCache({ stdTTL: 1000 });
    ttl_long = 5
    ttl_short = 5
}





module.exports = {
    getBuildings: getBuildings,
    getRegions, getRegions,
    getRooms: getRooms,
    getRouteDefToName,
    getRouteDefToDef,
    getAreaId,
    doesAreaExist,
    flushAll:flushAll,
    getStats:getStats,
    flushStats:flushStats,
    close:close,
    open:open 
};